## What is the release?

## Does this MR meet the acceptance criteria?

- [ ] README.md is up to date
- [ ] CHANGELOG.md is up to date
- [ ] NOTICE is up to date
- [ ] docs/site-docs files are up to date
- [ ] `go mod tidy` has been executed and result reviewed

## License and Developer Certificate of Origin

- [ ] By contributing to Pantomath SAS, You accept and agree to the following terms and conditions for Your present and future Contributions submitted to Pantomath SAS. Except for the license granted herein to Pantomath SAS and recipients of software distributed by Pantomath SAS, You reserve all right, title, and interest in and to Your Contributions. All Contributions are subject to the following [Developer Certificate of Origin](DCO) and [License](LICENSE) terms.
