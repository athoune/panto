// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package agent

import (
	"fmt"

	"gitlab.com/pantomath-io/panto/agent/probe"
)

// Spooler is used to spool data from a failed report, to try again when connectivity is re-established
type Spooler struct {
	capacity int
	results  []*probe.Result
}

// NewSpooler creates and returns a Spooler that can store up to maxResults. maxResults is the maximum number of results
// spooled. Once the maximum is reached, the spooler evicts the oldest results.
func NewSpooler(capacity int) (*Spooler, error) {
	if capacity <= 0 {
		return nil, fmt.Errorf("max results should be a non-zero positive integer")
	}
	return &Spooler{
		capacity: capacity,
		results:  make([]*probe.Result, 0, capacity),
	}, nil
}

// Count returns the number of items currently in the Spooler.
func (s Spooler) Count() int {
	return len(s.results)
}

// Results returns the spooled results as an array.
func (s Spooler) Results() []*probe.Result {
	return s.results
}

// Clear clears all results from the Spooler.
func (s *Spooler) Clear() {
	s.results = s.results[:0]
}

// AddResults adds a batch of results to the Spooler. If the Spooler reaches max capacity, the oldest results are
// evicted.
func (s *Spooler) AddResults(results []*probe.Result) {
	if len(s.results)+len(results) > s.capacity {
		log.Infof(
			"Result spooler is at full capacity, %d results dropped",
			len(s.results)+len(results)-s.capacity,
		)
	}
	for _, r := range results {
		s.add(r)
	}
}

func (s *Spooler) add(res *probe.Result) {
	// Chrales 2018-03-23
	// Currently the result is implemented as an unsorted array. This is not an efficient structure for this. We could
	// keep the array sorted as we insert, or even use a binary tree or something. But keeping with the UNIX philosophy:
	// don't use fancy algorithms until you need them.
	if len(s.results) == s.capacity {
		// Spooler at max capacity, evict the oldest result
		minIdx := 0
		minTs := s.results[0].Timestamp
		for i, r := range s.results {
			if r.Timestamp.Before(minTs) {
				minIdx = i
				minTs = r.Timestamp
			}
		}
		if minIdx+1 < s.capacity {
			s.results = append(s.results[:minIdx], s.results[minIdx+1:]...)
		} else {
			s.results = s.results[:minIdx]
		}
	}
	s.results = append(s.results, res)
}
