// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package agent

import (
	"math"
	"sync"
	"testing"
	"time"

	"github.com/google/uuid"
)

func TestPackageInit(t *testing.T) {
	validateScheduler(t, DefaultScheduler, "DefaultScheduler")
}

func TestNewScheduler(t *testing.T) {
	s := NewScheduler()
	validateScheduler(t, s, "Scheduler")
}

func TestResults(t *testing.T) {
	s := NewScheduler()
	c := s.Results()
	if c == nil {
		t.Error("couldn't get results channel")
	}
	select {
	case res, ok := <-c:
		if !ok {
			t.Error("results channel was closed")
		}
		t.Errorf("received unexpected result from channel: %+v", res)
	case <-time.After(1 * time.Millisecond):
		// Timed out without receiving from the channel: OK
	}

	go s.Run()
	s.AddProbe(
		"dummy",
		map[string]interface{}{},
		uuid.New().String(),
		uuid.New().String(),
		1*time.Millisecond,
	)

	for i := 0; i < 2; i++ {
		select {
		case _, ok := <-c:
			if !ok {
				t.Error("results channel was closed before enough results received")
			}
		case <-time.After(10 * time.Millisecond):
			t.Error("timed out waiting for probe result")
		}
	}
}

func TestAddRemoveProbe(t *testing.T) {
	// This test should work only with unbuffered channels
	// If this test starts failing, check that you haven't
	// changed the buffering of Scheduler's channels.

	s := NewScheduler()
	go s.Run()

	// Add a probe that never fires (actually, around 2300 CE, at the time of coding)
	s.AddProbe(
		"dummy",
		map[string]interface{}{},
		uuid.New().String(),
		uuid.New().String(),
		math.MaxInt64*time.Nanosecond,
	)

	{
		probes := s.probes
		if len(probes) != 1 {
			t.Errorf("failed adding probe. # of probes: expected 1, got %d", len(probes))
		}
	}
}

func validateScheduler(t *testing.T, s *Scheduler, schedName string) {
	if s == nil {
		t.Errorf("%s is nil", schedName)
	}
	if s.probes == nil {
		t.Errorf("%s.probes is nil", schedName)
	}
	if s.addChan == nil {
		t.Errorf("%s.addChan is nil", schedName)
	}
	if s.removeChan == nil {
		t.Errorf("%s.removeChan is nil", schedName)
	}
	if s.tickChan == nil {
		t.Errorf("%s.tickChan is nil", schedName)
	}
	if s.resultChan == nil {
		t.Errorf("%s.resultChan is nil", schedName)
	}
}

func TestConcurrency(t *testing.T) {
	// The goal of this test is to give the race detector a chance to detect some race conditions
	s := NewScheduler()
	go s.Run()

	var wg sync.WaitGroup
	wg.Add(11)

	for i := 0; i < 10; i++ {
		go func() {
			s.AddProbe(
				"dummy",
				map[string]interface{}{},
				uuid.New().String(),
				uuid.New().String(),
				20*time.Second,
			)
			wg.Done()
		}()
	}
	go func() {
		s.RemoveAllProbes()
		wg.Done()
	}()

	wg.Wait()

	s.RemoveAllProbes()
}
