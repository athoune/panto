// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"github.com/shirou/gopsutil/mem"
)

//--------------
// Memory probe
//--------------

// Memory probes memory usage
type Memory struct{}

const (

	// Memory configuration parameters

	// Memory results

	// MemoryResultTotal : Total amount of RAM on this system, in bytes (`uint64`)
	MemoryResultTotal = "total"
	// MemoryResultAvailable : An estimate amount of RAM available to programs, in bytes (`uint64`)
	MemoryResultAvailable = "available"
	// MemoryResultUsed : Total amount of RAM used by programs, in bytes (`uint64`)
	MemoryResultUsed = "used"
	// MemoryResultUsedPercent : Percentage of RAM used by programs (`float32`)
	MemoryResultUsedPercent = "used-percent"
	// MemoryResultFree : Total amout of RAM not used by programs, in bytes (`uint64`)
	MemoryResultFree = "free"

	// MemoryResultSwapTotal : Total amount of swap memory on this system, in bytes (`uint64`)
	MemoryResultSwapTotal = "swap-total"
	// MemoryResultSwapFree : Total amout of swap memory not used by programs, in bytes (`uint64`)
	MemoryResultSwapFree = "swap-free"
	// MemoryResultSwapUsed : Total amount of swap memory used by programs, in bytes (`uint64`)
	MemoryResultSwapUsed = "swap-used"
	// MemoryResultSwapUsedPercent : Percentage of swap memory used by programs (`float32`)
	MemoryResultSwapUsedPercent = "swap-used-percent"
)

// Name returns the probe name "memory"
func (m *Memory) Name() string {
	return "memory"
}

// Configure configures the Memory probe
func (m *Memory) Configure(config map[string]interface{}) error {
	return nil
}

// Execute runs the Memory probe and returns details about memory usage
func (m *Memory) Execute() ([]*Result, error) {
	vmStat, err := mem.VirtualMemory()
	if err != nil {
		return nil, err
	}
	log.Debugf("collected system memory info: %s", vmStat)

	swapStat, err := mem.SwapMemory()
	if err != nil {
		return nil, err
	}
	log.Debugf("collected swap memory info: %s", swapStat)

	fields := map[string]interface{}{
		MemoryResultTotal:           vmStat.Total,
		MemoryResultAvailable:       vmStat.Available,
		MemoryResultUsed:            vmStat.Used,
		MemoryResultUsedPercent:     float32(vmStat.UsedPercent),
		MemoryResultFree:            vmStat.Free,
		MemoryResultSwapTotal:       swapStat.Total,
		MemoryResultSwapFree:        swapStat.Free,
		MemoryResultSwapUsed:        swapStat.Used,
		MemoryResultSwapUsedPercent: float32(swapStat.UsedPercent),
	}

	return []*Result{{Fields: fields}}, nil
}
