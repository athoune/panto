// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// +build !darwin

package probe

import (
	"reflect"
	"testing"
)

// Compile error if sysinfo probes do not conform to Probe interface
var _ Probe = (*Process)(nil)

func TestProcessName(t *testing.T) {
	p := Process{}
	if p.Name() != "process" {
		t.Error("Process probe name is not \"process\"")
	}
}

func TestProcessConfigure(t *testing.T) {
	p := Process{}
	err := p.Configure(map[string]interface{}{})
	if err == nil {
		t.Fatalf("Unexpected success configuring process probe")
	}
	err = p.Configure(map[string]interface{}{
		ProcessParamNames: "",
	})
	if err == nil {
		t.Fatalf("Unexpected success configuring process probe")
	}

	err = p.Configure(map[string]interface{}{
		ProcessParamNames: "go",
	})
	if err != nil {
		t.Fatalf("Failed to configure process probe: %s", err)
	}
	if len(p.names) != 1 {
		t.Fatalf("Expected 1 process name, got %d", len(p.names))
	}

	err = p.Configure(map[string]interface{}{
		ProcessParamNames: "go,panto",
	})
	if err != nil {
		t.Fatalf("Failed to configure process probe: %s", err)
	}
	if len(p.names) != 2 {
		t.Fatalf("Expected 2 process names, got %d", len(p.names))
	}
}

func TestProcessExecute(t *testing.T) {
	p := Process{}

	err := p.Configure(map[string]interface{}{
		ProcessParamNames: "go",
	})
	if err != nil {
		t.Fatalf("Failed to configure process probe: %s", err)
	}
	res, err := p.Execute()
	if err != nil {
		t.Fatalf("Failed to execute process probe: %s", err)
	}
	if len(res) == 0 {
		t.Error("Expected at least one result for current process")
	}
	if res[0].Tags[ProcessTagName] != "go" {
		t.Fatalf("Unexpected \"%s\" tag found. Got %s, expected %s.", ProcessTagName, res[0].Tags[ProcessTagName], "go")
	}
	for _, r := range res {
		t.Logf("%v", *r)
	}

	// Validate types
	expectedKeys := map[string]interface{}{
		ProcessResultCommandLine: reflect.String,
		ProcessResultPID:         reflect.Int32,
		ProcessResultCreateTime:  reflect.Int64,
		ProcessResultStatus:      reflect.String,
		ProcessResultCPU:         reflect.Float32,
		ProcessResultMemory:      reflect.Float32,
		ProcessResultRSS:         reflect.Uint64,
		ProcessResultVMS:         reflect.Uint64,
		ProcessResultThreads:     reflect.Uint64,
		ProcessResultOpenFiles:   reflect.Uint64,
		ProcessResultConnections: reflect.Uint64,
	}
	for k := range expectedKeys {
		if _, ok := res[0].Fields[k]; !ok {
			t.Fatalf("missing key \"%s\" in process results", k)
		}
	}
	for k, field := range res[0].Fields {
		kind, ok := expectedKeys[k]
		if !ok {
			t.Fatalf("unexpected key \"%s\" in process results", k)
			continue
		}
		if kind != reflect.TypeOf(field).Kind() {
			t.Fatalf("invalid type for field %s, expected %s, got %s", k, kind, reflect.TypeOf(field).Kind())
			continue
		}
	}
}
