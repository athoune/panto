// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"reflect"
	"testing"

	"github.com/spf13/viper"

	"gitlab.com/pantomath-io/panto/util"
)

// Compile error if sysinfo probes do not conform to Probe interface
var _ Probe = (*Nginx)(nil)

func TestNginxName(t *testing.T) {
	r := Nginx{}
	if r.Name() != "nginx" {
		t.Error("Nginx probe name is not \"nginx\"")
	}
}

func TestNginxConfigure(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live testing...")
	}

	r := Nginx{}
	err := r.Configure(map[string]interface{}{NginxParamURL: viper.GetString("nginx.url")})
	if err != nil {
		t.Errorf("couldn't configure Nginx probe: %s", err)
	}
}

func TestNginxExecute(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live testing...")
	}

	r := Nginx{}

	err := r.Configure(map[string]interface{}{NginxParamURL: viper.GetString("nginx.url")})
	if err != nil {
		t.Fatalf("couldn't configure Nginx probe: %s", err)
	}
	res, err := r.Execute()
	if err != nil {
		t.Fatalf("couldn't execute Nginx probe: %s", err)
	}
	if len(res) != 1 {
		t.Fatalf("Expected %d results, got %d", 1, len(res))
	}
	if len(res[0].Fields) != 7 {
		t.Fatalf("Expected %d fields in result, got %d", 7, len(res[0].Fields))
	}
}

func TestParseNginxResponse(t *testing.T) {
	fields, err := parseNginxResponse([]byte(testNginxStatus))
	if err != nil {
		t.Fatalf("Failed to scan nginx status: %s", err)
	}
	if len(fields) != 7 {
		t.Fatalf("Expected %d results, got %d", 7, len(fields))
	}
	t.Logf("Nginx status: %v", fields)

	expectedKeys := map[string]interface{}{
		NginxResultActiveConnections: reflect.Int,
		NginxResultAccepts:           reflect.Int,
		NginxResultHandled:           reflect.Int,
		NginxResultRequests:          reflect.Int,
		NginxResultReading:           reflect.Int,
		NginxResultWriting:           reflect.Int,
		NginxResultWaiting:           reflect.Int,
	}
	for k := range expectedKeys {
		if _, ok := fields[k]; !ok {
			t.Errorf("missing key \"%s\" in elasticsearch cluster health results", k)
		}
	}
	for k, field := range fields {
		kind, ok := expectedKeys[k]
		if !ok {
			t.Errorf("unexpected key \"%s\" in elasticsearch cluster health results", k)
			continue
		}
		if kind != reflect.TypeOf(field).Kind() {
			t.Errorf("invalid type for field %s, expected %s, got %s", k, kind, reflect.TypeOf(field).Kind())
			continue
		}
	}
}

// A mock of a elasticsearch _cluster/health
const testNginxStatus = `Active connections: 1
server accepts handled requests
 13 13 25
Reading: 0 Writing: 1 Waiting: 0
`
