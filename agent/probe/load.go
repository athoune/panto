// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"github.com/shirou/gopsutil/load"
)

//------------
// Load probe
//------------

// Load probes system load
type Load struct{}

const (

	// Load results

	// LoadResultLoad1 : System load (# of processes waiting for I/O or in the run queue) averaged over the last
	// minute (`float32`)
	LoadResultLoad1 = "load1"
	// LoadResultLoad5 : System load averaged over the last 5 minutes (`float32`)
	LoadResultLoad5 = "load5"
	// LoadResultLoad15 : System load averaged over the last 15 minutes (`float32`)
	LoadResultLoad15 = "load15"
	// LoadResultRunning : Number of processes currently running (`int`)
	LoadResultRunning = "proc-running"
	// LoadResultBlocked : Number of processes currently blocked, i.e. waiting for I/O (`int`)
	LoadResultBlocked = "proc-blocked"
)

// Name returns the name of the probe "load"
func (l *Load) Name() string {
	return "load"
}

// Configure configures the load probe
func (l *Load) Configure(config map[string]interface{}) error {
	return nil
}

// Execute runs the probe and gathers system load information
func (l *Load) Execute() ([]*Result, error) {
	avg, err := load.Avg()
	if err != nil {
		return nil, err
	}
	log.Debugf("collected load avg info: %s", avg)

	misc, err := load.Misc()
	if err != nil {
		return nil, err
	}
	log.Debugf("collected misc process info: %s", misc)

	fields := map[string]interface{}{
		LoadResultLoad1:   float32(avg.Load1),
		LoadResultLoad5:   float32(avg.Load5),
		LoadResultLoad15:  float32(avg.Load15),
		LoadResultRunning: misc.ProcsRunning,
		LoadResultBlocked: misc.ProcsBlocked,
	}

	return []*Result{{Fields: fields}}, nil
}
