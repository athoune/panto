// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"reflect"
	"testing"
)

// Compile error if Dummy does not conform to Probe interface
var _ Probe = (*Dummy)(nil)

func TestDummyName(t *testing.T) {
	d := &Dummy{}

	if d.Name() != "dummy" {
		t.Error("dummy probe name is not \"dummy\"")
	}
}

func TestDummyConfigure(t *testing.T) {
	d := &Dummy{}

	err := d.Configure(nil)
	if err != nil {
		t.Error("dummy probe could not be configured with nil config map")
	}

	err = d.Configure(map[string]interface{}{})
	if err != nil {
		t.Error("dummy probe could not be configured with empty map")
	}

	err = d.Configure(map[string]interface{}{"hello": "world"})
	if err != nil {
		t.Error("dummy probe could not be configured with arbitrary map")
	}
}

func TestDummyExecute(t *testing.T) {
	d := &Dummy{}

	res, err := d.Execute()
	if err != nil {
		t.Errorf("dummy probe failed to execute: %s", err)
	}

	resExpect := []*Result{
		{
			Fields: map[string]interface{}{"hello": "world"},
			Tags:   map[string]string{},
		},
	}
	if !reflect.DeepEqual(res, resExpect) {
		t.Errorf("dummy probe returned invalid result. Expected: %v, got: %v", resExpect, res)
	}
}
