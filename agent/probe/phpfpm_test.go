// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"reflect"
	"strings"
	"testing"

	"github.com/spf13/viper"

	"gitlab.com/pantomath-io/panto/util"
)

// Compile error if sysinfo probes do not conform to Probe interface
var _ Probe = (*PHPFPM)(nil)

func TestPHPFPMName(t *testing.T) {
	r := PHPFPM{}
	if r.Name() != "PHP-FPM" {
		t.Error("PHPFPM probe name is not \"PHP-FPM\"")
	}
}

func TestPHPFPMConfigure(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live testing...")
	}

	{
		r := PHPFPM{}
		err := r.Configure(map[string]interface{}{PHPFPMParamAddress: viper.GetString("PHPFPM.address")})
		if err != nil {
			t.Errorf("couldn't configure PHPFPM probe: %s", err)
		}
	}
	{
		r := PHPFPM{}
		err := r.Configure(map[string]interface{}{PHPFPMParamAddress: viper.GetString("PHPFPM.address"), PHPFPMParamURL: viper.GetString("PHPFPM.url")})
		if err != nil {
			t.Errorf("couldn't configure PHPFPM probe: %s", err)
		}
	}
}

func TestPHPFPMExecute(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live testing...")
	}

	r := PHPFPM{}

	err := r.Configure(map[string]interface{}{PHPFPMParamAddress: viper.GetString("PHPFPM.address")})
	if err != nil {
		t.Fatalf("couldn't configure PHPFPM probe: %s", err)
	}
	res, err := r.Execute()
	if err != nil {
		t.Fatalf("couldn't execute PHPFPM probe: %s", err)
	}
	if len(res) == 0 {
		t.Fatalf("no items in results")
	}

	stats, err := scanPHPFPMStats(strings.NewReader(testPHPFPMStatus))
	if err != nil {
		t.Fatalf("Failed to scan status: %s", err)
	}
	if len(stats) != 14 {
		t.Errorf("%v", stats)
		t.Fatalf("Expected %d results, got %d", 14, len(stats))
	}
	t.Logf("Status: %v", stats)

	fields := gatherFieldsPHPFPM(stats)
	if err != nil {
		t.Fatalf("Failed to parse stats: %v", err)
	}

	t.Logf("Fields: %v", fields)
}

func TestPHPFPMGatherFieldsPHPFPM(t *testing.T) {
	stats, _ := scanPHPFPMStats(strings.NewReader(testPHPFPMStatus))
	fields := gatherFieldsPHPFPM(stats)

	t.Logf("Fields: %v", fields)

	expectedKeys := map[string]interface{}{
		PHPFPMResultListenQueue:     reflect.Uint64,
		PHPFPMResultIdleProcesses:   reflect.Uint64,
		PHPFPMResultActiveProcesses: reflect.Uint64,
		PHPFPMResultSlowRequests:    reflect.Uint64,
	}
	for k := range expectedKeys {
		if _, ok := fields[k]; !ok {
			t.Errorf("missing key \"%s\" in PHP-FPM results", k)
		}
	}
	for k, field := range fields {
		kind, ok := expectedKeys[k]
		if !ok {
			t.Errorf("unexpected key \"%s\" in PHP-FPM results", k)
			continue
		}
		if kind != reflect.TypeOf(field).Kind() {
			t.Errorf("invalid type for field %s, expected %s, got %s", k, kind, reflect.TypeOf(field).Kind())
			continue
		}
	}
}

// A mock of a PHPFPM status endpoint
const testPHPFPMStatus = `pool:                 www
process manager:      dynamic
start time:           17/May/2013:13:54:02 +0530
start since:          886617
accepted conn:        1619617
listen queue:         0
max listen queue:     0
listen queue len:     0
idle processes:       28
active processes:     2
total processes:      30
max active processes: 31
max children reached: 0
slow requests:        0
`
