// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"reflect"
	"testing"
)

func TestRegisterAndNew(t *testing.T) {
	t.Logf("Default probe set registered.\n%v", registry)

	err := RegisterProbe(&Dummy{})
	if err != nil {
		t.Fatalf("error registering dummy probe: %s", err)
	}

	err = RegisterProbe(&Dummy{})
	if err == nil {
		t.Errorf("registered dummy probe for the same name, expected to fail")
	}

	p, err := NewProbe("dummy")
	if err != nil {
		t.Fatalf("failed to create new dummy probe: %s", err)
	}
	if !reflect.DeepEqual(&Dummy{}, p) {
		t.Fatalf("failed to create new dummy probe, expected %v, got %v", &Dummy{}, p)
	}
}
