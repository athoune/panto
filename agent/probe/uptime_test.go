// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"reflect"
	"testing"
)

// Compile error if uptime probes do not conform to Probe interface
var _ Probe = (*Uptime)(nil)

func TestUptimeName(t *testing.T) {
	l := Uptime{}
	if l.Name() != "uptime" {
		t.Error("Uptime probe name is not \"uptime\"")
	}
}

func TestUptimeConfigure(t *testing.T) {
	l := Uptime{}
	err := l.Configure(map[string]interface{}{})
	if err != nil {
		t.Errorf("couldn't configure Uptime probe: %s", err)
	}
}

func TestUptimeExecute(t *testing.T) {
	l := Uptime{}

	res, err := l.Execute()
	if err != nil {
		t.Errorf("couldn't execute Uptime probe: %s", err)
	}
	if len(res) == 0 {
		t.Errorf("no items in results")
	}

	expectedKeys := map[string]interface{}{
		UptimeResultUptime:   reflect.Uint64,
		UptimeResultBoottime: reflect.Uint64,
	}
	for k := range expectedKeys {
		if _, ok := res[0].Fields[k]; !ok {
			t.Errorf("missing key \"%s\" in uptime results", k)
		}
	}
	for k, field := range res[0].Fields {
		kind, ok := expectedKeys[k]
		if !ok {
			t.Errorf("unexpected key \"%s\" in uptime results", k)
			continue
		}
		if kind != reflect.TypeOf(field).Kind() {
			t.Errorf("invalid type for field %s, expected %s, got %s", k, kind, reflect.TypeOf(field).Kind())
			continue
		}
	}
}
