// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"reflect"
	"testing"
)

// Compile error if NTP does not conform to Probe interface
var _ Probe = (*NTP)(nil)

func TestNTPName(t *testing.T) {
	h := &NTP{}

	if h.Name() != "ntp" {
		t.Error("NTP probe name is not \"ntp\"")
	}
}

func TestNTPConfigureBasic(t *testing.T) {
	h := &NTP{}
	config := map[string]interface{}{
		NTPParamAddress: "0.fr.pool.ntp.org",
	}
	err := h.Configure(config)
	if err != nil {
		t.Errorf("NTP probe could not be configured with %s, %s", config, err)
	}
}

func TestNTPConfigureErrors(t *testing.T) {
	configs := []map[string]interface{}{
		// nil config
		nil,
		// empty config
		{},
		// missing server
		{
			"unknown": 42,
		},
	}

	h := &NTP{}
	for _, config := range configs {
		t.Logf("Testing NTP.Configure with %v", config)
		err := h.Configure(config)
		if h.address != "" {
			t.Error("invalid NTP probe's server is not an empty string")
		}
		if err == nil {
			t.Errorf("NTP probe successfully configured, expected error")
		}

	}
}

func TestNTPExecute(t *testing.T) {

	h := &NTP{}

	config := map[string]interface{}{
		NTPParamAddress: "0.fr.pool.ntp.org",
	}
	h.Configure(config)
	res, err := h.Execute()
	if err != nil {
		t.Fatalf("unexpected error executing NTP probe: %s", err)
	}

	expectedKeys := map[string]interface{}{
		NTPResultClockOffset: reflect.Int64,
		NTPResutRTT:          reflect.Int64,
	}
	for k := range expectedKeys {
		if _, ok := res[0].Fields[k]; !ok {
			t.Fatalf("missing \"%s\" field in NTP results", k)
		}
	}

	for k, field := range res[0].Fields {
		kind, ok := expectedKeys[k]
		if !ok {
			t.Errorf("unexpected key \"%s\" in NTP results", k)
			continue
		}
		if kind != reflect.TypeOf(field).Kind() {
			t.Errorf("invalid type for field %s, expected %s, got %s", k, kind, reflect.TypeOf(field).Kind())
			continue
		}
	}
}
