// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"fmt"

	"github.com/beevik/ntp"
)

// NTP is a probe that request a NTP server
// and gathers metadata on the time.
type NTP struct {
	address string
}

const (
	// NTP configuration parameters

	// NTPParamAddress : NTP server (`string`)
	NTPParamAddress = "address"

	// NTP results

	// NTPResultClockOffset : the estimated offset of the local system clock relative to the server's clock in nanosecond (`int64`)
	NTPResultClockOffset = "clock-offset"
	// NTPResutRTT : an estimate of the round-trip-time delay between the client and the server in nanosecond (`int64`)
	NTPResutRTT = "rtt"
)

// Name returns the name of the probe "ntp"
func (h *NTP) Name() string {
	return "ntp"
}

// Configure configures the NTP probe and prepares the NTP request.
func (h *NTP) Configure(config map[string]interface{}) error {
	// Check for mandatory server field
	_, ok := config[NTPParamAddress]
	if !ok {
		return fmt.Errorf("missing \"%s\" field in configuration", NTPParamAddress)
	}
	h.address, ok = config[NTPParamAddress].(string)
	if !ok {
		return fmt.Errorf("\"%s\" field is not string: %v", NTPParamAddress, config[NTPParamAddress])
	}

	return nil
}

// Execute sends an NTP request to the server and synchronously waits for the reply.
func (h *NTP) Execute() ([]*Result, error) {
	result := map[string]interface{}{
		NTPResultClockOffset: int64(0),
		NTPResutRTT:          int64(0),
	}

	log.Debugf("Dialing NTP connection: %s", h.address)

	response, err := ntp.Query(h.address)
	if err != nil {
		return nil, fmt.Errorf("unable to dial \"%s\": %v", h.address, err)
	}

	result[NTPResultClockOffset] = response.ClockOffset.Nanoseconds()
	result[NTPResutRTT] = response.RTT.Nanoseconds()

	log.Debugf("return %v", result)
	return []*Result{{Fields: result}}, nil
}
