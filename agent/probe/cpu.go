// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"fmt"
	"strconv"
	"time"

	"github.com/shirou/gopsutil/cpu"
)

//-----------
// CPU Probe
//-----------

// CPU probes CPU usage
type CPU struct {
	perCPU   bool
	interval time.Duration
}

const (

	// CPU configuration parameters

	// CPUParamPerCPU : (optional, default `true`) set to `false` to collect utilization % averaged over all CPUs/cores (`bool`)
	CPUParamPerCPU = "per-cpu"
	// CPUParamInterval : (optional, default 1 second) interval over which CPU utilization is calculated
	CPUParamInterval = "interval"

	// CPU result fields

	// CPUResultUsage : % of utilization for a specific CPU/core. (`float32`)
	CPUResultUsage = "usage"

	// CPU result tags

	// CPUTagCPU : number of the CPU usage result, "all" for average over all CPUs/cores
	CPUTagCPU = "cpu"
)

// Name returns the name of the probe "cpu"
func (c *CPU) Name() string {
	return "cpu"
}

// Configure configures the CPU probe
func (c *CPU) Configure(config map[string]interface{}) error {
	// Check for optional  argument
	_, ok := config[CPUParamPerCPU]
	if !ok {
		c.perCPU = true
	} else {
		c.perCPU, ok = config[CPUParamPerCPU].(bool)
		if !ok {
			return fmt.Errorf("\"%s\" field is not bool: %v", CPUParamPerCPU, config[CPUParamPerCPU])
		}
	}

	// Check for optional interval argument
	interval, ok := config[CPUParamInterval]
	if ok {
		switch i := interval.(type) {
		case float64:
			interval = time.Duration(i)
		}
		c.interval, ok = interval.(time.Duration)
		if !ok {
			return fmt.Errorf("\"%s\" field is not time.Duration: %v", CPUParamInterval, config[CPUParamInterval])
		}
	}
	if c.interval <= 0 {
		c.interval = 1 * time.Second
	}

	return nil
}

// Execute runs the CPU probe and returns details on the CPU utilization
func (c *CPU) Execute() ([]*Result, error) {
	percent, err := cpu.Percent(c.interval, c.perCPU)
	if err != nil {
		return nil, err
	}
	if len(percent) == 0 {
		return nil, fmt.Errorf("CPU probe collected empty data")
	}
	log.Debugf("Collected CPU info: %v", percent)

	res := make([]*Result, len(percent))
	if !c.perCPU {
		res[0] = &Result{
			Fields: map[string]interface{}{CPUResultUsage: float32(percent[0])},
			Tags:   map[string]string{CPUTagCPU: "all"},
		}
	} else {
		for i, p := range percent {
			res[i] = &Result{
				Fields: map[string]interface{}{CPUResultUsage: float32(p)},
				Tags:   map[string]string{CPUTagCPU: strconv.Itoa(i)},
			}
		}
	}

	return res, nil
}
