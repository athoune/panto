// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"reflect"
	"testing"

	sqlmock "github.com/DATA-DOG/go-sqlmock"
	"github.com/spf13/viper"

	"gitlab.com/pantomath-io/panto/util"
)

// Compile error if PostgreSQL probe do not conform to Probe interface
var _ Probe = (*Postgresql)(nil)

func TestPostgresqlName(t *testing.T) {
	r := Postgresql{}
	if r.Name() != "postgresql" {
		t.Error("Postgresql probe name is not \"postgresql\"")
	}
}

func TestPostgresqlConfigure(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live testing...")
	}

	r := Postgresql{}
	err := r.Configure(map[string]interface{}{
		PostgresqlParamAddress:  viper.GetString("postgresql.address"),
		PostgresqlParamLogin:    viper.GetString("postgresql.login"),
		PostgresqlParamPassword: viper.GetString("postgresql.password"),
		PostgresqlParamSSLMode:  viper.GetString("postgresql.sslmode"),
	})
	if err != nil {
		t.Errorf("couldn't configure Postgresql probe: %s", err)
	}
}

func TestPostgresqlExecute(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live testing...")
	}

	r := Postgresql{}

	err := r.Configure(map[string]interface{}{
		PostgresqlParamAddress:  viper.GetString("postgresql.address"),
		PostgresqlParamLogin:    viper.GetString("postgresql.login"),
		PostgresqlParamPassword: viper.GetString("postgresql.password"),
		PostgresqlParamSSLMode:  viper.GetString("postgresql.sslmode"),
	})
	if err != nil {
		t.Fatalf("couldn't configure Postgresql probe: %s", err)
	}
	res, err := r.Execute()
	if err != nil {
		t.Fatalf("couldn't execute Postgresql probe: %s", err)
	}
	if len(res) == 0 {
		t.Fatalf("no items in results")
	}
}

func TestScanPostgresqlStatsDatabase(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	fields := make(map[string]interface{})
	rows := sqlmock.NewRows([]string{PostgresqlResultCurrentConnections, PostgresqlResultSharedBufferHits, PostgresqlResultSharedBufferReads, PostgresqlResultTempFilesCount, PostgresqlResultTempFilesBytes, PostgresqlResultRowsReturned, PostgresqlResultRowsFetched, PostgresqlResultRowsInserted, PostgresqlResultRowsUpdated, PostgresqlResultRowsDeleted, PostgresqlResultDeadlocks}).
		AddRow(42, 42, 42, 42, 42, 42, 42, 42, 42, 42, 42)

	expectedKeys := map[string]interface{}{
		PostgresqlResultCurrentConnections: reflect.Uint,
		PostgresqlResultSharedBufferHits:   reflect.Uint,
		PostgresqlResultSharedBufferReads:  reflect.Uint,
		PostgresqlResultTempFilesCount:     reflect.Uint,
		PostgresqlResultTempFilesBytes:     reflect.Uint,
		PostgresqlResultRowsReturned:       reflect.Uint,
		PostgresqlResultRowsFetched:        reflect.Uint,
		PostgresqlResultRowsInserted:       reflect.Uint,
		PostgresqlResultRowsUpdated:        reflect.Uint,
		PostgresqlResultRowsDeleted:        reflect.Uint,
		PostgresqlResultDeadlocks:          reflect.Uint,
	}

	mock.ExpectQuery(`.*`).WillReturnRows(rows)
	res, err := db.Query(`
		SELECT SUM(numbackends) AS cur_connections,
			SUM(blks_hit) AS shared_buffer_hits,
			SUM(blks_read) AS shared_buffer_reads,
			SUM(temp_files) AS temp_files_count,
			SUM(temp_bytes) AS temp_file_bytes,
			SUM(tup_returned) AS rows_returned,
			SUM(tup_fetched) AS rows_fetched,
			SUM(tup_inserted) AS rows_inserted,
			SUM(tup_updated) AS rows_updated,
			SUM(tup_deleted) AS rows_deleted,
			SUM(deadlocks) AS deadlocks_count
		FROM pg_stat_database;`)
	if err != nil {
		t.Fatalf("unable to execute query: %s", err)
	}

	err = scanPostgresqlStatsDatabase(fields, res)
	if err != nil {
		t.Fatalf("unable to parse output: %v", err)
	}

	for k := range expectedKeys {
		if _, ok := fields[k]; !ok {
			t.Errorf("missing key \"%s\" in postgres database results", k)
		}
	}
	for k, field := range fields {
		kind, ok := expectedKeys[k]
		if !ok {
			t.Errorf("unexpected key \"%s\" in postgres database results", k)
			continue
		}
		if kind != reflect.TypeOf(field).Kind() {
			t.Errorf("invalid type for field %s, expected %s, got %s", k, kind, reflect.TypeOf(field).Kind())
			continue
		}
	}
}

func TestScanPostgresqlStatsSettings(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	fields := make(map[string]interface{})
	rows := sqlmock.NewRows([]string{"setting"}).AddRow(42)

	expectedKeys := map[string]interface{}{
		PostgresqlResultMaxConnections: reflect.Uint,
	}

	mock.ExpectQuery(`.*`).WillReturnRows(rows)
	res, err := db.Query(`SELECT setting FROM pg_settings;`)
	if err != nil {
		t.Fatalf("unable to execute query: %s", err)
	}

	err = scanPostgresqlStatsSettings(fields, res)
	if err != nil {
		t.Fatalf("unable to parse output: %v", err)
	}

	for k := range expectedKeys {
		if _, ok := fields[k]; !ok {
			t.Errorf("missing key \"%s\" in postgres settings results", k)
		}
	}
	for k, field := range fields {
		kind, ok := expectedKeys[k]
		if !ok {
			t.Errorf("unexpected key \"%s\" in postgres settings results", k)
			continue
		}
		if kind != reflect.TypeOf(field).Kind() {
			t.Errorf("invalid type for field %s, expected %s, got %s", k, kind, reflect.TypeOf(field).Kind())
			continue
		}
	}
}

func TestScanPostgresqlStatsSize(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	fields := make(map[string]interface{})
	rows := sqlmock.NewRows([]string{PostgresqlResultIndexSize, PostgresqlResultToastSize, PostgresqlResultTableSize}).
		AddRow(42, 42, 42)

	expectedKeys := map[string]interface{}{
		PostgresqlResultIndexSize: reflect.Uint,
		PostgresqlResultToastSize: reflect.Uint,
		PostgresqlResultTableSize: reflect.Uint,
	}

	mock.ExpectQuery(`.*`).WillReturnRows(rows)
	res, err := db.Query(`
		SELECT SUM(index_bytes) as index,
			SUM(toast_bytes) as toast,
			SUM(table_bytes) as table
		  FROM (
		  SELECT *, total_bytes-index_bytes-COALESCE(toast_bytes,0) AS table_bytes FROM (
		      SELECT c.oid,nspname AS table_schema, relname AS TABLE_NAME
		              , c.reltuples AS row_estimate
		              , pg_total_relation_size(c.oid) AS total_bytes
		              , pg_indexes_size(c.oid) AS index_bytes
		              , pg_total_relation_size(reltoastrelid) AS toast_bytes
		          FROM pg_class c
		          LEFT JOIN pg_namespace n ON n.oid = c.relnamespace
		          WHERE relkind = 'r'
		  ) a
		) a;`)
	if err != nil {
		t.Fatalf("unable to execute query: %s", err)
	}

	err = scanPostgresqlStatsSize(fields, res)
	if err != nil {
		t.Fatalf("unable to parse output: %v", err)
	}

	for k := range expectedKeys {
		if _, ok := fields[k]; !ok {
			t.Errorf("missing key \"%s\" in postgres size results", k)
		}
	}
	for k, field := range fields {
		kind, ok := expectedKeys[k]
		if !ok {
			t.Errorf("unexpected key \"%s\" in postgres size results", k)
			continue
		}
		if kind != reflect.TypeOf(field).Kind() {
			t.Errorf("invalid type for field %s, expected %s, got %s", k, kind, reflect.TypeOf(field).Kind())
			continue
		}
	}
}

func TestScanPostgresqlStatsRows(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	fields := make(map[string]interface{})
	rows := sqlmock.NewRows([]string{PostgresqlResultRowsDead, PostgresqlResultRowsLive}).AddRow(42, 42)

	expectedKeys := map[string]interface{}{
		PostgresqlResultRowsDead: reflect.Uint,
		PostgresqlResultRowsLive: reflect.Uint,
	}

	mock.ExpectQuery(`.*`).WillReturnRows(rows)
	res, err := db.Query(`SELECT SUM(n_dead_tup) AS dead, SUM(n_live_tup) AS live FROM pg_stat_all_tables;`)
	if err != nil {
		t.Fatalf("unable to execute query: %s", err)
	}

	err = scanPostgresqlStatsRows(fields, res)
	if err != nil {
		t.Fatalf("unable to parse output: %v", err)
	}

	for k := range expectedKeys {
		if _, ok := fields[k]; !ok {
			t.Errorf("missing key \"%s\" in postgres rows results", k)
		}
	}
	for k, field := range fields {
		kind, ok := expectedKeys[k]
		if !ok {
			t.Errorf("unexpected key \"%s\" in postgres rows results", k)
			continue
		}
		if kind != reflect.TypeOf(field).Kind() {
			t.Errorf("invalid type for field %s, expected %s, got %s", k, kind, reflect.TypeOf(field).Kind())
			continue
		}
	}
}

func TestScanPostgresqlStatsWrite(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	fields := make(map[string]interface{})
	rows := sqlmock.NewRows([]string{PostgresqlResultCheckpointsRequested, PostgresqlResultCheckpointsScheduled, PostgresqlResultBuffersBackend, PostgresqlResultBuffersBackground, PostgresqlResultBuffersCheckpoint}).
		AddRow(42, 42, 42, 42, 42)

	expectedKeys := map[string]interface{}{
		PostgresqlResultCheckpointsRequested: reflect.Uint,
		PostgresqlResultCheckpointsScheduled: reflect.Uint,
		PostgresqlResultBuffersBackend:       reflect.Uint,
		PostgresqlResultBuffersBackground:    reflect.Uint,
		PostgresqlResultBuffersCheckpoint:    reflect.Uint,
	}

	mock.ExpectQuery(`.*`).WillReturnRows(rows)
	res, err := db.Query(`SELECT setting FROM pg_settings;`)
	if err != nil {
		t.Fatalf("unable to execute query: %s", err)
	}

	err = scanPostgresqlStatsWrite(fields, res)
	if err != nil {
		t.Fatalf("unable to parse output: %v", err)
	}

	for k := range expectedKeys {
		if _, ok := fields[k]; !ok {
			t.Errorf("missing key \"%s\" in postgres settings results", k)
		}
	}
	for k, field := range fields {
		kind, ok := expectedKeys[k]
		if !ok {
			t.Errorf("unexpected key \"%s\" in postgres settings results", k)
			continue
		}
		if kind != reflect.TypeOf(field).Kind() {
			t.Errorf("invalid type for field %s, expected %s, got %s", k, kind, reflect.TypeOf(field).Kind())
			continue
		}
	}
}
