// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"encoding/json"
	"reflect"
	"strings"
	"testing"

	"github.com/spf13/viper"

	influx "github.com/influxdata/influxdb/client/v2"
	"gitlab.com/pantomath-io/panto/util"
)

// Compile error if sysinfo probes do not conform to Probe interface
var _ Probe = (*InfluxDB)(nil)

func TestInfluxDBName(t *testing.T) {
	r := InfluxDB{}
	if r.Name() != "influxdb" {
		t.Error("InfluxDB probe name is not \"influxdb\"")
	}
}

func TestInfluxDBConfigure(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live testing...")
	}

	r := InfluxDB{}
	err := r.Configure(map[string]interface{}{InfluxDBParamAddress: viper.GetString("influxdb.address")})
	if err != nil {
		t.Errorf("couldn't configure InfluxDB probe: %s", err)
	}
}

func TestInfluxDBExecute(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live testing...")
	}

	r := InfluxDB{}

	err := r.Configure(map[string]interface{}{InfluxDBParamAddress: viper.GetString("influxdb.address")})
	if err != nil {
		t.Fatalf("couldn't configure InfluxDB probe: %s", err)
	}
	res, err := r.Execute()
	if err != nil {
		t.Fatalf("couldn't execute InfluxDB probe: %s", err)
	}
	if len(res) == 0 {
		t.Fatalf("no items in results")
	}
}

func TestInfluxDBscanStatsForFields(t *testing.T) {
	var response influx.Response
	var dec = json.NewDecoder(strings.NewReader(bodyStats))
	dec.UseNumber()
	var decErr = dec.Decode(&response)
	// ignore this error if we got an invalid status code
	if decErr != nil && decErr.Error() == "EOF" {
		decErr = nil
	}

	fields := scanStatsForFields(response.Results[0])

	t.Logf("Fields: %v", fields)

	expectedKeys := map[string]interface{}{
		InfluxDBResultRuntimeAlloc:         reflect.Int64,
		InfluxDBResultRuntimeFrees:         reflect.Int64,
		InfluxDBResultRuntimeHeapAlloc:     reflect.Int64,
		InfluxDBResultRuntimeHeapIdle:      reflect.Int64,
		InfluxDBResultRuntimeHeapInUse:     reflect.Int64,
		InfluxDBResultRuntimeHeapObjects:   reflect.Int64,
		InfluxDBResultRuntimeHeapReleased:  reflect.Int64,
		InfluxDBResultRuntimeHeapSys:       reflect.Int64,
		InfluxDBResultRuntimeLookups:       reflect.Int64,
		InfluxDBResultRuntimeMallocs:       reflect.Int64,
		InfluxDBResultRuntimeNumGC:         reflect.Int64,
		InfluxDBResultRuntimeNumGoroutine:  reflect.Int64,
		InfluxDBResultRuntimePauseTotalNs:  reflect.Int64,
		InfluxDBResultRuntimeSys:           reflect.Int64,
		InfluxDBResultRuntimeTotalAlloc:    reflect.Int64,
		InfluxDBResultQueriesActive:        reflect.Int64,
		InfluxDBResultQueriesExecuted:      reflect.Int64,
		InfluxDBResultQueriesFinished:      reflect.Int64,
		InfluxDBResultQueryDurationNs:      reflect.Int64,
		InfluxDBResultQERecoveredPanics:    reflect.Int64,
		InfluxDBResultPointReq:             reflect.Int64,
		InfluxDBResultPointReqLocal:        reflect.Int64,
		InfluxDBResultWReq:                 reflect.Int64,
		InfluxDBResultSubWriteDrop:         reflect.Int64,
		InfluxDBResultSubWriteOk:           reflect.Int64,
		InfluxDBResultWriteDrop:            reflect.Int64,
		InfluxDBResultWriteError:           reflect.Int64,
		InfluxDBResultWriteOk:              reflect.Int64,
		InfluxDBResultWriteTimeout:         reflect.Int64,
		InfluxDBResultCreateFailures:       reflect.Int64,
		InfluxDBResultPointsWritten:        reflect.Int64,
		InfluxDBResultWriteFailures:        reflect.Int64,
		InfluxDBResultQueryFail:            reflect.Int64,
		InfluxDBResultQueryOk:              reflect.Int64,
		InfluxDBResultAuthFail:             reflect.Int64,
		InfluxDBResultClientError:          reflect.Int64,
		InfluxDBResultPingReq:              reflect.Int64,
		InfluxDBResultPointsWrittenDropped: reflect.Int64,
		InfluxDBResultPointsWrittenFail:    reflect.Int64,
		InfluxDBResultPointsWrittenOK:      reflect.Int64,
		InfluxDBResultQueryReq:             reflect.Int64,
		InfluxDBResultQueryReqDurationNs:   reflect.Int64,
		InfluxDBResultQueryRespBytes:       reflect.Int64,
		InfluxDBResultRecoveredPanics:      reflect.Int64,
		InfluxDBResultReq:                  reflect.Int64,
		InfluxDBResultReqActive:            reflect.Int64,
		InfluxDBResultReqDurationNs:        reflect.Int64,
		InfluxDBResultServerError:          reflect.Int64,
		InfluxDBResultStatusReq:            reflect.Int64,
		InfluxDBResultWriteReq:             reflect.Int64,
		InfluxDBResultWriteReqActive:       reflect.Int64,
		InfluxDBResultWriteReqBytes:        reflect.Int64,
		InfluxDBResultWriteReqDurationNs:   reflect.Int64,
	}
	for k := range expectedKeys {
		if _, ok := fields[k]; !ok {
			t.Errorf("missing key \"%s\" in influxdb results", k)
		}
	}
	for k, field := range fields {
		kind, ok := expectedKeys[k]
		if !ok {
			t.Errorf("unexpected key \"%s\" in influxdb results", k)
			continue
		}
		if kind != reflect.TypeOf(field).Kind() {
			t.Errorf("invalid type for field %s, expected %s, got %s", k, kind, reflect.TypeOf(field).Kind())
			continue
		}
	}
}

const bodyStats = `
{
    "results": [
        {
            "statement_id": 0,
            "series": [
                {
                    "name": "runtime",
                    "columns": [
                        "Alloc",
                        "Frees",
                        "HeapAlloc",
                        "HeapIdle",
                        "HeapInUse",
                        "HeapObjects",
                        "HeapReleased",
                        "HeapSys",
                        "Lookups",
                        "Mallocs",
                        "NumGC",
                        "NumGoroutine",
                        "PauseTotalNs",
                        "Sys",
                        "TotalAlloc"
                    ],
                    "values": [
                        [
                            9805768,
                            2733523,
                            9805768,
                            47783936,
                            10903552,
                            150919,
                            45268992,
                            58687488,
                            2751,
                            2884442,
                            170,
                            22,
                            117426705,
                            64645368,
                            734267704
                        ]
                    ]
                },
                {
                    "name": "queryExecutor",
                    "columns": [
                        "queriesActive",
                        "queriesExecuted",
                        "queriesFinished",
                        "queryDurationNs",
                        "recoveredPanics"
                    ],
                    "values": [
                        [
                            1,
                            154,
                            153,
                            183449852,
                            0
                        ]
                    ]
                },
                {
                    "name": "write",
                    "columns": [
                        "pointReq",
                        "pointReqLocal",
                        "req",
                        "subWriteDrop",
                        "subWriteOk",
                        "writeDrop",
                        "writeError",
                        "writeOk",
                        "writeTimeout"
                    ],
                    "values": [
                        [
                            67248,
                            67248,
                            1664,
                            0,
                            1664,
                            0,
                            0,
                            1724,
                            0
                        ]
                    ]
                },
                {
                    "name": "subscriber",
                    "columns": [
                        "createFailures",
                        "pointsWritten",
                        "writeFailures"
                    ],
                    "values": [
                        [
                            0,
                            0,
                            0
                        ]
                    ]
                },
                {
                    "name": "cq",
                    "columns": [
                        "queryFail",
                        "queryOk"
                    ],
                    "values": [
                        [
                            0,
                            0
                        ]
                    ]
                },
                {
                    "name": "httpd",
                    "tags": {
                        "bind": ":8086"
                    },
                    "columns": [
                        "authFail",
                        "clientError",
                        "pingReq",
                        "pointsWrittenDropped",
                        "pointsWrittenFail",
                        "pointsWrittenOK",
                        "promReadReq",
                        "promWriteReq",
                        "queryReq",
                        "queryReqDurationNs",
                        "queryRespBytes",
                        "recoveredPanics",
                        "req",
                        "reqActive",
                        "reqDurationNs",
                        "serverError",
                        "statusReq",
                        "writeReq",
                        "writeReqActive",
                        "writeReqBytes",
                        "writeReqDurationNs"
                    ],
                    "values": [
                        [
                            0,
                            5,
                            1,
                            0,
                            0,
                            40080,
                            0,
                            0,
                            157,
                            258583646,
                            228750,
                            0,
                            240,
                            1,
                            2151166741,
                            0,
                            0,
                            82,
                            0,
                            8202527,
                            1822714352
                        ]
                    ]
                }
            ]
        }
    ]
}
`
