// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"crypto/md5"
	"crypto/sha1"
	"crypto/sha256"
	"fmt"
	"hash"
	"hash/crc32"
	"io"
	"os"
)

// Checksum probe returns the checksum of a file
type Checksum struct {
	path string
	hash string
}

const (
	// ChecksumHashCRC32 designates an IEEE 802.3 CRC-32 checksum
	ChecksumHashCRC32 = "crc32"
	// ChecksumHashMD5 designates an MD5 checksum
	ChecksumHashMD5 = "md5"
	// ChecksumHashSHA1 designates a SHA-1 checksum
	ChecksumHashSHA1 = "sha1"
	// ChecksumHashSHA256 designates a SHA-256 checksum
	ChecksumHashSHA256 = "sha256"
)

const (
	// Checksum configuration parameters

	// ChecksumParamPath : (mandatory) The path of the file to check. (string)
	ChecksumParamPath = "path"
	// ChecksumParamHash : The hash algorithm to use. One of "crc32" (default), "md5", "sha1", "sha256". (string)
	ChecksumParamHash = "hash"

	// Checksum result fields

	// ChecksumResultChecksum : The result of a checksum as a hexadecimal string. (string)
	ChecksumResultChecksum = "checksum"

	// Disk result tags

	// ChecksumTagPath : The path of the file that was checked.
	ChecksumTagPath = "path"
)

// Name returns the name of the probe "checksum"
func (c *Checksum) Name() string {
	return "checksum"
}

// Configure configures the checksum probe
func (c *Checksum) Configure(config map[string]interface{}) error {
	if path, ok := config[ChecksumParamPath]; ok {
		if c.path, ok = path.(string); !ok {
			return fmt.Errorf("\"%s\" param is not string: %v", ChecksumParamPath, config[ChecksumParamPath])
		}
	} else {
		return fmt.Errorf("missing param \"%s\"", ChecksumParamPath)
	}

	var hash string
	if paramHash, ok := config[ChecksumParamHash]; ok {
		if hash, ok = paramHash.(string); !ok {
			return fmt.Errorf("\"%s\" param is not string: %v", ChecksumParamHash, config[ChecksumParamHash])
		}
	}

	switch hash {
	case "":
		c.hash = ChecksumHashCRC32
	case ChecksumHashCRC32, ChecksumHashMD5, ChecksumHashSHA1, ChecksumHashSHA256:
		c.hash = hash
	default:
		return fmt.Errorf("Invalid value for param \"%s\": %s", ChecksumParamHash, hash)
	}

	return nil
}

// Execute opens a file and calculates its checksum
func (c *Checksum) Execute() ([]*Result, error) {
	f, err := os.Open(c.path)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	var h hash.Hash
	switch c.hash {
	case ChecksumHashCRC32:
		h = crc32.NewIEEE()
	case ChecksumHashMD5:
		h = md5.New()
	case ChecksumHashSHA1:
		h = sha1.New()
	case ChecksumHashSHA256:
		h = sha256.New()
	default:
		return nil, fmt.Errorf("Invalid hash algorithm: %s", c.hash)
	}

	_, err = io.Copy(h, f)
	if err != nil {
		return nil, err
	}

	res := []*Result{
		{
			Fields: map[string]interface{}{ChecksumResultChecksum: fmt.Sprintf("%x", h.Sum(nil))},
			Tags:   map[string]string{ChecksumTagPath: c.path},
		},
	}
	return res, nil
}
