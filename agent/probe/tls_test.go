// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import "testing"

// Compile error if TLS does not conform to Probe interface
var _ Probe = (*TLS)(nil)

func TestTLSName(t *testing.T) {
	h := &TLS{}

	if h.Name() != "tls" {
		t.Error("TLS probe name is not \"tls\"")
	}
}

func TestTLSConfigureBasic(t *testing.T) {
	h := &TLS{}
	config := map[string]interface{}{
		TLSParamHost: "localhost:443",
	}
	err := h.Configure(config)
	if err != nil {
		t.Errorf("TLS probe could not be configured with %s, %s", config, err)
	}
}

func TestTLSConfigureErrors(t *testing.T) {
	configs := []map[string]interface{}{
		// nil config
		nil,
		// empty config
		{},
		// missing URL
		{
			"unknown": 42,
		},
	}

	h := &TLS{}
	for _, config := range configs {
		t.Logf("Testing TLS.Configure with %v", config)
		err := h.Configure(config)
		if h.host != "" {
			t.Error("invalid TLS probe's host is not an empty string")
		}
		if err == nil {
			t.Errorf("TLS probe successfully configured, expected error")
		}

	}
}

func TestTLSExecute(t *testing.T) {

	h := &TLS{}

	config := map[string]interface{}{
		TLSParamHost: "pantomath.io:443",
	}
	h.Configure(config)
	res, err := h.Execute()
	if err != nil {
		t.Fatalf("unexpected error executing TLS probe: %s", err)
	}

	expectedKeys := []string{
		TLSExpiresIn,
		TLSChainExpiresIn,
		TLSHash,
		TLSSignatureList,
	}
	for _, k := range expectedKeys {
		if _, ok := res[0].Fields[k]; !ok {
			t.Fatalf("missing \"%s\" field in TLS results", k)
		}
	}
}
