// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"regexp"
	"strconv"
	"time"
)

// Nginx is a probe that sends an HTTP request to Nginx status page
// and gathers data from the response.
type Nginx struct {
	client *http.Client
	req    *http.Request
}

const (
	// Nginx configuration parameters

	// NginxParamURL : Nginx request URL (`string`)
	NginxParamURL = "url"
	// NginxParamTimeout (optional) : duration before the Nginx request times out (`time.Duration`)
	NginxParamTimeout = "timeout"

	// Nginx results

	// NginxResultActiveConnections : the number of active client connections including waiting connections. (`int`)
	NginxResultActiveConnections = "active-connections"
	// NginxResultAccepts : the total number of accepted connections (`int`)
	NginxResultAccepts = "accept-connections"
	// NginxResultHandled : the total number of handled connections. (`int`)
	NginxResultHandled = "handled-connections"
	// NginxResultRequests : the total number of client requests. (`int`)
	NginxResultRequests = "requests"
	// NginxResultReading : the current number of connections where nginx is reading the request header (`int`)
	NginxResultReading = "reading"
	// NginxResultWriting : the current number of connections where nginx is writing the response back to the client (`int`)
	NginxResultWriting = "writing"
	// NginxResultWaiting : the current number of idle client connections waiting for a request (`int`)
	NginxResultWaiting = "waiting"
)

// Name returns the name of the probe "nginx"
func (n *Nginx) Name() string {
	return "nginx"
}

// Configure configures the Nginx probe and prepares the
// Nginx request.
func (n *Nginx) Configure(config map[string]interface{}) (err error) {
	n.client = nil
	n.req = nil

	// Check for mandatory URL field
	var url string
	_, ok := config[NginxParamURL]
	if !ok {
		return fmt.Errorf("missing \"%s\" field in configuration", NginxParamURL)
	}
	url, ok = config[NginxParamURL].(string)
	if !ok {
		return fmt.Errorf("\"%s\" field is not string: %v", NginxParamURL, config[NginxParamURL])
	}

	// Check for optional timeout
	var timeout time.Duration
	_, ok = config[NginxParamTimeout]
	if !ok {
		timeout = 0
	} else {
		timeout, ok = config[NginxParamTimeout].(time.Duration)
		if !ok {
			return fmt.Errorf("\"%s\" field is not time.Duration: %v", NginxParamTimeout, config[NginxParamTimeout])
		}
	}

	n.req, err = http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return err
	}

	n.client = &http.Client{Timeout: timeout}

	return nil
}

// Execute sends an Nginx request to the target
// and synchronously waits for the reply.
func (n *Nginx) Execute() ([]*Result, error) {
	if n.req == nil {
		return nil, fmt.Errorf("probe has no Nginx request, probably from an invalid configuration")
	}

	log.Debugf("Performing Nginx request: %s %s", n.req.Method, n.req.URL)
	res, err := n.client.Do(n.req)
	if err != nil {
		return nil, err
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, fmt.Errorf("unable to get cluster status result: %v", err)
	}
	res.Body.Close()

	result, err := parseNginxResponse(body)
	if err != nil {
		return nil, err
	}

	log.Debugf("collected: %v", result)
	return []*Result{{Fields: result}}, nil
}

func parseNginxResponse(content []byte) (fields map[string]interface{}, err error) {
	fields = make(map[string]interface{})
	r := regexp.MustCompile(
		"^[^0-9]*([0-9]+)[^0-9]*([0-9]+) ([0-9]+) ([0-9]+)[^0-9]*: ([0-9]+)[^0-9]*: ([0-9]+)[^0-9]*: ([0-9]+)")

	matches := r.FindSubmatch(content)
	if matches == nil {
		return nil, fmt.Errorf("unable to match status response: %v", string(content))
	}

	if m, err := strconv.Atoi(string(matches[1])); err == nil {
		fields[NginxResultActiveConnections] = m
	}
	if m, err := strconv.Atoi(string(matches[2])); err == nil {
		fields[NginxResultAccepts] = m
	}
	if m, err := strconv.Atoi(string(matches[3])); err == nil {
		fields[NginxResultHandled] = m
	}
	if m, err := strconv.Atoi(string(matches[4])); err == nil {
		fields[NginxResultRequests] = m
	}
	if m, err := strconv.Atoi(string(matches[5])); err == nil {
		fields[NginxResultReading] = m
	}
	if m, err := strconv.Atoi(string(matches[6])); err == nil {
		fields[NginxResultWriting] = m
	}
	if m, err := strconv.Atoi(string(matches[7])); err == nil {
		fields[NginxResultWaiting] = m
	}

	return
}
