// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"encoding/json"
	"fmt"

	influx "github.com/influxdata/influxdb/client/v2"
)

// InfluxDB probes an influxtb tsdb
type InfluxDB struct {
	client influx.Client
}

const (

	// InfluxDB configuration parameters

	// InfluxDBParamAddress : (mandatory) the address of the target influxdb server to gather metrics from (string)
	InfluxDBParamAddress = "address"

	// InfluxDB results

	// RuntimeStatistics : tracks a subset of the statistics exposed by the Golang memory allocator stats

	// InfluxDBResultRuntimeAlloc : Alloc is bytes of allocated heap objects (uint64)
	InfluxDBResultRuntimeAlloc = "runtime-alloc"
	// InfluxDBResultRuntimeFrees : Frees is the cumulative count of heap objects freed (uint64)
	InfluxDBResultRuntimeFrees = "runtime-frees"
	// InfluxDBResultRuntimeHeapAlloc : HeapAlloc is bytes of allocated heap objects (uint64)
	InfluxDBResultRuntimeHeapAlloc = "runtime-heap-alloc"
	// InfluxDBResultRuntimeHeapIdle : HeapIdle is bytes in idle (unused) spans (uint64)
	InfluxDBResultRuntimeHeapIdle = "runtime-heap-idle"
	// InfluxDBResultRuntimeHeapInUse : HeapInuse is bytes in in-use spans (uint64)
	InfluxDBResultRuntimeHeapInUse = "runtime-heap-in-use"
	// InfluxDBResultRuntimeHeapObjects : HeapObjects is the number of allocated heap objects (uint64)
	InfluxDBResultRuntimeHeapObjects = "runtime-heap-objects"
	// InfluxDBResultRuntimeHeapReleased : HeapReleased is bytes of physical memory returned to the OS (uint64)
	InfluxDBResultRuntimeHeapReleased = "runtime-heap-released"
	// InfluxDBResultRuntimeHeapSys : HeapSys is bytes of heap memory obtained from the OS (uint64)
	InfluxDBResultRuntimeHeapSys = "runtime-heap-sys"
	// InfluxDBResultRuntimeLookups : Lookups is the number of pointer lookups performed by the runtime (uint64)
	InfluxDBResultRuntimeLookups = "runtime-lookups"
	// InfluxDBResultRuntimeMallocs : Mallocs is the cumulative count of heap objects allocated (uint64)
	InfluxDBResultRuntimeMallocs = "runtime-mallocs"
	// InfluxDBResultRuntimeNumGC : NumGC is the number of completed GC cycles (uint32)
	InfluxDBResultRuntimeNumGC = "runtime-num-gc"
	// InfluxDBResultRuntimeNumGoroutine : NumGoroutine returns the number of goroutines that currently exist (int)
	InfluxDBResultRuntimeNumGoroutine = "runtime-num-goroutine"
	// InfluxDBResultRuntimePauseTotalNs : PauseTotalNs is the cumulative nanoseconds in GC stop-the-world pauses since the program started (uint64)
	InfluxDBResultRuntimePauseTotalNs = "runtime-pause-total-ns"
	// InfluxDBResultRuntimeSys : Sys is the total bytes of memory obtained from the OS (uint64)
	InfluxDBResultRuntimeSys = "runtime-sys"
	// InfluxDBResultRuntimeTotalAlloc : TotalAlloc is cumulative bytes allocated for heap objects (uint64)
	InfluxDBResultRuntimeTotalAlloc = "runtime-total-alloc"

	// queryExecutor : tracks statistics about the query executor portion of the InfluxDB engine

	// InfluxDBResultQueriesActive : queriesActive tracks the number of queries being handled at this instant in time (int)
	InfluxDBResultQueriesActive = "qe-queriesActive"
	// InfluxDBResultQueriesExecuted : Number of queries that have been executed (started) (int)
	InfluxDBResultQueriesExecuted = "qe-queriesExecuted"
	// InfluxDBResultQueriesFinished : Number of queries that have finished (int)
	InfluxDBResultQueriesFinished = "qe-queriesFinished"
	// InfluxDBResultQueryDurationNs : queryDurationNs tracks the cumulative wall time, in nanoseconds, of every query executed (int)
	InfluxDBResultQueryDurationNs = "qe-queryDurationNs"
	// InfluxDBResultQERecoveredPanics : Number of panics recovered by Query Executor (int)
	InfluxDBResultQERecoveredPanics = "qe-recoveredPanics"

	// Write : tracks statistics about writes at a system level

	// InfluxDBResultPointReq : pointReq is incremented for every point that is attempted to be written, regardless of success (int)
	InfluxDBResultPointReq = "write-pointreq"
	// InfluxDBResultPointReqLocal : pointReqLocal is incremented for every point that is attempted to be written into a shard, regardless of success (int)
	InfluxDBResultPointReqLocal = "write-pointreqlocal"
	// InfluxDBResultWReq : req is incremented every time a batch of points is attempted to be written, regardless of success (int)
	InfluxDBResultWReq = "write-req"
	// InfluxDBResultSubWriteDrop : subWriteDrop is incremented every time a batch write to a subscriber is dropped due to contention or write saturation (int)
	InfluxDBResultSubWriteDrop = "write-subwritedrop"
	// InfluxDBResultSubWriteOk : subWriteOk is incremented every time a batch write to a subscriber succeeds (int)
	InfluxDBResultSubWriteOk = "write-subwriteok"
	// InfluxDBResultWriteDrop : writeDrop is incremented for every point dropped due to having a timestamp that does not match any existing retention policy (int)
	InfluxDBResultWriteDrop = "write-writedrop"
	// InfluxDBResultWriteError : writeError is incremented for every batch that was attempted to be written to a shard but failed (int)
	InfluxDBResultWriteError = "write-writeerror"
	// InfluxDBResultWriteOk : writeOk is incremented for every batch that was successfully written to a shard (int)
	InfluxDBResultWriteOk = "write-writeok"
	// InfluxDBResultWriteTimeout : writeTimeout is incremented every time a write failed due to timing out (int)
	InfluxDBResultWriteTimeout = "write-writetimeout"

	// Subscriber : tracks subscriber statistics

	// InfluxDBResultCreateFailures :  (int)
	InfluxDBResultCreateFailures = "subscriber-createfailures"
	// InfluxDBResultPointsWritten : pointsWritten tracks the number of points successfully written to subscribers (int)
	InfluxDBResultPointsWritten = "subscriber-pointswritten"
	// InfluxDBResultWriteFailures : writeFailures tracks the number of batches that failed to send to subscribers (int)
	InfluxDBResultWriteFailures = "subscriber-writefailures"

	// cq : tracks statistics about the continuous query executor

	// InfluxDBResultQueryFail : queryFail  is incremented whenever a continuous query is executed but fails (int)
	InfluxDBResultQueryFail = "cq-queryfail"
	// InfluxDBResultQueryOk : queryOk is incremented whenever a continuous query is executed without a failure (int)
	InfluxDBResultQueryOk = "cq-queryok"

	// httpd : tracks statistics about the InfluxDB HTTP server

	// InfluxDBResultAuthFail : authFail indicates how many HTTP requests were aborted due to authentication being required but unsupplied or incorrect (int)
	InfluxDBResultAuthFail = "httpd-authfail"
	// InfluxDBResultClientError : clientError is incremented every time InfluxDB sends an HTTP response with a 4XX status code (int)
	InfluxDBResultClientError = "httpd-clienterror"
	// InfluxDBResultPingReq : pingReq is incremented every time InfluxDB serves the /ping HTTP endpoint (int)
	InfluxDBResultPingReq = "httpd-pingreq"
	// InfluxDBResultPointsWrittenDropped : Number of points dropped by the storage engine (int)
	InfluxDBResultPointsWrittenDropped = "httpd-pointswrittendropped"
	// InfluxDBResultPointsWrittenFail : pointsWrittenFail is incremented for every point (not every batch) that was accepted by the /write HTTP endpoint but was unable to be persisted (int)
	InfluxDBResultPointsWrittenFail = "httpd-pointswrittenfail"
	// InfluxDBResultPointsWrittenOK : pointsWrittenOK is incremented for every point (not every batch) that was accepted by the /write HTTP endpoint and persisted successfully (int)
	InfluxDBResultPointsWrittenOK = "httpd-pointswrittenok"
	// InfluxDBResultQueryReq : queryReq is incremented every time InfluxDB serves the /query HTTP endpoint (int)
	InfluxDBResultQueryReq = "httpd-queryreq"
	// InfluxDBResultQueryReqDurationNs : queryReqDurationNs tracks the cumulative wall time, in nanoseconds, of every query served (int)
	InfluxDBResultQueryReqDurationNs = "httpd-queryreqdurationns"
	// InfluxDBResultQueryRespBytes : queryRespBytes is increased for every byte InfluxDB sends in a successful query response (int)
	InfluxDBResultQueryRespBytes = "httpd-queryrespbytes"
	// InfluxDBResultRecoveredPanics : Number of panics recovered by HTTP handler (int)
	InfluxDBResultRecoveredPanics = "httpd-recoveredpanics"
	// InfluxDBResultReq : req is incremented for every HTTP request InfluxDB receives (int)
	InfluxDBResultReq = "httpd-req"
	// InfluxDBResultReqActive : reqActive is incremented when InfluxDB begins accepting an HTTP request and is decremented whenever InfluxDB finishes serving that request (int)
	InfluxDBResultReqActive = "httpd-reqactive"
	// InfluxDBResultReqDurationNs : reqDurationNs tracks the cumulative wall time, in nanoseconds, of every request served (int)
	InfluxDBResultReqDurationNs = "httpd-reqdurationns"
	// InfluxDBResultServerError : serverError is incremented every time InfluxDB sends an HTTP response with a 5XX status code (int)
	InfluxDBResultServerError = "httpd-servererror"
	// InfluxDBResultStatusReq : statusReq is incremented every time InfluxDB serves the /status HTTP endpoint (int)
	InfluxDBResultStatusReq = "httpd-statusreq"
	// InfluxDBResultWriteReq : writeReq is incremented every time InfluxDB serves the /write HTTP endpoint (int)
	InfluxDBResultWriteReq = "httpd-writereq"
	// InfluxDBResultWriteReqActive : writeReqActive tracks the number of write requests over HTTP being handled at this instant in time (int)
	InfluxDBResultWriteReqActive = "httpd-writereqactive"
	// InfluxDBResultWriteReqBytes : writeReqBytes tracks the total number of bytes of line protocol received by the /write endpoint (int)
	InfluxDBResultWriteReqBytes = "httpd-writereqbytes"
	// InfluxDBResultWriteReqDurationNs : writeReqDurationNs tracks the cumulative wall time, in nanoseconds, of every write request served (int)
	InfluxDBResultWriteReqDurationNs = "httpd-writereqdurationns"
)

// Name returns the name of the probe "influxdb"
func (tsdb *InfluxDB) Name() string {
	return "influxdb"
}

// Configure configures the influxdb probe
func (tsdb *InfluxDB) Configure(config map[string]interface{}) (err error) {

	var ok bool
	var addr string
	if addr, ok = config[InfluxDBParamAddress].(string); !ok {
		return fmt.Errorf("\"%s\" field is not string: %v", InfluxDBParamAddress, config[InfluxDBParamAddress])
	}

	tsdb.client, err = influx.NewHTTPClient(influx.HTTPConfig{Addr: addr})
	if err != nil {
		return err
	}

	return nil
}

// Execute runs the probe and gathers information about the influxdb server
func (tsdb *InfluxDB) Execute() ([]*Result, error) {
	q := influx.NewQuery("SHOW STATS", "", "")
	stats, err := tsdb.client.Query(q)
	if err != nil {
		return nil, err
	}
	if len(stats.Results) < 1 {
		return nil, fmt.Errorf("no stat found")
	}

	fields := scanStatsForFields(stats.Results[0])

	return []*Result{{Fields: fields}}, nil
}

func scanStatsForFields(res influx.Result) (fields map[string]interface{}) {
	fields = make(map[string]interface{})

	for _, serie := range res.Series {
		// we should have only one row per serie
		if len(serie.Values) == 1 {
			row := serie.Values[0]

			if serie.Name == "runtime" {
				for j, s := range row {
					switch serie.Columns[j] {
					case "Alloc":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultRuntimeAlloc] = n
						}
					case "Frees":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultRuntimeFrees] = n
						}
					case "HeapAlloc":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultRuntimeHeapAlloc] = n
						}
					case "HeapIdle":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultRuntimeHeapIdle] = n
						}
					case "HeapInUse":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultRuntimeHeapInUse] = n
						}
					case "HeapObjects":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultRuntimeHeapObjects] = n
						}
					case "HeapReleased":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultRuntimeHeapReleased] = n
						}
					case "HeapSys":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultRuntimeHeapSys] = n
						}
					case "Lookups":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultRuntimeLookups] = n
						}
					case "Mallocs":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultRuntimeMallocs] = n
						}
					case "NumGC":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultRuntimeNumGC] = n
						}
					case "NumGoroutine":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultRuntimeNumGoroutine] = n
						}
					case "PauseTotalNs":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultRuntimePauseTotalNs] = n
						}
					case "Sys":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultRuntimeSys] = n
						}
					case "TotalAlloc":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultRuntimeTotalAlloc] = n
						}
					}
				}
			}
			if serie.Name == "queryExecutor" {
				for j, s := range row {
					switch serie.Columns[j] {
					case "queriesActive":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultQueriesActive] = n
						}
					case "queriesExecuted":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultQueriesExecuted] = n
						}
					case "queriesFinished":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultQueriesFinished] = n
						}
					case "queryDurationNs":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultQueryDurationNs] = n
						}
					case "recoveredPanics":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultQERecoveredPanics] = n
						}
					}
				}
			}
			if serie.Name == "write" {
				for j, s := range row {
					switch serie.Columns[j] {
					case "pointReq":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultPointReq] = n
						}
					case "pointReqLocal":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultPointReqLocal] = n
						}
					case "req":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultWReq] = n
						}
					case "subWriteDrop":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultSubWriteDrop] = n
						}
					case "subWriteOk":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultSubWriteOk] = n
						}
					case "writeDrop":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultWriteDrop] = n
						}
					case "writeError":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultWriteError] = n
						}
					case "writeOk":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultWriteOk] = n
						}
					case "writeTimeout":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultWriteTimeout] = n
						}
					}
				}
			}
			if serie.Name == "subscriber" {
				for j, s := range row {
					switch serie.Columns[j] {
					case "createFailures":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultCreateFailures] = n
						}
					case "pointsWritten":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultPointsWritten] = n
						}
					case "writeFailures":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultWriteFailures] = n
						}
					}
				}
			}
			if serie.Name == "cq" {
				for j, s := range row {
					switch serie.Columns[j] {
					case "queryFail":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultQueryFail] = n
						}
					case "queryOk":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultQueryOk] = n
						}
					}
				}
			}
			if serie.Name == "httpd" {
				for j, s := range row {
					switch serie.Columns[j] {
					case "authFail":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultAuthFail] = n
						}
					case "clientError":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultClientError] = n
						}
					case "pingReq":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultPingReq] = n
						}
					case "pointsWrittenDropped":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultPointsWrittenDropped] = n
						}
					case "pointsWrittenFail":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultPointsWrittenFail] = n
						}
					case "pointsWrittenOK":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultPointsWrittenOK] = n
						}
					case "queryReq":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultQueryReq] = n
						}
					case "queryReqDurationNs":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultQueryReqDurationNs] = n
						}
					case "queryRespBytes":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultQueryRespBytes] = n
						}
					case "recoveredPanics":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultRecoveredPanics] = n
						}
					case "req":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultReq] = n
						}
					case "reqActive":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultReqActive] = n
						}
					case "reqDurationNs":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultReqDurationNs] = n
						}
					case "serverError":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultServerError] = n
						}
					case "statusReq":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultStatusReq] = n
						}
					case "writeReq":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultWriteReq] = n
						}
					case "writeReqActive":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultWriteReqActive] = n
						}
					case "writeReqBytes":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultWriteReqBytes] = n
						}
					case "writeReqDurationNs":
						if n, err := s.(json.Number).Int64(); err == nil {
							fields[InfluxDBResultWriteReqDurationNs] = n
						}
					}
				}
			}
		}
	}

	return
}
