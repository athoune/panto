// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"github.com/shirou/gopsutil/host"
)

//------------
// Uptime probe
//------------

// Uptime probes system uptime
type Uptime struct{}

const (

	// Uptime results

	// UptimeResultUptime : System uptime (number of seconds since last boot) (`uint64`)
	UptimeResultUptime = "uptime"
	// UptimeResultBoottime : System last boot time (expressed in seconds since epoch) (`uint64`)
	UptimeResultBoottime = "boottime"
)

// Name returns the name of the probe "uptime"
func (l *Uptime) Name() string {
	return "uptime"
}

// Configure configures the uptime probe
func (l *Uptime) Configure(config map[string]interface{}) error {
	return nil
}

// Execute runs the probe and gathers system uptime information
func (l *Uptime) Execute() ([]*Result, error) {
	up, err := host.Uptime()
	if err != nil {
		return nil, err
	}
	log.Debugf("collected uptime info: %s", up)

	boot, err := host.BootTime()
	if err != nil {
		return nil, err
	}
	log.Debugf("collected boottime info: %s", boot)

	fields := map[string]interface{}{
		UptimeResultUptime:   up,
		UptimeResultBoottime: boot,
	}

	return []*Result{{Fields: fields}}, nil
}
