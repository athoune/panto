// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"net/http"
	"strings"
	"testing"
	"time"
)

const responseHeadersPrefix = "Content-Length: 12\r\nContent-Type: text/plain; charset=utf-8\r\nDate:"
const responseBody = "Hello World!"

// Compile error if HTTP does not conform to Probe interface
var _ Probe = (*HTTP)(nil)

func TestHTTPName(t *testing.T) {
	h := &HTTP{}

	if h.Name() != "http" {
		t.Error("HTTP probe name is not \"http\"")
	}
}

func TestHTTPConfigureBasic(t *testing.T) {
	h := &HTTP{}
	config := map[string]interface{}{
		HTTPParamURL: "http://localhost:9999",
	}
	err := h.Configure(config)
	if err != nil {
		t.Errorf("HTTP probe could not be configured with %s, %s", config, err)
	}
}

func TestHTTPConfigureFull(t *testing.T) {
	h := &HTTP{}
	config := map[string]interface{}{
		HTTPParamMethod:           http.MethodGet,
		HTTPParamURL:              "http://localhost:9999",
		HTTPParamBody:             "Hello World!",
		HTTPResultResponseContent: HTTPResponseContentNone,
	}
	err := h.Configure(config)
	if err != nil {
		t.Errorf("HTTP probe could not be configured with %s, %s", config, err)
	}
}

func TestHTTPConfigureErrors(t *testing.T) {
	configs := []map[string]interface{}{
		// nil config
		nil,
		// empty config
		{},
		// missing URL
		{
			HTTPParamMethod: http.MethodGet,
			HTTPParamBody:   "Hello World!",
		},
	}

	h := &HTTP{}
	for _, config := range configs {
		t.Logf("Testing HTTP.Configure with %v", config)
		err := h.Configure(config)
		if h.req != nil {
			t.Error("invalid HTTP probe's request is not nil")
		}
		if err == nil {
			t.Errorf("HTTP probe successfully configured, expected error")
		}

	}
}

func TestHTTPExecute(t *testing.T) {

	h := &HTTP{}

	config := map[string]interface{}{
		HTTPParamURL: "http://localhost:9999",
	}
	h.Configure(config)
	res, err := h.Execute()
	if err != nil {
		t.Fatalf("unexpected error executing HTTP probe: %s", err)
	}

	expectedKeys := []string{
		HTTPResultRoundTripTime,
		HTTPResultStatusCode,
	}
	for _, k := range expectedKeys {
		if _, ok := res[0].Fields[k]; !ok {
			t.Fatalf("missing \"%s\" field in HTTP results", k)
		}
	}
	statusCode, ok := res[0].Fields[HTTPResultStatusCode].(int)
	if !ok {
		t.Errorf("invalid status code type, expected int, got %T", res[0].Fields[HTTPResultStatusCode])
	}
	if statusCode != http.StatusOK {
		t.Errorf("status code is invalid, expected %d, got %d", http.StatusOK, statusCode)
	}
}

func TestHTTPExecuteStatusCode(t *testing.T) {
	h := HTTP{}

	config := map[string]interface{}{
		HTTPParamURL: "http://localhost:9999",
	}
	h.Configure(config)
	res, err := h.Execute()
	if err != nil {
		t.Errorf("unexpected error executing HTTP probe: %s", err)
	}
	statusCode, ok := res[0].Fields[HTTPResultStatusCode].(int)
	if !ok {
		t.Errorf("invalid status code type, expected int, got %T", res[0].Fields[HTTPResultStatusCode])
	}
	if statusCode != http.StatusOK {
		t.Errorf("unexpected error code: got %d, expected %d", statusCode, http.StatusOK)
	}

	http.HandleFunc("/not-found", func(w http.ResponseWriter, _ *http.Request) {
		w.WriteHeader(http.StatusNotFound)
	})
	config[HTTPParamURL] = "http://localhost:9999/not-found"
	h.Configure(config)
	res, err = h.Execute()
	if err != nil {
		t.Errorf("unexpected error executing HTTP probe: %s", err)
	}
	statusCode, ok = res[0].Fields[HTTPResultStatusCode].(int)
	if !ok {
		t.Errorf("invalid status code type, expected int, got %T", res[0].Fields[HTTPResultStatusCode])
	}
	if statusCode != 404 {
		t.Errorf("unexpected error code: got %d, expected 404", statusCode)
	}
}

func TestHTTPExecuteTimeout(t *testing.T) {

	h := &HTTP{}

	config := map[string]interface{}{
		HTTPParamURL:     "http://localhost:9999",
		HTTPParamTimeout: 1 * time.Millisecond,
	}
	h.Configure(config)
	_, err := h.Execute()
	if err == nil {
		t.Errorf("expected timeout error executing HTTP probe")
	}
}

func TestHTTPExecuteContent(t *testing.T) {

	h := &HTTP{}

	config := map[string]interface{}{
		HTTPParamURL:             "http://localhost:9999",
		HTTPParamResponseContent: HTTPResponseContentNone,
	}

	h.Configure(config)
	res, err := h.Execute()
	if err != nil {
		t.Errorf("unexpected error executing HTTP probe: %s", err)
	}
	if len(res) == 0 {
		t.Errorf("no items in results")
	}
	if res[0].Fields[HTTPParamResponseContent] != "" {
		t.Errorf("response content is not empty string: %s", res[0].Fields[HTTPParamResponseContent])
	}

	config[HTTPParamResponseContent] = HTTPResponseContentHeadersOnly
	h.Configure(config)
	res, _ = h.Execute()
	if err != nil {
		t.Errorf("unexpected error executing HTTP probe: %s", err)
	}
	if len(res) == 0 {
		t.Errorf("no items in results")
	}
	content, ok := res[0].Fields[HTTPParamResponseContent].(string)
	if !ok {
		t.Fatalf("invalid content type, expected string, got %T", res[0].Fields[HTTPParamResponseContent])
	}
	if !strings.HasPrefix(content, responseHeadersPrefix) {
		t.Errorf("response content does not have correct prefix: expected %#v, got %#v", responseHeadersPrefix, res[0].Fields[HTTPParamResponseContent])
	}
	if !strings.HasPrefix(content, responseHeadersPrefix) {
		t.Errorf("response content does not have correct prefix: expected %#v, got %#v", responseHeadersPrefix, res[0].Fields[HTTPParamResponseContent])
	}

	config[HTTPParamResponseContent] = HTTPResponseContentBodyOnly
	h.Configure(config)
	res, err = h.Execute()
	if err != nil {
		t.Errorf("unexpected error executing HTTP probe: %s", err)
	}
	if len(res) == 0 {
		t.Errorf("no items in results")
	}
	content, ok = res[0].Fields[HTTPParamResponseContent].(string)
	if !ok {
		t.Fatalf("invalid content type, expected string, got %T", res[0].Fields[HTTPParamResponseContent])
	}
	if content != responseBody {
		t.Errorf("response content is invalid: expected %#v, got %#v", responseBody, content)
	}

	config[HTTPParamResponseContent] = HTTPResponseContentFull
	h.Configure(config)
	res, _ = h.Execute()
	if err != nil {
		t.Errorf("unexpected error executing HTTP probe: %s", err)
	}
	if len(res) == 0 {
		t.Errorf("no items in results")
	}
	content, ok = res[0].Fields[HTTPParamResponseContent].(string)
	if !ok {
		t.Fatalf("invalid content type, expected string, got %T", res[0].Fields[HTTPParamResponseContent])
	}
	if !strings.Contains(content, responseHeadersPrefix) {
		t.Errorf("response content does not have correct headers: expected %#v, got %#v", responseHeadersPrefix, content)
	}
	if !strings.HasSuffix(content, responseBody) {
		t.Errorf("response content does not have correct suffix: expected %#v, got %#v", responseBody, content)
	}
	if !strings.Contains(content, responseHeadersPrefix) {
		t.Errorf("response content does not have correct headers: expected %#v, got %#v", responseHeadersPrefix, content)
	}
	if !strings.HasSuffix(content, responseBody) {
		t.Errorf("response content does not have correct suffix: expected %#v, got %#v", responseBody, content)
	}
}
