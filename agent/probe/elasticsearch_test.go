// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"reflect"
	"testing"

	"github.com/spf13/viper"

	"gitlab.com/pantomath-io/panto/util"
)

// Compile error if sysinfo probes do not conform to Probe interface
var _ Probe = (*Elasticsearch)(nil)

func TestElasticsearchName(t *testing.T) {
	r := Elasticsearch{}
	if r.Name() != "elasticsearch" {
		t.Error("Elasticsearch probe name is not \"elasticsearch\"")
	}
}

func TestElasticsearchConfigure(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live testing...")
	}

	r := Elasticsearch{}
	err := r.Configure(map[string]interface{}{ElasticsearchParamAddress: viper.GetString("elasticsearch.address")})
	if err != nil {
		t.Errorf("couldn't configure Elasticsearch probe: %s", err)
	}
}

func TestElasticsearchExecute(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live testing...")
	}

	r := Elasticsearch{}

	err := r.Configure(map[string]interface{}{ElasticsearchParamAddress: viper.GetString("elasticsearch.address")})
	if err != nil {
		t.Fatalf("couldn't configure Elasticsearch probe: %s", err)
	}
	res, err := r.Execute()
	if err != nil {
		t.Fatalf("couldn't execute Elasticsearch probe: %s", err)
	}
	if len(res) != 1 {
		t.Fatalf("Expected %d results, got %d", 1, len(res))
	}
}

func TestElasticsearchMultipleExecute(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live testing...")
	}

	r := Elasticsearch{}

	err := r.Configure(map[string]interface{}{ElasticsearchParamAddress: viper.GetString("elasticsearch.address")})
	if err != nil {
		t.Fatalf("couldn't configure Elasticsearch probe: %s", err)
	}

	res, err := r.Execute()
	if err != nil {
		t.Fatalf("couldn't execute Elasticsearch probe: %s", err)
	}
	if len(res) != 1 {
		t.Fatalf("Expected %d results, got %d", 1, len(res))
	}
	if len(res[0].Fields) != 26 {
		t.Fatalf("Expected %d fields in result, got %d", 26, len(res[0].Fields))
	}

	res, err = r.Execute()
	if err != nil {
		t.Fatalf("couldn't execute Elasticsearch probe: %s", err)
	}
	if len(res) != 1 {
		t.Fatalf("Expected %d results, got %d on second exec", 1, len(res))
	}
	if len(res[0].Fields) != 26 {
		t.Fatalf("Expected %d fields in result for second exec, got %d", 26, len(res[0].Fields))
	}
}

func TestScanElasticsearchStats(t *testing.T) {
	stats, err := scanElasticsearchStats([]byte(testClusterHealth))
	if err != nil {
		t.Fatalf("Failed to scan cluster health: %s", err)
	}
	if len(stats) != 15 {
		t.Fatalf("Expected %d results, got %d", 15, len(stats))
	}
	t.Logf("ClusterHealth: %v", stats)

	stats, err = scanElasticsearchStats([]byte(testClusterStats))
	if err != nil {
		t.Fatalf("Failed to scan cluster stats: %s", err)
	}
	if len(stats) != 6 {
		t.Fatalf("Expected %d results, got %d", 6, len(stats))
	}
	t.Logf("ClusterStats: %v", stats)
}

func TestGatherElasticsearchFields(t *testing.T) {
	fields := make(map[string]interface{})
	stats, _ := scanElasticsearchStats([]byte(testClusterHealth))
	gatherElasticsearchFields(fields, ElasticsearchURLClusterHealth, stats)

	t.Logf("Fields: %v", fields)

	expectedKeys := map[string]interface{}{
		ElasticsearchResultClusterName:                 reflect.String,
		ElasticsearchResultStatus:                      reflect.String,
		ElasticsearchResultTimedOut:                    reflect.Bool,
		ElasticsearchResultNumberOfNodes:               reflect.Float64,
		ElasticsearchResultNumberOfDataNodes:           reflect.Float64,
		ElasticsearchResultActivePrimaryShards:         reflect.Float64,
		ElasticsearchResultActiveShards:                reflect.Float64,
		ElasticsearchResultRelocatingShards:            reflect.Float64,
		ElasticsearchResultInitializingShards:          reflect.Float64,
		ElasticsearchResultUnassignedShards:            reflect.Float64,
		ElasticsearchResultDelayedUnassignedShards:     reflect.Float64,
		ElasticsearchResultNumberOfPendingTasks:        reflect.Float64,
		ElasticsearchResultNumberOfInFlightFetch:       reflect.Float64,
		ElasticsearchResultTaskMaxWaitingInQueueMillis: reflect.Float64,
	}
	for k := range expectedKeys {
		if _, ok := fields[k]; !ok {
			t.Errorf("missing key \"%s\" in elasticsearch cluster health results", k)
		}
	}
	for k, field := range fields {
		kind, ok := expectedKeys[k]
		if !ok {
			t.Errorf("unexpected key \"%s\" in elasticsearch cluster health results", k)
			continue
		}
		if kind != reflect.TypeOf(field).Kind() {
			t.Errorf("invalid type for field %s, expected %s, got %s", k, kind, reflect.TypeOf(field).Kind())
			continue
		}
	}

	fields = make(map[string]interface{})
	stats, _ = scanElasticsearchStats([]byte(testClusterStats))
	gatherElasticsearchFields(fields, ElasticsearchURLClusterStats, stats)

	t.Logf("Fields: %v", fields)

	expectedKeys = map[string]interface{}{
		ElasticsearchResultIndicesCount: reflect.Float64,
		ElasticsearchResultDocsCount:    reflect.Float64,
		ElasticsearchResultDocsDeleted:  reflect.Float64,
		ElasticsearchResultStoreSize:    reflect.Float64,
		ElasticsearchResultQCMemory:     reflect.Float64,
		ElasticsearchResultQCCountHit:   reflect.Float64,
		ElasticsearchResultQCCountMiss:  reflect.Float64,
		ElasticsearchResultQCCountCache: reflect.Float64,
		ElasticsearchResultQCEvictions:  reflect.Float64,
		ElasticsearchResultJVMHeapMax:   reflect.Float64,
		ElasticsearchResultJVMHeapUsed:  reflect.Float64,
		ElasticsearchResultJVMThreads:   reflect.Float64,
	}
	for k := range expectedKeys {
		if _, ok := fields[k]; !ok {
			t.Errorf("missing key \"%s\" in elasticsearch cluster health results", k)
		}
	}
	for k, field := range fields {
		kind, ok := expectedKeys[k]
		if !ok {
			t.Errorf("unexpected key \"%s\" in elasticsearch cluster health results", k)
			continue
		}
		if kind != reflect.TypeOf(field).Kind() {
			t.Errorf("invalid type for field %s, expected %s, got %s", k, kind, reflect.TypeOf(field).Kind())
			continue
		}
	}
}

// A mock of a elasticsearch _cluster/health
const testClusterHealth = `{
  "cluster_name": "docker-cluster",
  "status": "green",
  "timed_out": false,
  "number_of_nodes": 1,
  "number_of_data_nodes": 1,
  "active_primary_shards": 0,
  "active_shards": 0,
  "relocating_shards": 0,
  "initializing_shards": 0,
  "unassigned_shards": 0,
  "delayed_unassigned_shards": 0,
  "number_of_pending_tasks": 0,
  "number_of_in_flight_fetch": 0,
  "task_max_waiting_in_queue_millis": 0,
  "active_shards_percent_as_number": 100.0
}`

// A mock of a elasticsearch _cluster/stats
const testClusterStats = `{
  "_nodes": {
    "total": 1,
    "successful": 1,
    "failed": 0
  },
  "cluster_name": "docker-cluster",
  "timestamp": 1530085728450,
  "status": "green",
  "indices": {
    "count": 0,
    "shards": {},
    "docs": {
      "count": 0,
      "deleted": 0
    },
    "store": {
      "size_in_bytes": 0
    },
    "fielddata": {
      "memory_size_in_bytes": 0,
      "evictions": 0
    },
    "query_cache": {
      "memory_size_in_bytes": 0,
      "total_count": 0,
      "hit_count": 0,
      "miss_count": 0,
      "cache_size": 0,
      "cache_count": 0,
      "evictions": 0
    },
    "completion": {
      "size_in_bytes": 0
    },
    "segments": {
      "count": 0,
      "memory_in_bytes": 0,
      "terms_memory_in_bytes": 0,
      "stored_fields_memory_in_bytes": 0,
      "term_vectors_memory_in_bytes": 0,
      "norms_memory_in_bytes": 0,
      "points_memory_in_bytes": 0,
      "doc_values_memory_in_bytes": 0,
      "index_writer_memory_in_bytes": 0,
      "version_map_memory_in_bytes": 0,
      "fixed_bit_set_memory_in_bytes": 0,
      "max_unsafe_auto_id_timestamp": -9223372036854775808,
      "file_sizes": {}
    }
  },
  "nodes": {
    "count": {
      "total": 1,
      "data": 1,
      "coordinating_only": 0,
      "master": 1,
      "ingest": 1
    },
    "versions": [
      "6.3.0"
    ],
    "os": {
      "available_processors": 2,
      "allocated_processors": 2,
      "names": [
        {
          "name": "Linux",
          "count": 1
        }
      ],
      "mem": {
        "total_in_bytes": 3080871936,
        "free_in_bytes": 144560128,
        "used_in_bytes": 2936311808,
        "free_percent": 5,
        "used_percent": 95
      }
    },
    "process": {
      "cpu": {
        "percent": 0
      },
      "open_file_descriptors": {
        "min": 200,
        "max": 200,
        "avg": 200
      }
    },
    "jvm": {
      "max_uptime_in_millis": 39081154,
      "versions": [
        {
          "version": "10.0.1",
          "vm_name": "OpenJDK 64-Bit Server VM",
          "vm_version": "10.0.1+10",
          "vm_vendor": "Oracle Corporation",
          "count": 1
        }
      ],
      "mem": {
        "heap_used_in_bytes": 239119400,
        "heap_max_in_bytes": 1056309248
      },
      "threads": 28
    },
    "fs": {
      "total_in_bytes": 62748692480,
      "free_in_bytes": 46641311744,
      "available_in_bytes": 43423444992
    },
    "plugins": [
      {
        "name": "ingest-user-agent",
        "version": "6.3.0",
        "elasticsearch_version": "6.3.0",
        "java_version": "1.8",
        "description": "Ingest processor that extracts information from a user agent",
        "classname": "org.elasticsearch.ingest.useragent.IngestUserAgentPlugin",
        "extended_plugins": [],
        "has_native_controller": false
      },
      {
        "name": "ingest-geoip",
        "version": "6.3.0",
        "elasticsearch_version": "6.3.0",
        "java_version": "1.8",
        "description": "Ingest processor that uses looksup geo data based on ip adresses using the Maxmind geo database",
        "classname": "org.elasticsearch.ingest.geoip.IngestGeoIpPlugin",
        "extended_plugins": [],
        "has_native_controller": false
      }
    ],
    "network_types": {
      "transport_types": {
        "security4": 1
      },
      "http_types": {
        "security4": 1
      }
    }
  }
}`
