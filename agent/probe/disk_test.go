// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"reflect"
	"testing"
)

// Compile error if sysinfo probes do not conform to Probe interface
var _ Probe = (*Load)(nil)
var _ Probe = (*Disk)(nil)

func TestDiskName(t *testing.T) {
	d := Disk{}
	if d.Name() != "disk" {
		t.Error("Disk probe name is not \"disk\"")
	}
}

func TestDiskConfigure(t *testing.T) {
	d := Disk{}
	err := d.Configure(map[string]interface{}{})
	if err != nil {
		t.Errorf("couldn't configure Disk probe: %s", err)
	}
}

func TestDiskExecute(t *testing.T) {
	d := Disk{}

	res, err := d.Execute()
	if err != nil {
		t.Errorf("couldn't execute Disk probe: %s", err)
	}
	if len(res) == 0 {
		t.Errorf("no items in results")
	}

	expectedKeys := map[string]interface{}{
		DiskResultFree:              reflect.Uint64,
		DiskResultUsed:              reflect.Uint64,
		DiskResultUsedPercent:       reflect.Float32,
		DiskResultInodesFree:        reflect.Uint64,
		DiskResultInodesUsed:        reflect.Uint64,
		DiskResultInodesUsedPercent: reflect.Float32,
	}
	for _, r := range res {
		for k := range expectedKeys {
			if _, ok := r.Fields[k]; !ok {
				t.Errorf("missing key \"%s\" in load results", k)
			}
		}
		for k, field := range r.Fields {
			kind, ok := expectedKeys[k]
			if !ok {
				t.Errorf("unexpected key \"%s\" in load results", k)
				continue
			}
			if kind != reflect.TypeOf(field).Kind() {
				t.Errorf("invalid type for field %s, expected %s, got %s", k, kind, reflect.TypeOf(field).Kind())
				continue
			}
		}
		percent := r.Fields[DiskResultUsedPercent].(float32)
		if percent < 0.0 || percent > 100.0 {
			t.Fatalf("Disk usage %% is not in range [0, 100]: %f", percent)
		}
		percent = r.Fields[DiskResultInodesUsedPercent].(float32)
		if percent < 0.0 || percent > 100.0 {
			t.Errorf("Disk free inodes %% is not in range [0, 100]: %f", percent)
		}

		path, ok := r.Tags[DiskTagPath]
		if !ok {
			t.Errorf("Missing tag %s", DiskTagPath)
		}
		if path == "" {
			t.Errorf("Empty disk path")
		}
	}
}
