// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"reflect"
	"testing"

	"github.com/spf13/viper"

	"gitlab.com/pantomath-io/panto/util"
)

// Compile error if sysinfo probes do not conform to Probe interface
var _ Probe = (*Redis)(nil)

func TestRedisName(t *testing.T) {
	r := Redis{}
	if r.Name() != "redis" {
		t.Error("Redis probe name is not \"redis\"")
	}
}

func TestRedisConfigure(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live testing...")
	}

	r := Redis{}
	err := r.Configure(map[string]interface{}{RedisParamAddress: viper.GetString("redis.address")})
	if err != nil {
		t.Errorf("couldn't configure Redis probe: %s", err)
	}
}

func TestRedisExecute(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live testing...")
	}

	r := Redis{}

	err := r.Configure(map[string]interface{}{RedisParamAddress: viper.GetString("redis.address")})
	if err != nil {
		t.Fatalf("couldn't configure Redis probe: %s", err)
	}
	res, err := r.Execute()
	if err != nil {
		t.Fatalf("couldn't execute Redis probe: %s", err)
	}
	if len(res) == 0 {
		t.Fatalf("no items in results")
	}

	stats, err := scanInfo(testInfo)
	if err != nil {
		t.Fatalf("Failed to scan INFO: %s", err)
	}
	if len(stats) != 110 {
		t.Fatalf("Expected %d results, got %d", 110, len(stats))
	}
	t.Logf("Stats: %v", stats)

	fields := gatherFields(stats)

	t.Logf("Fields: %v", fields)
}

func TestRedisScanInfo(t *testing.T) {
	stats, err := scanInfo(testInfo)
	if err != nil {
		t.Fatalf("Failed to scan INFO: %s", err)
	}
	if len(stats) != 110 {
		t.Fatalf("Expected %d results, got %d", 110, len(stats))
	}
	t.Logf("Stats: %v", stats)
}

func TestRedisGatherStats(t *testing.T) {
	stats, _ := scanInfo(testInfo)
	fields := gatherFields(stats)

	t.Logf("Fields: %v", fields)

	expectedKeys := map[string]interface{}{
		RedisResultUptime:                   reflect.Uint64,
		RedisResultConnectedClients:         reflect.Uint64,
		RedisResultBlockedClients:           reflect.Uint64,
		RedisResultUsedMemory:               reflect.Uint64,
		RedisResultMemoryFragmentationRatio: reflect.Float32,
		RedisResultCacheHitRatio:            reflect.Float32,
		RedisResultChangesSinceLastSave:     reflect.Uint64,
		RedisResultLastSaveTime:             reflect.Uint64,
		RedisResultOpsPerSec:                reflect.Uint64,
		RedisResultRejectedConnections:      reflect.Uint64,
		RedisResultInputKbps:                reflect.Float32,
		RedisResultOutputKbps:               reflect.Float32,
		RedisResultExpiredKeys:              reflect.Uint64,
		RedisResultEvictedKeys:              reflect.Uint64,
		RedisResultConnectedSlaves:          reflect.Uint64,
	}
	for k := range expectedKeys {
		if _, ok := fields[k]; !ok {
			t.Errorf("missing key \"%s\" in redis results", k)
		}
	}
	for k, field := range fields {
		kind, ok := expectedKeys[k]
		if !ok {
			t.Errorf("unexpected key \"%s\" in redis results", k)
			continue
		}
		if kind != reflect.TypeOf(field).Kind() {
			t.Errorf("invalid type for field %s, expected %s, got %s", k, kind, reflect.TypeOf(field).Kind())
			continue
		}
	}
}

// A mock of a redis INFO command
const testInfo = `# Server
redis_version:4.0.7
redis_git_sha1:00000000
redis_git_dirty:0
redis_build_id:8f3d1effb8103d27
redis_mode:standalone
os:Darwin 16.7.0 x86_64
arch_bits:64
multiplexing_api:kqueue
atomicvar_api:atomic-builtin
gcc_version:4.2.1
process_id:50612
run_id:7bd1d8404da69ba3a2dd960448539de9f947440f
tcp_port:6379
uptime_in_seconds:523430
uptime_in_days:6
hz:10
lru_clock:11005558
executable:/usr/local/opt/redis/bin/redis-server
config_file:/usr/local/etc/redis.conf

# Clients
connected_clients:1
client_longest_output_list:0
client_biggest_input_buf:0
blocked_clients:0

# Memory
used_memory:2090128
used_memory_human:1.99M
used_memory_rss:606208
used_memory_rss_human:592.00K
used_memory_peak:2090128
used_memory_peak_human:1.99M
used_memory_peak_perc:100.05%
used_memory_overhead:2078616
used_memory_startup:963376
used_memory_dataset:11512
used_memory_dataset_perc:1.02%
total_system_memory:8589934592
total_system_memory_human:8.00G
used_memory_lua:37888
used_memory_lua_human:37.00K
maxmemory:0
maxmemory_human:0B
maxmemory_policy:noeviction
mem_fragmentation_ratio:0.29
mem_allocator:libc
active_defrag_running:0
lazyfree_pending_objects:0

# Persistence
loading:0
rdb_changes_since_last_save:0
rdb_bgsave_in_progress:0
rdb_last_save_time:1520606696
rdb_last_bgsave_status:ok
rdb_last_bgsave_time_sec:0
rdb_current_bgsave_time_sec:-1
rdb_last_cow_size:0
aof_enabled:0
aof_rewrite_in_progress:0
aof_rewrite_scheduled:0
aof_last_rewrite_time_sec:-1
aof_current_rewrite_time_sec:-1
aof_last_bgrewrite_status:ok
aof_last_write_status:ok
aof_last_cow_size:0

# Stats
total_connections_received:35
total_commands_processed:86975
instantaneous_ops_per_sec:0
total_net_input_bytes:3290207
total_net_output_bytes:125241
instantaneous_input_kbps:0.04
instantaneous_output_kbps:0.00
rejected_connections:0
sync_full:1
sync_partial_ok:29
sync_partial_err:0
expired_keys:1
evicted_keys:0
keyspace_hits:8
keyspace_misses:4
pubsub_channels:0
pubsub_patterns:0
latest_fork_usec:464
migrate_cached_sockets:0
slave_expires_tracked_keys:0
active_defrag_hits:0
active_defrag_misses:0
active_defrag_key_hits:0
active_defrag_key_misses:0

# Replication
role:master
connected_slaves:1
slave0:ip=127.0.0.1,port=7777,state=online,offset=75474,lag=1
master_replid:89353b2a4fccd03f8c4441fa79d40c826326f570
master_replid2:0000000000000000000000000000000000000000
master_repl_offset:75474
second_repl_offset:-1
repl_backlog_active:1
repl_backlog_size:1048576
repl_backlog_first_byte_offset:1
repl_backlog_histlen:75474

# CPU
used_cpu_sys:53.97
used_cpu_user:29.33
used_cpu_sys_children:0.01
used_cpu_user_children:0.00

# Commandstats
cmdstat_set:calls=3,usec=11796,usec_per_call=3932.00
cmdstat_info:calls=9,usec=67474,usec_per_call=7497.11
cmdstat_ping:calls=30,usec=61,usec_per_call=2.03
cmdstat_command:calls=3,usec=11543,usec_per_call=3847.67
cmdstat_psync:calls=30,usec=5779808,usec_per_call=192660.27
cmdstat_select:calls=4,usec=9,usec_per_call=2.25
cmdstat_replconf:calls=86896,usec=130257,usec_per_call=1.50

# Cluster
cluster_enabled:0

# Keyspace
db0:keys=1,expires=0,avg_ttl=0
db4:keys=1,expires=0,avg_ttl=0`
