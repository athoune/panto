// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"os"
	"reflect"
	"testing"
)

// Compile error if sysinfo probes do not conform to Probe interface
var _ Probe = (*Checksum)(nil)

func TestChecksumName(t *testing.T) {
	c := Checksum{}
	if c.Name() != "checksum" {
		t.Error("Checksum probe name is not \"checksum\"")
	}
}

func TestChecksumConfigure(t *testing.T) {
	c := Checksum{}
	err := c.Configure(map[string]interface{}{})
	if err == nil {
		t.Errorf("Unexpected success when configuring Checksum probe.")
	}
	err = c.Configure(map[string]interface{}{
		ChecksumParamPath: "/etc/passwd",
	})
	if err != nil {
		t.Errorf("Failed to configure Checksum probe: %s", err)
	}
	err = c.Configure(map[string]interface{}{
		ChecksumParamPath: "/etc/passwd",
		ChecksumParamHash: ChecksumHashSHA256,
	})
	if err != nil {
		t.Errorf("Failed to configure Checksum probe: %s", err)
	}
	err = c.Configure(map[string]interface{}{
		ChecksumParamPath: "/etc/passwd",
		ChecksumParamHash: "ripemd160",
	})
	if err == nil {
		t.Errorf("Unexpected success when configuring Checksum probe.")
	}
	err = c.Configure(map[string]interface{}{
		ChecksumParamHash: ChecksumHashMD5,
	})
	if err == nil {
		t.Errorf("Unexpected success when configuring Checksum probe.")
	}
}

func TestChecksumExecute(t *testing.T) {
	f, err := os.OpenFile("/tmp/test_file", os.O_CREATE|os.O_RDWR|os.O_TRUNC, os.FileMode(0600))
	if err != nil {
		panic(err)
	}
	defer f.Close()
	_, err = f.WriteString("Hello World!")
	if err != nil {
		panic(err)
	}

	c := Checksum{}

	_, err = c.Execute()
	if err == nil {
		t.Errorf("Unexpected success when executing Checksum probe.")
	}

	err = c.Configure(map[string]interface{}{
		ChecksumParamPath: "/tmp/test_file",
	})
	if err != nil {
		t.Errorf("Failed to configure Checksum probe: %s", err)
	}
	res, err := c.Execute()
	if err != nil {
		t.Errorf("Failed to execute Checksum probe: %s", err)
	}
	if len(res) != 1 {
		t.Errorf("Unexpected results count: got %d, expected %d", len(res), 1)
	} else {
		expectedKeys := map[string]interface{}{
			ChecksumResultChecksum: reflect.String,
		}
		for k := range expectedKeys {
			if _, ok := res[0].Fields[k]; !ok {
				t.Errorf("missing key \"%s\" in checksum results", k)
			}
		}
		for k, field := range res[0].Fields {
			kind, ok := expectedKeys[k]
			if !ok {
				t.Errorf("unexpected key \"%s\" in checksum results", k)
				continue
			}
			if kind != reflect.TypeOf(field).Kind() {
				t.Errorf("invalid type for field %s, expected %s, got %s", k, kind, reflect.TypeOf(field).Kind())
				continue
			}
		}

		if sum, ok := res[0].Fields[ChecksumResultChecksum]; ok {
			if s, ok := sum.(string); ok {
				if s != "1c291ca3" {
					t.Errorf("Unexpected CRC-32 checksum: got %s, expected %s", s, "1c291ca3")
				}
			}
		}
	}
	for _, r := range res {
		t.Logf("%v", *r)
	}

	err = c.Configure(map[string]interface{}{
		ChecksumParamPath: "/tmp/test_file",
		ChecksumParamHash: ChecksumHashSHA256,
	})
	if err != nil {
		t.Errorf("Failed to configure Checksum probe: %s", err)
	}
	res, err = c.Execute()
	if err != nil {
		t.Errorf("Failed to execute Checksum probe: %s", err)
	}
	if len(res) != 1 {
		t.Errorf("Unexpected results count: got %d, expected %d", len(res), 1)
	} else {
		if sum, ok := res[0].Fields[ChecksumResultChecksum]; ok {
			if s, ok := sum.(string); ok {
				if s != "7f83b1657ff1fc53b92dc18148a1d65dfc2d4b1fa3d677284addd200126d9069" {
					t.Errorf("Unexpected SHA256 checksum: got %s, expected %s", s, "7f83b1657ff1fc53b92dc18148a1d65dfc2d4b1fa3d677284addd200126d9069")
				}
			}
		}
	}
	for _, r := range res {
		t.Logf("%v", *r)
	}
}
