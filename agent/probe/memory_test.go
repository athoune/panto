// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"reflect"
	"testing"
)

// Compile error if sysinfo probes do not conform to Probe interface
var _ Probe = (*Memory)(nil)

func TestMemoryName(t *testing.T) {
	m := Memory{}
	if m.Name() != "memory" {
		t.Error("Memory probe name is not \"memory\"")
	}
}

func TestMemoryConfigure(t *testing.T) {
	m := Memory{}
	err := m.Configure(map[string]interface{}{})
	if err != nil {
		t.Errorf("couldn't configure Memory probe: %s", err)
	}
}

func TestMemoryExecute(t *testing.T) {
	m := Memory{}

	res, err := m.Execute()
	if err != nil {
		t.Errorf("couldn't execute memory probe: %s", err)
	}
	if len(res) == 0 {
		t.Errorf("no items in results")
	}

	expectedKeys := map[string]interface{}{
		MemoryResultTotal:           reflect.Uint64,
		MemoryResultAvailable:       reflect.Uint64,
		MemoryResultUsed:            reflect.Uint64,
		MemoryResultUsedPercent:     reflect.Float32,
		MemoryResultFree:            reflect.Uint64,
		MemoryResultSwapTotal:       reflect.Uint64,
		MemoryResultSwapFree:        reflect.Uint64,
		MemoryResultSwapUsed:        reflect.Uint64,
		MemoryResultSwapUsedPercent: reflect.Float32,
	}
	for k := range expectedKeys {
		if _, ok := res[0].Fields[k]; !ok {
			t.Errorf("missing key \"%s\" in memory results", k)
		}
	}
	for k, field := range res[0].Fields {
		kind, ok := expectedKeys[k]
		if !ok {
			t.Errorf("unexpected key \"%s\" in memory results", k)
			continue
		}
		if kind != reflect.TypeOf(field).Kind() {
			t.Errorf("invalid type for field %s, expected %s, got %s", k, kind, reflect.TypeOf(field).Kind())
			continue
		}
	}
}
