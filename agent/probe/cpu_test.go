// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"strconv"
	"testing"
	"time"
)

// Compile error if sysinfo probes do not conform to Probe interface
var _ Probe = (*CPU)(nil)

func TestCPUName(t *testing.T) {
	cpu := CPU{}
	if cpu.Name() != "cpu" {
		t.Error("CPU probe name is not \"cpu\"")
	}
}

func TestCPUConfigure(t *testing.T) {
	cpu := CPU{}

	config := map[string]interface{}{}

	err := cpu.Configure(config)
	if err != nil {
		t.Fatalf("couldn't configure CPU probe: %s", err)
	}
	if !cpu.perCPU {
		t.Error("default value for per CPU count is false, expected true")
	}
	expectInterval := 1 * time.Second
	if cpu.interval != expectInterval {
		t.Fatalf("default value for interval CPU count is %s, expected %s", cpu.interval, expectInterval)
	}

	config = map[string]interface{}{
		CPUParamPerCPU:   false,
		CPUParamInterval: 42 * time.Millisecond,
	}

	err = cpu.Configure(config)
	if err != nil {
		t.Fatalf("couldn't configure CPU probe: %s", err)
	}
	if cpu.perCPU != config[CPUParamPerCPU] {
		t.Fatalf("invalid \"per CPU\" value: expected %t, got %t", config[CPUParamPerCPU], cpu.perCPU)
	}
	if cpu.interval != config[CPUParamInterval] {
		t.Fatalf("invalid \"interval\" value: expected %s, got %s", config[CPUParamInterval], cpu.interval)
	}
}

func TestCPUExecuteDefault(t *testing.T) {
	cpu := CPU{}
	cpu.Configure(map[string]interface{}{})
	res, err := cpu.Execute()
	if err != nil {
		t.Fatalf("couldn't execute CPU probe: %s", err)
	}
	if len(res) == 0 {
		t.Fatalf("no items in results")
	}

	for i, r := range res {
		percent, ok := r.Fields[CPUResultUsage].(float32)
		if !ok {
			t.Fatalf("invalid CPU usage type, expected float32, got %T", r.Fields[CPUResultUsage])
		}
		if percent < 0.0 || percent > 100.0 {
			t.Fatalf("CPU %d usage is not in range [0, 100]: %f", i, percent)
		}
		n, ok := r.Tags[CPUTagCPU]
		if !ok {
			t.Fatalf("missing tag %s", CPUTagCPU)
		}
		if n != strconv.Itoa(i) {
			t.Fatalf("expected %d, got %#v", i, n)
		}
	}
}

func TestCPUExecuteAllCPUs(t *testing.T) {
	cpu := CPU{}
	config := map[string]interface{}{
		CPUParamPerCPU: false,
	}
	err := cpu.Configure(config)
	if err != nil {
		t.Fatalf("couldn't configure CPU probe: %s", err)
	}
	res, err := cpu.Execute()
	if err != nil {
		t.Fatalf("couldn't execute CPU probe: %s", err)
	}
	if len(res) == 0 {
		t.Fatalf("no items in results")
	}

	if len(res) != 1 {
		t.Fatalf("expected 1 result, got %d", len(res))
	}

	r := res[0]
	percent, ok := r.Fields[CPUResultUsage].(float32)
	if !ok {
		t.Fatalf("invalid CPU usage type, expected float32, got %T", r.Fields[CPUResultUsage])
	}
	if percent < 0.0 || percent > 100.0 {
		t.Fatalf("CPU usage is not in range [0, 100]: %f", percent)
	}
	n, ok := r.Tags[CPUTagCPU]
	if !ok {
		t.Fatalf("missing tag %s", CPUTagCPU)
	}
	if n != "all" {
		t.Fatalf("expected %#v, got %#v", "all", n)
	}
}
