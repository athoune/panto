// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"database/sql"
	"fmt"

	"gitlab.com/pantomath-io/panto/util"

	// Pure Go Postgres driver for database/sql
	_ "github.com/lib/pq"
)

// Postgresql is a probe that request stats from a Postgresql server.
type Postgresql struct {
	conn     *sql.DB
	address  string
	login    string
	password string
	sslmode  string
}

// availableSSLMode is the list of supported SSL modes (see https://www.postgresql.org/docs/10/static/libpq-ssl.html)
var availableSSLMode = []string{"disable", "allow", "prefer", "require", "verify-ca", "verify-full"}

const (
	// Postgresql configuration parameters

	// PostgresqlParamAddress : Postgresql server (host:port) (`string`)
	PostgresqlParamAddress = "address"
	// PostgresqlParamLogin : Postgresql user account (`string`)
	PostgresqlParamLogin = "login"
	// PostgresqlParamPassword : Postgresql user password (`string`)
	PostgresqlParamPassword = "password"
	// PostgresqlParamSSLMode : Postgresql connection SSL Mode (`string`)
	PostgresqlParamSSLMode = "sslmode"

	// Postgresql results

	// PostgresqlResultCurrentConnections : Number of backends currently connected to this database
	PostgresqlResultCurrentConnections = "current_connections"
	// PostgresqlResultMaxConnections : The maximum number of client connections allowed.
	PostgresqlResultMaxConnections = "max_connections"

	// PostgresqlResultSharedBufferHits : Number of times disk blocks were found already in the buffer cache, so that a read was not necessary
	PostgresqlResultSharedBufferHits = "shared_buffer_hits"
	// PostgresqlResultSharedBufferReads : Number of disk blocks read
	PostgresqlResultSharedBufferReads = "shared_buffer_reads"

	// PostgresqlResultTempFilesCount : Number of temporary files created by queries
	PostgresqlResultTempFilesCount = "temp_files_count"
	// PostgresqlResultTempFilesBytes : Total amount of data written to temporary files by queries
	PostgresqlResultTempFilesBytes = "temp_file_bytes"

	// PostgresqlResultRowsReturned : Number of rows returned by queries
	PostgresqlResultRowsReturned = "rows_returned"
	// PostgresqlResultRowsFetched : Number of rows fetched by queries
	PostgresqlResultRowsFetched = "rows_fetched"
	// PostgresqlResultRowsInserted : Number of rows inserted by queries
	PostgresqlResultRowsInserted = "rows_inserted"
	// PostgresqlResultRowsUpdated : Number of rows updated by queries
	PostgresqlResultRowsUpdated = "rows_updated"
	// PostgresqlResultRowsDeleted : Number of rows deleted by queries
	PostgresqlResultRowsDeleted = "rows_deleted"

	// PostgresqlResultDeadlocks : Number of deadlocks detected
	PostgresqlResultDeadlocks = "deadlocks"

	// PostgresqlResultIndexSize : Total size of index on disk
	PostgresqlResultIndexSize = "index_size"
	// PostgresqlResultTableSize : Total size of table on disk
	PostgresqlResultTableSize = "table_size"
	// PostgresqlResultToastSize : Total size of toast on disk
	PostgresqlResultToastSize = "toast_size"

	// PostgresqlResultRowsDead : Estimated number of dead rows
	PostgresqlResultRowsDead = "n_dead_tup"
	// PostgresqlResultRowsLive : Estimated number of live rows
	PostgresqlResultRowsLive = "n_live_tup"

	// PostgresqlResultCheckpointsRequested : Number of requested checkpoints that have been performed
	PostgresqlResultCheckpointsRequested = "checkpoints_requested"
	// PostgresqlResultCheckpointsScheduled : Number of scheduled checkpoints that have been performed
	PostgresqlResultCheckpointsScheduled = "checkpoints_scheduled"

	// PostgresqlResultBuffersBackend : Number of buffers written directly by a backend
	PostgresqlResultBuffersBackend = "buffers_backend"
	// PostgresqlResultBuffersBackground : Number of buffers written by the background writer
	PostgresqlResultBuffersBackground = "buffers_background"
	// PostgresqlResultBuffersCheckpoint : Number of buffers written during checkpoints
	PostgresqlResultBuffersCheckpoint = "buffers_checkpoint"
)

// Name returns the name of the probe "postgresql"
func (db *Postgresql) Name() string {
	return "postgresql"
}

// Configure configures the Postgresql probe and prepares the Postgresql request.
func (db *Postgresql) Configure(config map[string]interface{}) (err error) {
	// Check for mandatory server field
	_, ok := config[PostgresqlParamAddress]
	if !ok {
		return fmt.Errorf("missing \"%s\" field in configuration", PostgresqlParamAddress)
	}
	db.address, ok = config[PostgresqlParamAddress].(string)
	if !ok {
		return fmt.Errorf("\"%s\" field is not string: %v", PostgresqlParamAddress, config[PostgresqlParamAddress])
	}
	// Check for mandatory login field
	_, ok = config[PostgresqlParamLogin]
	if !ok {
		return fmt.Errorf("missing \"%s\" field in configuration", PostgresqlParamLogin)
	}
	db.login, ok = config[PostgresqlParamLogin].(string)
	if !ok {
		return fmt.Errorf("\"%s\" field is not string: %v", PostgresqlParamLogin, config[PostgresqlParamLogin])
	}
	// Check for mandatory password field
	_, ok = config[PostgresqlParamPassword]
	if !ok {
		return fmt.Errorf("missing \"%s\" field in configuration", PostgresqlParamPassword)
	}
	db.password, ok = config[PostgresqlParamPassword].(string)
	if !ok {
		return fmt.Errorf("\"%s\" field is not string: %v", PostgresqlParamPassword, config[PostgresqlParamPassword])
	}
	// Check for optional SSLMode field
	db.sslmode = "disable"
	_, ok = config[PostgresqlParamSSLMode]
	if ok {
		sslmode, ok := config[PostgresqlParamSSLMode].(string)
		if !ok {
			return fmt.Errorf("\"%s\" field is not string: %v", PostgresqlParamSSLMode, config[PostgresqlParamSSLMode])
		}
		if !util.Contains(availableSSLMode, sslmode) {
			return fmt.Errorf("\"%s\" is not a valid SSLMode value", config[PostgresqlParamSSLMode])
		}
		db.sslmode = sslmode
	}

	return nil
}

// Execute sends an Postgresql stats request to the server and synchronously waits for the reply.
func (db *Postgresql) Execute() ([]*Result, error) {
	var err error

	dsn := fmt.Sprintf("postgres://%s:%s@%s?sslmode=%s", db.login, db.password, db.address, db.sslmode)

	db.conn, err = sql.Open("postgres", dsn)
	if err != nil {
		return nil, fmt.Errorf("unable to open connection to PostgreSQL server: %v", err)
	}
	defer db.conn.Close()

	err = db.conn.Ping()
	if err != nil {
		return nil, fmt.Errorf("connection to %s is not working: %v", db.address, err)
	}

	log.Debugf("Requesting PostgreSQL stats: %s", db.address)

	fields := make(map[string]interface{})

	res, err := db.conn.Query(`
		SELECT SUM(numbackends) AS cur_connections,
			SUM(blks_hit) AS shared_buffer_hits,
			SUM(blks_read) AS shared_buffer_reads,
			SUM(temp_files) AS temp_files_count,
			SUM(temp_bytes) AS temp_file_bytes,
			SUM(tup_returned) AS rows_returned,
			SUM(tup_fetched) AS rows_fetched,
			SUM(tup_inserted) AS rows_inserted,
			SUM(tup_updated) AS rows_updated,
			SUM(tup_deleted) AS rows_deleted,
			SUM(deadlocks) AS deadlocks_count
		FROM pg_stat_database;`)
	if err != nil {
		return nil, fmt.Errorf("unable to get database stats from \"%s\": %v", db.address, err)
	}

	err = scanPostgresqlStatsDatabase(fields, res)
	if err != nil {
		return nil, fmt.Errorf("unable to parse output: %v", err)
	}

	res, err = db.conn.Query(`SELECT setting FROM pg_settings WHERE name='max_connections';`)
	if err != nil {
		return nil, fmt.Errorf("unable to get settings stats from \"%s\": %v", db.address, err)
	}

	err = scanPostgresqlStatsSettings(fields, res)
	if err != nil {
		return nil, fmt.Errorf("unable to parse output: %v", err)
	}

	res, err = db.conn.Query(`
		SELECT SUM(index_bytes) as index,
			SUM(toast_bytes) as toast,
			SUM(table_bytes) as table
		  FROM (
		  SELECT *, total_bytes-index_bytes-COALESCE(toast_bytes,0) AS table_bytes FROM (
		      SELECT c.oid,nspname AS table_schema, relname AS TABLE_NAME
		              , c.reltuples AS row_estimate
		              , pg_total_relation_size(c.oid) AS total_bytes
		              , pg_indexes_size(c.oid) AS index_bytes
		              , pg_total_relation_size(reltoastrelid) AS toast_bytes
		          FROM pg_class c
		          LEFT JOIN pg_namespace n ON n.oid = c.relnamespace
		          WHERE relkind = 'r'
		  ) a
		) a;`)
	if err != nil {
		return nil, fmt.Errorf("unable to get size stats from \"%s\": %v", db.address, err)
	}

	err = scanPostgresqlStatsSize(fields, res)
	if err != nil {
		return nil, fmt.Errorf("unable to parse output: %v", err)
	}

	res, err = db.conn.Query(`SELECT SUM(n_dead_tup) AS dead, SUM(n_live_tup) AS live FROM pg_stat_all_tables;`)
	if err != nil {
		return nil, fmt.Errorf("unable to get size stats from \"%s\": %v", db.address, err)
	}

	err = scanPostgresqlStatsRows(fields, res)
	if err != nil {
		return nil, fmt.Errorf("unable to parse output: %v", err)
	}

	res, err = db.conn.Query(`
		SELECT
			SUM(checkpoints_req) AS checkpoints_requested,
			SUM(checkpoints_timed) AS checkpoints_scheduled,
			SUM(buffers_backend) AS buffers_backend,
			SUM(buffers_clean) AS buffers_background,
			SUM(buffers_checkpoint) AS buffers_checkpoint
		FROM pg_stat_bgwriter;`)
	if err != nil {
		return nil, fmt.Errorf("unable to get size stats from \"%s\": %v", db.address, err)
	}

	err = scanPostgresqlStatsWrite(fields, res)
	if err != nil {
		return nil, fmt.Errorf("unable to parse output: %v", err)
	}

	log.Debugf("returning %v", fields)
	return []*Result{{Fields: fields}}, nil
}

func scanPostgresqlStatsDatabase(f map[string]interface{}, r *sql.Rows) (err error) {
	var currentConnections, sharedBufferHits, sharedBufferReads, tempFilesCount, tempFilesBytes, rowsReturned, rowsFetched, rowsInserted, rowsUpdated, rowsDeleted, deadlocks uint
	for r.Next() {
		err := r.Scan(
			&currentConnections,
			&sharedBufferHits,
			&sharedBufferReads,
			&tempFilesCount,
			&tempFilesBytes,
			&rowsReturned,
			&rowsFetched,
			&rowsInserted,
			&rowsUpdated,
			&rowsDeleted,
			&deadlocks,
		)
		if err != nil {
			log.Errorf("unable to scan line: %s", err)
			continue
		}
		f[PostgresqlResultCurrentConnections] = currentConnections
		f[PostgresqlResultSharedBufferHits] = sharedBufferHits
		f[PostgresqlResultSharedBufferReads] = sharedBufferReads
		f[PostgresqlResultTempFilesCount] = tempFilesCount
		f[PostgresqlResultTempFilesBytes] = tempFilesBytes
		f[PostgresqlResultRowsReturned] = rowsReturned
		f[PostgresqlResultRowsFetched] = rowsFetched
		f[PostgresqlResultRowsInserted] = rowsInserted
		f[PostgresqlResultRowsUpdated] = rowsUpdated
		f[PostgresqlResultRowsDeleted] = rowsDeleted
		f[PostgresqlResultDeadlocks] = deadlocks
	}
	err = r.Err()
	if err != nil {
		if err == sql.ErrNoRows {
			return nil
		}
	}

	return
}

func scanPostgresqlStatsSettings(f map[string]interface{}, r *sql.Rows) (err error) {
	var maxConnections uint
	for r.Next() {
		err := r.Scan(
			&maxConnections,
		)
		if err != nil {
			log.Errorf("unable to scan line: %s", err)
			continue
		}
		f[PostgresqlResultMaxConnections] = maxConnections
	}
	err = r.Err()
	if err != nil {
		if err == sql.ErrNoRows {
			return nil
		}
	}

	return
}

func scanPostgresqlStatsSize(f map[string]interface{}, r *sql.Rows) (err error) {
	var indexSize, toastSize, tableSize uint
	for r.Next() {
		err := r.Scan(
			&indexSize,
			&toastSize,
			&tableSize,
		)
		if err != nil {
			log.Errorf("unable to scan line: %s", err)
			continue
		}

		f[PostgresqlResultIndexSize] = indexSize
		f[PostgresqlResultToastSize] = toastSize
		f[PostgresqlResultTableSize] = tableSize
	}
	err = r.Err()
	if err != nil {
		if err == sql.ErrNoRows {
			return nil
		}
	}

	return
}

func scanPostgresqlStatsRows(f map[string]interface{}, r *sql.Rows) (err error) {
	var rowsDead, rowsLive uint
	for r.Next() {
		err := r.Scan(
			&rowsDead,
			&rowsLive,
		)
		if err != nil {
			log.Errorf("unable to scan line: %s", err)
			continue
		}

		f[PostgresqlResultRowsDead] = rowsDead
		f[PostgresqlResultRowsLive] = rowsLive
	}
	err = r.Err()
	if err != nil {
		if err == sql.ErrNoRows {
			return nil
		}
	}

	return
}

func scanPostgresqlStatsWrite(f map[string]interface{}, r *sql.Rows) (err error) {
	var checkpointsRequested, checkpointsScheduled, buffersBackend, buffersBackground, buffersCheckpoint uint
	for r.Next() {
		err := r.Scan(
			&checkpointsRequested,
			&checkpointsScheduled,
			&buffersBackend,
			&buffersBackground,
			&buffersCheckpoint,
		)
		if err != nil {
			log.Errorf("unable to scan line: %s", err)
			continue
		}

		f[PostgresqlResultCheckpointsRequested] = checkpointsRequested
		f[PostgresqlResultCheckpointsScheduled] = checkpointsScheduled
		f[PostgresqlResultBuffersBackend] = buffersBackend
		f[PostgresqlResultBuffersBackground] = buffersBackground
		f[PostgresqlResultBuffersCheckpoint] = buffersCheckpoint
	}
	err = r.Err()
	if err != nil {
		if err == sql.ErrNoRows {
			return nil
		}
	}

	return
}
