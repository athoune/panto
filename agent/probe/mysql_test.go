// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"reflect"
	"testing"

	"github.com/spf13/viper"

	"gitlab.com/pantomath-io/panto/util"
)

// Compile error if MySQL probe do not conform to Probe interface
var _ Probe = (*Mysql)(nil)

func TestMysqlName(t *testing.T) {
	r := Mysql{}
	if r.Name() != "mysql" {
		t.Error("Mysql probe name is not \"mysql\"")
	}
}

func TestMysqlConfigure(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live testing...")
	}

	r := Mysql{}
	err := r.Configure(map[string]interface{}{
		MysqlParamAddress:  viper.GetString("mysql.address"),
		MysqlParamLogin:    viper.GetString("mysql.login"),
		MysqlParamPassword: viper.GetString("mysql.password"),
	})
	if err != nil {
		t.Errorf("couldn't configure Mysql probe: %s", err)
	}
}

func TestMysqlExecute(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live testing...")
	}

	r := Mysql{}

	err := r.Configure(map[string]interface{}{
		MysqlParamAddress:  viper.GetString("mysql.address"),
		MysqlParamLogin:    viper.GetString("mysql.login"),
		MysqlParamPassword: viper.GetString("mysql.password"),
	})
	if err != nil {
		t.Fatalf("couldn't configure Mysql probe: %s", err)
	}
	res, err := r.Execute()
	if err != nil {
		t.Fatalf("couldn't execute Mysql probe: %s", err)
	}
	if len(res) == 0 {
		t.Fatalf("no items in results")
	}
}

func TestGatherMysqlFields(t *testing.T) {
	fields := gatherMysqlFields(testStats)

	t.Logf("Fields: %v", fields)

	expectedKeys := map[string]interface{}{
		MysqlResultAbortedClients:                 reflect.Uint64,
		MysqlResultAbortedConnects:                reflect.Uint64,
		MysqlResultBinlogCacheUse:                 reflect.Uint64,
		MysqlResultBinlogStmtCacheUse:             reflect.Uint64,
		MysqlResultBytesReceived:                  reflect.Uint64,
		MysqlResultBytesSent:                      reflect.Uint64,
		MysqlResultComBegin:                       reflect.Uint64,
		MysqlResultComChangeDb:                    reflect.Uint64,
		MysqlResultComChangeMaster:                reflect.Uint64,
		MysqlResultComCommit:                      reflect.Uint64,
		MysqlResultComCreateDb:                    reflect.Uint64,
		MysqlResultComDelete:                      reflect.Uint64,
		MysqlResultComDeleteMulti:                 reflect.Uint64,
		MysqlResultComInsert:                      reflect.Uint64,
		MysqlResultComRollback:                    reflect.Uint64,
		MysqlResultComSelect:                      reflect.Uint64,
		MysqlResultComStmtExecute:                 reflect.Uint64,
		MysqlResultComStmtFetch:                   reflect.Uint64,
		MysqlResultComTruncate:                    reflect.Uint64,
		MysqlResultComUpdate:                      reflect.Uint64,
		MysqlResultConnectionErrorsAccept:         reflect.Uint64,
		MysqlResultConnectionErrorsInternal:       reflect.Uint64,
		MysqlResultConnectionErrorsMaxConnections: reflect.Uint64,
		MysqlResultConnectionErrorsPeerAddress:    reflect.Uint64,
		MysqlResultConnectionErrorsSelect:         reflect.Uint64,
		MysqlResultConnectionErrorsTcpwrap:        reflect.Uint64,
		MysqlResultConnections:                    reflect.Uint64,
		MysqlResultCreatedTmpDiskTables:           reflect.Uint64,
		MysqlResultCreatedTmpTables:               reflect.Uint64,
		MysqlResultFlushCommands:                  reflect.Uint64,
		MysqlResultHandlerReadFirst:               reflect.Uint64,
		MysqlResultHandlerReadKey:                 reflect.Uint64,
		MysqlResultHandlerReadLast:                reflect.Uint64,
		MysqlResultHandlerReadNext:                reflect.Uint64,
		MysqlResultHandlerReadPrev:                reflect.Uint64,
		MysqlResultHandlerReadRnd:                 reflect.Uint64,
		MysqlResultHandlerReadRndNext:             reflect.Uint64,
		MysqlResultInnodbBufferPoolPagesData:      reflect.Uint64,
		MysqlResultInnodbBufferPoolPagesDirty:     reflect.Uint64,
		MysqlResultInnodbBufferPoolPagesFlushed:   reflect.Uint64,
		MysqlResultInnodbBufferPoolPagesFree:      reflect.Uint64,
		MysqlResultInnodbBufferPoolPagesMisc:      reflect.Uint64,
		MysqlResultInnodbDataFsyncs:               reflect.Uint64,
		MysqlResultInnodbDataReads:                reflect.Uint64,
		MysqlResultInnodbDataWrites:               reflect.Uint64,
		MysqlResultInnodbLogWaits:                 reflect.Uint64,
		MysqlResultInnodbLogWrites:                reflect.Uint64,
		MysqlResultInnodbPageSize:                 reflect.Uint64,
		MysqlResultInnodbPagesRead:                reflect.Uint64,
		MysqlResultInnodbPagesWritten:             reflect.Uint64,
		MysqlResultInnodbRowLockTimeMax:           reflect.Uint64,
		MysqlResultInnodbRowLockWaits:             reflect.Uint64,
		MysqlResultKeyBlocksUnused:                reflect.Uint64,
		MysqlResultKeyBlocksUsed:                  reflect.Uint64,
		MysqlResultKeyReads:                       reflect.Uint64,
		MysqlResultKeyWrites:                      reflect.Uint64,
		MysqlResultLockedConnects:                 reflect.Uint64,
		MysqlResultOpenFiles:                      reflect.Uint64,
		MysqlResultOpenStreams:                    reflect.Uint64,
		MysqlResultOpenTables:                     reflect.Uint64,
		MysqlResultPreparedStmtCount:              reflect.Uint64,
		MysqlResultQueries:                        reflect.Uint64,
		MysqlResultSelectFullJoin:                 reflect.Uint64,
		MysqlResultSelectFullRangeJoin:            reflect.Uint64,
		MysqlResultSlowQueries:                    reflect.Uint64,
		MysqlResultUptime:                         reflect.Uint64,
	}
	for k := range expectedKeys {
		if _, ok := fields[k]; !ok {
			t.Errorf("missing key \"%s\" in mysql results", k)
		}
	}
	for k, field := range fields {
		kind, ok := expectedKeys[k]
		if !ok {
			t.Errorf("unexpected key \"%s\" in mysql results", k)
			continue
		}
		if kind != reflect.TypeOf(field).Kind() {
			t.Errorf("invalid type for field %s, expected %s, got %s", k, kind, reflect.TypeOf(field).Kind())
			continue
		}
	}
}

// A mock of a mysql SHOW STATUS command
var testStats = map[string]string{
	"aborted_clients":                   "42",
	"aborted_connects":                  "42",
	"binlog_cache_use":                  "42",
	"binlog_stmt_cache_use":             "42",
	"bytes_received":                    "42",
	"bytes_sent":                        "42",
	"com_begin":                         "42",
	"com_change_db":                     "42",
	"com_change_master":                 "42",
	"com_commit":                        "42",
	"com_create_db":                     "42",
	"com_delete":                        "42",
	"com_delete_multi":                  "42",
	"com_insert":                        "42",
	"com_rollback":                      "42",
	"com_select":                        "42",
	"com_stmt_execute":                  "42",
	"com_stmt_fetch":                    "42",
	"com_truncate":                      "42",
	"com_update":                        "42",
	"connection_errors_accept":          "42",
	"connection_errors_internal":        "42",
	"connection_errors_max_connections": "42",
	"connection_errors_peer_address":    "42",
	"connection_errors_select":          "42",
	"connection_errors_tcpwrap":         "42",
	"connections":                       "42",
	"created_tmp_disk_tables":           "42",
	"created_tmp_tables":                "42",
	"flush_commands":                    "42",
	"handler_read_first":                "42",
	"handler_read_key":                  "42",
	"handler_read_last":                 "42",
	"handler_read_next":                 "42",
	"handler_read_prev":                 "42",
	"handler_read_rnd":                  "42",
	"handler_read_rnd_next":             "42",
	"innodb_buffer_pool_pages_data":     "42",
	"innodb_buffer_pool_pages_dirty":    "42",
	"innodb_buffer_pool_pages_flushed":  "42",
	"innodb_buffer_pool_pages_free":     "42",
	"innodb_buffer_pool_pages_misc":     "42",
	"innodb_data_fsyncs":                "42",
	"innodb_data_reads":                 "42",
	"innodb_data_writes":                "42",
	"innodb_log_waits":                  "42",
	"innodb_log_writes":                 "42",
	"innodb_page_size":                  "42",
	"innodb_pages_read":                 "42",
	"innodb_pages_written":              "42",
	"innodb_row_lock_time_max":          "42",
	"innodb_row_lock_waits":             "42",
	"key_blocks_unused":                 "42",
	"key_blocks_used":                   "42",
	"key_reads":                         "42",
	"key_writes":                        "42",
	"locked_connects":                   "42",
	"open_files":                        "42",
	"open_streams":                      "42",
	"open_tables":                       "42",
	"prepared_stmt_count":               "42",
	"queries":                           "42",
	"select_full_join":                  "42",
	"select_full_range_join":            "42",
	"slow_queries":                      "42",
	"uptime":                            "42",
}
