// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"fmt"
	"time"

	"github.com/loderunner/go-ping"
)

// Ping is a probe that sends an ICMP Echo message
// to a host and reports statistics on the round trip.
type Ping struct {
	pinger *ping.Pinger
}

const (
	// Ping configuration parameters

	// PingParamAddress : (mandatory) target address, IP or hostname (`string`)
	PingParamAddress = "address"
	// PingParamInterval : (optional) the wait time between each packet send, default 1s (`time.Duration`)
	PingParamInterval = "interval"
	// PingParamCount : (optional) the number of ICMP packets to send, default 4 (`int`)
	PingParamCount = "count"
	// PingParamTimeout : (optional) the time to run the ping, until it exits. If the timeout is reached before the
	// packet count has been reached, exits anyway. Defaults to 10s (`time.Duration`)
	PingParamTimeout = "timeout"

	// Ping results

	// PingResultSent : the number of packets sent (`int`)
	PingResultSent = "sent"
	// PingResultRecv : the number of packets received (`int`)
	PingResultRecv = "received"
	// PingResultMin : the minimum round trip time, in nanoseconds (`int64`)
	PingResultMin = "min"
	// PingResultMax : the maximum round trip time, in nanoseconds (`int64`)
	PingResultMax = "max"
	// PingResultAvg : the average round trip time, in nanoseconds (`int64`)
	PingResultAvg = "avg"
	// PingResultStdDev : the standard deviation of round trip times, in nanoseconds (`int64`)
	PingResultStdDev = "stddev"

	// Ping tags

	// PingTagAddress : the address of the target that was pinged
	PingTagAddress = "address"
)

// Name returns the name of the probe "ping"
func (p *Ping) Name() string {
	return "ping"
}

// Configure configures the Ping probe and instantiates underlying
// structures.
func (p *Ping) Configure(config map[string]interface{}) (err error) {
	var ok bool
	if _, ok = config[PingParamAddress]; !ok {
		return fmt.Errorf("Missing \"%s\" field", ProcessParamNames)
	}
	var addr string
	if addr, ok = config[PingParamAddress].(string); !ok {
		return fmt.Errorf("\"%s\" field is not string: %v", PingParamAddress, config[PingParamAddress])
	}

	p.pinger, err = ping.NewPinger(addr)
	if err != nil {
		return err
	}

	if paramInterval, ok := config[PingParamInterval]; ok {
		if p.pinger.Interval, ok = paramInterval.(time.Duration); !ok {
			return fmt.Errorf("\"%s\" field is not time.Duration: %v", PingParamInterval, config[PingParamInterval])
		}
	}

	if paramCount, ok := config[PingParamCount]; ok {
		if p.pinger.Count, ok = paramCount.(int); !ok {
			return fmt.Errorf("\"%s\" field is not int: %v", PingParamCount, config[PingParamCount])
		}
	} else {
		p.pinger.Count = 4
	}

	if paramTimeout, ok := config[PingParamTimeout]; ok {
		if p.pinger.Timeout, ok = paramTimeout.(time.Duration); !ok {
			return fmt.Errorf("\"%s\" field is not time.Duration: %v", PingParamTimeout, config[PingParamTimeout])
		}
	} else {
		p.pinger.Timeout = 10 * time.Second
	}

	return nil
}

// Execute sends ICMP echo packets until either the count or the timeout is reached.
func (p *Ping) Execute() ([]*Result, error) {
	p.pinger.Run()

	stats := p.pinger.Statistics()

	res := []*Result{
		{
			Fields: map[string]interface{}{
				PingResultSent:   stats.PacketsSent,
				PingResultRecv:   stats.PacketsRecv,
				PingResultMin:    stats.MinRtt.Nanoseconds(),
				PingResultMax:    stats.MaxRtt.Nanoseconds(),
				PingResultAvg:    stats.AvgRtt.Nanoseconds(),
				PingResultStdDev: stats.StdDevRtt.Nanoseconds(),
			},
			Tags: map[string]string{
				PingTagAddress: p.pinger.Addr(),
			},
		},
	}

	return res, nil
}
