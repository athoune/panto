// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package agent

import (
	"context"
	"testing"
	"time"

	"gitlab.com/pantomath-io/panto/api"
)

func TestDefaultContext(t *testing.T) {
	// Test timeout sanity
	ctx, _ := defaultContext(ClientConfiguration{Timeout: 24 * time.Hour})
	deadline, ok := ctx.Deadline()
	if !ok {
		t.Error("New context has no deadline.", ctx)
	}
	if deadline.After(time.Now().Add(24 * time.Hour)) {
		t.Error("Deadline is too late.", ctx)
	}
	select {
	case <-ctx.Done():
		// This is a non-deterministic test. If for some reason
		// the tests take more than 24 hours, this will fail while
		// still retaining sanity...
		t.Error("Context has timed out too early.", ctx)
	default:
	}

	// Test actual timeout
	ctx, _ = defaultContext(ClientConfiguration{Timeout: time.Second})
	time.Sleep(3 * time.Second)
	deadline, _ = ctx.Deadline()
	if deadline.After(time.Now()) {
		t.Error("Deadline is too late.", ctx)
	}
	select {
	case <-ctx.Done():
		if ctx.Err() != context.DeadlineExceeded {
			t.Error("Error was not \"Deadline Exceeded\"", ctx.Err())
		}
	default:
		t.Error("Context has not timed out.", ctx)
	}

	// Test cancellation
	ctx, cancel := defaultContext(ClientConfiguration{Timeout: 24 * time.Hour})
	cancel()
	select {
	case <-ctx.Done():
		if ctx.Err() != context.Canceled {
			t.Error("Error was not \"Canceled\"", ctx.Err())
		}
	default:
		t.Error("Context has not been cancelled.", ctx)
	}
}

func TestLocalConfiguration(t *testing.T) {
	dumpPath := "/tmp/panto.test"
	refVersion := "42"
	refConf := &api.ListProbeConfigurationsResponse{
		ProbeConfigurations: []*api.ProbeConfiguration{
			{
				Name:          "organizations/5M_-Upw6Q1mNKlQNhOiojg/agents/noLlUczuSJqkyIEynnCoow/probeconfigurations/_KjCCng_T3GWxe-_lqH3PQ",
				Check:         "organizations/5M_-Upw6Q1mNKlQNhOiojg/checks/i2QYtZBVSnuXjLhQhuAV6w",
				Configuration: "{\"address\":\"localhost\"}",
				ProbeLabel:    "elasticsearch",
				Target: &api.Target{
					Name:    "organizations/5M_-Upw6Q1mNKlQNhOiojg/targets/MQF0eIJcRZyEBgG_sdLPWA",
					Address: "localhost",
				},
			},
		},
	}

	if err := localConfigurationStore(refConf, refVersion, dumpPath); err != nil {
		t.Fatalf("unable to dump configuration to file: %s", err)
	}

	gotConf, gotVersion, err := localConfigurationLoad(dumpPath)
	if err != nil {
		t.Fatalf("unable to read configuration from file: %s", err)
	} else {
		if gotVersion != refVersion {
			t.Fatalf("wrong version number: expect %s, got %s", refVersion, gotVersion)
		}
		if len(gotConf.ProbeConfigurations) != len(refConf.ProbeConfigurations) {
			t.Fatalf("wrong number of configuration retrieved: expect %d, got %d", len(gotConf.ProbeConfigurations), len(refConf.ProbeConfigurations))
		}
		for i, got := range gotConf.ProbeConfigurations {
			expect := refConf.ProbeConfigurations[i]
			if got.Name != expect.Name {
				t.Fatalf("Unexpected configuration name: expect %s, got %s", expect.Name, got.Name)
			}
			if got.Check != expect.Check {
				t.Fatalf("Unexpected configuration check: expect %s, got %s", expect.Check, got.Check)
			}
			if got.Configuration != expect.Configuration {
				t.Fatalf("Unexpected configuration config: expect %s, got %s", expect.Configuration, got.Configuration)
			}
			if got.ProbeLabel != expect.ProbeLabel {
				t.Fatalf("Unexpected configuration probe_label: expect %s, got %s", expect.ProbeLabel, got.ProbeLabel)
			}
			if got.Target.Name != expect.Target.Name {
				t.Fatalf("Unexpected configuration target name: expect %s, got %s", expect.Target.Name, got.Target.Name)
			}
			if got.Target.Address != expect.Target.Address {
				t.Fatalf("Unexpected configuration target name: expect %s, got %s", expect.Target.Address, got.Target.Address)
			}
		}
	}
}
