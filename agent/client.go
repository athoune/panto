// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package agent

import (
	"context"
	"crypto/x509"
	"fmt"
	"io/ioutil"
	"strings"
	"time"

	"github.com/spf13/viper"

	"gitlab.com/pantomath-io/panto/agent/probe"
	"gitlab.com/pantomath-io/panto/api"
	"gitlab.com/pantomath-io/wrapany"

	"github.com/golang/protobuf/proto"
	"github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/any"
	"github.com/op/go-logging"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/metadata"
)

// Create a new instance of the logger
var log = logging.MustGetLogger("main")

// ClientConfiguration contains all configuration parameters to instantiate
// and connect a `Client`.
type ClientConfiguration struct {
	Address           string
	AgentName         string
	CertFilePath      string
	NoTLS             bool
	Timeout           time.Duration
	ConfigurationDump string
}

// Client is the main structure for a Panto agent to connect to a
// Panto server and perform requests. It embeds a gRPC client for
// the Panto service, as defined in the proto files.
type Client struct {
	api.PantoClient
	conf      ClientConfiguration
	confDump  string
	conn      *grpc.ClientConn
	AgentName string
}

// defaultContext is a helper to create the default `context.Context` for all
// the `Client`'s request, according to the configuration. It also returns a
// `context.CancelFunc` that can be used to cancel the request.
func defaultContext(conf ClientConfiguration) (context.Context, context.CancelFunc) {
	return context.WithTimeout(context.Background(), conf.Timeout)
}

// GetRequestMetadata creates the gRPC request metadata from the client configuration.
func (c *ClientConfiguration) GetRequestMetadata(context.Context, ...string) (map[string]string, error) {
	return map[string]string{
		"agent":         c.AgentName,
		"agent-version": viper.GetString("agent-version"),
	}, nil
}

// RequireTransportSecurity indicates whether the credentials requires transport security
func (c *ClientConfiguration) RequireTransportSecurity() bool {
	return !c.NoTLS
}

// NewClient creates a gRPC client according to configuration and connects it to the
// server.
func NewClient(conf ClientConfiguration) (*Client, error) {
	var err error
	var conn *grpc.ClientConn
	var secOpt grpc.DialOption

	if !conf.NoTLS {
		// Create the client TLS credentials
		var creds credentials.TransportCredentials

		if conf.CertFilePath != "" {
			// create the credentials from the Certificate file provided — useful for self-signed certs
			creds, err = credentials.NewClientTLSFromFile(conf.CertFilePath, "")
			if err != nil {
				log.Errorf("could not load tls cert: %s", err)
				return nil, err
			}
		} else {
			// create the credentials from the system cert pool — useful for valid certs
			pool, err := x509.SystemCertPool()
			if err != nil {
				log.Errorf("could not load system cert pool: %s", err)
			}
			creds = credentials.NewClientTLSFromCert(pool, "")
		}

		// Set up a connection to the server with both credentials
		secOpt = grpc.WithTransportCredentials(creds)
	} else {
		secOpt = grpc.WithInsecure()
	}

	ctx, cancel := defaultContext(conf)
	defer cancel()

	conn, err = grpc.DialContext(ctx, conf.Address, secOpt, grpc.WithPerRPCCredentials(&conf))
	if err != nil {
		log.Errorf("did not connect: %s", err)
		return nil, err
	}

	client := Client{
		PantoClient: api.NewPantoClient(conn),
		conf:        conf,
		conn:        conn,
		AgentName:   conf.AgentName,
		confDump:    conf.ConfigurationDump,
	}
	return &client, nil
}

// Close disconnect the gRPC client from the server
func (c *Client) Close() {
	c.conn.Close()
}

// ReportResults sends the result of a probe to the server
// it returns the configuration version and a (possible) error.
func (c *Client) ReportResults(results []*probe.Result) (string, error) {
	var err error
	var header metadata.MD
	var agentVersion string

	req := api.AddResultsRequest{
		Results: make([]*api.Result, len(results)),
	}

	for i, result := range results {
		ts, err := ptypes.TimestampProto(result.Timestamp)
		if err != nil {
			log.Errorf("invalid timestamp: %s", err)
			continue
		}

		if result.Error {
			req.Results[i] = &api.Result{
				ProbeConfiguration: result.ProbeConfiguration,
				Check:              result.Check,
				Agent:              c.AgentName,
				Timestamp:          ts,
				Error:              true,
				ErrorMessage:       result.ErrorMessage,
			}
		} else {
			tags := result.Tags
			if tags == nil {
				tags = map[string]string{}
			}

			fields := make(map[string]*any.Any)

			for k, v := range result.Fields {
				fields[k], err = wrapany.Wrap(v)
				if err != nil {
					return "", fmt.Errorf("unable to convert %T value for field %s to any.Any", v, k)
				}
			}

			req.Results[i] = &api.Result{
				ProbeConfiguration: result.ProbeConfiguration,
				Check:              result.Check,
				Agent:              c.AgentName,
				Timestamp:          ts,
				Fields:             fields,
				Tags:               tags,
			}
		}
	}

	ctx, cancel := defaultContext(c.conf)
	defer cancel()

	_, err = c.AddResults(
		ctx,
		&req,
		grpc.Header(&header),
	)
	if err != nil {
		return "", err
	}

	agentVersion, _ = extractVersion(header)

	return agentVersion, nil
}

// RequestConfiguration sends a request for the agent's configuration to
// the server
func (c *Client) RequestConfiguration() (res *api.ListProbeConfigurationsResponse, agentVersion string, err error) {
	var header metadata.MD

	ctx, cancel := defaultContext(c.conf)
	defer cancel()

	res, err = c.ListProbeConfigurations(
		ctx,
		// TODO: PageSize and PageToken are part of the protocol and should be used
		// 		 to loop over "pages" of results
		&api.ListProbeConfigurationsRequest{
			Parent:    c.AgentName,
			PageSize:  0,
			PageToken: "",
		},
		grpc.Header(&header),
	)
	if err != nil {
		log.Errorf("could not receive configuration: %s", err)
		res, agentVersion, err = localConfigurationLoad(c.confDump)
		if err != nil {
			log.Criticalf("could not load configuration from dump: %s", err)
			return nil, "", err
		}
	} else {
		agentVersion, err = extractVersion(header)
		if err != nil {
			log.Errorf("%s", err)
		}
	}

	log.Infof("Received configuration version %s with %d Probe Configuration(s)", agentVersion, len(res.ProbeConfigurations))

	if err = localConfigurationStore(res, agentVersion, c.confDump); err != nil {
		log.Errorf("%s", err)
	} else {
		log.Debugf("Configuration dumped on disk: %s", c.confDump)
	}

	return res, agentVersion, nil
}

// extractVersion returns the version out of the metadata sent from the server
func extractVersion(md metadata.MD) (string, error) {
	if _, ok := md["agent-configuration-version"]; !ok {
		return "", fmt.Errorf("Missing server header for agent configuration version")
	}

	return strings.Join(md["agent-configuration-version"], ""), nil
}

// localConfigurationStore dumps the conf (protobuf message) locally on the disk
func localConfigurationStore(conf *api.ListProbeConfigurationsResponse, version, path string) (err error) {
	// Check we have a path to dump to
	if path == "" {
		return fmt.Errorf("Path for the dump file is not set (see configuration-dump setting)")
	}
	// Create to proto message
	dump := &DumpedConfiguration{
		Version:        version,
		Configurations: conf,
	}

	// Write the message to disk.
	out, err := proto.Marshal(dump)
	if err != nil {
		return fmt.Errorf("unable to marshal conf while dumping: %s", err)
	}

	if err := ioutil.WriteFile(path, out, 0644); err != nil {
		return fmt.Errorf("unable to write configuration to disk: %s", err)
	}

	return
}

// localConfigurationLoad loads the configuration from local dump file
func localConfigurationLoad(path string) (*api.ListProbeConfigurationsResponse, string, error) {
	log.Infof("trying to load configuration from dump: %s", path)
	// Check we have a path to load from
	if path == "" {
		return nil, "", fmt.Errorf("Path for the dump file is not set (see configuration-dump setting)")
	}

	in, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, "", fmt.Errorf("unable to read dump file: %s", err)
	}

	dump := &DumpedConfiguration{}

	if err = proto.Unmarshal(in, dump); err != nil {
		return nil, "", fmt.Errorf("unable to parse dump file for configurations: %s", err)
	}

	return dump.Configurations, dump.Version, nil
}
