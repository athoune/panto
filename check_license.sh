#!/bin/bash
#
# Copyright 2017 Pantomath SAS
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Generate the NOTICE file by scanning the repository for dependencies

readonly PROGNAME=$(basename "$0")
readonly PROGDIR=$(dirname "$0")

readonly LICENSE_CHECKER_GO="$( which wwhrd )"
readonly LICENSE_CHECKER_JS="$( which license-checker )"
readonly ROOT_PATHS_GO="./..."
readonly ROOT_PATHS_JS="web"

readonly NOTICE_FILE="$( mktemp )"
readonly NOTICE_FILE_HEADER="Pantomath panto, panto-agent, panto-web, panto-ctl
Copyright 2017-2018 Pantomath SAS

This product includes software developed at
Pantomath SAS (https://pantomath.io).


The following components are included in this product:

"

main() {
  
  echo "${NOTICE_FILE_HEADER}" > "${NOTICE_FILE}"

  cd "${PROGDIR}"
  # Get licenses for go code and format ouput
  go mod vendor
  wwhrd ls 2>&1 \
    | cut -c 67- \
    | sed -E $'s/\x1b\[[0-9]+m//g' \
    | awk -F '[ =]' '{print "https://"$4"\nLicensed under "$2"\n"}' \
    >> "${NOTICE_FILE}"
  rm -rf vendor


  cd "${ROOT_PATHS_JS}"
  # Get licenses for js code and format output
  "${LICENSE_CHECKER_JS}" --production --csv \
    | sed -n '1!p' \
    | grep -v "^$" \
    | grep -v "panto-web" \
    | tr '@' ' ' \
    | awk -F "," '{print $1"\n"$3"\nLicensed under "$2"\n"}' \
    | tr -d '"' \
    >> "${NOTICE_FILE}"
  cd - > /dev/null

  cat "${NOTICE_FILE}"
  rm -f "${NOTICE_FILE}"
}

main "$@"
