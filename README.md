# Panto

__Develop__ [![build status](https://gitlab.com/pantomath-io/panto/badges/develop/build.svg)](https://gitlab.com/pantomath-io/panto/commits/develop) [![coverage report](https://gitlab.com/pantomath-io/panto/badges/develop/coverage.svg)](https://gitlab.com/pantomath-io/panto/commits/develop)

__Master__ [![build status](https://gitlab.com/pantomath-io/panto/badges/master/build.svg)](https://gitlab.com/pantomath-io/panto/commits/master) [![coverage report](https://gitlab.com/pantomath-io/panto/badges/master/coverage.svg)](https://gitlab.com/pantomath-io/panto/commits/master)

[Panto](https://panto.app) is a platform for monitoring your infrastructure, servers and applications in production. Agents collect metrics about your systems in real time, and report their health and performance to the server. The server stores metrics, evaluates the state according to user-defined rules, and, in case the state is considered dysfunctional, dispatches alerts to your teams.

## Downloading Panto

Pre-build panto binaries (linux/x86_64 only) can be downloaded directly from the [downloads page](https://packages.panto.app/bin/stable).

## Building Panto from source

After installing the [prerequisites](docs/BUILD.md#prerequisites), clone the repository and run `make` from the repository root. The built executables will be in the `bin` directory. The built web client will be in the `web/dist` directory.

``shell
git clone https://gitlab.com/pantomath-io/panto
cd panto
make
```

Detailed build instructions can be found [here](docs/BUILD.md).

## Running Panto

### Server

The Panto server can be run by creating a configuration file ([example](conf/panto.yaml)) in `/etc/panto/panto.yaml` and running the executable:

```shell
panto
```

### Web client

The web client is a static site served with [Caddy](https://caddyserver.com). After building Panto ([see below](#building-panto)), `cd` to the `web` directory and run the server:

```shell
cd web
caddy
```

### Agent

The Panto agent should be run from the targets you want to monitor. Download the agent binary from the [downloads page](https://packages.panto.app/bin), create a configuration file ([example](conf/panto-agent.yaml)), and run:

```shell
panto-agent
```

Detailed configuration file and command-line options can be found [here](docs/site-docs/004-Running-Panto.md)

### Docker

Docker images are available for the panto agent and server. See the Panto [registry](https://gitlab.com/pantomath-io/panto/container_registry). Detailed documentation [here](docs/RUNNING.md#docker).


## Documentation

See the [official documentation site](https://about.panto.app).

## License

Copyright 2017-18 Pantomath SAS

The Panto source code is licensed under the [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0).

By contributing to Pantomath SAS, You accept and agree to the following terms and conditions for Your present and future Contributions submitted to Pantomath SAS. Except for the license granted herein to Pantomath SAS and recipients of software distributed by Pantomath SAS, You reserve all right, title, and interest in and to Your Contributions. All Contributions are subject to the following [Developer Certificate of Origin](DCO) and [License](LICENSE) terms.

All Documentation content that resides under the [`docs` directory](docs) of this repository is licensed under Creative Commons: [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).
