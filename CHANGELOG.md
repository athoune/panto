# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## Unreleased


## 0.4 - Richard-Lenoir - 2018-10-03

### Added
- Support stacked-lines charts in the Dashboard (#400)
- The Panto server now monitors data presence from agents. If an agent stops sending data, a `missing_data` State will be set, helping identify issues in the monitoring. (#105)
- Agent list sidebar shows a spinning icon for Agents that aren't live
- Agent list sidebar shows a warning icon for Agents that require an update. Detailed Agent version info is available in the Agent settings dialog. (#413)
- Add an activity spinner to Save button in Probe Configuration screen
- Add tooltips to describe State and Alert types (#298)

### Changed
- The time range control in the Dashboard page now supports arbitrary ranges, or shortcuts for common ranges (#401)
- Filter out "exotic" filesystem types in disk Probe (#421)
- Hide Checks without a linked ProbeConfiguration in Alert creation module (#416)
- Change interpolation algorithm in graph to avoid backward curves (#402)
- Enhance the graph density (#423)
- Enhance graph readability with thiner lines and points (#398)
- Disable Save button in Probe Configuration screen when all changes are saved
- Split tagged datasets into multiple graphs (#415)
- Display graph figures in their human readable format (#399)
- TLS probe reports duration-based result in nanoseconds, and not hours (!385)

### Fixed
- Fix copy-to-clipboard button, (e.g. in the Agent dialog) (!358)
- Fix issue that prevented the HTTP probe from appearing in the probe list (#425)
- Fix API errors when page_size was 0 and page_offset was not equal to 0 (#333)
- Graph links in the Pulse screen now navigate to the expected Target and scroll the expected graph into view (#354)
- Fix bug where it was impossible to save Probe Configuration and States changes, when State conditions were empty (#431 #432)

### Removed
- Remove Check API name from the Dashboard page (#420)
- Remove tooltips on Agent name in the Environment sidebar (#406)


## 0.3 - Stalingrad - 2018-09-14

### Added
- Document proxy with nginx setup (#385)
- Display placeholders and defaults for probe configuration parameters (#364)
- Document initd integration (#387)
- Add large Create Alert button when Alert list is empty (#391)
- Probe for MongoDB (#48)
- Add errors from Probes in QueryResults API responses (#404)
- Graph lines are discontinuous in case the Probe encountered an error (#404)
- Documentation for developers on goose use (#150)
- Display Probe Configuration validation errors in the UI (#383)
- Added Probe name to Probe Configuration screen (#387)
- Probe for PostgreSQL (#52)

### Changed
- Database migrations are directly packaged in the binary, no longer unpacked to the filesystem (#390)
- Check's `state_type` and `state_configuration` are deprecated, replaced by `type` and `configuration` (#389)
- Verbosity is no longer set in Docker command line (#407)

### Removed
- Remove all occurences of "Probe State" in the project (#378)

### Fixed
- Don't send `null` from the web client when configuring a Probe Configuration with empty fields (#384)
- Fixed a possible concurrent access crash in the TSDB layer (!338)
- Prevent cases where the Create/Update Alert dialog would only show the title bar (#391) 
- Probe Configurations with invalid (empty) format are now properly empty (!341)
- Improve "unsaved changes" detection in the Probe Configuration screen: no more infinite alerts (#392)
- The "loading spinners" on the graphs no longer appear over the navigation bar (#357)
- Fixed a CORS incompatibility that prevents PUT calls (!357)
- Fixed crash when sending `null` fields in the REST API (#426)
- Refreshing the Alerts page no longer triggers an error (#412)


## 0.2 - Strasbourg Saint-Denis - 2018-09-02

### Added
- Autoselection of the first Target when opening Dashboard
- Autoselection of the first Agent when opening Environment
- Show Panto version in tooltip when hovering over the Panto logo in the top left
- In Create Alert dialog, it is now possible to see details about the Check by hovering over an icon in the dropdown list
- Configure the sender email adress for notifications
- Errors from the backend are notified to the user in a popdown alert
- Get an alert when leaving the Probe Configuration screen could result in unsaved changes (#331)
- Graph shows an error if data could not be loaded (#359)
- Added last activity and version to Create/Update Agent dialog (#374)
- Support `~` home directory expansion in paths (#294)
- Dashboard URLs now include time range, allowing for better refresh UX and sharing of URLs (#371)

### Changed
- Better template for email notification
- Improvements on Check and Probe Configuration screen (#331)
- HTTP Alert Channel now sends a JSON payload (#206)
- Move default path for agent configuration file (#264)
- Better (sticky) table of content in documentation - see https://about.panto.app (#328)
- More logs in the agent by default (log level 'Info') (#368)
- Rewrite all API error messages (#249)
- Null values in JSON configurations (e.g. Alerts, ProbeConfigurations) ignore `null` values as `undefined` (#384)

### Removed
- HTTP Alert Channel no longer takes a `body_tmpl` configuration parameter (#206)

### Fixed
- Several cosmetic changes in the UI
- Graph loading ends if no data available, rather than waiting forever
- Better validation of `state_configuration` format for `state_type=none` (#380)
- Fixed an issue where only the title of the Create/Update Alert dialog appeared (#381)
- Fix time tooltips in Pulse screen (#353)
- Empty graphs: Don't display graphs for Checks not associated to a Probe Configuration (#386)
- Navigate to the Agent view after deleting a Probe Configuration (#388)
- Check server.address configuration format before starting the Agent (#382)


## 0.1 - Barbès-Rochechouart - 2018-08-17

Initial Release

### Added
- (optional) Google Analytics in panto-web

### Fixed
- Remove disabled button from the top nav
- Fix an issue where the list of checks could appear empty in the alert page
- Fix 'Help' link in the Probe Configuration dialog
- Remove empty row in dashboard when no graphs are associated to check
- Fix template for site documentation (links and favicon)
- Remove useless +/... buttons in Dashboard graphs
- Warning or critical state configurations with no conditions no longer evaluate automatically as true