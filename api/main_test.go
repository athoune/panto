// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package api

import (
	"math/rand"
	"net/url"
	"testing"

	"github.com/google/uuid"

	"gitlab.com/pantomath-io/panto/util"
)

const numberOfNames = 10000

func TestParseNameOrganization(t *testing.T) {
	var names [numberOfNames]string
	var uuids [numberOfNames]uuid.UUID
	for i := 0; i < numberOfNames; i++ {
		uuids[i] = uuid.New()
		names[i] = ResourceNameOrganization + "/" + util.Base64Encode(uuids[i])
	}
	for i, name := range names {
		o, err := ParseNameOrganization(name)
		if err != nil {
			t.Fatalf("failed to parse %s: %s", name, err)
		}
		if o != uuids[i] {
			t.Fatalf("expected Organization ID %s, got %s", uuids[i], o)
		}
	}

	errorNames := []string{
		"",
		"asdgfsadfgdsf",
		"organizations",
		"organizations/",
		"organizations/X63L5CfxRt2kWhP7aKyzM",
		"organizations/X63L5CfxRt2kWhP7aKyzMQd",
		"organizations/X63L5CfxRt2kWhP*aKyzMQ",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ ",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/agents",
		// "organizations/X63L5CfxRt2kWhP7aKyzMQ",
	}
	for _, name := range errorNames {
		_, err := ParseNameOrganization(name)
		if err == nil {
			t.Fatalf("expected error when parsing: %s", name)
		}
	}
}

func TestParseNameProbe(t *testing.T) {
	var names [numberOfNames]string
	var uuids [numberOfNames]uuid.UUID
	for i := 0; i < numberOfNames; i++ {
		uuids[i] = uuid.New()
		names[i] = ResourceNameProbe + "/" + util.Base64Encode(uuids[i])
	}
	for i, name := range names {
		p, err := ParseNameProbe(name)
		if err != nil {
			t.Fatalf("failed to parse %s: %s", name, err)
		}
		if p != uuids[i] {
			t.Fatalf("expected Probe ID %s, got %s", uuids[i], p)
		}
	}

	errorNames := []string{
		"",
		"asdgfsadfgdsf",
		"probes",
		"probes/",
		"probes/X63L5CfxRt2kWhP7aKyzM",
		"probes/X63L5CfxRt2kWhP7aKyzMQd",
		"probes/X63L5CfxRt2kWhP*aKyzMQ",
		"probes/X63L5CfxRt2kWhP7aKyzMQ ",
		"probes/X63L5CfxRt2kWhP7aKyzMQ/",
		"probes/X63L5CfxRt2kWhP7aKyzMQ/agents",
		// "probes/X63L5CfxRt2kWhP7aKyzMQ",
	}
	for _, name := range errorNames {
		_, err := ParseNameProbe(name)
		if err == nil {
			t.Fatalf("expected error when parsing: %s", name)
		}
	}
}

func TestParseNameChannelType(t *testing.T) {
	var names [numberOfNames]string
	var uuids [numberOfNames]uuid.UUID
	for i := 0; i < numberOfNames; i++ {
		uuids[i] = uuid.New()
		names[i] = ResourceNameChannelType + "/" + util.Base64Encode(uuids[i])
	}
	for i, name := range names {
		ct, err := ParseNameChannelType(name)
		if err != nil {
			t.Fatalf("failed to parse %s: %s", name, err)
		}
		if ct != uuids[i] {
			t.Fatalf("expected ChannelType ID %s, got %s", uuids[i], ct)
		}
	}

	errorNames := []string{
		"",
		"asdgfsadfgdsf",
		"channeltypes",
		"channeltypes/",
		"channeltypes/X63L5CfxRt2kWhP7aKyzM",
		"channeltypes/X63L5CfxRt2kWhP7aKyzMQd",
		"channeltypes/X63L5CfxRt2kWhP*aKyzMQ",
		"channeltypes/X63L5CfxRt2kWhP7aKyzMQ ",
		"channeltypes/X63L5CfxRt2kWhP7aKyzMQ/",
		"channeltypes/X63L5CfxRt2kWhP7aKyzMQ/agents",
		// "channeltypes/X63L5CfxRt2kWhP7aKyzMQ",
	}
	for _, name := range errorNames {
		_, err := ParseNameChannelType(name)
		if err == nil {
			t.Fatalf("expected error when parsing: %s", name)
		}
	}
}

func TestParseNameAgent(t *testing.T) {
	var names [numberOfNames]string
	var uuids [numberOfNames][2]uuid.UUID
	for i := 0; i < numberOfNames; i++ {
		for j := 0; j < 2; j++ {
			uuids[i][j] = uuid.New()
		}
		names[i] = ResourceNameOrganization +
			"/" + util.Base64Encode(uuids[i][0]) +
			"/" + ResourceNameAgent +
			"/" + util.Base64Encode(uuids[i][1])
	}
	for i, name := range names {
		o, a, err := ParseNameAgent(name)
		if err != nil {
			t.Fatalf("failed to parse %s: %s", name, err)
		}
		if o != uuids[i][0] {
			t.Fatalf("expected Organization ID %s, got %s", uuids[i][0], o)
		}
		if a != uuids[i][1] {
			t.Fatalf("expected Agent ID %s, got %s", uuids[i][1], a)
		}
	}

	errorNames := []string{
		"",
		"asdgfsadfgdsf",
		"organizations",
		"organizations/",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/agents",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/agents/",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/agents/JuLBrUltTAyhIBTsRyZJP",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/agents/JuLBrUltTAyhIBTsRyZJPAt",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/agents/JuLBrUltTAyhIB*sRyZJPA",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/agents/JuLBrUltTAyhIBTsRyZJPA ",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/agents/JuLBrUltTAyhIBTsRyZJPA/",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/agents/JuLBrUltTAyhIBTsRyZJPA/probeconfigurations",
		// "organizations/X63L5CfxRt2kWhP7aKyzMQ/agents/JuLBrUltTAyhIBTsRyZJPA",
	}
	for _, name := range errorNames {
		_, _, err := ParseNameAgent(name)
		if err == nil {
			t.Fatalf("expected error when parsing: %s", name)
		}
	}
}

func TestParseNameTarget(t *testing.T) {
	var names [numberOfNames]string
	var uuids [numberOfNames][2]uuid.UUID
	for i := 0; i < numberOfNames; i++ {
		for j := 0; j < 2; j++ {
			uuids[i][j] = uuid.New()
		}
		names[i] = ResourceNameOrganization +
			"/" + util.Base64Encode(uuids[i][0]) +
			"/" + ResourceNameTarget +
			"/" + util.Base64Encode(uuids[i][1])
	}
	for i, name := range names {
		o, tt, err := ParseNameTarget(name)
		if err != nil {
			t.Fatalf("failed to parse %s: %s", name, err)
		}
		if o != uuids[i][0] {
			t.Fatalf("expected Organization ID %s, got %s", uuids[i][0], o)
		}
		if tt != uuids[i][1] {
			t.Fatalf("expected Target ID %s, got %s", uuids[i][1], tt)
		}
	}

	errorNames := []string{
		"",
		"asdgfsadfgdsf",
		"organizations",
		"organizations/",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/targets",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/targets/",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/targets/JuLBrUltTAyhIBTsRyZJP",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/targets/JuLBrUltTAyhIBTsRyZJPAt",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/targets/JuLBrUltTAyhIBTsRyZJPA ",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/targets/JuLBrUltTAyhIBTsRyZJPA/",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/targets/JuLBrUltTAyhIBTsRyZJPA/probeconfigurations",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/targets/JuLBrUltTAyhIB*sRyZJPA",
		// "organizations/X63L5CfxRt2kWhP7aKyzMQ/targets/JuLBrUltTAyhIBTsRyZJPA",
	}
	for _, name := range errorNames {
		_, _, err := ParseNameTarget(name)
		if err == nil {
			t.Fatalf("expected error when parsing: %s", name)
		}
	}
}

func TestParseNameCheck(t *testing.T) {
	var names [numberOfNames]string
	var uuids [numberOfNames][2]uuid.UUID
	for i := 0; i < numberOfNames; i++ {
		for j := 0; j < 2; j++ {
			uuids[i][j] = uuid.New()
		}
		names[i] = ResourceNameOrganization +
			"/" + util.Base64Encode(uuids[i][0]) +
			"/" + ResourceNameCheck +
			"/" + util.Base64Encode(uuids[i][1])
	}
	for i, name := range names {
		o, c, err := ParseNameCheck(name)
		if err != nil {
			t.Fatalf("failed to parse %s: %s", name, err)
		}
		if o != uuids[i][0] {
			t.Fatalf("expected Organization ID %s, got %s", uuids[i][0], o)
		}
		if c != uuids[i][1] {
			t.Fatalf("expected Check ID %s, got %s", uuids[i][1], c)
		}
	}

	errorNames := []string{
		"",
		"asdgfsadfgdsf",
		"organizations",
		"organizations/",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/checks",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/checks/",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/checks/JuLBrUltTAyhIBTsRyZJP",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/checks/JuLBrUltTAyhIBTsRyZJPAt",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/checks/JuLBrUltTAyhIBTsRyZJPA ",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/checks/JuLBrUltTAyhIBTsRyZJPA/",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/checks/JuLBrUltTAyhIBTsRyZJPA/probeconfigurations",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/checks/JuLBrUltTAyhIB*sRyZJPA",
		// "organizations/X63L5CfxRt2kWhP7aKyzMQ/checks/JuLBrUltTAyhIBTsRyZJPA",
	}
	for _, name := range errorNames {
		_, _, err := ParseNameCheck(name)
		if err == nil {
			t.Fatalf("expected error when parsing: %s", name)
		}
	}
}

func TestParseNameAlert(t *testing.T) {
	var names [numberOfNames]string
	var uuids [numberOfNames][2]uuid.UUID
	for i := 0; i < numberOfNames; i++ {
		for j := 0; j < 2; j++ {
			uuids[i][j] = uuid.New()
		}
		names[i] = ResourceNameOrganization +
			"/" + util.Base64Encode(uuids[i][0]) +
			"/" + ResourceNameAlert +
			"/" + util.Base64Encode(uuids[i][1])
	}
	for i, name := range names {
		o, a, err := ParseNameAlert(name)
		if err != nil {
			t.Fatalf("failed to parse %s: %s", name, err)
		}
		if o != uuids[i][0] {
			t.Fatalf("expected Organization ID %s, got %s", uuids[i][0], o)
		}
		if a != uuids[i][1] {
			t.Fatalf("expected Alert ID %s, got %s", uuids[i][1], a)
		}
	}

	errorNames := []string{
		"",
		"asdgfsadfgdsf",
		"organizations",
		"organizations/",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/alerts",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/alerts/",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/alerts/JuLBrUltTAyhIBTsRyZJP",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/alerts/JuLBrUltTAyhIBTsRyZJPAt",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/alerts/JuLBrUltTAyhIBTsRyZJPA ",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/alerts/JuLBrUltTAyhIBTsRyZJPA/",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/alerts/JuLBrUltTAyhIBTsRyZJPA/probeconfigurations",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/alerts/JuLBrUltTAyhIB*sRyZJPA",
		// "organizations/X63L5CfxRt2kWhP7aKyzMQ/alerts/JuLBrUltTAyhIBTsRyZJPA",
	}
	for _, name := range errorNames {
		_, _, err := ParseNameAlert(name)
		if err == nil {
			t.Fatalf("expected error when parsing: %s", name)
		}
	}
}

func TestParseNameChannel(t *testing.T) {
	var names [numberOfNames]string
	var uuids [numberOfNames][2]uuid.UUID
	for i := 0; i < numberOfNames; i++ {
		for j := 0; j < 2; j++ {
			uuids[i][j] = uuid.New()
		}
		names[i] = ResourceNameOrganization +
			"/" + util.Base64Encode(uuids[i][0]) +
			"/" + ResourceNameChannel +
			"/" + util.Base64Encode(uuids[i][1])
	}
	for i, name := range names {
		o, c, err := ParseNameChannel(name)
		if err != nil {
			t.Fatalf("failed to parse %s: %s", name, err)
		}
		if o != uuids[i][0] {
			t.Fatalf("expected Organization ID %s, got %s", uuids[i][0], o)
		}
		if c != uuids[i][1] {
			t.Fatalf("expected Channel ID %s, got %s", uuids[i][1], c)
		}
	}

	errorNames := []string{
		"",
		"asdgfsadfgdsf",
		"organizations",
		"organizations/",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/channels",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/channels/",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/channels/JuLBrUltTAyhIBTsRyZJP",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/channels/JuLBrUltTAyhIBTsRyZJPAt",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/channels/JuLBrUltTAyhIBTsRyZJPA ",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/channels/JuLBrUltTAyhIBTsRyZJPA/",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/channels/JuLBrUltTAyhIBTsRyZJPA/probeconfigurations",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/channels/JuLBrUltTAyhIB*sRyZJPA",
		// "organizations/X63L5CfxRt2kWhP7aKyzMQ/channels/JuLBrUltTAyhIBTsRyZJPA",
	}
	for _, name := range errorNames {
		_, _, err := ParseNameChannel(name)
		if err == nil {
			t.Fatalf("expected error when parsing: %s", name)
		}
	}
}

// All characters except whitespace and URL reserved
var uriCharacterSet = []byte{
	0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
	0x0e, 0x0f, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16,
	0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f,
	0x22, 0x25, 0x2d, 0x2e, 0x30, 0x31, 0x32, 0x33, 0x34,
	0x35, 0x36, 0x37, 0x38, 0x39, 0x3c, 0x3e, 0x41, 0x42,
	0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 0x4a, 0x4b,
	0x4c, 0x4d, 0x4e, 0x4f, 0x50, 0x51, 0x52, 0x53, 0x54,
	0x55, 0x56, 0x57, 0x58, 0x59, 0x5a, 0x5c, 0x5e, 0x5f,
	0x60, 0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68,
	0x69, 0x6a, 0x6b, 0x6c, 0x6d, 0x6e, 0x6f, 0x70, 0x71,
	0x72, 0x73, 0x74, 0x75, 0x76, 0x77, 0x78, 0x79, 0x7a,
	0x7b, 0x7c, 0x7d, 0x7e, 0x7f, 0x80, 0x81, 0x82, 0x83,
	0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8a, 0x8b, 0x8c,
	0x8d, 0x8e, 0x8f, 0x90, 0x91, 0x92, 0x93, 0x94, 0x95,
	0x96, 0x97, 0x98, 0x99, 0x9a, 0x9b, 0x9c, 0x9d, 0x9e,
	0x9f, 0xa0, 0xa1, 0xa2, 0xa3, 0xa4, 0xa5, 0xa6, 0xa7,
	0xa8, 0xa9, 0xaa, 0xab, 0xac, 0xad, 0xae, 0xaf, 0xb0,
	0xb1, 0xb2, 0xb3, 0xb4, 0xb5, 0xb6, 0xb7, 0xb8, 0xb9,
	0xba, 0xbb, 0xbc, 0xbd, 0xbe, 0xbf, 0xc0, 0xc1, 0xc2,
	0xc3, 0xc4, 0xc5, 0xc6, 0xc7, 0xc8, 0xc9, 0xca, 0xcb,
	0xcc, 0xcd, 0xce, 0xcf, 0xd0, 0xd1, 0xd2, 0xd3, 0xd4,
	0xd5, 0xd6, 0xd7, 0xd8, 0xd9, 0xda, 0xdb, 0xdc, 0xdd,
	0xde, 0xdf, 0xe0, 0xe1, 0xe2, 0xe3, 0xe4, 0xe5, 0xe6,
	0xe7, 0xe8, 0xe9, 0xea, 0xeb, 0xec, 0xed, 0xee, 0xef,
	0xf0, 0xf1, 0xf2, 0xf3, 0xf4, 0xf5, 0xf6, 0xf7, 0xf8,
	0xf9, 0xfa, 0xfb, 0xfc, 0xfd, 0xfe, 0xff,
}

func randomTagName() string {
	l := rand.Intn(25) + 3 // 3-28 length
	b := make([]byte, l)
	for i := 0; i < l; i++ {
		b[i] = uriCharacterSet[rand.Intn(len(uriCharacterSet))]
	}
	return string(b)
}

func TestParseNameTag(t *testing.T) {
	var names [numberOfNames]string
	var uuids [numberOfNames]uuid.UUID
	var tagNames [numberOfNames]string
	for i := 0; i < numberOfNames; i++ {
		uuids[i] = uuid.New()
		tagNames[i] = randomTagName()
		names[i] = ResourceNameOrganization +
			"/" + util.Base64Encode(uuids[i]) +
			"/" + ResourceNameTag +
			"/" + url.PathEscape(tagNames[i])
	}
	for i, name := range names {
		o, a, err := ParseNameTag(name)
		if err != nil {
			t.Fatalf("failed to parse %s: %s", name, err)
		}
		if o != uuids[i] {
			t.Fatalf("expected Organization ID %s, got %s", uuids[i], o)
		}
		if a != tagNames[i] {
			t.Fatalf("expected Tag name %s, got %s", tagNames[i], a)
		}
	}

	errorNames := []string{
		"",
		"asdgfsadfgdsf",
		"organizations",
		"organizations/",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/tags",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/tags/",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/tags/blue ",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/tags/blue/",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/tags/b;lue",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/tags/b*lue",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/tags/blue/probeconfigurations",
		// "organizations/X63L5CfxRt2kWhP7aKyzMQ/tags/blue",
	}
	for _, name := range errorNames {
		_, _, err := ParseNameTag(name)
		if err == nil {
			t.Fatalf("expected error when parsing: %s", name)
		}
	}
}

func TestParseNameProbeConfiguration(t *testing.T) {
	var names [numberOfNames]string
	var uuids [numberOfNames][3]uuid.UUID
	for i := 0; i < numberOfNames; i++ {
		for j := 0; j < 3; j++ {
			uuids[i][j] = uuid.New()
		}
		names[i] = ResourceNameOrganization +
			"/" + util.Base64Encode(uuids[i][0]) +
			"/" + ResourceNameAgent +
			"/" + util.Base64Encode(uuids[i][1]) +
			"/" + ResourceNameProbeConfiguration +
			"/" + util.Base64Encode(uuids[i][2])
	}
	for i, name := range names {
		o, a, pc, err := ParseNameProbeConfiguration(name)
		if err != nil {
			t.Fatalf("failed to parse %s: %s", name, err)
		}
		if o != uuids[i][0] {
			t.Fatalf("expected Organization ID %s, got %s", uuids[i][0], o)
		}
		if a != uuids[i][1] {
			t.Fatalf("expected Agent ID %s, got %s", uuids[i][1], a)
		}
		if pc != uuids[i][2] {
			t.Fatalf("expected ProbeConfiguration ID %s, got %s", uuids[i][2], pc)
		}
	}

	errorNames := []string{
		"",
		"asdgfsadfgdsf",
		"organizations",
		"organizations/",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/agents",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/agents/",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/agents/JuLBrUltTAyhIBTsRyZJPA",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/agents/JuLBrUltTAyhIBTsRyZJPA/",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/agents/JuLBrUltTAyhIBTsRyZJPA/probeconfigurations",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/agents/JuLBrUltTAyhIBTsRyZJPA/probeconfigurations/",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/agents/JuLBrUltTAyhIBTsRyZJPA/probeconfigurations/6HVa_I64S2a3gc0LJ0AI1",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/agents/JuLBrUltTAyhIBTsRyZJPA/probeconfigurations/6HVa_I64S2a3gc0LJ0AI1Qd",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/agents/JuLBrUltTAyhIBTsRyZJPA/probeconfigurations/6HVa_I64S2a3gc0LJ0AI1Q/",
		"organizations/X63L5CfxRt2kWhP7aKyzMQ/agents/JuLBrUltTAyhIBTsRyZJPA/probeconfigurations/6HVa_I64S2a3gc0LJ0AI1Q ",
		// "organizations/X63L5CfxRt2kWhP7aKyzMQ/agents/JuLBrUltTAyhIBTsRyZJPA/probeconfigurations/6HVa_I64S2a3gc0LJ0AI1Q",
	}
	for _, name := range errorNames {
		_, _, _, err := ParseNameProbeConfiguration(name)
		if err == nil {
			t.Fatalf("expected error when parsing: %s", name)
		}
	}
}
