# Panto API

The Panto API is based on [gRPC](http://www.grpc.io/) with [Protobuf](https://developers.google.com/protocol-buffers/).

As not all clients may have a gRPC implementation, Panto also provides clients with a REST API, that is a simple proxy to the gRPC server. All the concepts and schema are the same between the two interfaces of the API.

## Design

The design is based on [Google API Design guide](https://cloud.google.com/apis/design/), and is a Resource Oriented Design.

The detailed list of resources and methods is available in [the RESOURCES document](https://about.panto.app/007-API-Reference.md). The REST API is also documented with a [swagger file](panto.swagger.json).


## General mecanisms

### Authentication

To be identified and authenticated, the API clients must provide two fields in every request to the API, whether they request the gRPC interface or the REST interface:

* `agent`: the agent UUID field (encoded as a base64 string without padding), given by the server as a unique identifier of the API client;

In the gRPC implementation, these fields are [Request Metadata](https://github.com/grpc/grpc-go/blob/master/Documentation/grpc-metadata.md), and are automatically included in any RPC call as per the gRPC Dial configuration.

In the REST implementation, these fields should be included in the HTTP headers.

The server will look for these fields **before** handling the RCP/REST call, and will return an error if the data is missing or not validated.

### Agent version

For every call originated from an Agent, the server will include a data field in every response, containing the configuration version the Agent should be running:

* `agent-configuration-version`: the configuration version the Agent should be running (encoded as a string).

In the gRPC implementation, this field is [Response Metadata](https://github.com/grpc/grpc-go/blob/master/Documentation/grpc-metadata.md), and should be handled by the client explicitly (see [the official documention](https://github.com/grpc/grpc-go/blob/master/Documentation/grpc-metadata.md#receiving-metadata)).

In the REST implementation, this field is included in the HTTP headers of the response, and its name changes for `Grpc-Metadata-Agent-Configuration-Version`.

## Generating code and documentation

The simplest way to update all files auto-generated from the Protobuf schema is by using the Makefile target. From a shell prompt, navigate to the root of the repository and type:

```shell
make api
```

The rule will invoke all `protoc` calls required to regenerate files dependant on [`panto.proto`](panto.proto).

### gRPC

To define a new message or a new service, you need to update the [protobuf file](panto.proto) and run the following command:

```shell
protoc -I api/ -I${GOPATH}/src -I${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis --go_out=plugins=grpc:api api/panto.proto
```

This will re-generate the [gRPC code](panto.pb.go).

You can now use the endpoints in your code (when defining new services) or the messages structs (when defining new messages).

### REST

Panto exposes an HTTP/1.1 REST API gateway to the (HTTP/2) gRPC endpoints, using [gRPC Gateway](https://github.com/grpc-ecosystem/grpc-gateway). After changing the Protobuf schema, be sure to re-generate the [gRPC gateway code](panto.pb.gw.go) with the following command:

```shell
protoc -I api/ -I${GOPATH}/src -I${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis --grpc-gateway_out=logtostderr=true:api api/panto.proto
```

### Swagger

Re-generate the Swagger definitions with:

```shell
protoc -I api/ -I${GOPATH}/src -I${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis --swagger_out=logtostderr=true:api api/panto.proto
```
