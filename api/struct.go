package api

import (
	"bytes"
	"encoding/json"

	"github.com/golang/protobuf/jsonpb"
	"github.com/golang/protobuf/ptypes/struct"
)

// NewStruct creates a new *structpb.Value (a glorified JSON wrapper) out of a Go type
func NewStruct(v interface{}) (*structpb.Value, error) {
	var s structpb.Value
	b, err := json.Marshal(v)
	if err != nil {
		return nil, err
	}
	err = jsonpb.Unmarshal(bytes.NewReader(b), &s)
	if err != nil {
		return nil, err
	}
	return &s, nil
}
