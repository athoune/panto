// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

syntax = "proto3";
package api;

import "google/api/annotations.proto";
import "google/protobuf/any.proto";
import "google/protobuf/duration.proto";
import "google/protobuf/empty.proto";
import "google/protobuf/field_mask.proto";
import "google/protobuf/struct.proto";
import "google/protobuf/timestamp.proto";

option go_package = "gitlab.com/pantomath-io/panto/api";

// The Panto API exposes all gRPCs endpoints to manipulate the resources on
// the Panto server. The complete documentation of resources and operations
// can be found here:
// https://gitlab.com/pantomath-io/panto/api/RESOURCES.md.
//
// The API also exposes the endpoints on an HTTP/1.1 REST interface that uses
// JSON.
//
// * TODO: Concepts
// * TODO: Getting started
// * TODO: Examples
// * TODO: Swagger
// * TODO: Comment fields
service Panto {
  rpc GetInfo(google.protobuf.Empty) returns (Info) {
    option (google.api.http) = {
      get: "/v1/info"
    };
  }

  // ListOrganizations returns all Organizations in Panto.
  rpc ListOrganizations(ListOrganizationsRequest)
      returns (ListOrganizationsResponse) {
    option (google.api.http) = {
      get: "/v1/organizations"
    };
  }

  // GetOrganization returns a specific Organization.
  rpc GetOrganization(GetOrganizationRequest) returns (Organization) {
    option (google.api.http) = {
      get: "/v1/{name=organizations/*}"
    };
  }

  // UpdateOrganization updates an existing Organizations and returns it.
  rpc UpdateOrganization(UpdateOrganizationRequest) returns (Organization) {
    option (google.api.http) = {
      put: "/v1/{organization.name=organizations/*}"
      body: "*"
    };
  }

  // ListAgents returns all Agents for a given Organization.
  rpc ListAgents(ListAgentsRequest) returns (ListAgentsResponse) {
    option (google.api.http) = {
      get: "/v1/{parent=organizations/*}/agents"
    };
  }

  // CreateAgent creates a new Agent for a given Organization and returns it.
  rpc CreateAgent(CreateAgentRequest) returns (Agent) {
    option (google.api.http) = {
      post: "/v1/{parent=organizations/*}/agents"
      body: "*"
    };
  }

  // GetAgent returns a specific Agent.
  rpc GetAgent(GetAgentRequest) returns (Agent) {
    option (google.api.http) = {
      get: "/v1/{name=organizations/*/agents/*}"
    };
  }

  // UpdateAgent updates an existing Agent and returns it.
  rpc UpdateAgent(UpdateAgentRequest) returns (Agent) {
    option (google.api.http) = {
      put: "/v1/{agent.name=organizations/*/agents/*}"
      body: "*"
    };
  }

  // DeleteAgent deletes an existing Agent and corresponding
  // ProbeConfigurations.
  rpc DeleteAgent(DeleteAgentRequest) returns (google.protobuf.Empty) {
    option (google.api.http) = {
      delete: "/v1/{name=organizations/*/agents/*}"
    };
  }

  // ListTargets returns all Targets for a given Organization.
  rpc ListTargets(ListTargetsRequest) returns (ListTargetsResponse) {
    option (google.api.http) = {
      get: "/v1/{parent=organizations/*}/targets"
    };
  }

  // GetTarget returns a specific Target.
  rpc GetTarget(GetTargetRequest) returns (Target) {
    option (google.api.http) = {
      get: "/v1/{name=organizations/*/targets/*}"
    };
  }

  // CreateTarget creates a new Target for a given Organization and returns it.
  rpc CreateTarget(CreateTargetRequest) returns (Target) {
    option (google.api.http) = {
      post: "/v1/{parent=organizations/*}/targets"
      body: "*"
    };
  }

  // UpdateTarget updates an existing Target and returns it.
  rpc UpdateTarget(UpdateTargetRequest) returns (Target) {
    option (google.api.http) = {
      put: "/v1/{target.name=organizations/*/targets/*}"
      body: "*"
    };
  }

  // DeleteTarget deletes an existing Target, corresponding Checks and
  // ProbeConfigurations and unlinks it from Tags.
  rpc DeleteTarget(DeleteTargetRequest) returns (google.protobuf.Empty) {
    option (google.api.http) = {
      delete: "/v1/{name=organizations/*/targets/*}"
    };
  }

  // ListProbeConfigurations returns the configuration for all probes to be run
  // by a given Agent.
  rpc ListProbeConfigurations(ListProbeConfigurationsRequest)
      returns (ListProbeConfigurationsResponse) {
    option (google.api.http) = {
      get: "/v1/{parent=organizations/*/agents/*}/probeconfigurations"
    };
  }

  // CreateProbeConfiguration creates a configuration for a specific probe to be
  // run by the Agent.
  rpc CreateProbeConfiguration(CreateProbeConfigurationRequest)
      returns (ProbeConfiguration) {
    option (google.api.http) = {
      post: "/v1/{parent=organizations/*/agents/*}/probeconfigurations"
      body: "*"
    };
  }

  // GetProbeConfiguration returns the configuration for a specific probe on a
  // given agent.
  rpc GetProbeConfiguration(GetProbeConfigurationRequest)
      returns (ProbeConfiguration) {
    option (google.api.http) = {
      get: "/v1/{name=organizations/*/agents/*/probeconfigurations/*}"
    };
  }

  // UpdateProbeConfiguration update a configuration for a specific probe to be
  // run by the Agent.
  rpc UpdateProbeConfiguration(UpdateProbeConfigurationRequest)
      returns (ProbeConfiguration) {
    option (google.api.http) = {
      put: "/v1/{probe_configuration.name=organizations/*/agents/*/"
           "probeconfigurations/*}"
      body: "*"
    };
  }

  // DeleteProbeConfiguration returns the configuration for a specific probe on
  // a given agent.
  rpc DeleteProbeConfiguration(DeleteProbeConfigurationRequest)
      returns (google.protobuf.Empty) {
    option (google.api.http) = {
      delete: "/v1/{name=organizations/*/agents/*/probeconfigurations/*}"
    };
  }

  // QueryProbeConfigurations returns the probeconfigurations for a list of
  // given Checks in the Parent Organisation.
  rpc QueryProbeConfigurations(QueryProbeConfigurationsRequest)
      returns (QueryProbeConfigurationsResponse) {
    option (google.api.http) = {
      post: "/v1/{organization=organizations/*}/probeconfigurations/query"
      body: "*"
    };
  }

  // ListProbes returns all Probes available in Panto.
  rpc ListProbes(ListProbesRequest) returns (ListProbesResponse) {
    option (google.api.http) = {
      get: "/v1/probes"
    };
  }

  // GetProbe returns a specific Probe.
  rpc GetProbe(GetProbeRequest) returns (Probe) {
    option (google.api.http) = {
      get: "/v1/{name=probes/*}"
    };
  }

  // ListChecks returns a list of all Checks for a given Organization
  rpc ListChecks(ListChecksRequest) returns (ListChecksResponse) {
    option (google.api.http) = {
      get: "/v1/{parent=organizations/*}/checks"
    };
  }

  // GetCheck returns a specific Check.
  rpc GetCheck(GetCheckRequest) returns (Check) {
    option (google.api.http) = {
      get: "/v1/{name=organizations/*/checks/*}"
    };
  }

  // CreateCheck creates a new Check and returns it.
  rpc CreateCheck(CreateCheckRequest) returns (Check) {
    option (google.api.http) = {
      post: "/v1/{parent=organizations/*}/checks"
      body: "*"
    };
  }

  // UpdateCheck updates an existing Check and returns it.
  rpc UpdateCheck(UpdateCheckRequest) returns (Check) {
    option (google.api.http) = {
      put: "/v1/{check.name=organizations/*/checks/*}"
      body: "*"
    };
  }

  // DeleteCheck deletes an existing Check, corresponding ProbeConfigurations.
  rpc DeleteCheck(DeleteCheckRequest) returns (google.protobuf.Empty) {
    option (google.api.http) = {
      delete: "/v1/{name=organizations/*/checks/*}"
    };
  }

  // ListAlerts returns all Alerts for a given Organization.
  rpc ListAlerts(ListAlertsRequest) returns (ListAlertsResponse) {
    option (google.api.http) = {
      get: "/v1/{parent=organizations/*}/alerts"
    };
  }

  // GetAlert returns a specific Alert.
  rpc GetAlert(GetAlertRequest) returns (Alert) {
    option (google.api.http) = {
      get: "/v1/{name=organizations/*/alerts/*}"
    };
  }

  // CreateAlert creates a new Alert for a given Organization and returns it.
  rpc CreateAlert(CreateAlertRequest) returns (Alert) {
    option (google.api.http) = {
      post: "/v1/{parent=organizations/*}/alerts"
      body: "*"
    };
  }

  // UpdateAlert updates an existing Alert and returns it.
  rpc UpdateAlert(UpdateAlertRequest) returns (Alert) {
    option (google.api.http) = {
      put: "/v1/{alert.name=organizations/*/alerts/*}"
      body: "*"
    };
  }

  // DeleteAlert deletes an existing Alert and corresponding links to Checks.
  rpc DeleteAlert(DeleteAlertRequest) returns (google.protobuf.Empty) {
    option (google.api.http) = {
      delete: "/v1/{name=organizations/*/alerts/*}"
    };
  }

  // ListChannelTypes lists all ChannelTypes.
  rpc ListChannelTypes(ListChannelTypesRequest)
      returns (ListChannelTypesResponse) {
    option (google.api.http) = {
      get: "/v1/channeltypes"
    };
  }

  // ListChannels lists all Channels for a given Organization.
  rpc ListChannels(ListChannelsRequest) returns (ListChannelsResponse) {
    option (google.api.http) = {
      get: "/v1/{parent=organizations/*}/channels"
    };
  }

  // GetChannel returns a specific Channel.
  rpc GetChannel(GetChannelRequest) returns (Channel) {
    option (google.api.http) = {
      get: "/v1/{name=organizations/*/channels/*}"
    };
  }

  // CreateChannel creates a new Channel for a given Organization and returns
  // it.
  rpc CreateChannel(CreateChannelRequest) returns (Channel) {
    option (google.api.http) = {
      post: "/v1/{parent=organizations/*}/channels"
      body: "*"
    };
  }

  // QueryResults returns all Results from Probes that match the constraints
  // specified in the query.
  rpc QueryResults(QueryResultsRequest) returns (QueryResultsResponse) {
    option (google.api.http) = {
      post: "/v1/{parent=organizations/*}/results/query"
      body: "*"
    };
  }

  // AddResults adds a batch of results from Probes and returns all added
  // results.
  rpc AddResults(AddResultsRequest) returns (AddResultsResponse) {
    option (google.api.http) = {
      post: "/v1/results/add"
      body: "*"
    };
  }

  // QueryStates returns all current States for a Check, Agent pair.
  rpc QueryStates(QueryStatesRequest) returns (QueryStatesResponse) {
    option (google.api.http) = {
      post: "/v1/{parent=organizations/*}/states/query"
      body: "*"
    };
  }

  // QueryNotifications returns all latest Notifications for ProbeConfigurations
  // (or the whole Organization if no ProbeConfiguration provided).
  rpc QueryNotifications(QueryNotificationsRequest)
      returns (QueryNotificationsResponse) {
    option (google.api.http) = {
      post: "/v1/{parent=organizations/*}/notifications/query"
      body: "*"
    };
  }

  // AddNotification adds the latest Notifications for a ProbeConfiguration.
  rpc AddNotification(AddNotificationRequest) returns (Notification) {
    option (google.api.http) = {
      post: "/v1/{parent=organizations/*}/notifications/add"
      body: "*"
    };
  }

  // ListTags lists all Tags for a given Organization.
  rpc ListTags(ListTagsRequest) returns (ListTagsResponse) {
    option (google.api.http) = {
      get: "/v1/{parent=organizations/*}/tags"
    };
  }

  // GetTag returns a specific Tag.
  rpc GetTag(GetTagRequest) returns (Tag) {
    option (google.api.http) = {
      get: "/v1/{name=organizations/*/tags/*}"
    };
  }

  // CreateTag creates a new Tag for a given Organization and returns it.
  rpc CreateTag(CreateTagRequest) returns (Tag) {
    option (google.api.http) = {
      post: "/v1/{parent=organizations/*}/tags"
      body: "*"
    };
  }

  // UpdateTag updates an existing Tag and returns it.
  rpc UpdateTag(UpdateTagRequest) returns (Tag) {
    option (google.api.http) = {
      put: "/v1/{tag.name=organizations/*/tags/*}"
      body: "*"
    };
  }

  // DeleteTag deletes an existing Tag and unlinks it from Targets.
  rpc DeleteTag(DeleteTagRequest) returns (google.protobuf.Empty) {
    option (google.api.http) = {
      delete: "/v1/{name=organizations/*/tags/*}"
    };
  }
}

// Order is used in List* RPCs to order results by ascending or descending time.
// The default value is ASCENDING. Lack of a value designates the default value.
enum Order {
  ASCENDING = 0;
  DESCENDING = 1;
}

message Info {
  string version = 1;
  bool with_tls = 2;
  string grpc_address = 3;
  string rest_address = 4;
}

message Organization {
  string name = 1;
  string display_name = 2;
}

message ListOrganizationsRequest {
  int32 page_size = 1;
  string page_token = 2;
}

message ListOrganizationsResponse {
  repeated Organization organizations = 1;
  string next_page_token = 2;
}

message GetOrganizationRequest {
  string name = 1;
}

message UpdateOrganizationRequest {
  Organization organization = 1;
  google.protobuf.FieldMask update_mask = 2;
}

// Agent represents an instance of a running process executing probes on one or
// several Targets.
message Agent {
  string name = 1;
  string display_name = 2;
  google.protobuf.Timestamp last_activity = 3;
  string last_version = 4;
}

message ListAgentsRequest {
  string parent = 1;
  int32 page_size = 2;
  string page_token = 3;
}

message ListAgentsResponse {
  repeated Agent agents = 1;
  string next_page_token = 2;
}

message CreateAgentRequest {
  string parent = 1;
  Agent agent = 2;
}

message GetAgentRequest {
  string name = 1;
}

message UpdateAgentRequest {
  Agent agent = 1;
  google.protobuf.FieldMask update_mask = 2;
}

message DeleteAgentRequest {
  string name = 1;
}

// Target represents an instance of a destination of probes, executed by Agents.
message Target {
  string name = 1;
  string address = 2;
  string note = 3;
  repeated Tag tags = 4;
}

message ListTargetsRequest {
  string parent = 1;
  int32 page_size = 2;
  string page_token = 3;
}

message ListTargetsResponse {
  repeated Target targets = 1;
  string next_page_token = 2;
}

message GetTargetRequest {
  string name = 1;
}

message CreateTargetRequest {
  string parent = 1;
  Target target = 2;
}

message UpdateTargetRequest {
  Target target = 1;
  google.protobuf.FieldMask update_mask = 2;
}

message DeleteTargetRequest {
  string name = 1;
}

// Probe represents a type of probe that can be executed by an Agent on a
// Target. A Probe collects information about the Target, that the Agent then
// sends to the server.
message Probe {
  // Parameter is a configuration option for a Probe.
  message Parameter {
    string name = 1;
    string type = 2;
    string description = 3;
    bool mandatory = 4;
    google.protobuf.Value placeholder = 5;
    google.protobuf.Value default = 6;
  }

  // Metric is a value returned by the probe
  message Metric {
    string name = 1;
    string type = 2;
    string description = 3;
  }

  // Graph is a value returned by the probe
  message Graph {
    string title = 1;
    string type = 2;
    repeated string metrics = 3;
    repeated string tags = 4;
    string unit_format = 5;
  }

  // Tag is a value returned by the probe
  message Tag {
    string name = 1;
    string description = 2;
  }

  string name = 1;
  string probe_name = 2;
  string display_name = 3;
  string description = 4;
  repeated Parameter configuration_template = 5;
  repeated Metric metrics = 6;
  repeated Tag tags = 7;
  repeated Graph graphs = 8;
}

message ListProbesRequest {
  int32 page_size = 1;
  string page_token = 2;
}

message ListProbesResponse {
  repeated Probe probes = 1;
  string next_page_token = 2;
}

message GetProbeRequest {
  string name = 1;
}

// Check contains the conditions against which to compare results from a
// Probe, on a specific Target. If one or more of the conditions is validated,
// the most critical validated state is set (CRITICAL is more critical than
// WARNING).
message Check {
  string name = 1;
  string target = 2;
  string probe = 3;
  string state_type = 4 [deprecated = true];
  string state_configuration = 5 [deprecated = true];

  message Children {
    Target target = 1;
    Probe probe = 2;
  }
  Children children = 7;

  string type = 8;
  string configuration = 9;
}

message ListChecksRequest {
  string parent = 1;
  repeated string target = 2;
  repeated string probe = 3;
  google.protobuf.FieldMask children_mask = 4;
  int32 page_size = 5;
  string page_token = 6;
}

message ListChecksResponse {
  repeated Check checks = 1;
  string next_page_token = 2;
}

message GetCheckRequest {
  string name = 1;
  google.protobuf.FieldMask children_mask = 2;
}

message CreateCheckRequest {
  string parent = 1;
  Check check = 2;
}

message UpdateCheckRequest {
  Check check = 1;
  google.protobuf.FieldMask update_mask = 2;
}

message DeleteCheckRequest {
  string name = 1;
}

// ProbeConfiguration is the set of configuration options that an Agent should
// use to execute a Probe.
message ProbeConfiguration {
  string name = 1;
  string check = 2;
  string configuration = 3;
  string probe_label = 4;
  google.protobuf.Duration schedule = 5;
  Target target = 6;
}

message ListProbeConfigurationsRequest {
  string parent = 1;
  bool with_target = 2;
  int32 page_size = 3;
  string page_token = 4;
}

message ListProbeConfigurationsResponse {
  repeated ProbeConfiguration probe_configurations = 1;
  string next_page_token = 2;
}

message GetProbeConfigurationRequest {
  string name = 1;
  bool with_target = 2;
}

message CreateProbeConfigurationRequest {
  string parent = 1;
  ProbeConfiguration probe_configuration = 2;
}

message UpdateProbeConfigurationRequest {
  ProbeConfiguration probe_configuration = 1;
  google.protobuf.FieldMask update_mask = 2;
}

message DeleteProbeConfigurationRequest {
  string name = 1;
}

message QueryProbeConfigurationsRequest {
  string organization = 1;
  repeated string checks = 2;
  int32 page_size = 3;
  string page_token = 4;
}

message QueryProbeConfigurationsResponse {
  repeated ProbeConfiguration probe_configurations = 1;
  string next_page_token = 2;
}

// Result is a set of information collected by a Probe once an Agent has
// executed it.
message Result {
  string check = 1;
  string agent = 2;
  string probe_configuration = 3;
  google.protobuf.Timestamp timestamp = 4;
  map<string, google.protobuf.Any> fields = 5;
  map<string, string> tags = 6;
  bool error = 7;
  string error_message = 8;
}

message QueryResultsRequest {
  string parent = 1;
  string probe_configuration = 2;
  google.protobuf.Timestamp start_time = 3;
  google.protobuf.Timestamp end_time = 4;
  map<string, string> tags = 5;
  Order order_by = 6;
  int32 page_size = 7;
  string page_token = 8;
}

message QueryResultsResponse {
  repeated Result results = 1;
  string next_page_token = 2;
}

message AddResultsRequest {
  repeated Result results = 1;
}

message AddResultsResponse {
  repeated Result results = 1;
}

// State is the result of evaluating all the conditions from a Check on a
// Result.
message State {
  enum Type {
    UNKNOWN = 0;
    OK = 1;
    WARNING = 2;
    CRITICAL = 3;
    MISSING_DATA = 4;
  }

  string check = 1;
  string agent = 2;
  string probe_configuration = 3;
  google.protobuf.Timestamp timestamp = 4;
  Type type = 5;
  string reason = 6;

  message Children {
    Check check = 1;
    Agent agent = 2;
    ProbeConfiguration probe_configuration = 3;
  }
  Children children = 7;
}

message QueryStatesRequest {
  string parent = 1;
  repeated string probe_configurations = 2;
  int32 page_size = 3;
  string page_token = 4;
  google.protobuf.FieldMask children_mask = 5;
}

message QueryStatesResponse {
  repeated State states = 1;
  string next_page_token = 2;
}

// Event is the type of notification
enum Event {
  UNKNOWN = 0;
  NOTIFIED = 1;
  ACKNOWLEDGED = 2;
  SNOOZED = 3;
}

// Notification is the result of evaluating all the conditions from a Check on
// an Alert.
message Notification {
  string check = 1;
  string agent = 2;
  string probe_configuration = 3;
  google.protobuf.Timestamp timestamp = 4;
  Event event = 5;

  message Children {
    Check check = 1;
    Agent agent = 2;
  }
  Children children = 6;
}

message QueryNotificationsRequest {
  string parent = 1;
  repeated string probe_configurations = 2;
  int32 page_size = 3;
  string page_token = 4;
  google.protobuf.FieldMask children_mask = 5;
}

message QueryNotificationsResponse {
  repeated Notification notifications = 1;
  string next_page_token = 2;
}

message AddNotificationRequest {
  string parent = 1;
  string probe_configuration = 2;
  Event event = 3;
}

// Alert is an action to be performed on specific conditions after evaluation
// of a State.
message Alert {
  string name = 1;
  repeated string checks = 2;
  string channel = 3;
  string type = 4;
  string configuration = 5;

  message Children {
    repeated Check checks = 1;
    Channel channel = 2;
  }
  Children children = 6;
}

message ListAlertsRequest {
  string parent = 1;
  google.protobuf.FieldMask children_mask = 2;
  int32 page_size = 3;
  string page_token = 4;
}

message ListAlertsResponse {
  repeated Alert alerts = 1;
  string next_page_token = 2;
}

message GetAlertRequest {
  string name = 1;
  google.protobuf.FieldMask children_mask = 2;
}

message CreateAlertRequest {
  string parent = 1;
  Alert alert = 2;
}

message UpdateAlertRequest {
  Alert alert = 1;
  google.protobuf.FieldMask update_mask = 2;
}

message DeleteAlertRequest {
  string name = 1;
}

// Channel defines a medium to communicate on upon sending an Alert.
message Channel {
  string name = 1;
  string channel_type = 2;
  string display_name = 3;
  string configuration = 4;

  message Children {
    ChannelType channel_type = 1;
  }
  Children children = 5;
}

message ListChannelsRequest {
  string parent = 1;
  google.protobuf.FieldMask children_mask = 2;
  int32 page_size = 3;
  string page_token = 4;
}

message GetChannelRequest {
  string name = 1;
  google.protobuf.FieldMask children_mask = 2;
}

message ListChannelsResponse {
  repeated Channel channels = 1;
  string next_page_token = 2;
}

message CreateChannelRequest {
  string parent = 1;
  Channel channel = 2;
}

// ChannelType defines the types of medium to communicate on upon sending an
// Alert.
message ChannelType {
  // Parameter is a configuration option for a Probe.
  message Parameter {
    string name = 1;
    string type = 2;
    string description = 3;
    bool mandatory = 4;
  }

  string name = 1;
  string label = 2;
  repeated Parameter configuration = 5;
}

message ListChannelTypesRequest {
  int32 page_size = 1;
  string page_token = 2;
}

message ListChannelTypesResponse {
  repeated ChannelType channeltypes = 1;
  string next_page_token = 2;
}

// Tag defines a tag that can be associated with the Targets.
message Tag {
  string name = 1;
  string display_name = 2;
  string note = 3;
  string color = 4;
}

message ListTagsRequest {
  string parent = 1;
  int32 page_size = 2;
  string page_token = 3;
}

message ListTagsResponse {
  repeated Tag tags = 1;
  string next_page_token = 2;
}

message GetTagRequest {
  string name = 1;
}

message CreateTagRequest {
  string parent = 1;
  Tag tag = 2;
}

message UpdateTagRequest {
  Tag tag = 1;
  google.protobuf.FieldMask update_mask = 2;
}

message DeleteTagRequest {
  string name = 1;
}
