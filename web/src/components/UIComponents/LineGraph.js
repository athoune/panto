import { Line } from 'vue-chartjs'

export default {
  name: 'line-graph',
  extends: Line,
  props: ['chartData', 'chartOptions'],
  methods: {
    render () {
      this.renderChart(this.chartData, this.chartOptions)
    }
  }
}
