import { Bar } from 'vue-chartjs'

export default {
  name: 'bar-graph',
  extends: Bar,
  props: ['data', 'options'],
  mounted () {
    this.renderChart(JSON.parse(JSON.stringify(this.data)), this.options)
  }
}
