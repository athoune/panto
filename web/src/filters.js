import moment from 'moment'

export default {
  formatDate: function (value, format) {
    return moment(value).format(format || 'DD-MM-YYYY')
  },
  fromNowDate: function (value) {
    return moment(value).fromNow()
  }
}
