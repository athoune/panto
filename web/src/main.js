// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import Vue from 'vue'
import './pollyfills'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueRouter from 'vue-router'
import ElementUI from 'element-ui'
import lang from 'element-ui/lib/locale/lang/en'
import locale from 'element-ui/lib/locale'
import App from './App.vue'
import store from './store'
import helpers from './helpers'
import filters from './filters'
import VueAnalytics from 'vue-analytics'
import VueClipboard from 'vue-clipboard2'


// Plugins
import GlobalComponents from './globalComponents'
import GlobalDirectives from './globalDirectives'
import topBarLinks from './topBarLinks'
import TopBar from './components/UIComponents/TopBarPlugin'
import ConfigurationForm from './components/UIComponents/ConfigurationForm'

// router setup
import routes from './routes/routes'

// library imports
import './assets/less/style.less'
import 'element-theme-chalk'
// plugin setup
Vue.use(VueRouter)
axios.defaults.baseURL = process.env.API_BASE_URL
axios.defaults.headers.post['Content-Type'] = 'application/json'
axios.defaults.headers.put['Content-Type'] = 'application/json'
axios.interceptors.response.use(null, error => {
  let errMsg = `<h4 style="margin-bottom: 4px">Oops! Something went wrong.</h4>`
  if (error.response.data && error.response.data.error) {
    errMsg += `<small>${error.response.data.error}</small>`
  }
  ElementUI.Message.error({
    message: errMsg,
    dangerouslyUseHTMLString: true,
    showClose: true,
    duration: 5000
  })
  return Promise.reject(error)
})
Vue.use(VueAxios, axios)
Vue.use(VueClipboard)
Vue.use(GlobalDirectives)
Vue.use(GlobalComponents)
Vue.use(TopBar, {topBarLinks: topBarLinks})
Vue.use(ElementUI, { locale })
Vue.use(ConfigurationForm)
locale.use(lang)

Object.keys(filters).map(function (key) {
  Vue.filter(key, filters[key])
})

// configure router
const router = new VueRouter({
  routes,
  hashbang: false,
  mode: 'history',
  linkActiveClass: 'active'
})
Vue.router = router


// configure Google Analytics, if the environment variable is available
if (process.env.GOOGLE_ANALYTICS_ID !== undefined && process.env.GOOGLE_ANALYTICS_ID !== '') {
  Vue.use(VueAnalytics, {id: process.env.GOOGLE_ANALYTICS_ID, router})
}

Vue.prototype.$helpers = helpers
Vue.prototype.$env = process.env
Vue.prototype.$version = process.version
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  render: h => h(App),
  router,
  store
})
