// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/* eslint-disable */
module.exports = {
  request: function (req, token) {
    this.options.http._setHeaders.call(this, req, {Authorization: 'JWT ' + token})
  },
  response: function (res) {
    if (res.status == 200) {
      var headers = this.options.http._getHeaders.call(this, res)
      var token = headers.Authorization || headers.authorization
      if (token) {
        token = token.split(/JWT\:?\s?/i)
        return token[token.length > 1 ? 1 : 0].trim()
      }
    } else if (res.status == 403) {
      window.localStorage.removeItem('default_auth_token')
      // this.logout()
      if (window.location.pathname.startsWith('/public') === false) {
        window.location = '/login'
      }
    }
  }
}
