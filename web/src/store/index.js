// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import Vue from 'vue'
import Vuex from 'vuex'
import globals from './modules/globals'
import user from './modules/user'
import organization from './modules/organization'
import agent from './modules/agent'
import probe from './modules/probe'
import probeConfiguration from './modules/probeConfiguration'
import target from './modules/target'
import check from './modules/check'
import tag from './modules/tag'
import info from './modules/info'
import state from './modules/state'
import graphCollection from './modules/graphCollection'
import alert from './modules/alert'
import channel from './modules/channel'
import channelType from './modules/channelType'
import notification from './modules/notification'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    globals,
    user,
    organization,
    agent,
    probe,
    probeConfiguration,
    target,
    check,
    tag,
    info,
    state,
    graphCollection,
    alert,
    channel,
    channelType,
    notification
  },
  strict: debug
})
