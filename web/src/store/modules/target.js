import Vue from 'vue'
import * as types from '../mutation-types'

// initial state
const state = {
  targets: [],
  target: {}
}

// getters
const getters = {
  targets: state => state.targets,
  target: state => state.target
}

// actions
const actions = {
  listTargets ({ state, commit }, args) {
    return Vue.axios.get(`${args.organization}/targets`, {params: args.filters}).then((response) => {
      commit(types.RECEIVE_TARGETS, { response })
    })
  },
  getTarget ({ state, commit }, args) {
    return Vue.axios.get(`${args.target}`, {
      params: {
        children_mask: args.children_mask
      }
    }).then((response) => {
      commit(types.RECEIVE_TARGET, { response })
    })
  },
  setTarget ({ state, commit }, args) {
    commit(types.SET_TARGET, { target: args.target })
  },
  createTarget ({ state, commit }, args) {
    return Vue.axios.post(`/${args.organization_name}/targets`, JSON.stringify({
      target: {'address': args.address}
    })).then((response) => {
      commit(types.RECEIVE_CREATED_TARGET, { response })
    })
  },
  updateTarget ({ state, commit }, args) {
    return Vue.axios.put(args.target.name, JSON.stringify({
      target: args.target,
      update_mask: args.update_mask
    })).then((response) => {
      commit(types.RECEIVE_UPDATED_TARGET, { response })
    })
  },
  deleteTarget ({ state, commit }, args) {
    return Vue.axios.delete(`/${args.target}`, {}).then((response) => {
      commit(types.DELETE_TARGET)
    })
  }
}

// mutations
const mutations = {
  [types.RECEIVE_TARGETS] (state, { response }) {
    if (response.data.targets) {
      state.targets = response.data.targets
    } else {
      state.targets = []
    }
  },
  [types.SET_TARGET] (state, { target }) {
    if (target) {
      state.target = target
    } else {
      state.target = undefined
    }
  },
  [types.RECEIVE_TARGET] (state, { response }) {
    state.target = response.data
  },
  [types.RECEIVE_CREATED_TARGET] (state, { response }) {
    state.target = response.data
  },
  [types.RECEIVE_UPDATED_TARGET] (state, { response }) {
    state.target = response.data
  },
  [types.DELETE_TARGET] (state, { response }) {
    state.target = {}
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
