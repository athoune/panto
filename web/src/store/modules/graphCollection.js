import * as types from '../mutation-types'

let collections = [
  {
    label: 'My graph collection',
    graphs: [
      {
        label: 'Metric #1',
        type: 'line',
        data: {
          labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
          datasets: [
            {
              label: '# of pings',
              data: [120, 190, 300, 50, 200, 30]
            }
          ]
        }
      },
      {
        label: 'Metric #2',
        type: 'bar',
        data: {
          labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
          datasets: [
            {
              label: '# of Votes',
              data: [12, 19, 13, 5, 20, 13]
            },
            {
              label: '# of topics',
              data: [6, 10, 5, 2, 6, 8]
            }
          ]
        }
      }
    ]
  }
]
// initial state
const state = {
  graphCollections: [],
  graphCollection: {}
}

// getters
const getters = {
  graphCollections: state => state.graphCollections,
  graphCollection: state => state.graphCollection
}

// actions
const actions = {
  listGraphCollections ({ state, commit }, args) {
    // commit(types.RECEIVE_GRAPH_COLLECTIONS, { collections })
  },
  getGraphCollection ({ state, commit }, args) {
    let collection = collections[0]
    commit(types.RECEIVE_GRAPH_COLLECTION, { collection })
  }
}

// mutations
const mutations = {
  [types.RECEIVE_GRAPH_COLLECTIONS] (state, { collections }) {
    state.graphCollections = collections
  },
  [types.RECEIVE_GRAPH_COLLECTION] (state, { collection }) {
    state.graphCollection = collection
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
