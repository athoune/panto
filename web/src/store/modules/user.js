// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import Vue from 'vue'
import * as types from '../mutation-types'
import helpers from '@/helpers'

// initial state
const state = {
  user: {},
  users: [],
  pagination: {}
}

// getters
const getters = {
  user: state => state.user,
  users: state => state.users,
  users_pagination: state => state.pagination
}

// actions
const actions = {
  getUsers ({ state, commit }, filters) {
    Vue.axios.get('/users/', {params: filters}).then((response) => {
      commit(types.RECEIVE_USERS, { response })
    })
  },
  getUser ({ state, commit }, id) {
    Vue.axios.get(`/users/${id}/`, {params: {id: id}}).then((response) => {
      commit(types.RECEIVE_USER, { response })
    })
  },
  saveUser ({ state, commit }) {
    Vue.axios.put(`/users/${state.user.id}/`, state.user).then((response) => {
      commit(types.RECEIVE_USER, { response })
    })
  }
}

// mutations
const mutations = {
  [types.RECEIVE_USERS] (state, { response }) {
    state.users = response.data
    state.pagination = helpers.pagination(response.headers)
  },
  [types.RECEIVE_USER] (state, { response }) {
    state.user = response.data
  },
  [types.UPDATE_USER_VALUE] (state, value) {
    state.user = Object.assign(state.user, value)
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
