import Vue from 'vue'
import * as types from '../mutation-types'

// initial state
const state = {
  tags: [],
  tag: {},
  tag_error: null
}

// getters
const getters = {
  tags: state => state.tags,
  tag: state => state.tag,
  tag_error: state => state.tag_error
}

// actions
const actions = {
  listTags ({ state, commit }, args) {
    Vue.axios.get(`${args.organization}/tags`, JSON.stringify({})).then((response) => {
      commit(types.RECEIVE_TAGS, { response })
    })
  },
  getTag ({ state, commit }, args) {
    Vue.axios.get(`${args.tag}`, {}).then((response) => {
      commit(types.RECEIVE_TAG, { response })
    })
    .catch(error => {
      commit(types.RECEIVE_TAG_ERROR, { error })
    })
  },
  createTag ({ state, commit }, args) {
    Vue.axios.post(`/${args.organization}/tags`, JSON.stringify({
      tag: {
        display_name: args.display_name,
        note: args.note,
        color: args.color
      }
    })).then((response) => {
      commit(types.RECEIVE_CREATED_TAG, { response })
    })
  }
}

// mutations
const mutations = {
  [types.RECEIVE_TAGS] (state, { response }) {
    state.tags = response.data.tags
  },
  [types.RECEIVE_TAG] (state, { response }) {
    state.tag = response.data
  },
  [types.RECEIVE_TAG_ERROR] (state, { error }) {
    state.tag_error = error
  },
  [types.RECEIVE_CREATED_TAG] (state, { response }) {
    state.tag = response.data
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
