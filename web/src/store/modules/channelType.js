import Vue from 'vue'
import * as types from '../mutation-types'

// initial state
const state = {
  channelTypes: []
}

// getters
const getters = {
  channelTypes: state => state.channelTypes
}

// actions
const actions = {
  listChannelTypes ({ state, commit }, args) {
    return Vue.axios.get(`/channeltypes`, JSON.stringify({})).then((response) => {
      commit(types.RECEIVE_CHANNEL_TYPES, { response })
    })
  }
}

// mutations
const mutations = {
  [types.RECEIVE_CHANNEL_TYPES] (state, { response }) {
    if (response.data.channeltypes) {
      state.channelTypes = response.data.channeltypes
    }
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
