// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import Vue from 'vue'
import * as types from '../mutation-types'
import helpers from '@/helpers'

// initial state
const state = {
  current_organization_name: null,
  organization: {},
  organizations: [],
  organizations_pagination: {}
}

// getters
const getters = {
  current_organization_name: state => state.current_organization_name,
  organization: state => state.organization,
  organizations: state => state.organizations,
  organizations_pagination: state => state.organizations_pagination
}

// actions
const actions = {
  listOrganizations ({ state, commit }, args) {
    Vue.axios.get('/organizations', {params: args.filters}).then((response) => {
      commit(types.RECEIVE_ORGANIZATIONS, { response })
      if (args.set_current_organization) {
        commit(types.SET_ORGANIZATION, response.data.organizations[0])
        let name = response.data.organizations[0].name
        commit(types.SET_CURRENT_ORGANIZATION_NAME, { name })
      }
    })
  },
  setCurrentOrganizationName ({ state, commit }, name) {
    commit(types.SET_CURRENT_ORGANIZATION_NAME, { name })
  }
}

// mutations
const mutations = {
  [types.RECEIVE_ORGANIZATIONS] (state, { response }) {
    state.organizations = response.data
    state.organizations_pagination = helpers.pagination(response.headers)
  },
  [types.SET_CURRENT_ORGANIZATION_NAME] (state, { name }) {
    if (name) {
      state.current_organization_name = name
    }
  },
  [types.SET_ORGANIZATION] (state, organization) {
    if (organization) {
      state.organization = organization
    }
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
