import Vue from 'vue'
import * as types from '../mutation-types'

// initial state
const state = {
  states: []
}

// getters
const getters = {
  states: state => state.states
}

// actions
const actions = {
  queryStates ({ state, commit }, args) {
    return Vue.axios.post(`${args.organization}/states/query`, JSON.stringify(
      args.filters
    )).then((response) => {
      commit(types.RECEIVE_STATES, { response })
    })
  }
}

// mutations
const mutations = {
  [types.RECEIVE_STATES] (state, { response }) {
    state.states = response.data.states
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
