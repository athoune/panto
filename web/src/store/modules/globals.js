// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import Vue from 'vue'
import * as types from '../mutation-types'

// initial state
const state = {
  globals: {}
}

// getters
const getters = {
  globals: state => state.globals
}

// actions
const actions = {
  getGlobals ({ state, commit }) {
    if (Object.keys(state.globals).length === 0) {
      Vue.axios.get('/globals/').then((response) => {
        commit(types.RECEIVE_GLOBALS, { response })
      })
    }
  }
}

// mutations
const mutations = {
  [types.RECEIVE_GLOBALS] (state, { response }) {
    state.globals = response.data
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
