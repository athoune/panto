import Vue from 'vue'
import * as types from '../mutation-types'

// initial state
const state = {
  checks: [],
  check: {}
}

// getters
const getters = {
  checks: state => state.checks,
  check: state => state.check
}

// actions
const actions = {
  listChecks ({ state, commit }, args) {
    return Vue.axios.get(`/${args.organization}/checks`, {params: {...args.filters, page_size: 100}}).then((response) => {
      commit(types.RECEIVE_CHECKS, { response })
    })
  },
  getCheck ({ state, commit }, args) {
    return Vue.axios.get(`${args.check}`, {
      params: {children_mask: args.children_mask}
    }).then((response) => {
      commit(types.RECEIVE_CHECK, { response })
    })
  },
  createCheck ({ state, commit }, args) {
    return Vue.axios.post(`/${args.organization}/checks`, JSON.stringify({
      check: {
        target: args.target,
        probe: args.probe,
        type: args.type,
        configuration: JSON.stringify(args.configuration)
      }
    })).then((response) => {
      commit(types.RECEIVE_CREATED_CHECK, { response })
    })
  },
  updateCheck ({ state, commit }, args) {
    return Vue.axios.put(`/${args.name}`, JSON.stringify({
      check: {
        target: args.target_name,
        type: args.type,
        configuration: JSON.stringify(args.configuration)
      },
      update_mask: {
        paths: ['target', 'type', 'configuration']
      }
    })).then((response) => {
      commit(types.RECEIVE_CHECK, { response })
    })
  },
  setCheck ({ state, commit }, check) {
    commit(types.SET_CHECK, { check })
  }
}

// mutations
const mutations = {
  [types.RECEIVE_CHECKS] (state, { response }) {
    // console.log(JSON.parse(JSON.stringify(response.data.checks)))
    if (response.data.checks) {
      response.data.checks.forEach((check) => {
        if (check.configuration) {
          check.configuration = JSON.parse(check.configuration)
        }
      })
      state.checks = response.data.checks
    }
  },
  [types.RECEIVE_CHECK] (state, { response }) {
    if (response.data.configuration) {
      response.data.configuration = JSON.parse(response.data.configuration)
    }
    state.check = response.data
  },
  [types.RECEIVE_CREATED_CHECK] (state, { response }) {
    if (response.data.configuration) {
      response.data.configuration = JSON.parse(response.data.configuration)
    }
    state.check = response.data
  },
  [types.SET_CHECK] (state, { check }) {
    state.check = check
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
