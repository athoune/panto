# -*- mode: ruby -*-
# vi: set ft=ruby :
# Copyright 2017 Pantomath SAS
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

Vagrant.configure("2") do |config|
  config.vm.box = "debian/stretch64"

  config.ssh.forward_agent = true

  config.vm.network "private_network", type: "dhcp"

  config.vm.synced_folder "../.", "/home/vagrant/go/src/gitlab.com/pantomath-io/panto", type: "nfs"

  config.vm.provider "virtualbox" do |vb|
    vb.name = "panto"
    vb.memory = "2048"
    vb.cpus = "2"
  end

  # provisioning with ansible, the setup part
  config.vm.provision "ansible" do |ansible|
    ansible.compatibility_mode = "2.0"
    ansible.galaxy_role_file = "../ansible/requirements.yml"
    ansible.playbook = "../ansible/playbook-setup.yml"
  end

  # forward default API ports for testing from the host
  config.vm.network :forwarded_port, guest: 7575, host: 7575   # gRPC
  config.vm.network :forwarded_port, guest: 7576, host: 7576   # REST
  config.vm.network :forwarded_port, guest: 8080, host: 8080   # pantoweb
end
