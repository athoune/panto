// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"fmt"
	"path/filepath"
	"strconv"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"gitlab.com/pantomath-io/panto/cmd/panto-ctl/ask"
	"gitlab.com/pantomath-io/panto/cmd/panto-ctl/conf"
	"gitlab.com/pantomath-io/panto/server/dbconf"
	"gitlab.com/pantomath-io/panto/util"
)

var db dbconf.ConfigurationDB

// dbconfCmd represents the dbconf command
var dbconfCmd = &cobra.Command{
	Use:          "dbconf",
	Short:        "manage the configuration database",
	Long:         `Init, upgrade, check the configuration database from command line`,
	SilenceUsage: true,
	PersistentPreRunE: func(cmd *cobra.Command, args []string) (err error) {
		for _, o := range conf.DBConfOptions {
			if err := util.BindOption(o, cmd.Flags()); err != nil {
				panic(fmt.Sprint("failed to bind option:", err))
			}
		}

		// Load configuration file
		viper.SetConfigName("panto")

		confPathFlag := cmd.Flags().Lookup("conf")
		if confPathFlag != nil && confPathFlag.Changed {
			confPath, err := util.ExpandAbs(confPathFlag.Value.String())
			if err != nil {
				return err
			}
			viper.SetConfigFile(confPath)
		}

		viper.AddConfigPath(filepath.Join("/etc", "panto"))
		viper.AddConfigPath(filepath.Join("etc", "panto"))

		if err = viper.ReadInConfig(); err != nil && confPathFlag != nil && confPathFlag.Changed {
			return fmt.Errorf("unable to read configuration file: %s", err)
		}

		if !viper.IsSet("db.path") || viper.GetString("db.path") == "" {
			return fmt.Errorf("no configuration database file: use --db-path or specify a configuration file")
		}
		dbPath := viper.GetString("db.path")
		dbPath, err = util.ExpandAbs(dbPath)
		if err != nil {
			return err
		}
		db, err = dbconf.NewSQLiteDB(dbPath)
		if err != nil {
			return err
		}
		fmt.Printf("Using configuration database file: %s\n", dbPath)

		return nil
	},
}

// dbconfVersionCmd displays current version of the dbconf
var dbconfVersionCmd = &cobra.Command{
	Use:   "version",
	Short: "displays the current version of the configuration database",
	Long:  `Displays the current version of the configuration database.`,
	RunE: func(cmd *cobra.Command, args []string) error {
		dbconfVersion, err := dbconf.GetVersion(db)
		if err != nil {
			return fmt.Errorf("unable to get configuration database version: %s", err)
		}

		fmt.Printf("Configuration database is currently at version: %d\n", dbconfVersion)
		return nil
	},
	SilenceUsage: true,
}

// dbconfIsVersionOKCmd checks if current version of the dbconf is th configured one
var dbconfIsVersionOKCmd = &cobra.Command{
	Use:   "check",
	Short: "makes sure the current version of the configuration database is the configured one",
	Long:  `Makes sure the current version of the configuration database is the configured one.`,
	RunE: func(cmd *cobra.Command, args []string) error {
		dbconfVersionOK, err := dbconf.IsVersion(db, dbconf.RequiredVersion())
		if err != nil {
			return fmt.Errorf("unable to test configuration database version: %s", err)
		}
		if !dbconfVersionOK {
			dbconfVersion, err := dbconf.GetVersion(db)
			if err != nil {
				return fmt.Errorf("unable to get configuration database version: %s", err)
			}

			return fmt.Errorf(
				"the configuration database (version %d) is not up to date (version %d)",
				dbconfVersion,
				dbconf.RequiredVersion(),
			)
		}

		fmt.Printf("The database is up to date (version %d)\n", dbconf.RequiredVersion())
		return nil
	},
	SilenceUsage: true,
}

// dbconfPrintMigrationsCmd allows to print the list of migrations to apply on the dbconf
var dbconfPrintMigrationsCmd = &cobra.Command{
	Use:   "list-migrations",
	Short: "lists the pending migrations to be applied on the configuration database",
	Long:  `Lists all the pending migrations to be applied on the configuration database.`,
	RunE: func(cmd *cobra.Command, args []string) error {
		dbconfVersion, err := dbconf.GetVersion(db)
		if err != nil {
			return fmt.Errorf("unable to get configuration database version: %s", err)
		}

		printAvailableMigrations(dbconfVersion, dbconf.RequiredVersion())
		return nil
	},
	SilenceUsage: true,
}

// dbconfUpgradeCmd allows to apply migrations on the dbconf
var dbconfUpgradeCmd = &cobra.Command{
	Use:   "upgrade [version]",
	Short: "applies all the migrations to the configuration database (except if [version] is provided)",
	Long:  `Applies all the migrations on the configuration database.`,
	RunE: func(cmd *cobra.Command, args []string) error {
		var versionTarget int64
		var err error

		dbconfVersion, err := dbconf.GetVersion(db)
		if err != nil {
			return fmt.Errorf("unable to get configuration database version: %s", err)
		}

		if len(args) == 0 {
			// upgrade to the version of the configuration file
			versionTarget = dbconf.RequiredVersion()

			printAvailableMigrations(dbconfVersion, versionTarget)
			if !askIfContinue() {
				return nil
			}

			err := dbconf.Migrate(db)
			if err != nil {
				return fmt.Errorf("unable to apply migration scripts to configuration database: %s", err)
			}

		} else if len(args) == 1 {
			// upgrade to the specified version
			versionTarget, err = strconv.ParseInt(args[0], 10, 64)
			if err != nil {
				return fmt.Errorf("argument is not integer: %s", args[0])
			}

			printAvailableMigrations(dbconfVersion, versionTarget)
			if !askIfContinue() {
				return nil
			}

			err := dbconf.MigrateTo(db, versionTarget)
			if err != nil {
				return fmt.Errorf("unable to apply migration scripts to configuration database: %s", err)
			}
		} else {
			return fmt.Errorf("too many arguments")
		}
		return nil
	},
	SilenceUsage: true,
}

// dbconfDowngradeCmd allows to revert last migration on the dbconf
var dbconfDowngradeCmd = &cobra.Command{
	Use:   "downgrade",
	Short: "reverts the last migration to the configuration database.",
	Long:  `Reverts the last migration on the configuration database.`,
	RunE: func(cmd *cobra.Command, args []string) error {
		var err error

		dbconfVersion, err := dbconf.GetVersion(db)
		if err != nil {
			return fmt.Errorf("unable to get configuration database version: %s", err)
		}

		printLastMigrations(dbconfVersion)
		if !askIfContinue() {
			return nil
		}

		err = dbconf.Downgrade(db)
		if err != nil {
			return fmt.Errorf("unable to apply migration scripts to configuration database: %s", err)
		}

		return nil
	},
	SilenceUsage: true,
}

func init() {
	rootCmd.AddCommand(dbconfCmd)
	dbconfCmd.AddCommand(
		dbconfVersionCmd,
		dbconfIsVersionOKCmd,
		dbconfPrintMigrationsCmd,
		dbconfUpgradeCmd,
		dbconfDowngradeCmd,
	)

	for _, o := range conf.DBConfOptions {
		if err := util.AddOption(o, dbconfCmd.PersistentFlags()); err != nil {
			panic(fmt.Sprint("failed to add option:", err))
		}
	}
}

// ************************* Helpers *************************//
func printAvailableMigrations(current, target int64) {
	list, err := dbconf.ListPendingMigrations(current, target)
	if err != nil {
		return
	}

	if len(list) == 0 {
		fmt.Printf("No pending migration\n")
	} else {
		if len(list) > 1 {
			fmt.Printf("Pending migrations:\n")
		} else {
			fmt.Printf("Pending migration:\n")
		}
		for _, migration := range list {
			fmt.Printf("\t%s\n", migration.Name)
		}
	}
}

func printLastMigrations(current int64) {
	list, err := dbconf.ListLastMigration(current)
	if err != nil {
		return
	}

	if len(list) == 0 {
		fmt.Printf("No pending migration\n")
	} else {
		if len(list) > 1 {
			fmt.Printf("Pending migrations:\n")
		} else {
			fmt.Printf("Pending migration:\n")
		}
		for _, migration := range list {
			fmt.Printf("\t%s\n", migration.Name)
		}
	}
}

func askIfContinue() (b bool) {
	res, err := ask.Bool("Do you want to continue?").
		Default(true).
		Ask()
	if err != nil {
		fmt.Print(err.Error())
		return false
	}

	return res
}
