// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package conf

import (
	"gitlab.com/pantomath-io/panto/util"
)

// InitOptionsPersistent is the list of options for the `panto-ctl init` command and subcommands
var InitOptionsPersistent = []util.Option{
	{
		Name:    "dry-run",
		Default: false,
		Usage:   "do not perform any operations, just output what would be done",
		Flag:    "dry-run",
		Short:   "n",
	},
	{
		Name:    "verbose",
		Default: false,
		Usage:   "write activity to stdout",
		Flag:    "verbose",
		Short:   "v",
	},
}

// InitOptions is the list of options for the `panto-ctl init` command
var InitOptions = []util.Option{
	{
		Name:    "install-prefix",
		Default: "/",
		Usage:   "the default prefix for the Panto installation",
		Flag:    "install-prefix",
	},
}

// InitConfigOptions is the list of options for the `panto-ctl init config` command
var InitConfigOptions = []util.Option{
	{
		Name:    "data-path",
		Default: "",
		Usage:   "the path to store data files to",
		Flag:    "data-path",
	},
	{
		Name:    "db-path",
		Default: "",
		Usage:   "the path of the SQLite database file",
		Flag:    "db-path",
	},
	{
		Name:    "conf-path",
		Default: "",
		Usage:   "the path to store the configuration file to",
		Flag:    "conf-path",
	},
	{
		Name:    "log-path",
		Default: "",
		Usage:   "the path of a file to log Panto output",
		Flag:    "log-path",
	},
	{
		Name:    "grpc-address",
		Default: "",
		Usage:   "the address to bind the gRPC API server to",
		Flag:    "grpc-address",
	},
	{
		Name:    "rest-address",
		Default: "",
		Usage:   "the address to bind the REST API server to",
		Flag:    "rest-address",
	},
	{
		Name:    "certfile",
		Default: "",
		Usage:   "the path of a PEM certificate file",
		Flag:    "certfile",
	},
	{
		Name:    "certkey",
		Default: "",
		Usage:   "the path of a certificate key file",
		Flag:    "certkey",
	},
}

// InitDataOptions is the list of options for the `panto-ctl init data` command
var InitDataOptions = []util.Option{
	{
		Name:    "db-path",
		Default: "",
		Usage:   "the path of the SQLite database file",
		Flag:    "db-path",
	},
	{
		Name:    "organization-name",
		Default: "",
		Usage:   "the name of the initial organization",
		Flag:    "organization-name",
	},
}

// DBConfOptions is the list of options for the `panto-ctl dbconf` command
var DBConfOptions = []util.Option{
	{
		Name:    "db.path",
		Default: "",
		Usage:   "path to the SQLite database file",
		Flag:    "db-path",
	},
	{
		Default: "",
		Usage:   "path to the configuration file",
		Flag:    "conf",
	},
}
