// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

type configTemplate struct {
	LogPath              string
	GRPCAddress          string
	RESTAddress          string
	AllowOrigin          string
	CertFile, CertKey    string
	DBPath               string
	InfluxDBAddress      string
	InfluxDBDatabaseName string
	SMTPServer           string
	SMTPPort             int
	SMTPUsername         string
	SMTPPassword         string
	SMTPFrom             string
}

var configTemplateString = `### Panto configuration file

# This is a configuration file for the Panto server. Uncomment and override
# the template values to change the Panto configuration. Configuration values
# will change the next time the server is restarted.

# WARNING: All paths can be absolute or relative. Absolute paths are highly
# recommended, as relative paths will depend on the working directory when
# Panto was launched.

### Log configuration
# file - A path of a file to log to
# syslog - If true, logs will be sent to syslog.
{{with .LogPath -}}
log:
  file: {{js . | printf "%q"}}
{{- else -}}
# log:
#   file: "/var/log/panto/panto.log"
#   syslog: true
{{- end}}

### Server options
# grpc-address - the local address to bind the gRPC API server to (e.g. '127.0.0.1:7575', ':2018')
# rest-address - the local address to bind the REST API server to (e.g. '127.0.0.1:7576', ':2019')
# allow-origin - a comma-separated list of allowed origin hosts for CORS
# certfile - the location of a PEM certificate file
# certkey - the location of the corresponding PEM private key file
# no-tls - set to true to use insecure transport (no TLS)
server:
{{- with .GRPCAddress}}
  grpc-address: {{js . | printf "%q"}}
{{- else}}
# grpc-address: ":7575"
{{- end -}}
{{- with .RESTAddress}}
  rest-address: {{js . | printf "%q"}}
{{- else}}
# rest-address: "0.0.0.0"
{{- end}}
# allow-origin: "http://localhost:8080,http://127.0.0.1:8080,http://localhost:8000,http://127.0.0.1:8000"
{{- with .AllowOrigin}}
  allow-origin: {{js . | printf "%q"}}
{{- end -}}
{{- if print .CertFile .CertKey}}
  certfile: {{js .CertFile | printf "%q"}}
  certkey: {{js .CertKey | printf "%q"}}
# no-tls: false
{{- else}}
# certfile: "/etc/panto/fullchain.pem"
# certkey: "/etc/panto/privkey.pem"
  no-tls: true
{{- end}}

### Configuration database options
# path - the path to the SQLite database file
db:
{{- with .DBPath}}
  path: {{js . | printf "%q"}}
{{- else}}
# path: "/var/lib/panto/panto.sqlite"
{{- end}}

### InfluxDB configuration
# address - a URL where the InfluxDB instance can be reached
# database - the name of the database to use
influxdb:
{{- with .InfluxDBAddress}}
  address: {{js . | printf "%q"}}
{{- else}}
# address: "http://localhost:8086"
{{- end -}}
{{- with .InfluxDBDatabaseName}}
  database: {{js . | printf "%q"}}
{{- else}}
# database: "panto"
{{- end}}

### SMTP server options
### The SMTP server configuration is used in e-mail alerts
# server - the address of the SMTP server
# port - the port of the SMTP server
# username - the username used to authenticate with the SMTP server
# password - the password used to authenticate with the SMTP server
# from - email address to send the mails from
smtp:
{{- with .SMTPServer}}
  server: {{js . | printf "%q"}}
{{- else}}
# server: "smtp.example.com"
{{- end -}}
{{- with .SMTPPort}}
  port: {{.}}
{{- else}}
# port: 587
{{- end -}}
{{- with .SMTPUsername}}
  username: {{js . | printf "%q"}}
{{- else}}
# username: "username"
{{- end -}}
{{- with .SMTPPassword}}
  password: {{js . | printf "%q"}}
{{- else}}
# password: "password"
{{- end}}
{{- with .SMTPFrom}}
  from: {{js . | printf "%q"}}
{{- else}}
# from: "hello@panto.app"
{{- end -}}
`
