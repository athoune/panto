// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"encoding/json"
	"fmt"
	"os"
	"os/signal"
	"path/filepath"
	"runtime"
	"syscall"

	"gitlab.com/pantomath-io/panto/api"
	"gitlab.com/pantomath-io/panto/cmd/panto/conf"
	"gitlab.com/pantomath-io/panto/server"
	"gitlab.com/pantomath-io/panto/util"

	"github.com/op/go-logging"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

// Create a new instance of the logger
var log = logging.MustGetLogger("main")

// program ID
var (
	family  = "panto"
	program = "panto"
	version string
	githash string
)

// configureLogger chooses the backend(s) to set
func configureLogger() {
	// Configure log level, select most verbose first
	verbose := viper.GetInt("verbose")
	if verbose > 3 {
		verbose = 3
	}
	var logLevel logging.Level
	switch verbose {
	case 0:
		// silence output, don't configure any loggers
		logging.SetBackend()
		return
	case 1:
		logLevel = logging.WARNING
	case 2:
		logLevel = logging.INFO
	case 3:
		logLevel = logging.DEBUG
	default:
		logLevel = logging.WARNING
	}

	var backendList = make([]logging.Backend, 0, 3)

	var format = logging.MustStringFormatter(
		`%{color}%{time:15:04:05.000} [%{level:.4s}] %{shortfunc}%{color:reset}: %{message} (%{shortfile})`,
	)

	// Configure console logger
	consoleBackend := logging.NewLogBackend(os.Stderr, "", 0)
	consoleBackendFormatter := logging.NewBackendFormatter(consoleBackend, format)
	consoleBackendLeveled := logging.AddModuleLevel(consoleBackendFormatter)
	consoleBackendLeveled.SetLevel(logLevel, "")
	backendList = append(backendList, consoleBackendLeveled)

	// Configure file logger
	if viper.IsSet("log.file") && viper.GetString("log.file") != "" {
		var fileformat = logging.MustStringFormatter(
			`%{time:2006-01-02T15:04:05.000} [%{level:.4s}] %{shortfunc}: %{message}`,
		)

		path, err := util.ExpandAbs(viper.GetString("log.file"))
		if err != nil {
			log.Errorf("error getting log file path: %s", err)
			goto Syslog
		}

		f, err := os.OpenFile(path, os.O_APPEND|os.O_CREATE|os.O_RDWR, 0644)
		if err != nil {
			log.Errorf("Error opening log file: %v", err)
		} else {
			filebackend := logging.NewLogBackend(f, "", 0)
			filebackendFormatter := logging.NewBackendFormatter(filebackend, fileformat)
			filebackendLeveled := logging.AddModuleLevel(filebackendFormatter)
			filebackendLeveled.SetLevel(logLevel, "")

			backendList = append(backendList, filebackendLeveled)
		}
	}

Syslog:
	// Configure syslog logger
	if viper.IsSet("log.syslog") && viper.GetBool("log.syslog") {
		syslogBackend, err := logging.NewSyslogBackend("")
		if err != nil {
			log.Errorf("Error with syslog: %v", err)
		}
		backendList = append(backendList, syslogBackend)
	}

	logging.SetBackend(backendList...)
}

// configureFlags configures all the command-line flags
func configureFlags() error {
	for _, o := range conf.Options {
		if err := util.AddOption(o, pflag.CommandLine); err != nil {
			return fmt.Errorf("failed to add option: %s", err)
		}
		if err := util.BindOption(o, pflag.CommandLine); err != nil {
			return fmt.Errorf("failed to bind option: %s", err)
		}
	}

	pflag.CommandLine.SortFlags = false

	pflag.Usage = func() {
		fmt.Fprintln(os.Stderr, "Usage:")
		fmt.Fprintf(os.Stderr, "%s [flags]\n", program)
		fmt.Fprintln(os.Stderr)
		fmt.Fprintln(os.Stderr, "Flags:")
		pflag.PrintDefaults()
		fmt.Fprintln(os.Stderr)
		fmt.Fprintln(os.Stderr, "Configuration file:")
		fmt.Fprintf(os.Stderr, "More options can be configured using a configuration file.\n")
		fmt.Fprintf(os.Stderr, "%s will look for a configuration file in the following locations:\n", program)
		fmt.Fprintf(os.Stderr, "  * at the path specified on the command line, if present\n")
		fmt.Fprintf(os.Stderr, "  * /etc/%s/%s.yaml\n", family, program)
		fmt.Fprintf(os.Stderr, "  * <PWD>/etc/%s/%s.yaml\n", family, program)
		fmt.Fprintf(os.Stderr, "Options set on the command line will override options from the configuration file\n")
		fmt.Fprintln(os.Stderr)
	}

	pflag.Parse()

	// if --quiet, silence all output
	quietFlag := pflag.Lookup("quiet")
	if quietFlag != nil && quietFlag.Value.String() == "true" {
		viper.Set("verbose", 0)
	}

	return nil
}

// loadConfiguration reads config file and set default values
func loadConfiguration() error {
	viper.SetConfigName(program) // config file named panto.yaml

	confPathFlag := pflag.Lookup("conf")
	if confPathFlag != nil && confPathFlag.Changed {
		confPath, err := util.ExpandAbs(confPathFlag.Value.String())
		if err != nil {
			return fmt.Errorf("could not resolve configuration file path: %s", err)
		}
		viper.SetConfigFile(confPath)
	}

	viper.AddConfigPath(filepath.Join("/etc", family)) // config file in /etc/panto
	viper.AddConfigPath(filepath.Join("etc", family))  // config file in $PWD/etc/panto

	err := viper.ReadInConfig()
	if err != nil && confPathFlag != nil && confPathFlag.Changed {
		return fmt.Errorf("could not load config file: %s", err)
	}

	return nil
}

// sigHandler manages OS signals:
// os.Interrupt and SIGTERM exits the program
func sigHandler() {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)

	for sig := range c {
		log.Debug("Received signal ", sig)

		switch sig {
		case syscall.SIGTERM, os.Interrupt:
			log.Info("Bye bye")
			os.Exit(0)
		}
	}
}

func main() {
	// Command line management
	if err := configureFlags(); err != nil {
		log.Critical(err.Error())
		os.Exit(1)
	}

	versionString := fmt.Sprintf("%s %s-%s", program, version, githash)
	versionFlag := pflag.Lookup("version")
	if versionFlag != nil && versionFlag.Value.String() == "true" {
		fmt.Println(versionString)
		os.Exit(0)
	}

	if err := loadConfiguration(); err != nil {
		log.Critical(err.Error())
		os.Exit(1)
	}

	configureLogger()

	log.Infof(versionString)

	jsonSettings, _ := json.Marshal(viper.AllSettings())
	log.Debugf("Configuration: %s", jsonSettings)

	// Set number of parallel threads to number of CPUs.
	runtime.GOMAXPROCS(runtime.NumCPU())

	// manage signals
	go sigHandler()

	certFilePath, err := util.ExpandAbs(viper.GetString("server.certfile"))
	if err != nil {
		log.Criticalf("unable to resolve TLS certificate file path: %s", err)
		os.Exit(1)
	}
	keyFilePath, err := util.ExpandAbs(viper.GetString("server.certkey"))
	if err != nil {
		log.Criticalf("unable to resolve TLS key file path: %s", err)
		os.Exit(1)
	}

	// start API
	err = server.Serve(
		server.Configuration{
			GRPCAddress:  viper.GetString("server.grpc-address"),
			RESTAddress:  viper.GetString("server.rest-address"),
			AllowOrigin:  viper.GetString("server.allow-origin"),
			CertFilePath: certFilePath,
			KeyFilePath:  keyFilePath,
			NoTLS:        viper.GetBool("server.no-tls"),
		},
		api.Info{
			Version:     version,
			WithTls:     !viper.GetBool("server.no-tls"),
			GrpcAddress: viper.GetString("server-info.public-grpc-address"),
			RestAddress: viper.GetString("server-info.public-rest-address"),
		},
	)

	if err != nil {
		log.Fatalf("Panto server stopped: %s", err)
	}
}
