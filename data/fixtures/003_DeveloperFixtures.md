# Developer fixtures

The developer fixtures are a set of SQLite and InfluxDB fixtures for quickly deploying a sensible Panto environment, so that developers can observe and play around with a decent dataset.

> :warning: **WARNING:** The fixtures will overwrite your current dataset. Make sure you backup any critical data before continuing.

To deploy the fixtures on your current setup:

```shell
cat data/fixtures/003_DeveloperFixtures.sql | sqlite3 data/panto.sqlite
influxd restore -portable -db panto data/fixtures/003_DeveloperFixtures.influx
```

This assumes your InfluxDB database is named `panto`. To rename a InfluxDB database, open a `influx` CLI and type:

```
CREATE DATABASE panto2;
SELECT * INTO "panto2"."autogen".:MEASUREMENT FROM "panto"."autogen"./.*/ GROUP BY *;
```
