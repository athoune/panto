-- Copyright 2017 Pantomath SAS
-- 
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
-- 
--   http://www.apache.org/licenses/LICENSE-2.0
-- 
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.

-- These are developer fixtures. To be used to set up a complete mock environment.
-- Use in pair with the TSDB fixtures.
--
-- WARNING: This erases everything in your DB when applied. Use with caution.

BEGIN TRANSACTION;

-- Create Organization named "Pantomath"
DELETE FROM `organization`;
INSERT INTO `organization` (id, uuid, name) VALUES (1, '903b49cf-c12a-4340-bb2a-0385eaac317e', 'Pantomath');

-- Create Target named "target.pantomath.io"
DELETE FROM `target`;
INSERT INTO `target` (id, uuid, address, organization) VALUES (1, '31017478-825c-459c-8406-01bfb1d2cf58', 'target.pantomath.io', 1);

-- Create 2 Tags "production", "preprod"
DELETE FROM `tag`;
INSERT INTO `tag` (id, slug, display_name, note, color, organization) VALUES (1, 'production', 'Production', 'Production server', '#a9200e', 1);
INSERT INTO `tag` (id, slug, display_name, note, color, organization) VALUES (2, 'pre-production', 'Pre-production', 'Pre-production server', '#cb4a1a', 1);

-- Link "Production" tag to target
DELETE FROM `target_tag`;
INSERT INTO `target_tag` (target, tag) VALUES (1, 1);

-- Create Agent name "agent.pantomath.io"
DELETE FROM `agent`;
INSERT INTO `agent` (id, uuid, display_name, organization) VALUES (1, '9e82e551-ccee-489a-a4c8-81329e70a8a3', 'agent.pantomath.io', 1);

-- Create Checks and ProbeConfigurations
DELETE FROM `target_probe`;
DELETE FROM `target_probe_agent`;

-- -- Ping: 8.8.8.8 every minute
INSERT INTO `target_probe` (id, uuid, target, probe) VALUES (1, 'efc1e4be-66b1-472b-a2fc-b77e44c1c4a3', 1, 1);
INSERT INTO `target_probe_agent` (target_probe, agent, uuid, schedule, probe_configuration) VALUES (1, 1, '79984d4c-03be-4d89-bdef-b90eb1011bed', 60000000000, '{"address":"8.8.8.8"}');

-- -- CPU: every minute
INSERT INTO `target_probe` (id, uuid, target, probe) VALUES (2, '9b79cd15-24e8-4436-971a-285bfabc518f', 1, 3);
INSERT INTO `target_probe_agent` (target_probe, agent, uuid, schedule, probe_configuration) VALUES (2, 1, 'ed222b3d-a4be-433d-9b19-eb445bd1fd49', 60000000000, '{"per-cpu":false}');

-- -- Disk: every 5 minutes
INSERT INTO `target_probe` (id, uuid, target, probe) VALUES (3, '4c23312a-5585-47d6-90fa-eb9ef2146d2e', 1, 4);
INSERT INTO `target_probe_agent` (target_probe, agent, uuid, schedule, probe_configuration) VALUES (3, 1, 'f0072387-6a24-45c6-bca9-ea8fbdb49bbb', 300000000000, '{}');

-- -- Load: every minute
INSERT INTO `target_probe` (id, uuid, target, probe) VALUES (4, 'ddcb3e6a-4bcd-4c7b-afa5-044b3e455652', 1, 6);
INSERT INTO `target_probe_agent` (target_probe, agent, uuid, schedule, probe_configuration) VALUES (4, 1, 'ddcb3e6a-4bcd-4c7b-afa5-044b3e455652', 60000000000, '{}');

-- -- Memory: every 3 minutes
INSERT INTO `target_probe` (id, uuid, target, probe) VALUES (5, 'f75a603e-e869-4d17-a05b-010f2bb36701', 1, 7);
INSERT INTO `target_probe_agent` (target_probe, agent, uuid, schedule, probe_configuration) VALUES (5, 1, '6822811a-0d8d-4b26-811e-a844847b4582', 180000000000, '{}');

-- -- Network: every 5 minutes
INSERT INTO `target_probe` (id, uuid, target, probe) VALUES (6, '43ae3b90-8ce6-44be-a80f-84180e0d6af7', 1, 8);
INSERT INTO `target_probe_agent` (target_probe, agent, uuid, schedule, probe_configuration) VALUES (6, 1, 'd4246674-17e7-4c74-b7e2-52692314ac35', 300000000000, '{}');

-- Configure Checks: state conditions

-- -- Ping
UPDATE `target_probe` SET (state_type, state_configuration) = (
    'threshold', 
    '{"states":[{"state":"critical","match":"any","conditions":[{"field":"avg","operator":"gt","value":40000000,"reason":"Ping on 8.8.8.8 too high"},{"field":"avg","operator":"le","value":0,"reason":"Ping on 8.8.8.8 is 0"}]},{"state":"warning","match":"all","conditions":[{"field":"avg","operator":"gt","value":10000000,"reason":"Ping on 8.8.8.8 is slow"}]}]}'
) WHERE id = 1;

-- -- CPU
UPDATE `target_probe` SET (state_type, state_configuration) = (
    'threshold', 
    '{"states":[{"state":"critical","match":"any","conditions":[{"field":"cpu_all","operator":"gt","value":80,"reason":"CPU utilization is too high"}]},{"state":"warning","match":"any","conditions":[{"field":"cpu_all","operator":"gt","value":50,"reason":"CPU utilization is high"}]}]}'
) WHERE id = 2;

-- -- Disk
UPDATE `target_probe` SET (state_type, state_configuration) = (
    'trend', 
    '{"interval":"1m","states":[{"state":"warning","match":"all","conditions":[{"field":"used-percent","operator":"ge","value":1,"reason":"Disk used space is increasing at over 1% per minute"}]}]}'
) WHERE id = 3;
UPDATE `target_probe` SET (state_type, state_configuration) = (
    'threshold', 
    '{"states":[{"state":"critical","match":"all","conditions":[{"field":"used-percent","operator":"ge","value":90,"reason":"Disk used space is over 90%"}]},{"state":"warning","match":"all","conditions":[{"field":"used-percent","operator":"ge","value":50,"reason":"Disk used space is over 50%"}]}]}'
) WHERE id = 3;

-- -- Load
UPDATE `target_probe` SET (state_type, state_configuration) = (
    'none', 
    '{}'
) WHERE id = 4;

-- -- Memory
UPDATE `target_probe` SET (state_type, state_configuration) = (
    'threshold', 
    '{"states":[{"state":"warning","match":"any","conditions":[{"field":"used-percent","operator":"ge","value":70,"reason":"Used memory is over 70%"},{"field":"available","operator":"lt","value":1000000000,"reason":"Available memory is lower than 1 GB"}]}]}'
) WHERE id = 5;

-- -- Load
UPDATE `target_probe` SET (state_type, state_configuration) = (
    'none', 
    '{}'
) WHERE id = 6;

-- Alert channel on slack
DELETE FROM `channel`;
INSERT INTO `channel` (id, uuid, organization, display_name, type, configuration)
    VALUES (1, '1643295c-fb00-49e6-9432-f21afea6e0dc', 1, 'Panto slack alert', 'slack', '{"webhook_url":"https://hooks.slack.com/services/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"}');

-- Alert for a state change on all checks
DELETE FROM `alert`;
DELETE FROM `target_probe_alert`;
INSERT INTO `alert` (id, uuid, type, configuration, channel) VALUES (1, 'ff9881aa-77ce-4663-92ce-602bd2379965', 'state_change', '{}', 1);
INSERT INTO `target_probe_alert` (target_probe, alert) VALUES ((SELECT id FROM target_probe), 1);

COMMIT TRANSACTION;
