-- Copyright 2017 Pantomath SAS
-- 
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
-- 
--   http://www.apache.org/licenses/LICENSE-2.0
-- 
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.

INSERT INTO `client` (uuid,label,price_plan) VALUES ('f6e48ddb-e94c-4916-baf3-bf5dbe3a2fb3','Pantomath SAS',0);
INSERT INTO `organization` (uuid,name,client) VALUES ('903b49cf-c12a-4340-bb2a-0385eaac317e','pantomath',1);
INSERT INTO `agent` (uuid,display_name,organization) VALUES ('9e82e551-ccee-489a-a4c8-81329e70a8a3','agent.pantomath.io',1);

INSERT INTO `channel` (uuid, organization, type, configuration) VALUES ('e559e715-db7d-4023-a7be-965923fac36c', 1, 'email', '{"dest":"hello@pantomath.io"}');
INSERT INTO `target` (uuid, address, organization) VALUES ('31017478-825c-459c-8406-01bfb1d2cf58', 'localhost', 1);
INSERT INTO `target_probe` (uuid, target, probe, state_type, state_configuration) VALUES ('8b6418b5-9055-4a7b-978c-b85086e015eb', 1, 1, 'threshold', '{"states": [{"state": "critical","match": "any","conditions": [{"field": "avg","operator": "gt","value": 40000000,"reason": "way too high"},{"field": "avg","operator": "le","value": 0,"reason": "too small to be true"}]},{"state": "warning","match": "all","conditions": [{"field": "avg","operator": "gt","value": 10000000,"reason": "too high"}]}]}');
INSERT INTO `target_probe_agent` (target_probe, agent, uuid, probe_configuration, schedule) VALUES (1, 1, 'fca8c20a-783f-4f71-96c5-efbf96a1f73d', '{"address":"localhost"}', 1000000000);

INSERT INTO `tag` (name, note, color, organization) VALUES ('production', 'Production server', '#0000ff', 1);
INSERT INTO `target_tag` (target, tag) VALUES (1, 1);

INSERT INTO `alert` (uuid, type, configuration, channel) VALUES ('6ba644a7-fa80-4d2d-b3fc-ed96086d60be', 'state_recurrence', '{"recurrence": 3}', 1);
INSERT INTO `target_probe_alert` (target_probe, alert) VALUES (1, 1);
