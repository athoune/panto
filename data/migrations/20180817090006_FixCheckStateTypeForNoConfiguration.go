// Copyright 2018 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package migrations

import (
	"github.com/loderunner/goose"
)

func init() {
	goose.AddMigration(Up20180817090006, Down20180817090006)
}

// Up20180817090006 sets the type of state to none when the configuration is empty
func Up20180817090006(qe goose.QueryExecer) error {
	_, err := qe.Exec(`
UPDATE target_probe
SET state_type='none'
WHERE state_configuration LIKE '{}' AND state_type='threshold';
`)
	if err != nil {
		return err
	}
	return nil
}

// Down20180817090006 sets the type of state to none when the configuration is empty
func Down20180817090006(qe goose.QueryExecer) error {
	return nil
}
