// Copyright 2018 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package migrations

import (
	"github.com/loderunner/goose"
)

func init() {
	goose.AddMigration(Up20180817090005, Down20180817090005)
}

// Up20180817090005 updates empty state_configuration in target_probe to the right format
func Up20180817090005(qe goose.QueryExecer) error {
	_, err := qe.Exec(`
UPDATE target_probe
SET state_configuration='{}'
WHERE state_configuration LIKE '{"states":{}}';
`)
	if err != nil {
		return err
	}
	return nil
}

// Down20180817090005 updates empty state_configuration in target_probe to the right format
func Down20180817090005(qe goose.QueryExecer) error {
	return nil
}
