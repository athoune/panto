// Copyright 2018 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package migrations

import (
	"github.com/loderunner/goose"
)

func init() {
	goose.AddMigration(Up20180817090007, Down20180817090007)
}

// Up20180817090007 adds probe MongoDB
func Up20180817090007(qe goose.QueryExecer) error {
	_, err := qe.Exec(`
INSERT INTO ` + "`" + `probe` + "`" + ` (uuid, label, display_name, description, configuration_template, metrics, graphs)
VALUES (
	'a06a1253-aacc-4536-b2d4-6e55c51570dc',
	'mongodb',
	'MongoDB',
	'MongoDB probe returns the metrics from the serverStatus command of a MongoDB server.',
	'[{"name": "address", "type": "string", "description": "TCP address of the MongoDB server (host:port)", "mandatory": true},{"name": "databases", "type": "string", "description": "Comma separated list of databases to monitor", "mandatory": false},{"name": "username", "type": "string", "description": "Username for the server connection", "mandatory": false},{"name": "password", "type": "string", "description": "Password for the server connection", "mandatory": false}]',
	'{"metrics":[{"name":"uptime","type":"int64","description":"The number of seconds that the current MongoDB process has been active "},{"name":"connections_current","type":"int32","description":"The number of incoming connections from clients to the database server "},{"name":"connections_available","type":"int32","description":"The number of unused incoming connections available "},{"name":"gl_clients_readers","type":"int32","description":"The number of the active client connections performing read operations "},{"name":"gl_clients_writers","type":"int32","description":"The number of active client connections performing write operations "},{"name":"network_in","type":"int64","description":"The number of bytes that reflects the amount of network traffic received by this database "},{"name":"network_out","type":"int64","description":"The number of bytes that reflects the amount of network traffic sent from this database "},{"name":"ops_insert","type":"int32","description":"The total number of insert operations received since the mongod instance last started "},{"name":"ops_query","type":"int32","description":"The total number of queries operations received since the mongod instance last started "},{"name":"ops_update","type":"int32","description":"The total number of update operations received since the mongod instance last started "},{"name":"ops_delete","type":"int32","description":"The total number of delete operations received since the mongod instance last started "},{"name":"ops_getmore","type":"int32","description":"The total number of getmore operations received since the mongod instance last started "},{"name":"ops_command","type":"int32","description":"The total number of command operations received since the mongod instance last started "},{"name":"mem_resident","type":"int32","description":"The value of mem.resident is roughly equivalent to the amount of RAM, in megabytes (MB), currently used by the database process "},{"name":"mem_virtual","type":"int32","description":"mem.virtual displays the quantity, in megabytes (MB), of virtual memory used by the mongod process. "}]}',
	'{"graphs":[{"title":"Connections","type":"stacked-lines","metrics":["connections_current","connections_available"],"tags":[]},{"title":"Network traffic","type":"stacked-lines","metrics":["network_in","network_out"],"tags":[]},{"title":"Operations","type":"stacked-lines","metrics":["ops_insert","ops_query","ops_update","ops_delete","ops_getmore","ops_command"],"tags":[]},{"title":"Memory usage","type":"stacked-lines","metrics":["mem_resident","mem_virtual"],"tags":[]}]}');
`)
	if err != nil {
		return err
	}
	return nil
}

// Down20180817090007 removes probe MongoDB
func Down20180817090007(qe goose.QueryExecer) error {
	_, err := qe.Exec(`
DELETE FROM ` + "`" + `probe` + "`" + ` WHERE uuid='a06a1253-aacc-4536-b2d4-6e55c51570dc';
`)
	if err != nil {
		return err
	}
	return nil
}
