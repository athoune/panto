// Copyright 2018 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package migrations

import (
	"github.com/loderunner/goose"
)

func init() {
	goose.AddMigration(Up20180817090010, Down20180817090010)
}

// Up20180817090010 adds a format field in the graphs definition
func Up20180817090010(qe goose.QueryExecer) error {
	_, err := qe.Exec(`
UPDATE ` + "`" + `probe` + "`" + `
SET graphs='{"graphs":[{"title":"RTT","type":"lines","format":"duration","metrics":["min","max","avg"],"tags":[]}]}'
WHERE uuid='c549129c-6fc1-4950-a7f8-05f59213cb62';
`)
	if err != nil {
		return err
	}

	_, err = qe.Exec(`
UPDATE ` + "`" + `probe` + "`" + `
SET graphs='{"graphs":[{"title":"CPU usage","type":"lines","format":"percent","metrics":["usage"],"tags":["cpu"]}]}'
WHERE uuid='b05bb378-0a8c-4bab-b703-36f334a7c513';
`)
	if err != nil {
		return err
	}

	_, err = qe.Exec(`
UPDATE ` + "`" + `probe` + "`" + `
SET graphs='{"graphs":[{"title":"Disk usage (bytes)","type":"stacked-lines","format":"byte","metrics":["free","used"],"tags":["path"]},{"title":"Disk usage (inodes)","type":"stacked-lines","format":"bignumber","metrics":["inodes-free","inodes-used"],"tags":["path"]}]}'
WHERE uuid='f83680d1-86b6-410e-933d-7db11baecce6';
`)
	if err != nil {
		return err
	}

	_, err = qe.Exec(`
UPDATE ` + "`" + `probe` + "`" + `
SET graphs='{"graphs":[{"title":"Response time","type":"lines","format":"duration","metrics":["rtt"],"tags":[]}]}'
WHERE uuid='0a95d0ce-3395-4cc2-b30a-f3cb0275d9c9';
`)
	if err != nil {
		return err
	}

	_, err = qe.Exec(`
UPDATE ` + "`" + `probe` + "`" + `
SET graphs='{"graphs":[{"title":"Load average","type":"lines","format":"default","metrics":["load1","load5","load15"],"tags":[]},{"title":"Process count","type":"stacked-lines","format":"default","metrics":["proc-running","proc-blocked"],"tags":[]}]}'
WHERE uuid='35f999f9-066b-4250-9a0c-819767fd67fa';
`)
	if err != nil {
		return err
	}

	_, err = qe.Exec(`
UPDATE ` + "`" + `probe` + "`" + `
SET graphs='{"graphs":[{"title":"Memory usage","type":"lines","format":"byte","metrics":["total","available","used","free"],"tags":[]},{"title": "Swap usage","type":"lines","format":"byte","metrics":["swap-total","swap-free","swap-used"],"tags":[]}]}'
WHERE uuid='8a7f856f-8b10-4cca-9e2b-a72dfbf3a1c9';
`)
	if err != nil {
		return err
	}

	_, err = qe.Exec(`
UPDATE ` + "`" + `probe` + "`" + `
SET graphs='{"graphs":[{"title":"Network Throughput","type":"lines","format":"byte","metrics":["bytes-sent","bytes-received"],"tags":["interface"]},{"title":"Network Throughput (in packets)","type":"lines","format":"bignumber","metrics":["packets-sent","packets-received"],"tags":["interface"]},{"title":"Network errors","type":"lines","format":"bignumber","metrics":["error-in","error-out"],"tags":["interface"]},{"title":"Network connections","type":"stacked-lines","format":"bignumber","metrics":["tcp-connections","udp-connections"],"tags":["interface"]}]}'
WHERE uuid='667f9df2-0540-4801-9a71-da43e656020c';
`)
	if err != nil {
		return err
	}

	_, err = qe.Exec(`
UPDATE ` + "`" + `probe` + "`" + `
SET graphs='{"graphs":[{"title":"Clients","type":"stacked-lines","format":"bignumber","metrics":["connected-clients","blocked-clients"],"tags":[]},{"title":"Memory usage","type":"lines","format":"byte","metrics":["used-memory"],"tags":[]},{"title":"Pending changes","type":"lines","format":"bignumber","metrics":["changes-since-last-save"],"tags":[]},{"title":"Throughput","type":"stacked-lines","format":"byte","metrics":["input-kbps","output-kbps"],"tags":[]}]}'
WHERE uuid='ad7fd7d8-a38d-4337-a16a-cee1f415faf3';
`)
	if err != nil {
		return err
	}

	_, err = qe.Exec(`
UPDATE ` + "`" + `probe` + "`" + `
SET graphs='{"graphs":[{"title":"Heap usage","type":"lines","format":"byte","metrics":["runtime-heap-alloc","runtime-heap-idle","runtime-heap-in-use"],"tags":[]},{"title":"Heap objects","type":"lines","format":"bignumber","metrics":["runtime-heap-objects"],"tags":[]},{"title":"Garbage collection","type":"lines","format":"bignumber","metrics":["runtime-num-gc"],"tags":[]},{"title":"Go routines","type":"lines","format":"bignumber","metrics":["runtime-num-goroutine"],"tags":[]},{"title":"Query Engine","type":"stacked-lines","format":"bignumber","metrics":["qe-queriesActive","qe-queriesExecuted","qe-queriesFinished"],"tags":[]},{"title":"Write errors","type":"stacked-lines","format":"bignumber","metrics":["write-writedrop","write-writeerror","write-writetimeout"],"tags":[]}]}'
WHERE uuid='92709d8f-640e-4699-ba15-a760fb62fb5a';
`)
	if err != nil {
		return err
	}

	_, err = qe.Exec(`
UPDATE ` + "`" + `probe` + "`" + `
SET graphs='{"graphs":[{"title":"TTL","type":"lines","format":"duration","metrics":["expires-in","chain-expires-in"],"tags":[]}]}'
WHERE uuid='01ef6bca-7f39-41e5-8e7f-126842f89ba9';
`)
	if err != nil {
		return err
	}

	_, err = qe.Exec(`
UPDATE ` + "`" + `probe` + "`" + `
SET graphs='{"graphs":[{"title":"Clock offset","type":"lines","format":"duration","metrics":["clock-offset"],"tags":[]}]}'
WHERE uuid='ae94ddd1-8e99-466d-bcd8-a4fd43ee56ef';
`)
	if err != nil {
		return err
	}

	_, err = qe.Exec(`
UPDATE ` + "`" + `probe` + "`" + `
SET graphs='{"graphs":[{"title":"Current connections","type":"lines","format":"bignumber","metrics":["curr_connections"],"tags":[]},{"title":"Commands","type":"stacked-lines","format":"bignumber","metrics":["cmd_get","cmd_set","cmd_flush","cmd_touch"],"tags":[]},{"title":"Cache GET performance","type":"lines","format":"bignumber","metrics":["get_hits","get_misses","get_expired","get_flushed"],"tags":[]},{"title":"Cache DELETE","type":"stacked-lines","format":"bignumber","metrics":["delete_misses","delete_hits"],"tags":[]},{"title":"Throughput","type":"stacked-lines","format":"byte","metrics":["bytes_read","bytes_written"],"tags":[]},{"title":"Cache size","type":"lines","format":"byte","metrics":["bytes"],"tags":[]},{"title":"Cache items","type":"lines","format":"bignumber","metrics":["curr_items"],"tags":[]},{"title":"Cache evictions","type":"lines","format":"bignumber","metrics":["evictions"],"tags":[]}]}'
WHERE uuid='50fdbc01-0a04-473d-845e-16ae7d9d6c0a';
`)
	if err != nil {
		return err
	}

	_, err = qe.Exec(`
UPDATE ` + "`" + `probe` + "`" + `
SET graphs='{"graphs":[{"title":"Throughput","type":"stacked-lines","format":"byte","metrics":["bytes-received","bytes-sent"],"tags":[]},{"title":"Commands","type":"stacked-lines","format":"bignumber","metrics":["com-begin","com-change-db","com-change-master","com-commit","com-create-db","com-delete","com-delete-multi","com-insert","com-rollback","com-select","com-stmt-execute","com-stmt-fetch","com-truncate","com-update"],"tags":[]},{"title":"Connection errors","type":"stacked-lines","format":"bignumber","metrics":["connection-errors-accept","connection-errors-internal","connection-errors-max-connections","connection-errors-peer-address","connection-errors-select","connection-errors-tcpwrap"],"tags":[]},{"title":"Connections","format":"bignumber","metrics":["connections"],"tags":[]},{"title":"Temporary tables","type":"lines","format":"bignumber","metrics":["created-tmp-disk-tables","created-tmp-tables"],"tags":[]},{"title":"Innodb throughput","type":"stacked-lines","format":"byte","metrics":["innodb-data-reads","innodb-data-writes"],"tags":[]},{"title":"Open files","type":"lines","format":"bignumber","metrics":["open-files"],"tags":[]},{"title":"Open tables","type":"lines","format":"bignumber","metric":["open-tables"],"tags":[]},{"title":"Queries","type":"lines","format":"bignumber","metrics":["queries","slow-queries"],"tags":[]}]}'
WHERE uuid='a29e3925-569d-492f-b2e5-1003a8d7ac68';
`)
	if err != nil {
		return err
	}

	_, err = qe.Exec(`
UPDATE ` + "`" + `probe` + "`" + `
SET graphs='{"graphs":[{"title":"Cluster topology","type":"lines","format":"default","metrics":["number-of-nodes","number-of-data-nodes"],"tags":[]},{"title":"Shard topology","type":"stacked-lines","format":"default","metrics":["active-primary-shards","active-shards","relocating-shards","initializing-shards","unassigned-shards","delayed-unassigned-shards"],"tags":[]},{"title":"Pending tasks","type":"lines","format":"bignumber","metrics":["number-of-pending-tasks"],"tags":[]},{"title":"Documents","type":"stacked-lines","format":"bignumber","metrics":["docs-count","docs-deleted"],"tags":[]},{"title":"Cluster storage","type":"lines","format":"byte","metrics":["store-size"],"tags":[]},{"title":"Query cache size","type":"lines","format":"byte","metrics":["querycache-memory"],"tags":[]},{"title":"Query cache performance","type":"stacked-lines","format":"bignumber","metrics":["querycache-count-hit","querycache-count-miss"],"tags":[]},{"title":"Query cache evictions","type":"lines","format":"bignumber","metrics":["querycache-evictions"],"tags":[]},{"title":"JVM Heap","type":"lines","format":"byte","metrics":["jvm-heap-max","jvm-heap-used"],"tags":[]}]}'
WHERE uuid='c6d84e99-b6d0-4e89-b847-83085ff0f318';
`)
	if err != nil {
		return err
	}

	_, err = qe.Exec(`
UPDATE ` + "`" + `probe` + "`" + `
SET graphs='{"graphs":[{"title":"Current connection","type":"stacked-lines","format":"bignumber","metrics":["reading","writing","waiting"],"tags":[]}]}'
WHERE uuid='d96648a9-49fd-4e08-b086-de096d4196a6';
`)
	if err != nil {
		return err
	}

	_, err = qe.Exec(`
UPDATE ` + "`" + `probe` + "`" + `
SET graphs='{"graphs":[{"title":"Listen queue","type":"lines","format":"bignumber","metrics":["listen_queue"],"tags":[]},{"title":"Processes","type":"stacked-lines","format":"bignumber","metrics":["idle_processes","active_processes"],"tags":[]},{"title":"Slow requests","type":"lines","format":"bignumber","metrics":["slow_requests"],"tags":[]}]}'
WHERE uuid='dbc7b160-f44d-457f-9361-d6aae66f645f';
`)
	if err != nil {
		return err
	}

	_, err = qe.Exec(`
UPDATE ` + "`" + `probe` + "`" + `
SET graphs='{"graphs":[{"title":"Connections","type":"stacked-lines","format":"bignumber","metrics":["connections_current","connections_available"],"tags":[]},{"title":"Network traffic","type":"stacked-lines","format":"byte","metrics":["network_in","network_out"],"tags":[]},{"title":"Operations","type":"stacked-lines","format":"bignumber","metrics":["ops_insert","ops_query","ops_update","ops_delete","ops_getmore","ops_command"],"tags":[]},{"title":"Memory usage","type":"stacked-lines","format":"byte","metrics":["mem_resident","mem_virtual"],"tags":[]}]}'
WHERE uuid='a06a1253-aacc-4536-b2d4-6e55c51570dc';
`)
	if err != nil {
		return err
	}

	_, err = qe.Exec(`
UPDATE ` + "`" + `probe` + "`" + `
SET graphs='{"graphs":[{"title":"Connections","type":"lines","format":"bignumber","metrics":["current_connections","max_connections"],"tags":[]},{"title":"Buffer cache","type":"lines","format":"bignumber","metrics":["shared_buffer_hits","shared_buffer_reads"],"tags":[]},{"title":"Temporary files","type":"lines","format":"byte","metrics":["temp_file_bytes"],"tags":[]},{"title":"Rows","type":"lines","format":"bignumber","metrics":["rows_returned","rows_fetched","rows_inserted","rows_updated","rows_deleted"],"tags":[]},{"title":"Sizes","type":"lines","format":"byte","metrics":["index_size","table_size","toast_size"],"tags":[]},{"title":"Checkpoints","type":"lines","format":"bignumber","metrics":["checkpoints_requested","checkpoints_scheduled"],"tags":[]},{"title":"Writes","type":"stacked-lines","format":"byte","metrics":["buffers_backend","buffers_background","buffers_checkpoint"],"tags":[]}]}'
WHERE uuid='478eb9c0-d5b2-4bba-8e05-89ae7ba8d998';
`)
	if err != nil {
		return err
	}

	return nil
}

// Down20180817090010 removes the format field from the graphs definition
func Down20180817090010(qe goose.QueryExecer) error {
	_, err := qe.Exec(`
UPDATE ` + "`" + `probe` + "`" + `
SET graphs='{"graphs":[{"title":"RTT","type":"lines","metrics":["min","max","avg"],"tags":[]}]}'
WHERE uuid='c549129c-6fc1-4950-a7f8-05f59213cb62';
`)
	if err != nil {
		return err
	}

	_, err = qe.Exec(`
UPDATE ` + "`" + `probe` + "`" + `
SET graphs='{"graphs":[{"title":"CPU usage","type":"lines","metrics":["usage"],"tags":["cpu"]}]}'
WHERE uuid='b05bb378-0a8c-4bab-b703-36f334a7c513';
`)
	if err != nil {
		return err
	}

	_, err = qe.Exec(`
UPDATE ` + "`" + `probe` + "`" + `
SET graphs='{"graphs":[{"title":"Disk usage (bytes)","type":"stacked-lines","metrics":["free","used"],"tags":["path"]},{"title":"Disk usage (inodes)","type":"stacked-lines","metrics":["inodes-free","inodes-used"],"tags":["path"]}]}'
WHERE uuid='f83680d1-86b6-410e-933d-7db11baecce6';
`)
	if err != nil {
		return err
	}

	_, err = qe.Exec(`
UPDATE ` + "`" + `probe` + "`" + `
SET graphs='{"graphs":[{"title":"Response time","type":"lines","metrics":["rtt"],"tags":[]}]}'
WHERE uuid='0a95d0ce-3395-4cc2-b30a-f3cb0275d9c9';
`)
	if err != nil {
		return err
	}

	_, err = qe.Exec(`
UPDATE ` + "`" + `probe` + "`" + `
SET graphs='{"graphs":[{"title":"Load average","type":"lines","metrics":["load1","load5","load15"],"tags":[]},{"title":"Process count","type":"stacked-lines","metrics":["proc-running","proc-blocked"],"tags":[]}]}'
WHERE uuid='35f999f9-066b-4250-9a0c-819767fd67fa';
`)
	if err != nil {
		return err
	}

	_, err = qe.Exec(`
UPDATE ` + "`" + `probe` + "`" + `
SET graphs='{"graphs":[{"title":"Memory usage","type":"lines","metrics":["total","available","used","free"],"tags":[]},{"title": "Swap usage","type":"lines","metrics":["swap-total","swap-free","swap-used"],"tags":[]}]}'
WHERE uuid='8a7f856f-8b10-4cca-9e2b-a72dfbf3a1c9';
`)
	if err != nil {
		return err
	}

	_, err = qe.Exec(`
UPDATE ` + "`" + `probe` + "`" + `
SET graphs='{"graphs":[{"title":"Network Throughput","type":"lines","metrics":["bytes-sent","bytes-received"],"tags":["interface"]},{"title":"Network Throughput (in packets)","type":"lines","metrics":["packets-sent","packets-received"],"tags":["interface"]},{"title":"Network errors","type":"lines","metrics":["error-in","error-out"],"tags":["interface"]},{"title":"Network connections","type":"stacked-lines","metrics":["tcp-connections","udp-connections"],"tags":["interface"]}]}'
WHERE uuid='667f9df2-0540-4801-9a71-da43e656020c';
`)
	if err != nil {
		return err
	}

	_, err = qe.Exec(`
UPDATE ` + "`" + `probe` + "`" + `
SET graphs='{"graphs":[{"title":"Clients","type":"stacked-lines","metrics":["connected-clients","blocked-clients"],"tags":[]},{"title":"Memory usage","type":"lines","metrics":["used-memory"],"tags":[]},{"title":"Pending changes","type":"lines","metrics":["changes-since-last-save"],"tags":[]},{"title":"Throughput","type":"stacked-lines","metrics":["input-kbps","output-kbps"],"tags":[]}]}'
WHERE uuid='ad7fd7d8-a38d-4337-a16a-cee1f415faf3';
`)
	if err != nil {
		return err
	}

	_, err = qe.Exec(`
UPDATE ` + "`" + `probe` + "`" + `
SET graphs='{"graphs":[{"title":"Heap usage","type":"lines","metrics":["runtime-heap-alloc","runtime-heap-idle","runtime-heap-in-use"],"tags":[]},{"title":"Heap objects","type":"lines","metrics":["runtime-heap-objects"],"tags":[]},{"title":"Garbage collection","type":"lines","metrics":["runtime-num-gc"],"tags":[]},{"title":"Go routines","type":"lines","metrics":["runtime-num-goroutine"],"tags":[]},{"title":"Query Engine","type":"stacked-lines","metrics":["qe-queriesActive","qe-queriesExecuted","qe-queriesFinished"],"tags":[]},{"title":"Write errors","type":"stacked-lines","metrics":["write-writedrop","write-writeerror","write-writetimeout"],"tags":[]}]}'
WHERE uuid='92709d8f-640e-4699-ba15-a760fb62fb5a';
`)
	if err != nil {
		return err
	}

	_, err = qe.Exec(`
UPDATE ` + "`" + `probe` + "`" + `
SET graphs='{"graphs":[{"title":"TTL","type":"lines","metrics":["expires-in","chain-expires-in"],"tags":[]}]}'
WHERE uuid='01ef6bca-7f39-41e5-8e7f-126842f89ba9';
`)
	if err != nil {
		return err
	}

	_, err = qe.Exec(`
UPDATE ` + "`" + `probe` + "`" + `
SET graphs='{"graphs":[{"title":"Clock offset","type":"lines","metrics":["clock-offset"],"tags":[]}]}'
WHERE uuid='ae94ddd1-8e99-466d-bcd8-a4fd43ee56ef';
`)
	if err != nil {
		return err
	}

	_, err = qe.Exec(`
UPDATE ` + "`" + `probe` + "`" + `
SET graphs='{"graphs":[{"title":"Current connections","type":"lines","metrics":["curr_connections"],"tags":[]},{"title":"Commands","type":"stacked-lines","metrics":["cmd_get","cmd_set","cmd_flush","cmd_touch"],"tags":[]},{"title":"Cache GET performance","type":"lines","metrics":["get_hits","get_misses","get_expired","get_flushed"],"tags":[]},{"title":"Cache DELETE","type":"stacked-lines","metrics":["delete_misses","delete_hits"],"tags":[]},{"title":"Throughput","type":"stacked-lines","metrics":["bytes_read","bytes_written"],"tags":[]},{"title":"Cache size","type":"lines","metrics":["bytes"],"tags":[]},{"title":"Cache items","type":"lines","metrics":["curr_items"],"tags":[]},{"title":"Cache evictions","type":"lines","metrics":["evictions"],"tags":[]}]}'
WHERE uuid='50fdbc01-0a04-473d-845e-16ae7d9d6c0a';
`)
	if err != nil {
		return err
	}

	_, err = qe.Exec(`
UPDATE ` + "`" + `probe` + "`" + `
SET graphs='{"graphs":[{"title":"Throughput","type":"stacked-lines","metrics":["bytes-received","bytes-sent"],"tags":[]},{"title":"Commands","type":"stacked-lines","metrics":["com-begin","com-change-db","com-change-master","com-commit","com-create-db","com-delete","com-delete-multi","com-insert","com-rollback","com-select","com-stmt-execute","com-stmt-fetch","com-truncate","com-update"],"tags":[]},{"title":"Connection errors","type":"stacked-lines","metrics":["connection-errors-accept","connection-errors-internal","connection-errors-max-connections","connection-errors-peer-address","connection-errors-select","connection-errors-tcpwrap"],"tags":[]},{"title":"Connections","metrics":["connections"],"tags":[]},{"title":"Temporary tables","type":"lines","metrics":["created-tmp-disk-tables","created-tmp-tables"],"tags":[]},{"title":"Innodb throughput","type":"stacked-lines","metrics":["innodb-data-reads","innodb-data-writes"],"tags":[]},{"title":"Open files","type":"lines","metrics":["open-files"],"tags":[]},{"title":"Open tables","type":"lines","name":["open-tables"],"tags":[]},{"title":"Queries","type":"lines","metrics":["queries","slow-queries"],"tags":[]}]}'
WHERE uuid='a29e3925-569d-492f-b2e5-1003a8d7ac68';
`)
	if err != nil {
		return err
	}

	_, err = qe.Exec(`
UPDATE ` + "`" + `probe` + "`" + `
SET graphs='{"graphs":[{"title":"Cluster topology","type":"lines","metrics":["number-of-nodes","number-of-data-nodes"],"tags":[]},{"title":"Shard topology","type":"stacked-lines","metrics":["active-primary-shards","active-shards","relocating-shards","initializing-shards","unassigned-shards","delayed-unassigned-shards"],"tags":[]},{"title":"Pending tasks","type":"lines","metrics":["number-of-pending-tasks"],"tags":[]},{"title":"Documents","type":"stacked-lines","metrics":["docs-count","docs-deleted"],"tags":[]},{"title":"Cluster storage","type":"lines","metrics":["store-size"],"tags":[]},{"title":"Query cache size","type":"lines","metrics":["querycache-memory"],"tags":[]},{"title":"Query cache performance","type":"stacked-lines","metrics":["querycache-count-hit","querycache-count-miss"],"tags":[]},{"title":"Query cache evictions","type":"lines","metrics":["querycache-evictions"],"tags":[]},{"title":"JVM Heap","type":"lines","metrics":["jvm-heap-max","jvm-heap-used"],"tags":[]}]}'
WHERE uuid='c6d84e99-b6d0-4e89-b847-83085ff0f318';
`)
	if err != nil {
		return err
	}

	_, err = qe.Exec(`
UPDATE ` + "`" + `probe` + "`" + `
SET graphs='{"graphs":[{"title":"Current connection","type":"stacked-lines","metrics":["reading","writing","waiting"],"tags":[]}]}'
WHERE uuid='d96648a9-49fd-4e08-b086-de096d4196a6';
`)
	if err != nil {
		return err
	}

	_, err = qe.Exec(`
UPDATE ` + "`" + `probe` + "`" + `
SET graphs='{"graphs":[{"title":"Listen queue","type":"lines","metrics":["listen_queue"],"tags":[]},{"title":"Processes","type":"stacked-lines","metrics":["idle_processes","active_processes"],"tags":[]},{"title":"Slow requests","type":"lines","metrics":["slow_requests"],"tags":[]}]}'
WHERE uuid='dbc7b160-f44d-457f-9361-d6aae66f645f';
`)
	if err != nil {
		return err
	}

	_, err = qe.Exec(`
UPDATE ` + "`" + `probe` + "`" + `
SET graphs='{"graphs":[{"title":"Connections","type":"stacked-lines","metrics":["connections_current","connections_available"],"tags":[]},{"title":"Network traffic","type":"stacked-lines","metrics":["network_in","network_out"],"tags":[]},{"title":"Operations","type":"stacked-lines","metrics":["ops_insert","ops_query","ops_update","ops_delete","ops_getmore","ops_command"],"tags":[]},{"title":"Memory usage","type":"stacked-lines","metrics":["mem_resident","mem_virtual"],"tags":[]}]}'
WHERE uuid='a06a1253-aacc-4536-b2d4-6e55c51570dc';
`)
	if err != nil {
		return err
	}

	_, err = qe.Exec(`
UPDATE ` + "`" + `probe` + "`" + `
SET graphs='{"graphs":[{"title":"Connections","type":"lines","metrics":["current_connections","max_connections"],"tags":[]},{"title":"Buffer cache","type":"lines","metrics":["shared_buffer_hits","shared_buffer_reads"],"tags":[]},{"title":"Temporary files","type":"lines","metrics":["temp_file_bytes"],"tags":[]},{"title":"Rows","type":"lines","metrics":["rows_returned","rows_fetched","rows_inserted","rows_updated","rows_deleted"],"tags":[]},{"title":"Sizes","type":"lines","metrics":["index_size","table_size","toast_size"],"tags":[]},{"title":"Checkpoints","type":"lines","metrics":["checkpoints_requested","checkpoints_scheduled"],"tags":[]},{"title":"Writes","type":"stacked-lines","metrics":["buffers_backend","buffers_background","buffers_checkpoint"],"tags":[]}]}'
WHERE uuid='478eb9c0-d5b2-4bba-8e05-89ae7ba8d998';
`)
	if err != nil {
		return err
	}

	return nil
}
