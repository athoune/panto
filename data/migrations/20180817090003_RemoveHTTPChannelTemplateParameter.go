// Copyright 2018 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package migrations

import (
	"github.com/loderunner/goose"
)

func init() {
	goose.AddMigration(Up20180817090003, Down20180817090003)
}

// Up20180817090003 Remove body_tmpl parameter from HTTP probe
func Up20180817090003(qe goose.QueryExecer) error {
	_, err := qe.Exec(`
UPDATE channel_type 
SET configuration_template='[{"name":"url","type":"string","description":"URL to POST alert to","mandatory":true}]'
WHERE label='http';
`)
	if err != nil {
		return err
	}
	return nil
}

// Down20180817090003 Remove body_tmpl parameter from HTTP probe
func Down20180817090003(qe goose.QueryExecer) error {
	_, err := qe.Exec(`
UPDATE channel_type
SET configuration_template='[{"name": "url", "type":"string", "description": "URL to POST alert to", "mandatory": true},{"name": "body_tmpl", "type":"string", "description": "Template of the body of the POST request in Go template style", "mandatory": true}]'
WHERE label='http';
`)
	if err != nil {
		return err
	}
	return nil
}
