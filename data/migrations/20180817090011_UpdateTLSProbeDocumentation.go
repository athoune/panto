// Copyright 2018 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package migrations

import (
	"github.com/loderunner/goose"
)

func init() {
	goose.AddMigration(Up20180817090011, Down20180817090011)
}

// Up20180817090011 updates the metrics description
func Up20180817090011(qe goose.QueryExecer) error {
	_, err := qe.Exec(`
UPDATE ` + "`" + `probe` + "`" + `
SET metrics='{"metrics":[{"name":"expires-in","type":"int64","description":"time before the TLS certificate expires, in nanoseconds"},{"name":"chain-expires-in","type":"int64","description":"time before any TLS certificate in the chain expires, in nanoseconds"},{"name":"hash","type":"string","description":"the hash of the TLS certificate"},{"name":"signature-list","type":"string","description":"list of the signature algorithm in the full chain of the TLS certificate"}],"tags":[]}'
WHERE uuid='01ef6bca-7f39-41e5-8e7f-126842f89ba9';
`)
	if err != nil {
		return err
	}

	return nil
}

// Down20180817090011 restores the metrics description
func Down20180817090011(qe goose.QueryExecer) error {
	_, err := qe.Exec(`
UPDATE ` + "`" + `probe` + "`" + `
SET metrics='{"metrics":[{"name":"expires-in","type":"int64","description":"time before the TLS certificate expires, in hours"},{"name":"chain-expires-in","type":"int64","description":"time before any TLS certificate in the chain expires, in hours"},{"name":"hash","type":"string","description":"the hash of the TLS certificate"},{"name":"signature-list","type":"string","description":"list of the signature algorithm in the full chain of the TLS certificate"}],"tags":[]}'
WHERE uuid='01ef6bca-7f39-41e5-8e7f-126842f89ba9';
`)
	if err != nil {
		return err
	}

	return nil
}
