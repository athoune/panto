// Copyright 2018 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package migrations

import (
	"github.com/loderunner/goose"
)

func init() {
	goose.AddMigration(Up20180817090009, Down20180817090009)
}

// Up20180817090009 replaces the configuration_template with a valid one
func Up20180817090009(qe goose.QueryExecer) error {
	_, err := qe.Exec(`
UPDATE ` + "`" + `probe` + "`" + `
SET configuration_template='[{"description":"HTTP request method (GET, POST, etc.)","mandatory":false,"name":"method","type":"string","default":"GET"},{"description":"HTTP request URL","mandatory":true,"name":"url","type":"string","placeholder":"http://yourdomain.com"},{"description":"HTTP body of the request","mandatory":false,"name":"body","type":"string","default":""},{"description":"duration before the HTTP request times out","mandatory":false,"name":"timeout","type":"time.Duration","default":0}]'
WHERE uuid='0a95d0ce-3395-4cc2-b30a-f3cb0275d9c9'
`)
	if err != nil {
		return err
	}
	return nil
}

// Down20180817090009 does nothing
func Down20180817090009(qe goose.QueryExecer) error {
	return nil
}
