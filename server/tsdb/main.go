// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package tsdb manages everything related to the time-series
// database. This database will be used to store and retrieve
// monitoring metrics.
package tsdb

import (
	"time"

	"gitlab.com/pantomath-io/panto/util"

	"github.com/google/uuid"
)

const (
	keyTagCheck              = "check"
	keyTagTarget             = "target"
	keyTagAgent              = "agent"
	keyTagProbe              = "probe"
	keyTagOrganization       = "organization"
	keyTagProbeConfiguration = "probe_configuration"
	keyFieldError            = "error"
)

var reserved = []string{
	"time",
	keyTagCheck,
	keyTagTarget,
	keyTagAgent,
	keyTagProbe,
	keyTagOrganization,
	keyTagProbeConfiguration,
	keyFieldError,
}

// Order of query results: by ascending or descending timestamps
type Order bool

const (
	// OrderAscending returns results by ascending timestamp
	OrderAscending Order = true
	// OrderDescending returns results by descending timestamp
	OrderDescending Order = false
)

func (o Order) String() string {
	if o == OrderAscending {
		return "ascending"
	} else if o == OrderDescending {
		return "descending"
	}
	return "unknown ordering"
}

// TSDB represents an abstract time-series database.
type TSDB interface {
	AddResults(results []Result) error
	GetResults(check uuid.UUID, opts ...QueryOption) ([]Result, error)

	AddState(state State) error
	GetStates(checks []uuid.UUID, opts ...QueryOption) ([]State, error)

	AddAlertEvent(alert AlertEvent) error
	GetAlertEvents(probeConfigurations []uuid.UUID, opts ...QueryOption) ([]AlertEvent, error)

	AddActivity(activity Activity) error
	GetActivity(organization uuid.UUID, agent uuid.UUID, opts ...QueryOption) ([]Activity, error)
}

// Entry is a generic structure containing data common to all TSDB entries (probe results, states, alert events). It is
// meant to be embedded in each specific structure and serve as a kind of "abstract base class".
type Entry struct {
	Timestamp          time.Time
	Organization       uuid.UUID
	ProbeConfiguration uuid.UUID
	Probe              uuid.UUID
	Agent              uuid.UUID
	Check              uuid.UUID
	Target             uuid.UUID
}

// FromRow populates an `Entry` using a row from a TSDB query.
func (e *Entry) FromRow(row []interface{}, columns []string, tags map[string]string) (err error) {
	for i, col := range columns {
		err = e.assignKeyVal(col, row[i])
		if err != nil {
			return
		}
	}
	for k, v := range tags {
		err = e.assignKeyVal(k, v)
		if err != nil {
			return
		}
	}
	return
}

func (e *Entry) assignKeyVal(k string, v interface{}) (err error) {
	if v, ok := v.(string); ok {
		switch k {
		case "time":
			e.Timestamp, err = time.Parse(time.RFC3339Nano, v)
		case keyTagCheck:
			e.Check, err = util.Base64Decode(v)
		case keyTagProbe:
			e.Probe, err = util.Base64Decode(v)
		case keyTagTarget:
			e.Target, err = util.Base64Decode(v)
		case keyTagAgent:
			e.Agent, err = util.Base64Decode(v)
		case keyTagOrganization:
			e.Organization, err = util.Base64Decode(v)
		case keyTagProbeConfiguration:
			e.ProbeConfiguration, err = util.Base64Decode(v)
		}
		if err != nil {
			return
		}
	}
	return
}
