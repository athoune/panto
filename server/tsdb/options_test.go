// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package tsdb

import (
	"testing"
	"time"
)

func TestQueryOptions(t *testing.T) {
	ts := time.Now()
	n := 123
	o := OrderAscending
	g := "Barbès"
	tags := map[string][]string{"Barbès": {"Rochechouart"}}
	multitags := map[string][]string{"Barbès": {"Rochechouart", "Montpipeau"}}
	fields := map[string]string{"Réaumur": "Sébastopol"}
	cases := [][]QueryOption{
		{From(ts)},
		{To(ts)},
		{Count(n)},
		{Offset(n)},
		{OrderBy(o)},
		{GroupBy(g)},
		{Tag("Barbès", "Rochechouart")},
		{Tag("Barbès", "Rochechouart", "Montpipeau")},
		{Field("Réaumur", "Sébastopol")},
		{From(ts), To(ts), Count(n), Offset(n), OrderBy(o), GroupBy(g), Tag("Barbès", "Rochechouart", "Montpipeau"), Field("Réaumur", "Sébastopol")},
	}
	expect := []queryOptions{
		{from: &ts},
		{to: &ts},
		{count: &n},
		{offset: &n},
		{orderBy: &o},
		{groupBy: g},
		{tags: tags},
		{tags: multitags},
		{fields: fields},
		{
			from:    &ts,
			to:      &ts,
			count:   &n,
			offset:  &n,
			orderBy: &o,
			groupBy: g,
			tags:    multitags,
			fields:  fields,
		},
	}

	for i, c := range cases {
		opts := new(queryOptions)
		for _, f := range c {
			f(opts)
		}
		if opts.String() != expect[i].String() || opts.queryClauses() != expect[i].queryClauses() {
			t.Errorf("Unexpected result after applying options. Got %v, expected %v.", opts, expect[i])
		}
	}
}
