// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package tsdb

import (
	"os"
	"reflect"
	"testing"
	"time"

	"github.com/google/uuid"
	influx "github.com/influxdata/influxdb/client/v2"
	"github.com/spf13/viper"

	"gitlab.com/pantomath-io/panto/util"
)

var check, check2, agent, agent2, probe, target, probeConfiguration, organization uuid.UUID
var activityType string

func TestMain(m *testing.M) {
	setUp()        // Setup for tests
	res := m.Run() // Run the actual tests
	tearDown()     // Teardown after running the tests
	os.Exit(res)
}

func setUp() {
	// Check if this is live testing
	if util.LiveTest(nil) {
		// Create test database
		viper.BindEnv("influxdb.address", "INFLUXDB_ADDRESS")
		viper.BindEnv("influxdb.database", "INFLUXDB_DATABASE")

		viper.SetDefault("influxdb.address", "http://influxdb:8086")
		viper.SetDefault("influxdb.database", "pantotest")

		client, err := influx.NewHTTPClient(influx.HTTPConfig{Addr: viper.GetString("influxdb.address")})
		if err != nil {
			panic("Couldn't connect to influx database on influxdb:8086 : " + err.Error())
		}
		defer client.Close()

		_, err = client.Query(influx.NewQuery("CREATE DATABASE "+viper.GetString("influxdb.database"), "", ""))
		if err != nil {
			panic("Couldn't create database \"" + viper.GetString("influxdb.database") + "\" " + err.Error())
		}
	}

	// Setup some global variables for all tests
	check, _ = uuid.Parse("8b6418b5-9055-4a7b-978c-b85086e015eb")
	check2, _ = uuid.Parse("7b2a58b6-fe87-40f8-9537-3b5e5fa4f565")
	agent, _ = uuid.Parse("9e82e551-ccee-489a-a4c8-81329e70a8a3")
	agent2, _ = uuid.Parse("2fefe862-8bc1-41de-8466-133a67bd9f28")
	probeConfiguration, _ = uuid.Parse("fca8c20a-783f-4f71-96c5-efbf96a1f73d")
	probe, _ = uuid.Parse("c549129c-6fc1-4950-a7f8-05f59213cb62")
	target, _ = uuid.Parse("31017478-825c-459c-8406-01bfb1d2cf58")
	organization, _ = uuid.Parse("903b49cf-c12a-4340-bb2a-0385eaac317e")
	activityType = "/api.Panto/ListTags"
}

func tearDown() {
	// Check if this is live testing
	if util.LiveTest(nil) {
		// Cleanup test database
		client, _ := influx.NewHTTPClient(influx.HTTPConfig{Addr: viper.GetString("influxdb.address")})
		defer client.Close()

		_, err := client.Query(influx.NewQuery("DROP DATABASE "+viper.GetString("influxdb.database"), "", ""))
		if err != nil {
			panic("Couldn't drop database \"" + viper.GetString("influxdb.database") + "\" " + err.Error())
		}
	}
}

func TestEntryFromRow(t *testing.T) {
	rows := [][]interface{}{
		{"2018-02-05T16:59:47Z", "kDtJz8EqQ0C7KgOF6qwxfg", "i2QYtZBVSnuXjLhQhuAV6w", "noLlUczuSJqkyIEynnCoow", "xUkSnG_BSVCn-AX1khPLYg", "MQF0eIJcRZyEBgG_sdLPWA", "_KjCCng_T3GWxe-_lqH3PQ"},                      // Valid row
		{"2018-02-05T16:59:47Z", "kDtJz8EqQ0C7KgOF6qwxfg", "i2QYtZBVSnuXjLhQhuAV6w", "noLlUczuSJqkyIEynnCoow", "xUkSnG_BSVCn-AX1khPLYg", "MQF0eIJcRZyEBgG_sdLPWA", "_KjCCng_T3GWxe-_lqH3PQ", 1234, "Hello World"}, // Valid row with extra columns
		{"2018-02-05T16:59:47Z", "kDtJz8EqQ0C7KgOF6qwxfg", "i2QYtZBVSnuXjLhQhuAV6w", ":=$'^(`ùf$^s", "xUkSnG_BSVCn-AX1khPLYg", "MQF0eIJcRZyEBgG_sdLPWA", "_KjCCng_T3GWxe-_lqH3PQ"},                                // Gibberish agent
		{"2018-02-05 16:59:47", "kDtJz8EqQ0C7KgOF6qwxfg", "i2QYtZBVSnuXjLhQhuAV6w", "noLlUczuSJqkyIEynnCoow", "xUkSnG_BSVCn-AX1khPLYg", "MQF0eIJcRZyEBgG_sdLPWA", "_KjCCng_T3GWxe-_lqH3PQ"},                       // Gibberish timestamp
		{"2018-02-05T16:59:47Z", "kDtJz8EqQ0C7KgOF6qwxfg", "i2QYtZBVSnuXjLhQhuAV6w", "noLlUczuSJqkyIEynnCoow", ":=$'^(`ùf$^s", "MQF0eIJcRZyEBgG_sdLPWA", "_KjCCng_T3GWxe-_lqH3PQ"},                                // Gibberish probe
		{"2018-02-05T16:59:47Z", "kDtJz8EqQ0C7KgOF6qwxfg", "i2QYtZBVSnuXjLhQhuAV6w", "noLlUczuSJqkyIEynnCoow", "xUkSnG_BSVCn-AX1khPLYg", ":=$'^(`ùf$^s", "_KjCCng_T3GWxe-_lqH3PQ"},                                // Gibberish target
		{"2018-02-05T16:59:47Z", "kDtJz8EqQ0C7KgOF6qwxfg", "i2QYtZBVSnuXjLhQhuAV6w", "xUkSnG_BSVCn-AX1khPLYg", "MQF0eIJcRZyEBgG_sdLPWA", "_KjCCng_T3GWxe-_lqH3PQ"},                                                // Valid row missing agent
	}
	cols := [][]string{
		{"time", "organization", "check", "agent", "probe", "target", "probe_configuration"},
		{"time", "organization", "check", "agent", "probe", "target", "probe_configuration", "number", "message"},
		{"time", "organization", "check", "agent", "probe", "target", "probe_configuration"},
		{"time", "organization", "check", "agent", "probe", "target", "probe_configuration"},
		{"time", "organization", "check", "agent", "probe", "target", "probe_configuration"},
		{"time", "organization", "check", "agent", "probe", "target", "probe_configuration"},
		{"time", "organization", "check", "probe", "target", "probe_configuration"},
	}
	tags := []map[string]string{
		nil,
		nil,
		nil,
		nil,
		nil,
		nil,
		{"agent": "noLlUczuSJqkyIEynnCoow"},
	}
	expect := []*Entry{
		{
			Timestamp:          time.Unix(1517849987, 0).UTC(),
			Organization:       organization,
			Check:              check,
			Target:             target,
			ProbeConfiguration: probeConfiguration,
			Probe:              probe,
			Agent:              agent,
		},
		{
			Timestamp:          time.Unix(1517849987, 0).UTC(),
			Organization:       organization,
			Check:              check,
			Target:             target,
			ProbeConfiguration: probeConfiguration,
			Probe:              probe,
			Agent:              agent,
		},
		nil,
		nil,
		nil,
		nil,
		{
			Timestamp:          time.Unix(1517849987, 0).UTC(),
			Organization:       organization,
			Check:              check,
			Target:             target,
			ProbeConfiguration: probeConfiguration,
			Probe:              probe,
			Agent:              agent,
		},
	}

	// Check sanity of test data
	if len(cols) != len(rows) || len(cols) != len(expect) {
		t.Fatalf("Inconsistent test data: %d rows, %d cols, %d expect", len(rows), len(cols), len(expect))
	}

	for i, row := range rows {
		// check sanity of test data
		if len(row) != len(cols[i]) {
			t.Fatalf("Inconsistent test data for row %d: %d values, %d cols", i, len(row), len(cols[i]))
		}
		var e Entry
		err := e.FromRow(row, cols[i], tags[i])
		if expect[i] != nil {
			if err != nil {
				t.Errorf("Failed to convert row: %s", err)
				continue
			}
			if !reflect.DeepEqual(e, *expect[i]) {
				t.Error("Row did not convert successfully")
				t.Logf("Input row: (%s)%s", cols[i], row)
				t.Logf("Expected:  %v", *expect[i])
				t.Logf("Got:       %v", e)
				continue
			}
		} else {
			if err == nil {
				t.Error("Row converted successfully, but expected error")
				t.Logf("Input row: (%s)%s", cols[i], row)
				t.Logf("Got:       %v", e)
				continue
			}
		}
	}
}
