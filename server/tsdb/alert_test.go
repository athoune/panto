// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package tsdb

import (
	"encoding/json"
	"fmt"
	"reflect"
	"testing"
	"time"

	"github.com/google/uuid"
	influx "github.com/influxdata/influxdb/client/v2"
	"github.com/spf13/viper"

	"gitlab.com/pantomath-io/panto/util"
)

func TestAlertEventFromRow(t *testing.T) {
	rows := [][]interface{}{
		{"2018-02-05T16:59:47Z", "kDtJz8EqQ0C7KgOF6qwxfg", "i2QYtZBVSnuXjLhQhuAV6w", "noLlUczuSJqkyIEynnCoow", "notified", "xUkSnG_BSVCn-AX1khPLYg", "MQF0eIJcRZyEBgG_sdLPWA"},       // Valid row
		{"2018-02-05T16:59:47Z", "kDtJz8EqQ0C7KgOF6qwxfg", "i2QYtZBVSnuXjLhQhuAV6w", "noLlUczuSJqkyIEynnCoow", 123, "xUkSnG_BSVCn-AX1khPLYg", "MQF0eIJcRZyEBgG_sdLPWA"},              // Gibberish event
		{"2018-02-05T16:59:47Z", "kDtJz8EqQ0C7KgOF6qwxfg", "i2QYtZBVSnuXjLhQhuAV6w", "noLlUczuSJqkyIEynnCoow", json.Number(123), "xUkSnG_BSVCn-AX1khPLYg", "MQF0eIJcRZyEBgG_sdLPWA"}, // Gibberish event
		{"2018-02-05T16:59:47Z", "kDtJz8EqQ0C7KgOF6qwxfg", "i2QYtZBVSnuXjLhQhuAV6w", "noLlUczuSJqkyIEynnCoow", struct{}{}, "xUkSnG_BSVCn-AX1khPLYg", "MQF0eIJcRZyEBgG_sdLPWA"},       // Gibberish event
		{"2018-02-05T16:59:47Z", "kDtJz8EqQ0C7KgOF6qwxfg", "i2QYtZBVSnuXjLhQhuAV6w", "notified", "xUkSnG_BSVCn-AX1khPLYg", "MQF0eIJcRZyEBgG_sdLPWA"},                                 // Missing agent
	}
	cols := [][]string{
		{"time", "organization", "check", "agent", "event", "probe", "target"},
		{"time", "organization", "check", "agent", "event", "probe", "target"},
		{"time", "organization", "check", "agent", "event", "probe", "target"},
		{"time", "organization", "check", "agent", "event", "probe", "target"},
		{"time", "organization", "check", "event", "probe", "target"},
	}
	tags := []map[string]string{
		nil,
		nil,
		nil,
		nil,
		{"agent": "noLlUczuSJqkyIEynnCoow"},
	}
	expect := []*AlertEvent{
		{
			Entry: Entry{
				Timestamp:    time.Unix(1517849987, 0).UTC(),
				Organization: organization,
				Check:        check,
				Target:       target,
				Probe:        probe,
				Agent:        agent,
			},
			Event: EventValueNotified,
		},
		nil,
		nil,
		nil,
		{
			Entry: Entry{
				Timestamp:    time.Unix(1517849987, 0).UTC(),
				Organization: organization,
				Check:        check,
				Target:       target,
				Probe:        probe,
				Agent:        agent,
			},
			Event: EventValueNotified,
		},
	}

	// Check sanity of test data
	if len(cols) != len(rows) || len(cols) != len(expect) {
		panic(fmt.Sprintf("Inconsistent test data: %d rows, %d cols, %d expect", len(rows), len(cols), len(expect)))
	}

	for i, row := range rows {
		// check sanity of test data
		if len(row) != len(cols[i]) {
			panic(fmt.Sprintf("Inconsistent test data for row %d: %d values, %d cols", i, len(row), len(cols[i])))
		}
		var a AlertEvent
		err := a.FromRow(row, cols[i], tags[i])
		if expect[i] != nil {
			if err != nil {
				t.Errorf("Failed to convert row: %s", err)
				continue
			}
			if !reflect.DeepEqual(a, *expect[i]) {
				t.Error("Row did not convert successfully")
				t.Logf("Input row: (%s)%s", cols[i], row)
				t.Logf("Expected:  %#v", *expect[i])
				t.Logf("Got:       %#v", a)
				continue
			}
		} else {
			if err == nil {
				t.Error("Row converted successfully, but expected error")
				t.Logf("Input row: (%s)%s", cols[i], row)
				t.Logf("Got:       %v", a)
				continue
			}
		}
	}
}

func TestAddAlertEvents(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live testing...")
	}

	db, err := NewInfluxTSDB(viper.GetString("influxdb.address"), viper.GetString("influxdb.database"))
	if err != nil {
		t.Fatalf("Couldn't create TSDB instance: %s", err)
	}

	s := AlertEvent{
		Entry: Entry{
			Timestamp:          time.Unix(1517849987, 0).UTC(),
			Organization:       organization,
			Check:              check,
			Target:             target,
			Probe:              probe,
			ProbeConfiguration: probeConfiguration,
			Agent:              agent,
		},
		Event: EventValueNotified,
	}

	err = db.AddAlertEvent(s)
	if err != nil {
		t.Fatalf("Failed to add results to TSDB: %s", err)
	}

	measurementName := alertMeasurementPrefix + util.Base64Encode(probeConfiguration)
	res, err := db.client.Query(influx.NewQuery("SELECT * FROM \""+measurementName+"\"", "pantotest", "s"))
	if err != nil {
		t.Fatalf("Failed to query events from TSDB: %s", err)
	}
	if res.Error() != nil {
		t.Fatalf("Failed to query events from TSDB: %s", res.Error())
	}

	// build array of tsdb.AlertEvent from query results
	results := res.Results
	if len(results) == 0 || len(results[0].Series) == 0 {
		t.Fatalf("Query returned no results")
	}
	serie := results[0].Series[0]
	if len(serie.Values) != 1 {
		t.Fatalf("Invalid number of results: expected %d, got %d", 1, len(serie.Values))
	}

	// Check columns
Expected:
	for _, expect := range []string{"time", "check", "agent", "probe", "target", "event"} {
		for _, got := range serie.Columns {
			if got == expect {
				continue Expected // to next column
			}
		}
		t.Fatalf("Expected column \"%s\" not found: %s", expect, serie.Columns)
	}
Got:
	for _, got := range serie.Columns {
		for _, expect := range []string{"time", "organization", "check", "agent", "probe", "target", "probe_configuration", "event"} {
			if got == expect {
				continue Got // to next column
			}
		}
		t.Fatalf("Unexpected column \"%s\" was found: %s", got, serie.Columns)
	}

	row := serie.Values[0]
	t.Logf("Row 0: %s%s", serie.Columns, row)
	for i, col := range serie.Columns {
		switch col {
		case "time":
			n, ok := row[i].(json.Number)
			if ok {
				if n2, _ := n.Int64(); n2 == 1517849987 {
					continue
				}
			}
			t.Fatalf("Invalid timestamp in row 0: expected %v, got %v", 1517849987, row[i])
		case keyTagOrganization:
			if row[i].(string) != util.Base64Encode(organization) {
				t.Fatalf("Invalid check in row 0: expected %v, got %v", util.Base64Encode(organization), row[i])
			}
		case keyTagCheck:
			if row[i].(string) != util.Base64Encode(check) {
				t.Fatalf("Invalid check in row 0: expected %v, got %v", util.Base64Encode(check), row[i])
			}
		case keyTagProbe:
			if row[i].(string) != util.Base64Encode(probe) {
				t.Fatalf("Invalid probe in row 0: expected %v, got %v", util.Base64Encode(probe), row[i])
			}
		case keyTagTarget:
			if row[i].(string) != util.Base64Encode(target) {
				t.Fatalf("Invalid target in row 0: expected %v, got %v", util.Base64Encode(target), row[i])
			}
		case keyTagAgent:
			if row[i].(string) != util.Base64Encode(agent) {
				t.Fatalf("Invalid agent in row 0: expected %v, got %v", util.Base64Encode(agent), row[i])
			}
		case keyTagProbeConfiguration:
			if row[i].(string) != util.Base64Encode(probeConfiguration) {
				t.Fatalf("Invalid probe_configuration in row 0: expected %v, got %v", util.Base64Encode(probeConfiguration), row[i])
			}
		case "event":
			if row[i].(string) != "notified" {
				t.Fatalf("Invalid event in row 0: expected %v, got %v", "notified", row[i])
			}
		default:
			if row[i] != nil {
				t.Fatalf("Invalid field in row 0: (\"%s\": %v)", col, row[i])
			}
		}
	}
}

func TestGetLastAlertEvents(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live testing...")
	}

	db, err := NewInfluxTSDB(viper.GetString("influxdb.address"), viper.GetString("influxdb.database"))
	if err != nil {
		t.Fatalf("Couldn't create TSDB instance: %s", err)
	}

	res, err := db.GetAlertEvents([]uuid.UUID{probeConfiguration}, Count(1), OrderBy(OrderDescending))
	if err != nil {
		t.Fatalf("Could not get last events: %s", err)
	}
	t.Logf("Alert Events: %v", res)
	if len(res) != 1 {
		t.Fatalf("Invalid number of results: expected %d, got %d", 1, len(res))
	}

	expect := AlertEvent{
		Entry: Entry{
			Timestamp:    time.Unix(1517849987, 0).UTC(),
			Organization: organization,
			Check:        check,
			Target:       target,
			Probe:        probe,
			Agent:        agent,
		},
		Event: EventValueNotified,
	}

	if res[0].Check != check ||
		res[0].Agent != agent ||
		res[0].Event != EventValueNotified {
		t.Errorf("Unexpected result: got %v, expected %v", res[0], expect)
	}

	res, err = db.GetAlertEvents([]uuid.UUID{probeConfiguration}, From(time.Unix(1517849980, 0).UTC()), To(time.Now()))
	if err != nil {
		t.Fatalf("Could not get events in range: %s", err)
	}
	t.Logf("AlertEvents: %v", res)
	if len(res) != 1 {
		t.Fatalf("Invalid number of results: expected %d, got %d", 1, len(res))
	}

	expect = AlertEvent{
		Entry: Entry{
			Timestamp:    time.Unix(1517849987, 0).UTC(),
			Organization: organization,
			Check:        check,
			Target:       target,
			Probe:        probe,
			Agent:        agent,
		},
		Event: EventValueNotified,
	}
	if res[0].Check != check ||
		res[0].Agent != agent ||
		res[0].Event != EventValueNotified {
		t.Errorf("Unexpected result: got %v, expected %v", res[0], expect)
	}
}
