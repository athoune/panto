// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package tsdb

import (
	"encoding/json"
	"fmt"
	"reflect"
	"testing"
	"time"

	influx "github.com/influxdata/influxdb/client/v2"
	"github.com/spf13/viper"

	"gitlab.com/pantomath-io/panto/util"
)

func TestActivityFromRow(t *testing.T) {
	rows := [][]interface{}{
		{"2018-02-05T16:59:47Z", util.Base64Encode(agent), util.Base64Encode(organization), activityType, "0.0.0"}, // Valid row
		{"2018-02-05T16:59:47Z", util.Base64Encode(agent), util.Base64Encode(organization), 42, "0.0.0"},           // Gibberish type
		{"2018-02-05T16:59:47Z", util.Base64Encode(agent), util.Base64Encode(organization), activityType, 1.0},     // Gibberish version
	}
	cols := [][]string{
		{"time", "agent", "organization", "type", "version"},
		{"time", "agent", "organization", "type", "version"},
		{"time", "agent", "organization", "type", "version"},
	}
	expect := []*Activity{
		{
			Timestamp:    time.Unix(1517849987, 0).UTC(),
			Organization: organization,
			Agent:        agent,
			Type:         activityType,
			Version:      "0.0.0",
		},
		nil,
		nil,
	}

	// Check sanity of test data
	if len(cols) != len(rows) || len(cols) != len(expect) {
		panic(fmt.Sprintf("Inconsistent test data: %d rows, %d cols, %d expect", len(rows), len(cols), len(expect)))
	}

	for i, row := range rows {
		// check sanity of test data
		if len(row) != len(cols[i]) {
			panic(fmt.Sprintf("Inconsistent test data for row %d: %d values, %d cols", i, len(row), len(cols[i])))
		}
		var a Activity
		err := a.FromRow(row, cols[i])
		if expect[i] != nil {
			if err != nil {
				t.Errorf("Failed to convert row: %s", err)
				continue
			}
			if !reflect.DeepEqual(a, *expect[i]) {
				t.Error("Row did not convert successfully")
				t.Logf("Input row: (%s)%s", cols[i], row)
				t.Logf("Expected:  %#v", *expect[i])
				t.Logf("Got:       %#v", a)
				continue
			}
		} else {
			if err == nil {
				t.Error("Row converted successfully, but expected error")
				t.Logf("Input row: (%s)%s", cols[i], row)
				t.Logf("Got:       %v", a)
				continue
			}
		}
	}
}

func TestAddActivity(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live testing...")
	}

	db, err := NewInfluxTSDB(viper.GetString("influxdb.address"), viper.GetString("influxdb.database"))
	if err != nil {
		t.Fatalf("Couldn't create TSDB instance: %s", err)
	}

	s := Activity{
		Timestamp:    time.Unix(1517849987, 0).UTC(),
		Organization: organization,
		Agent:        agent,
		Type:         activityType,
		Version:      "0.0.0",
	}

	err = db.AddActivity(s)
	if err != nil {
		t.Fatalf("Failed to add activity to TSDB: %s", err)
	}

	measurementName := activityMeasurementPrefix + util.Base64Encode(organization)
	res, err := db.client.Query(influx.NewQuery("SELECT * FROM \""+measurementName+"\"", "pantotest", "s"))
	if err != nil {
		t.Fatalf("Failed to query events from TSDB: %s", err)
	}
	if res.Error() != nil {
		t.Fatalf("Failed to query events from TSDB: %s", res.Error())
	}

	// build array of tsdb.AlertEvent from query results
	results := res.Results
	if len(results) == 0 || len(results[0].Series) == 0 {
		t.Fatalf("Query returned no results")
	}
	serie := results[0].Series[0]
	if len(serie.Values) != 1 {
		t.Fatalf("Invalid number of results: expected %d, got %d", 1, len(serie.Values))
	}

	// Check columns
Expected:
	for _, expect := range []string{"time", "agent", "organization", "type", "version"} {
		for _, got := range serie.Columns {
			if got == expect {
				continue Expected // to next column
			}
		}
		t.Fatalf("Expected column \"%s\" not found: %s", expect, serie.Columns)
	}
Got:
	for _, got := range serie.Columns {
		for _, expect := range []string{"time", "agent", "organization", "type", "version"} {
			if got == expect {
				continue Got // to next column
			}
		}
		t.Fatalf("Unexpected column \"%s\" was found: %s", got, serie.Columns)
	}

	row := serie.Values[0]
	t.Logf("Row 0: %s%s", serie.Columns, row)
	for i, col := range serie.Columns {
		switch col {
		case "time":
			n, ok := row[i].(json.Number)
			if ok {
				if n2, _ := n.Int64(); n2 == 1517849987 {
					continue
				}
			}
			t.Fatalf("Invalid timestamp in row 0: expected %v, got %v", 1517849987, row[i])
		case keyTagAgent:
			if row[i].(string) != util.Base64Encode(agent) {
				t.Fatalf("Invalid agent in row 0: expected %v, got %v", util.Base64Encode(agent), row[i])
			}
		case keyTagOrganization:
			if row[i].(string) != util.Base64Encode(organization) {
				t.Fatalf("Invalid organization in row 0: expected %v, got %v", util.Base64Encode(organization), row[i])
			}
		case keyFieldType:
			if row[i].(string) != activityType {
				t.Fatalf("Invalid event in row 0: expected %v, got %v", activityType, row[i])
			}
		case keyFieldVersion:
			if row[i].(string) != "0.0.0" {
				t.Fatalf("Invalid event in row 0: expected %v, got %v", "0.0.0", row[i])
			}
		default:
			if row[i] != nil {
				t.Fatalf("Invalid field in row 0: (\"%s\": %v)", col, row[i])
			}
		}
	}
}

func TestGetLastActivity(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live testing...")
	}

	db, err := NewInfluxTSDB(viper.GetString("influxdb.address"), viper.GetString("influxdb.database"))
	if err != nil {
		t.Fatalf("Couldn't create TSDB instance: %s", err)
	}

	res, err := db.GetActivity(organization, agent, Count(1), OrderBy(OrderDescending))
	if err != nil {
		t.Fatalf("Could not get last activities: %s", err)
	}
	t.Logf("Activities: %v", res)
	if len(res) != 1 {
		t.Fatalf("Invalid number of results: expected %d, got %d", 1, len(res))
	}

	expect := Activity{
		Timestamp:    time.Unix(1517849987, 0).UTC(),
		Organization: organization,
		Agent:        agent,
		Type:         activityType,
		Version:      "0.0.0",
	}
	if !reflect.DeepEqual(res[0], expect) {
		t.Error("Unexpected entry for last result")
		t.Logf("Expected:  %v", expect)
		t.Logf("Got:       %v", res[0])
	}

	res, err = db.GetActivity(organization, agent, From(time.Unix(1517849980, 0).UTC()), To(time.Now()))
	if err != nil {
		t.Fatalf("Could not get activity in range: %s", err)
	}
	t.Logf("Activities: %v", res)
	if len(res) != 1 {
		t.Fatalf("Invalid number of activity: expected %d, got %d", 1, len(res))
	}

	expect = Activity{
		Timestamp:    time.Unix(1517849987, 0).UTC(),
		Organization: organization,
		Agent:        agent,
		Type:         activityType,
		Version:      "0.0.0",
	}
	if !reflect.DeepEqual(res[0], expect) {
		t.Error("Unexpected entry for last result")
		t.Logf("Expected:  %v", expect)
		t.Logf("Got:       %v", res[0])
	}
}
