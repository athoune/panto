// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package tsdb

import (
	"encoding/json"
	"fmt"
	"math"
	"strings"
	"time"

	"gitlab.com/pantomath-io/panto/util"

	"github.com/google/uuid"
	influx "github.com/influxdata/influxdb/client/v2"
)

// Result contains all the data for the server to enter a
// result from a probe into the time-series database.
type Result struct {
	Entry

	Tags         map[string]string
	Fields       map[string]interface{}
	Error        bool
	ErrorMessage string
}

// FromRow populates a `Result` using a row from a TSDB query. Optionally takes a schema to infer types
func (r *Result) FromRow(row []interface{}, columns []string, tags map[string]string, sch *schema) (err error) {
	err = r.Entry.FromRow(row, columns, tags)
	if err != nil {
		return
	}

	// Check for error
	for i, col := range columns {
		if col == keyFieldError {
			if errorMessage, ok := row[i].(string); ok && len(errorMessage) > 0 {
				r.Error = true
				r.ErrorMessage = errorMessage
				return nil
			}
		}
	}

	r.Fields = map[string]interface{}{}
	r.Tags = map[string]string{}
	for i, col := range columns {
		err = r.fromRowCol(row[i], col, sch)
		if err != nil {
			return
		}
	}
	for k, v := range tags {
		if util.Contains(reserved, k) {
			// explicitly ignore known tags or they end up duplicated
			continue
		}
		if sch != nil && !sch.tag(k) {
			return fmt.Errorf("schema mismatch: %s not found in schema", k)
		}
		r.Tags[k] = v
	}
	return
}

// fromRowCol fills a field or a tag in the result from a value and the name of the column it is from. It optionally
// takes a measurement schema to infer field types and tags. If the schema is `nil`, the value is guessed from JSON
// and set as a field.
func (r *Result) fromRowCol(v interface{}, c string, s *schema) error {
	// explicitly ignore known tags or they end up in the fields
	if util.Contains(reserved, c) {
		return nil
	}
	if v == nil {
		return nil
	}
	if s == nil {
		// No schema, try to guess from JSON
		// check row type, and try to unmarshal it from JSON
		switch j := v.(type) {
		case json.Number:
			if n, err := j.Int64(); err == nil {
				// the number is an integer
				r.Fields[c] = n
			} else if n, err := j.Float64(); err == nil {
				// the number is a float
				r.Fields[c] = n
			} else {
				// the number is not a number
				return fmt.Errorf("unable to parse JSON number: %s", j.String())
			}
		default:
			r.Fields[c] = v
		}
	} else {
		// Use schema to infer types
		if t, ok := s.field(c); ok {
			// It's a field, use the type from the schema
			switch t {
			case fieldTypeFloat:
				if j, ok := v.(json.Number); ok {
					if f, err := j.Float64(); err == nil {
						r.Fields[c] = f
						return nil
					}
				}
				return fmt.Errorf("schema mismatch: %s field %v is not a float", c, v)
			case fieldTypeInteger:
				if j, ok := v.(json.Number); ok {
					if f, err := j.Int64(); err == nil {
						r.Fields[c] = f
						return nil
					}
				}
				return fmt.Errorf("schema mismatch: %s field %v is not an integer", c, v)
			case fieldTypeString:
				if f, ok := v.(string); ok {
					r.Fields[c] = f
				} else {
					return fmt.Errorf("schema mismatch: %s field %v is not a string", c, v)
				}
			case fieldTypeBoolean:
				if f, ok := v.(bool); ok {
					r.Fields[c] = f
				} else {
					return fmt.Errorf("schema mismatch: %s field %v is not a boolean", c, v)
				}
			case fieldTypeTimestamp:
				if s, ok := v.(string); ok {
					if f, err := time.Parse(time.RFC3339Nano, s); err == nil {
						r.Fields[c] = f
						return nil
					}
				}
				return fmt.Errorf("schema mismatch: %s field %v is not a timestamp", c, v)
			default:
				return fmt.Errorf("schema error: %s field is unknown type %s", c, t)
			}
		} else if ok := s.tag(c); ok {
			// It's a tag
			if tag, ok := v.(string); ok {
				r.Tags[c] = tag
			} else {
				return fmt.Errorf("schema mismatch: %s tag %v is not a string", c, v)
			}
		} else {
			return fmt.Errorf("schema mismatch: %s not found in schema", c)
		}
	}

	return nil
}

// AddResults adds a batch of results from a check to the TSDB.
func (tsdb *InfluxTSDB) AddResults(results []Result) error {

	// Create a new point batch
	batch, err := influx.NewBatchPoints(influx.BatchPointsConfig{
		Database: tsdb.database,
	})
	if err != nil {
		return err
	}

	for _, result := range results {
		tags := result.Tags
		if tags == nil {
			tags = make(map[string]string)
		} else {
			for _, k := range reserved {
				if _, ok := tags[k]; ok {
					return fmt.Errorf("invalid key found in tags: %s", k)
				}
			}
		}
		tags[keyTagOrganization] = util.Base64Encode(result.Organization)
		tags[keyTagProbeConfiguration] = util.Base64Encode(result.ProbeConfiguration)
		tags[keyTagCheck] = util.Base64Encode(result.Check)
		tags[keyTagProbe] = util.Base64Encode(result.Probe)
		tags[keyTagTarget] = util.Base64Encode(result.Target)
		tags[keyTagAgent] = util.Base64Encode(result.Agent)

		fields := result.Fields
		if !result.Error {
			// Check input
			if fields == nil {
				return fmt.Errorf("missing fields map in probe result")
			}
			if len(fields) == 0 {
				return fmt.Errorf("no fields in probe result")
			}

			// ***** HACK *****
			// chrales 2017-11-16
			// Influxdb storage engine does not support uint64 at the time of writing
			// but the client library acts as though it does. If we find one of these types in the
			// fields, we cast it down to a type the engine understands.
			for k, v := range fields {
				switch v2 := v.(type) {
				case uint64:
					if v2 > math.MaxInt64 {
						return fmt.Errorf("value out of range: %d", v)
					}
					fields[k] = int64(v2)
				}
			}
		} else {
			fields = map[string]interface{}{keyFieldError: result.ErrorMessage}
		}

		// Create point
		point, err := influx.NewPoint(util.Base64Encode(result.Check), tags, fields, result.Timestamp)
		if err != nil {
			return err
		}
		batch.AddPoint(point)
	}

	// Write the batch
	return tsdb.client.Write(batch)
}

// GetResults retrieves results from the TSDB following the given filters
func (tsdb *InfluxTSDB) GetResults(check uuid.UUID, opts ...QueryOption) ([]Result, error) {
	// configure options
	o := new(queryOptions)
	for _, f := range opts {
		f(o)
	}

	log.Debugf(
		"Retrieving results from %s (%s)",
		check, o,
	)

	query := fmt.Sprintf("SELECT * FROM \"%s\" %s", util.Base64Encode(check), o.queryClauses())

	return queryResults(tsdb, check, query)
}

func queryResults(tsdb *InfluxTSDB, check uuid.UUID, cmd string) ([]Result, error) {
	measurementName := util.Base64Encode(check)
	sch, hasSchema := tsdb.measurements.Load(measurementName)
	if !hasSchema {
		// Fetch measurement schema along with results
		cmd = strings.Join(
			[]string{
				fmt.Sprintf("SHOW FIELD KEYS FROM \"%s\"", measurementName),
				fmt.Sprintf("SHOW TAG KEYS FROM \"%s\"", measurementName),
				cmd,
			},
			"; ",
		)
	}

	// run the query against the TSDB
	q := influx.Query{
		Command:  cmd,
		Database: tsdb.database,
	}
	log.Infof("Fetching results from %s", util.Base64Encode(check))
	log.Debugf("Query: %s", cmd)
	response, err := tsdb.client.Query(q)
	if err != nil {
		return nil, err
	}
	if response.Error() != nil {
		return nil, response.Error()
	}

	results := response.Results

	// parse schema
	if !hasSchema {
		if len(results) != 3 {
			return nil, fmt.Errorf("error fetching measurement schema")
		} else if len(results[0].Series) == 0 || len(results[1].Series) == 0 {
			// the measurement does not exist
			return []Result{}, nil
		}

		sch, err = parseSchema(results[0].Series[0].Values, results[1].Series[0].Values)
		if err != nil {
			return nil, fmt.Errorf("error parsing measurement schema: %s", err)
		}
		tsdb.measurements.Store(measurementName, sch)
		results = results[2:]
	}

	// build array of tsdb.Result from query results
	if len(results) == 0 || len(results[0].Series) == 0 {
		log.Infof("Fetched 0 rows")
		return []Result{}, nil
	}
	serie := results[0].Series[0]
	log.Infof("Fetched %d rows", len(serie.Values))
	res := make([]Result, len(serie.Values))
	s := sch.(schema)
	for i, row := range serie.Values {
		if err = res[i].FromRow(row, serie.Columns, serie.Tags, &s); err != nil {
			return nil, err
		}
	}
	return res, nil
}
