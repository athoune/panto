// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package tsdb

import (
	"fmt"
	"time"

	"gitlab.com/pantomath-io/panto/util"

	"github.com/google/uuid"
	influx "github.com/influxdata/influxdb/client/v2"
)

const activityMeasurementPrefix = "activity-"
const keyFieldType = "type"
const keyFieldVersion = "version"

// Activity contains the data from an agent activity
type Activity struct {
	Timestamp    time.Time
	Organization uuid.UUID
	Agent        uuid.UUID
	Version      string
	Type         string
}

// FromRow populates an `Activity` using a row from a TSDB query.
func (a *Activity) FromRow(row []interface{}, columns []string) (err error) {
	for i, col := range columns {
		switch col {
		case "time":
			a.Timestamp, err = time.Parse(time.RFC3339Nano, row[i].(string))
		case keyFieldType:
			atype, ok := row[i].(string)
			if !ok {
				err = fmt.Errorf("type from TSDB is not a string")
			}
			a.Type = atype
		case keyFieldVersion:
			v, ok := row[i].(string)
			if !ok {
				err = fmt.Errorf("agent version in not a string")
			}
			a.Version = v
		case keyTagAgent:
			agent, ok := row[i].(string)
			if !ok {
				err = fmt.Errorf("agent from TSDB is not a string")
			} else {
				a.Agent, err = util.Base64Decode(agent)
			}
		case keyTagOrganization:
			organization, ok := row[i].(string)
			if !ok {
				err = fmt.Errorf("organization from TSDB is not a string")
			} else {
				a.Organization, err = util.Base64Decode(organization)
			}
		}

		if err != nil {
			return
		}
	}
	return
}

// AddActivity adds an activity into the TSDB
func (tsdb *InfluxTSDB) AddActivity(activity Activity) error {
	// Create a new point batch
	batch, err := influx.NewBatchPoints(influx.BatchPointsConfig{
		Database: tsdb.database,
	})
	if err != nil {
		return err
	}

	fields := map[string]interface{}{
		keyFieldType:    activity.Type,
		keyFieldVersion: activity.Version,
	}

	tags := map[string]string{
		keyTagAgent:        util.Base64Encode(activity.Agent),
		keyTagOrganization: util.Base64Encode(activity.Organization),
	}

	// Set measurement name
	measurementName := activityMeasurementPrefix + util.Base64Encode(activity.Organization)

	point, err := influx.NewPoint(measurementName, tags, fields, activity.Timestamp)
	if err != nil {
		return err
	}
	batch.AddPoint(point)

	// Write the batch
	return tsdb.client.Write(batch)
}

// GetActivity retrieves the activity for an agent from the TSDB.
func (tsdb *InfluxTSDB) GetActivity(organization uuid.UUID, agent uuid.UUID, opts ...QueryOption) ([]Activity, error) {
	// Agent tag constraint
	opts = append(opts, Tag("agent", util.Base64Encode(agent)))

	// configure options
	o := new(queryOptions)
	for _, f := range opts {
		f(o)
	}

	log.Debugf(
		"Retrieving activity for agent:%s [organization:%s] (%s)",
		agent.String(), organization.String(), o,
	)

	// prepare statement
	cmd := fmt.Sprintf(
		"SELECT * FROM \"%s%s\" %s",
		activityMeasurementPrefix, util.Base64Encode(organization), o.queryClauses(),
	)

	q := influx.Query{
		Command:  cmd,
		Database: tsdb.database,
	}
	log.Infof("Fetching activity for %s", util.Base64Encode(agent))
	log.Debugf("Query: %s", cmd)
	response, err := tsdb.client.Query(q)
	if err != nil {
		return nil, err
	}
	if response.Error() != nil {
		return nil, response.Error()
	}

	// build array of tsdb.Activity from query results
	results := response.Results
	if len(results) == 0 || len(results[0].Series) == 0 {
		log.Infof("Fetched 0 rows")
		return []Activity{}, nil
	}
	serie := results[0].Series[0]
	log.Infof("Fetched %d rows", len(serie.Values))
	res := make([]Activity, len(serie.Values))
	for i, row := range serie.Values {
		if err = res[i].FromRow(row, serie.Columns); err != nil {
			return nil, err
		}
	}
	return res, nil
}
