// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package tsdb

import (
	"fmt"
)

// fieldType is an enumeration mapping InfluxDB types
type fieldType int8

const (
	fieldTypeFloat fieldType = iota
	fieldTypeInteger
	fieldTypeString
	fieldTypeBoolean
	fieldTypeTimestamp
)

func (t fieldType) String() string {
	switch t {
	case fieldTypeFloat:
		return "float"
	case fieldTypeInteger:
		return "integer"
	case fieldTypeString:
		return "string"
	case fieldTypeBoolean:
		return "boolean"
	case fieldTypeTimestamp:
		return "timestamp"
	default:
		return "unknown"
	}
}

// schema is a structure containing the schema of an InfluxDB measurement
type schema struct {
	// fields maps measurement field keys to their types
	fields map[string]fieldType
	// tags maps tag keys to a boolean. The boolean can be anything, the map is just a hack to implement a set
	tags map[string]bool
}

// field is a shorthand for looking up the fields map
func (s schema) field(key string) (t fieldType, ok bool) {
	t, ok = s.fields[key]
	return
}

// tag is a shorthand for looking up the tags map
func (s schema) tag(key string) (ok bool) {
	_, ok = s.tags[key]
	return
}

func parseSchema(fields, tags [][]interface{}) (schema, error) {
	s := schema{fields: make(map[string]fieldType), tags: make(map[string]bool)}
	for _, f := range fields {
		k := f[0].(string)
		switch f[1].(string) {
		case "float":
			s.fields[k] = fieldTypeFloat
		case "integer":
			s.fields[k] = fieldTypeInteger
		case "string":
			s.fields[k] = fieldTypeString
		case "boolean":
			s.fields[k] = fieldTypeBoolean
		case "timestamp":
			s.fields[k] = fieldTypeTimestamp
		default:
			return schema{}, fmt.Errorf("Unknown type in schema: %s", f[1].(string))
		}
	}
	for _, t := range tags {
		s.tags[t[0].(string)] = true
	}
	return s, nil
}
