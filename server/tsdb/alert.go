// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package tsdb

import (
	"fmt"
	"strings"
	"time"

	"gitlab.com/pantomath-io/panto/api"
	"gitlab.com/pantomath-io/panto/util"

	"github.com/google/uuid"
	influx "github.com/influxdata/influxdb/client/v2"
)

const alertMeasurementPrefix = "alert-"
const keyFieldAlertEvent = "event"

// EventValue encodes a value evaluated for an event
type EventValue int

const (
	// EventValueNotified encodes a "notified" event
	EventValueNotified = EventValue(api.Event_NOTIFIED)
	// EventValueAcknowledged encodes an "acknowledged" event
	EventValueAcknowledged = EventValue(api.Event_ACKNOWLEDGED)
	// EventValueSnoozed encodes a "snoozed" event
	EventValueSnoozed = EventValue(api.Event_SNOOZED)
)

func (ev EventValue) String() string {
	switch ev {
	case EventValueNotified:
		return "notified"
	case EventValueAcknowledged:
		return "acknowledged"
	case EventValueSnoozed:
		return "snoozed"
	default:
		panic(fmt.Sprintf("%d is not a valid EventValue", int(ev)))
	}
}

// EventValueFromString convert a string in EventValue
func EventValueFromString(s string) (ev EventValue, err error) {
	switch s {
	case "notified":
		ev = EventValueNotified
	case "acknowledged":
		ev = EventValueAcknowledged
	case "snoozed":
		ev = EventValueSnoozed
	default:
		return ev, fmt.Errorf("%s is not a valid EventValue", s)
	}

	return
}

// AlertEvent contains the data from an alert event in the TSDB.
type AlertEvent struct {
	Entry
	Event EventValue
}

// FromRow populates an `AlertEvent` using a row from a TSDB query.
func (a *AlertEvent) FromRow(row []interface{}, columns []string, tags map[string]string) (err error) {
	err = a.Entry.FromRow(row, columns, tags)
	if err != nil {
		return
	}
	for i, col := range columns {
		switch col {
		case keyFieldAlertEvent:
			event, ok := row[i].(string)
			if ok {
				ev, err := EventValueFromString(event)
				if err != nil {
					err = fmt.Errorf("unable to convert event value: %s", err)
					return err
				}
				a.Event = ev
			} else {
				err = fmt.Errorf("event from TSDB is not an EventValue")
			}
		}
		if err != nil {
			return
		}
	}
	for k, tag := range tags {
		switch k {
		case keyTagProbeConfiguration:
			pcUUID, err := util.Base64Decode(tag)
			if err != nil {
				err = fmt.Errorf("unable to convert probe_configuration tag: %s", err)
				return err
			}
			a.ProbeConfiguration = pcUUID
		}
		if err != nil {
			return
		}
	}
	return
}

// AddAlertEvent adds a new alert event to the TSDB.
func (tsdb *InfluxTSDB) AddAlertEvent(alert AlertEvent) error {
	// Create a new point batch
	batch, err := influx.NewBatchPoints(influx.BatchPointsConfig{
		Database: tsdb.database,
	})
	if err != nil {
		return err
	}

	// Create fields and tags map
	fields := map[string]interface{}{
		keyFieldAlertEvent: alert.Event,
	}
	tags := map[string]string{
		keyTagOrganization:       util.Base64Encode(alert.Organization),
		keyTagCheck:              util.Base64Encode(alert.Check),
		keyTagProbe:              util.Base64Encode(alert.Probe),
		keyTagTarget:             util.Base64Encode(alert.Target),
		keyTagAgent:              util.Base64Encode(alert.Agent),
		keyTagProbeConfiguration: util.Base64Encode(alert.ProbeConfiguration),
	}

	// Set timestamp to "now" by default
	var ts time.Time
	if alert.Timestamp.IsZero() {
		ts = time.Now()
	} else {
		ts = alert.Timestamp
	}

	// Set measurement name
	measurementName := alertMeasurementPrefix + util.Base64Encode(alert.ProbeConfiguration)

	point, err := influx.NewPoint(measurementName, tags, fields, ts)
	if err != nil {
		return err
	}
	batch.AddPoint(point)

	// Write the batch
	return tsdb.client.Write(batch)
}

// GetAlertEvents retrieves the alert events for a probeConfiguration from the TSDB.
func (tsdb *InfluxTSDB) GetAlertEvents(probeConfigurations []uuid.UUID, opts ...QueryOption) ([]AlertEvent, error) {
	// configure options
	o := new(queryOptions)
	for _, f := range opts {
		f(o)
	}

	alertMeasurements := make([]string, len(probeConfigurations))
	for i, probeConfiguration := range probeConfigurations {
		alertMeasurements[i] = fmt.Sprintf("\"%s%s\"", alertMeasurementPrefix, util.Base64Encode(probeConfiguration))
	}

	log.Debugf(
		"Retrieving alerts for %s (%s)",
		alertMeasurements, o,
	)

	// prepare statement
	query := fmt.Sprintf(
		"SELECT * FROM %s %s",
		strings.Join(alertMeasurements, ","), o.queryClauses(),
	)

	return queryAlertEvents(tsdb, probeConfigurations, query)
}

func queryAlertEvents(tsdb *InfluxTSDB, probeConfigurations []uuid.UUID, cmd string) ([]AlertEvent, error) {
	// run the query against the TSDB
	q := influx.Query{
		Command:  cmd,
		Database: tsdb.database,
	}

	probeConfigurationMeasurements := make([]string, len(probeConfigurations))
	for i, probeConfiguration := range probeConfigurations {
		probeConfigurationMeasurements[i] = util.Base64Encode(probeConfiguration)
	}
	log.Infof("Fetching alert events for %s", strings.Join(probeConfigurationMeasurements, ","))
	log.Debugf("Query: %s", cmd)
	response, err := tsdb.client.Query(q)
	if err != nil {
		return nil, err
	}
	if response.Error() != nil {
		return nil, response.Error()
	}

	// build array of tsdb.AlertEvent from query results
	results := response.Results
	if len(results) == 0 || len(results[0].Series) == 0 {
		log.Infof("Fetched 0 rows")
		return []AlertEvent{}, nil
	}

	res := make([]AlertEvent, 0)

	for _, serie := range results[0].Series {
		log.Infof("Fetched %d rows", len(serie.Values))
		for _, row := range serie.Values {
			a := AlertEvent{}
			err = a.FromRow(row, serie.Columns, serie.Tags)
			if err != nil {
				return nil, err
			}
			res = append(res, a)
		}
	}
	return res, nil
}
