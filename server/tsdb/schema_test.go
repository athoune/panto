// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package tsdb

import (
	"fmt"
	"reflect"
	"testing"
)

func TestField(t *testing.T) {
	s := schema{
		fields: map[string]fieldType{
			"field_float":     fieldTypeFloat,
			"field_integer":   fieldTypeInteger,
			"field_string":    fieldTypeString,
			"field_boolean":   fieldTypeBoolean,
			"field_timestamp": fieldTypeTimestamp,
		},
		tags: map[string]bool{
			"tag_test": true,
		},
	}

	f, ok := s.field("field_float")
	if !ok {
		t.Error("Field \"field_float\" not found")
	}
	if f != fieldTypeFloat {
		t.Errorf("Field \"field_float\" is not of type float")
	}

	f, ok = s.field("field_integer")
	if !ok {
		t.Error("Field \"field_integer\" not found")
	}
	if f != fieldTypeInteger {
		t.Errorf("Field \"field_integer\" is not of type integer")
	}

	_, ok = s.field("field_missing")
	if ok {
		t.Error("Field \"field_missing\" was found")
	}

	_, ok = s.field("tag_test")
	if ok {
		t.Error("Field \"tag_test\" was found")
	}
}

func TestTag(t *testing.T) {
	s := schema{
		fields: map[string]fieldType{
			"field_float":     fieldTypeFloat,
			"field_integer":   fieldTypeInteger,
			"field_string":    fieldTypeString,
			"field_boolean":   fieldTypeBoolean,
			"field_timestamp": fieldTypeTimestamp,
		},
		tags: map[string]bool{
			"tag_test": true,
		},
	}

	if !s.tag("tag_test") {
		t.Error("Tag \"tag_test\" not found")
	}
	if s.tag("tag_missing") {
		t.Error("Tag \"tag_missing\" was found")
	}
	if s.tag("field_float") {
		t.Error("Tag \"field_float\" was found")
	}
}

func TestParseSchema(t *testing.T) {
	// List of test cases
	// A test case is a list of rows
	// A row is a list of entries (key, type)
	// -> [][][]interface
	fields := [][][]interface{}{
		// Test case 0 - valid schema with all cases
		{
			// Rows
			{"field_float", "float"},
			{"field_integer", "integer"},
			{"field_string", "string"},
			{"field_boolean", "boolean"},
			{"field_timestamp", "timestamp"},
		},
		// Test case 1 - empty schema
		{},
		// Test case 3 - invalid type
		{
			{"field_invalid", "hello"},
		},
	}
	tags := [][][]interface{}{
		// Test case 0 - valid schema with all cases
		{
			// Rows
			{"tag_test"},
		},
		// Test case 1 - empty schema
		{},
		// Test case 3 - invalid type
		{},
	}
	expect := []schema{
		{
			fields: map[string]fieldType{
				"field_float":     fieldTypeFloat,
				"field_integer":   fieldTypeInteger,
				"field_string":    fieldTypeString,
				"field_boolean":   fieldTypeBoolean,
				"field_timestamp": fieldTypeTimestamp,
			},
			tags: map[string]bool{
				"tag_test": true,
			},
		},
		{fields: make(map[string]fieldType), tags: make(map[string]bool)},
		{},
	}
	if len(fields) != len(expect) || len(tags) != len(fields) || len(tags) != len(expect) {
		panic(fmt.Sprintf("Inconsistent test data: %d fields, %d tags, %d expect", len(fields), len(tags), len(expect)))
	}
	for i, f := range fields {
		s, err := parseSchema(f, tags[i])
		if !reflect.DeepEqual(s, expect[i]) {
			t.Error("parseSchema returned unexpected schema")
			t.Logf("Fields:    %#v", f)
			t.Logf("Tags:      %#v", tags[i])
			t.Logf("Expected:  %#v", expect[i])
			t.Logf("Got:       %#v", s)
		}
		if reflect.DeepEqual(s, schema{}) && err == nil {
			t.Error("parseSchema did not return error")
			t.Logf("Fields:    %#v", f)
			t.Logf("Tags:      %#v", tags[i])
			t.Logf("Expected:  %#v", expect[i])
			t.Logf("Got:       %#v", s)
		}
	}
}
