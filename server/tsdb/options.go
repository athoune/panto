// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package tsdb

import (
	"fmt"
	"strings"
	"time"
)

// QueryOption is used to configure queries on the TSDB
type QueryOption func(*queryOptions)

// From returns a QueryOption that defines a lower time bound to the results of the TSDB query. The query will return
// results starting after and including the given time.
func From(t time.Time) QueryOption {
	return func(opts *queryOptions) {
		opts.from = &t
	}
}

// To returns a QueryOption that defines an upper time bound to the results of the TSDB query. The query will return
// results before and not including the given time.
func To(t time.Time) QueryOption {
	return func(opts *queryOptions) {
		opts.to = &t
	}
}

// Count returns a QueryOption that defines a maximum number of results from the TSDB query.
func Count(c int) QueryOption {
	return func(opts *queryOptions) {
		opts.count = &c
	}
}

// Offset returns a QueryOption that offsets the result set from the TSDB query. Used for pagination.
func Offset(o int) QueryOption {
	return func(opts *queryOptions) {
		opts.offset = &o
	}
}

// OrderBy returns a QueryOption that defines the ordering according to time of the results from the TSDB query.
func OrderBy(o Order) QueryOption {
	return func(opts *queryOptions) {
		opts.orderBy = &o
	}
}

// GroupBy returns a QueryOption that defines the grouping of the results from the TSDB query.
func GroupBy(g string) QueryOption {
	return func(opts *queryOptions) {
		opts.groupBy = g
	}
}

// Tag returns a QueryOption that specifies a column-value pair that the results from the TSDB must match.
func Tag(k string, v ...string) QueryOption {
	return func(opts *queryOptions) {
		if opts.tags == nil {
			opts.tags = make(map[string][]string)
		}
		opts.tags[k] = v
	}
}

// Field returns a QueryOption that specifies a column-value pair that the results from the TSDB must match.
func Field(k, v string) QueryOption {
	return func(opts *queryOptions) {
		if opts.fields == nil {
			opts.fields = make(map[string]string)
		}
		opts.fields[k] = v
	}
}

// queryOptions is a structure to pass constraints on a TSDB query. A `nil` value in any of these
// fields represents an unset option, and lets the underlying implementation decide the default.
type queryOptions struct {
	from    *time.Time
	to      *time.Time
	count   *int
	offset  *int
	orderBy *Order
	groupBy string
	tags    map[string][]string
	fields  map[string]string
}

func (opts queryOptions) String() string {
	fields := make([]string, 0, 6)
	if opts.from != nil {
		fields = append(fields, fmt.Sprintf("From: %s", *opts.from))
	}
	if opts.to != nil {
		fields = append(fields, fmt.Sprintf("To: %s", *opts.to))
	}
	if opts.tags != nil {
		fields = append(fields, fmt.Sprintf("Tags: %s", opts.tags))
	}
	if opts.fields != nil {
		fields = append(fields, fmt.Sprintf("Fields: %s", opts.fields))
	}
	if opts.orderBy != nil {
		fields = append(fields, fmt.Sprintf("OrderBy: %s", *opts.orderBy))
	}
	if opts.groupBy != "" {
		fields = append(fields, fmt.Sprintf("GroupBy: %s", opts.groupBy))
	}
	if opts.count != nil {
		fields = append(fields, fmt.Sprintf("Count: %d", *opts.count))
	}
	if opts.offset != nil {
		fields = append(fields, fmt.Sprintf("Offset: %d", *opts.offset))
	}

	return fmt.Sprintf("tsdb.queryOptions{%s}", strings.Join(fields, ", "))
}

func (opts queryOptions) queryClauses() string {
	b := new(strings.Builder)

	// WHERE clause
	conditions := make([]string, 0, len(opts.tags)+2)
	if opts.from != nil {
		conditions = append(conditions, fmt.Sprintf("time>=%d", opts.from.UnixNano()))
	}
	if opts.to != nil {
		conditions = append(conditions, fmt.Sprintf("time<%d", opts.to.UnixNano()))
	}
	for k, v := range opts.tags {
		tags := make([]string, len(v))
		for i, value := range v {
			tags[i] = fmt.Sprintf("%s='%s'", k, value)
		}
		conditions = append(conditions, fmt.Sprintf("(%s)", strings.Join(tags, " OR ")))
	}
	for k, v := range opts.fields {
		conditions = append(conditions, fmt.Sprintf("%s='%s'", k, v))
	}
	if len(conditions) > 0 {
		fmt.Fprintf(b, "WHERE %s ", strings.Join(conditions, " AND "))
	}

	// GROUP BY clause
	if opts.groupBy != "" {
		fmt.Fprintf(b, "GROUP BY %s ", opts.groupBy)
	}

	// ORDER BY clause
	if opts.orderBy != nil {
		fmt.Fprint(b, "ORDER BY time ")
		if *opts.orderBy == OrderAscending {
			fmt.Fprint(b, "ASC ")
		} else {
			fmt.Fprint(b, "DESC ")
		}
	}

	// LIMIT and OFFSET
	if opts.count != nil {
		fmt.Fprintf(b, "LIMIT %d ", *opts.count)
	}
	if opts.offset != nil {
		fmt.Fprintf(b, "OFFSET %d", *opts.offset)
	}

	return b.String()
}
