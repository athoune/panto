// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package tsdb

import (
	"encoding/json"
	"fmt"
	"reflect"
	"testing"
	"time"

	"gitlab.com/pantomath-io/panto/util"

	influx "github.com/influxdata/influxdb/client/v2"
	"github.com/spf13/viper"
)

func TestResultFromRow(t *testing.T) {
	rows := [][]interface{}{
		{"2018-02-05T16:59:47Z", "kDtJz8EqQ0C7KgOF6qwxfg", "i2QYtZBVSnuXjLhQhuAV6w", "noLlUczuSJqkyIEynnCoow", "xUkSnG_BSVCn-AX1khPLYg", "MQF0eIJcRZyEBgG_sdLPWA", "_KjCCng_T3GWxe-_lqH3PQ"},                                        // Valid row
		{"2018-02-05T16:59:47Z", "kDtJz8EqQ0C7KgOF6qwxfg", "i2QYtZBVSnuXjLhQhuAV6w", "noLlUczuSJqkyIEynnCoow", "xUkSnG_BSVCn-AX1khPLYg", "MQF0eIJcRZyEBgG_sdLPWA", "_KjCCng_T3GWxe-_lqH3PQ", int64(1234), "Hello World!"},           // Valid row
		{"2018-02-05T16:59:47Z", "kDtJz8EqQ0C7KgOF6qwxfg", "i2QYtZBVSnuXjLhQhuAV6w", "noLlUczuSJqkyIEynnCoow", "xUkSnG_BSVCn-AX1khPLYg", "MQF0eIJcRZyEBgG_sdLPWA", "_KjCCng_T3GWxe-_lqH3PQ", json.Number("1234"), "Hello World!"},   // Valid row
		{"2018-02-05T16:59:47Z", "kDtJz8EqQ0C7KgOF6qwxfg", "i2QYtZBVSnuXjLhQhuAV6w", "noLlUczuSJqkyIEynnCoow", "xUkSnG_BSVCn-AX1khPLYg", "MQF0eIJcRZyEBgG_sdLPWA", "_KjCCng_T3GWxe-_lqH3PQ", float64(1234), "Hello World!"},         // Valid row
		{"2018-02-05T16:59:47Z", "kDtJz8EqQ0C7KgOF6qwxfg", "i2QYtZBVSnuXjLhQhuAV6w", "noLlUczuSJqkyIEynnCoow", "xUkSnG_BSVCn-AX1khPLYg", "MQF0eIJcRZyEBgG_sdLPWA", "_KjCCng_T3GWxe-_lqH3PQ", json.Number("1234.0"), "Hello World!"}, // Valid row
		{"2018-02-05T16:59:47Z", "kDtJz8EqQ0C7KgOF6qwxfg", "i2QYtZBVSnuXjLhQhuAV6w", ":=$'^(`ùf$^s", "xUkSnG_BSVCn-AX1khPLYg", "MQF0eIJcRZyEBgG_sdLPWA", "_KjCCng_T3GWxe-_lqH3PQ"},                                                  // Gibberish agent
		{"2018-02-05T16:59:47Z", "kDtJz8EqQ0C7KgOF6qwxfg", "i2QYtZBVSnuXjLhQhuAV6w", "xUkSnG_BSVCn-AX1khPLYg", "MQF0eIJcRZyEBgG_sdLPWA", "_KjCCng_T3GWxe-_lqH3PQ"},                                                                  // Missing agent
		{"2018-02-05T16:59:47Z", "kDtJz8EqQ0C7KgOF6qwxfg", "i2QYtZBVSnuXjLhQhuAV6w", "noLlUczuSJqkyIEynnCoow", "xUkSnG_BSVCn-AX1khPLYg", "MQF0eIJcRZyEBgG_sdLPWA", "_KjCCng_T3GWxe-_lqH3PQ", json.Number("1234.0"), "Hello World!"}, // Valid row
		{"2018-02-05T16:59:47Z", "kDtJz8EqQ0C7KgOF6qwxfg", "i2QYtZBVSnuXjLhQhuAV6w", "noLlUczuSJqkyIEynnCoow", "xUkSnG_BSVCn-AX1khPLYg", "MQF0eIJcRZyEBgG_sdLPWA", "_KjCCng_T3GWxe-_lqH3PQ", "failed to connect"},                   // Valid row
		{"2018-02-05T16:59:47Z", "kDtJz8EqQ0C7KgOF6qwxfg", "i2QYtZBVSnuXjLhQhuAV6w", "noLlUczuSJqkyIEynnCoow", "xUkSnG_BSVCn-AX1khPLYg", "MQF0eIJcRZyEBgG_sdLPWA", "_KjCCng_T3GWxe-_lqH3PQ", int64(1234), "Hello World!", ""},       // Valid row with empty error message
	}
	cols := [][]string{
		{"time", "organization", "check", "agent", "probe", "target", "probe_configuration"},
		{"time", "organization", "check", "agent", "probe", "target", "probe_configuration", "number", "message"},
		{"time", "organization", "check", "agent", "probe", "target", "probe_configuration", "number", "message"},
		{"time", "organization", "check", "agent", "probe", "target", "probe_configuration", "number", "message"},
		{"time", "organization", "check", "agent", "probe", "target", "probe_configuration", "number", "message"},
		{"time", "organization", "check", "agent", "probe", "target", "probe_configuration"},
		{"time", "organization", "check", "probe", "target", "probe_configuration"},
		{"time", "organization", "check", "agent", "probe", "target", "probe_configuration", "number", "message"},
		{"time", "organization", "check", "agent", "probe", "target", "probe_configuration", "error"},
		{"time", "organization", "check", "agent", "probe", "target", "probe_configuration", "number", "message", "error"},
	}
	tags := []map[string]string{
		nil,
		nil,
		nil,
		nil,
		nil,
		nil,
		{"agent": "noLlUczuSJqkyIEynnCoow"},
		{"Barbès": "Rochechouart"},
		nil,
		nil,
	}
	expect := []*Result{
		{
			Entry: Entry{
				Timestamp:          time.Unix(1517849987, 0).UTC(),
				Organization:       organization,
				Check:              check,
				Target:             target,
				ProbeConfiguration: probeConfiguration,
				Probe:              probe,
				Agent:              agent,
			},
			Tags:   map[string]string{},
			Fields: map[string]interface{}{},
		},
		{
			Entry: Entry{
				Timestamp:          time.Unix(1517849987, 0).UTC(),
				Organization:       organization,
				Check:              check,
				Target:             target,
				ProbeConfiguration: probeConfiguration,
				Probe:              probe,
				Agent:              agent,
			},
			Tags:   map[string]string{},
			Fields: map[string]interface{}{"number": int64(1234), "message": "Hello World!"},
		},
		{
			Entry: Entry{
				Timestamp:          time.Unix(1517849987, 0).UTC(),
				Organization:       organization,
				Check:              check,
				Target:             target,
				ProbeConfiguration: probeConfiguration,
				Probe:              probe,
				Agent:              agent,
			},
			Tags:   map[string]string{},
			Fields: map[string]interface{}{"number": int64(1234), "message": "Hello World!"},
		},
		{
			Entry: Entry{
				Timestamp:          time.Unix(1517849987, 0).UTC(),
				Organization:       organization,
				Check:              check,
				Target:             target,
				ProbeConfiguration: probeConfiguration,
				Probe:              probe,
				Agent:              agent,
			},
			Tags:   map[string]string{},
			Fields: map[string]interface{}{"number": float64(1234), "message": "Hello World!"},
		},
		{
			Entry: Entry{
				Timestamp:          time.Unix(1517849987, 0).UTC(),
				Organization:       organization,
				Check:              check,
				Target:             target,
				ProbeConfiguration: probeConfiguration,
				Probe:              probe,
				Agent:              agent,
			},
			Tags:   map[string]string{},
			Fields: map[string]interface{}{"number": float64(1234), "message": "Hello World!"},
		},
		nil,
		{
			Entry: Entry{
				Timestamp:          time.Unix(1517849987, 0).UTC(),
				Organization:       organization,
				Check:              check,
				Target:             target,
				ProbeConfiguration: probeConfiguration,
				Probe:              probe,
				Agent:              agent,
			},
			Tags:   map[string]string{},
			Fields: map[string]interface{}{},
		},
		{
			Entry: Entry{
				Timestamp:          time.Unix(1517849987, 0).UTC(),
				Organization:       organization,
				Check:              check,
				Target:             target,
				ProbeConfiguration: probeConfiguration,
				Probe:              probe,
				Agent:              agent,
			},
			Tags:   map[string]string{"Barbès": "Rochechouart"},
			Fields: map[string]interface{}{"number": float64(1234), "message": "Hello World!"},
		},
		{
			Entry: Entry{
				Timestamp:          time.Unix(1517849987, 0).UTC(),
				Organization:       organization,
				Check:              check,
				Target:             target,
				ProbeConfiguration: probeConfiguration,
				Probe:              probe,
				Agent:              agent,
			},
			Error:        true,
			ErrorMessage: "failed to connect",
		},
		{
			Entry: Entry{
				Timestamp:          time.Unix(1517849987, 0).UTC(),
				Organization:       organization,
				Check:              check,
				Target:             target,
				ProbeConfiguration: probeConfiguration,
				Probe:              probe,
				Agent:              agent,
			},
			Tags:   map[string]string{},
			Fields: map[string]interface{}{"number": int64(1234), "message": "Hello World!"},
		},
	}

	// Check sanity of test data
	if len(cols) != len(rows) || len(cols) != len(expect) {
		panic(fmt.Sprintf("Inconsistent test data: %d rows, %d cols, %d expect", len(rows), len(cols), len(expect)))
	}

	for i, row := range rows {
		// check sanity of test data
		if len(row) != len(cols[i]) {
			panic(fmt.Sprintf("Inconsistent test data for row %d: %d values, %d cols", i, len(row), len(cols[i])))
		}
		var r Result
		err := r.FromRow(row, cols[i], tags[i], nil)
		if expect[i] != nil {
			if err != nil {
				t.Errorf("Failed to convert row %d: %s", i, err)
				continue
			}
			if !reflect.DeepEqual(r, *expect[i]) {
				t.Error("Row did not convert successfully")
				t.Logf("Input row: (%s)%s", cols[i], row)
				t.Logf("Expected:  %#v", *expect[i])
				t.Logf("Got:       %#v", r)
				continue
			}
		} else {
			if err == nil {
				t.Error("Row converted successfully, but expected error")
				t.Logf("Input row: (%s)%s", cols[i], row)
				t.Logf("Got:       %v", r)
				continue
			}
		}
	}
}

func TestAddResults(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live testing...")
	}

	db, err := NewInfluxTSDB(viper.GetString("influxdb.address"), viper.GetString("influxdb.database"))
	if err != nil {
		t.Fatalf("Couldn't create TSDB instance: %s", err)
	}

	r := []Result{
		{
			Entry: Entry{
				Timestamp:          time.Unix(1517849987, 0).UTC(),
				Organization:       organization,
				Check:              check,
				Target:             target,
				ProbeConfiguration: probeConfiguration,
				Probe:              probe,
				Agent:              agent,
			},
			Tags:   map[string]string{"Barbès": "Rochechouart"},
			Fields: map[string]interface{}{"number": 1234, "Château": "Landon"},
		},
		{
			Entry: Entry{
				Timestamp:          time.Unix(1517849997, 0).UTC(),
				Organization:       organization,
				Check:              check,
				Target:             target,
				ProbeConfiguration: probeConfiguration,
				Probe:              probe,
				Agent:              agent,
			},
			Tags:   map[string]string{"Barbès": "Rochechouart"},
			Fields: map[string]interface{}{"number": 5678, "Château": "Rouge"},
		},
		{
			Entry: Entry{
				Timestamp:          time.Unix(1517850007, 0).UTC(),
				Organization:       organization,
				Check:              check,
				Target:             target,
				ProbeConfiguration: probeConfiguration,
				Probe:              probe,
				Agent:              agent,
			},
			Error:        true,
			ErrorMessage: "could not connect",
		},
	}

	err = db.AddResults(r)
	if err != nil {
		t.Fatalf("Failed to add results to TSDB: %s", err)
	}

	measurementName := util.Base64Encode(check)
	res, err := db.client.Query(influx.NewQuery("SELECT * FROM "+measurementName, "pantotest", "s"))
	if err != nil {
		t.Fatalf("Failed to query results from TSDB: %s", err)
	}
	if res.Error() != nil {
		t.Fatalf("Failed to query results from TSDB: %s", res.Error())
	}

	// build array of tsdb.Result from query results
	results := res.Results
	if len(results) == 0 || len(results[0].Series) == 0 {
		t.Fatalf("Query returned no results")
	}
	serie := results[0].Series[0]
	if len(serie.Values) != 3 {
		t.Fatalf("Invalid number of results: expected %d, got %d", 3, len(serie.Values))
	}

	// Check columns
	columns := []string{"time", "Barbès", "Château", "organization", "probe_configuration", "check", "agent", "number", "probe", "target", "error"}
Expected:
	for _, expect := range columns {
		for _, got := range serie.Columns {
			if got == expect {
				continue Expected // to next column
			}
		}
		t.Fatalf("Expected column \"%s\" not found: %s", expect, serie.Columns)
	}
Got:
	for _, got := range serie.Columns {
		for _, expect := range columns {
			if got == expect {
				continue Got // to next column
			}
		}
		t.Fatalf("Unexpected column \"%s\" was found: %s", got, serie.Columns)
	}

	// Check first row
	row := serie.Values[0]
	t.Logf("Row 0: %s%s", serie.Columns, row)
	for i, col := range serie.Columns {
		switch col {
		case "time":
			n, ok := row[i].(json.Number)
			if ok {
				if n2, _ := n.Int64(); n2 == 1517849987 {
					continue
				}
			}
			t.Fatalf("Invalid timestamp in row 0: expected %d, got %v", 1517849987, row[i])
		case keyTagOrganization:
			if row[i].(string) != util.Base64Encode(organization) {
				t.Fatalf("Invalid check in row 0: expected %v, got %v", util.Base64Encode(organization), row[i])
			}
		case keyTagCheck:
			if row[i].(string) != util.Base64Encode(check) {
				t.Fatalf("Invalid check in row 0: expected %v, got %v", util.Base64Encode(check), row[i])
			}
		case keyTagAgent:
			if row[i].(string) != util.Base64Encode(agent) {
				t.Fatalf("Invalid agent in row 0: expected %s, got %v", util.Base64Encode(agent), row[i])
			}
		case keyTagProbe:
			if row[i].(string) != util.Base64Encode(probe) {
				t.Fatalf("Invalid probe in row 0: expected %s, got %v", util.Base64Encode(probe), row[i])
			}
		case keyTagTarget:
			if row[i].(string) != util.Base64Encode(target) {
				t.Fatalf("Invalid target in row 0: expected %s, got %v", util.Base64Encode(target), row[i])
			}
		case keyTagProbeConfiguration:
			if row[i].(string) != util.Base64Encode(probeConfiguration) {
				t.Fatalf("Invalid probe configuration in row 0: expected %s, got %v", util.Base64Encode(probeConfiguration), row[i])
			}
		case "Barbès":
			if row[i].(string) != "Rochechouart" {
				t.Fatalf("Invalid \"Barbès\" tag in row 0: expected \"%s\", got %v", "Rochechouart", row[i])
			}
		case "number":
			n, ok := row[i].(json.Number)
			if ok {
				if n2, _ := n.Int64(); n2 == 1234 {
					continue
				}
			}
			t.Fatalf("Invalid \"number\" field in row 0: expected %d, got %v", 1234, row[i])
		case "Château":
			if row[i].(string) != "Landon" {
				t.Fatalf("Invalid \"Château\" field in row 0: expected \"%s\", got %v", "Landon", row[i])
			}
		case "error":
			if row[i] != nil && row[i].(string) != "" {
				t.Fatalf("Invalid error in row 0: expected %s, got %v", "", row[i])
			}
		default:
			if row[i] != nil {
				t.Fatalf("Invalid field in row 0: (\"%s\": %v)", col, row[i])
			}
		}
	}

	// Check second row
	row = serie.Values[1]
	t.Logf("Row 1: %s%s", serie.Columns, row)
	for i, col := range serie.Columns {
		switch col {
		case "time":
			n, ok := row[i].(json.Number)
			if ok {
				if n2, _ := n.Int64(); n2 == 1517849997 {
					continue
				}
			}
			t.Fatalf("Invalid timestamp in row 1: expected %d, got %v", 1517849997, row[i])
		case keyTagOrganization:
			if row[i].(string) != util.Base64Encode(organization) {
				t.Fatalf("Invalid check in row 1: expected %v, got %v", util.Base64Encode(organization), row[i])
			}
		case keyTagCheck:
			if row[i].(string) != util.Base64Encode(check) {
				t.Fatalf("Invalid check in row 1: expected %v, got %v", util.Base64Encode(check), row[i])
			}
		case keyTagAgent:
			if row[i].(string) != util.Base64Encode(agent) {
				t.Fatalf("Invalid agent in row 1: expected %s, got %v", util.Base64Encode(agent), row[i])
			}
		case keyTagProbe:
			if row[i].(string) != util.Base64Encode(probe) {
				t.Fatalf("Invalid probe in row 1: expected %s, got %v", util.Base64Encode(probe), row[i])
			}
		case keyTagTarget:
			if row[i].(string) != util.Base64Encode(target) {
				t.Fatalf("Invalid target in row 1: expected %s, got %v", util.Base64Encode(target), row[i])
			}
		case keyTagProbeConfiguration:
			if row[i].(string) != util.Base64Encode(probeConfiguration) {
				t.Fatalf("Invalid probe configuration in row 1: expected %s, got %v", util.Base64Encode(probeConfiguration), row[i])
			}
		case "Barbès":
			if row[i].(string) != "Rochechouart" {
				t.Fatalf("Invalid \"Barbès\" tag in row 1: expected \"%s\", got %v", "Rochechouart", row[i])
			}
		case "number":
			n, ok := row[i].(json.Number)
			if ok {
				if n2, _ := n.Int64(); n2 == 5678 {
					continue
				}
			}
			t.Fatalf("Invalid \"number\" field in row 1: expected %d, got %v", 5678, row[i])
		case "Château":
			if row[i].(string) != "Rouge" {
				t.Fatalf("Invalid \"Château\" field in row 1: expected \"%s\", got %v", "Rouge", row[i])
			}
		case "error":
			if row[i] != nil && row[i].(string) != "" {
				t.Fatalf("Invalid error in row 1: expected %s, got %v", "", row[i])
			}
		default:
			if row[i] != nil {
				t.Fatalf("Invalid field in row 1: (\"%s\": %v)", col, row[i])
			}
		}
	}

	// Check third row (error)
	row = serie.Values[2]
	t.Logf("Row 2: %s%s", serie.Columns, row)
	for i, col := range serie.Columns {
		switch col {
		case "time":
			n, ok := row[i].(json.Number)
			if ok {
				if n2, _ := n.Int64(); n2 == 1517850007 {
					continue
				}
			}
			t.Fatalf("Invalid timestamp in row 2: expected %d, got %v", 1517850007, row[i])
		case keyTagOrganization:
			if row[i].(string) != util.Base64Encode(organization) {
				t.Fatalf("Invalid check in row 2: expected %v, got %v", util.Base64Encode(organization), row[i])
			}
		case keyTagCheck:
			if row[i].(string) != util.Base64Encode(check) {
				t.Fatalf("Invalid check in row 2: expected %v, got %v", util.Base64Encode(check), row[i])
			}
		case keyTagAgent:
			if row[i].(string) != util.Base64Encode(agent) {
				t.Fatalf("Invalid agent in row 2: expected %s, got %v", util.Base64Encode(agent), row[i])
			}
		case keyTagProbe:
			if row[i].(string) != util.Base64Encode(probe) {
				t.Fatalf("Invalid probe in row 2: expected %s, got %v", util.Base64Encode(probe), row[i])
			}
		case keyTagTarget:
			if row[i].(string) != util.Base64Encode(target) {
				t.Fatalf("Invalid target in row 2: expected %s, got %v", util.Base64Encode(target), row[i])
			}
		case keyTagProbeConfiguration:
			if row[i].(string) != util.Base64Encode(probeConfiguration) {
				t.Fatalf("Invalid probe configuration in row 2: expected %s, got %v", util.Base64Encode(probeConfiguration), row[i])
			}
		case "error":
			if row[i].(string) != "could not connect" {
				t.Fatalf("Invalid error in row 2: expected %s, got %v", "could not connect", row[i])
			}
		default:
			if row[i] != nil {
				t.Fatalf("Invalid field in row 2: (\"%s\": %v)", col, row[i])
			}
		}
	}
}

func TestGetResults(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live testing...")
	}

	db, err := NewInfluxTSDB(viper.GetString("influxdb.address"), viper.GetString("influxdb.database"))
	if err != nil {
		t.Fatalf("Couldn't create TSDB instance: %s", err)
	}

	res, err := db.GetResults(
		check,
		From(time.Unix(1517849990, 0).UTC()),
		To(time.Unix(1517850000, 0).UTC()),
		Count(1),
	)
	if err != nil {
		t.Fatalf("Could not get results: %s", err)
	}
	t.Logf("Results: %v", res)
	if len(res) != 1 {
		t.Fatalf("Invalid number of results: expected %d, got %d", 1, len(res))
	}

	expect := Result{
		Entry: Entry{
			Timestamp:          time.Unix(1517849997, 0).UTC(),
			Organization:       organization,
			Check:              check,
			Target:             target,
			ProbeConfiguration: probeConfiguration,
			Probe:              probe,
			Agent:              agent,
		},
		Tags:   map[string]string{"Barbès": "Rochechouart"},
		Fields: map[string]interface{}{"number": int64(5678), "Château": "Rouge"},
	}
	if !reflect.DeepEqual(res[0], expect) {
		t.Error("Unexpected entry for result")
		t.Logf("Expected:  %#v", expect)
		t.Logf("Got:       %#v", res[0])
	}
}

func TestGetResultsInexistent(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live testing...")
	}

	db, err := NewInfluxTSDB(viper.GetString("influxdb.address"), viper.GetString("influxdb.database"))
	if err != nil {
		t.Fatalf("Couldn't create TSDB instance: %s", err)
	}

	res, err := db.GetResults(
		check2,
		From(time.Unix(1517849990, 0).UTC()),
		To(time.Now()),
		Count(1),
	)
	if err != nil {
		t.Fatalf("Could not get results: %s", err)
	}
	if len(res) > 0 {
		t.Logf("Results: %v", res)
		t.Fatalf("Invalid number of results: expected %d, got %d", 0, len(res))
	}
}

func TestFetchSchema(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live testing...")
	}

	db, err := NewInfluxTSDB(viper.GetString("influxdb.address"), viper.GetString("influxdb.database"))
	if err != nil {
		t.Fatalf("Couldn't create TSDB instance: %s", err)
	}

	_, err = db.GetResults(check, Count(1), OrderBy(OrderDescending))
	if err != nil {
		t.Fatalf("Could not get last result: %s", err)
	}

	s := schema{
		fields: map[string]fieldType{"number": fieldTypeInteger, "Château": fieldTypeString, "error": fieldTypeString},
		tags: map[string]bool{

			"organization":        true,
			"agent":               true,
			"probe":               true,
			"target":              true,
			"check":               true,
			"probe_configuration": true,
			"Barbès":              true,
		},
	}

	got, _ := db.measurements.Load(util.Base64Encode(check))
	if !reflect.DeepEqual(s, got) {
		t.Error("Schema has not been cached")
		t.Logf("Expected:  %#v", s)
		t.Logf("Got:       %#v", got)
	}
}
