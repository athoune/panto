// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package tsdb

import (
	"testing"
	"time"

	"github.com/google/uuid"
)

func TestNew_Errors(t *testing.T) {
	_, err := NewInfluxTSDB("", "")

	if err == nil {
		t.Fatal("Successfully created invalid new tsdb with empty address")
	}

	_, err = NewInfluxTSDB("foobar", "")

	if err == nil {
		t.Fatal("Successfully created invalid new tsdb with 'foobar' address")
	}
}

func defaultResult() Result {
	ts := time.Now()
	return Result{
		Entry: Entry{
			Organization: uuid.New(),
			Check:        uuid.New(),
			Probe:        uuid.New(),
			Target:       uuid.New(),
			Agent:        uuid.New(),
			Timestamp:    ts,
		},
		Tags:   map[string]string{},
		Fields: map[string]interface{}{"field": "value"},
	}
}

func TestAddResult_Errors(t *testing.T) {
	dummy := InfluxTSDB{}

	{
		report := defaultResult()
		report.Fields = nil
		err := dummy.AddResults([]Result{report})
		if err == nil {
			t.Fatal("Accepted report with nil fields map.")
		}
	}

	{
		report := defaultResult()
		report.Fields = make(map[string]interface{})
		err := dummy.AddResults([]Result{report})
		if err == nil {
			t.Fatal("Accepted report with empty fields map.")
		}
	}
}
