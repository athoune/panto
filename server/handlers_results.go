// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"context"
	"path"
	"time"

	"gitlab.com/pantomath-io/panto/api"
	"gitlab.com/pantomath-io/panto/server/services"
	"gitlab.com/pantomath-io/panto/server/tsdb"
	"gitlab.com/pantomath-io/panto/util"
	"gitlab.com/pantomath-io/wrapany"

	"github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/any"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// QueryResults lists results for an agent and a check in a timerange
func (s *server) QueryResults(ctx context.Context, req *api.QueryResultsRequest) (res *api.QueryResultsResponse, err error) {
	// Get TSDB
	ts, err := services.GetTSDB()
	if err != nil {
		log.Errorf("couldn't find tsdb service: %s", err)
		return nil, status.Errorf(codes.Internal, "Internal error")
	}
	// Get DBConf
	db, err := services.GetDBConf()
	if err != nil {
		log.Errorf("couldn't find dbconf service: %s", err)
		return nil, status.Errorf(codes.Internal, "Internal error")
	}

	// Parse Page Token
	var pageOffset int
	if req.PageToken != "" {
		valid, err := util.ValidatePageToken(req.PageToken, req, &pageOffset)
		if err != nil {
			log.Errorf("Failed to validate page token: %s", err)
			return nil, status.Errorf(codes.InvalidArgument, "Invalid page token")
		}
		if !valid {
			log.Errorf("Page token does not match request")
			return nil, status.Errorf(codes.InvalidArgument, "Page token does not match request")
		}
	}

	if len(req.Parent) == 0 {
		log.Errorf("missing parent Organization name")
		return nil, status.Error(codes.InvalidArgument, "Missing parent Organization name")
	}

	// Get the organization UUID
	organizationUUID, err := api.ParseNameOrganization(req.Parent)
	if err != nil {
		log.Errorf("couldn't parse organization name: %s", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid Organization")
	}

	// get the Probe Configuration from the DBConf
	o, agentUUID, probeConfigurationUUID, err := api.ParseNameProbeConfiguration(req.ProbeConfiguration)
	if err != nil {
		log.Errorf("couldn't parse probe_configuration name: %s", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid Probe Configuration name")
	}
	if o != organizationUUID {
		log.Errorf("organization and probe_configuration do not match")
		return nil, status.Error(codes.InvalidArgument, "Probe Configuration not in Organization")
	}
	tpa, err := db.GetTargetProbeAgent(organizationUUID, agentUUID, probeConfigurationUUID)
	if err != nil {
		log.Errorf("couldn't retrieve target_probe_agent: %s", err)
		return nil, status.Errorf(codes.Internal, "Probe Configuration not found")
	}
	if tpa == nil {
		return nil, status.Errorf(codes.NotFound, "Probe Configuration not found")
	}

	// Get start time
	var startTime *time.Time
	if req.StartTime != nil {
		t, err := ptypes.Timestamp(req.StartTime)
		log.Debugf("Converted start time proto %s to %s", req.StartTime, t)
		if err != nil {
			log.Errorf("Could not read start timestamp: %s", req.StartTime)
			return nil, status.Errorf(codes.InvalidArgument, "Invalid start timestamp")
		}
		if t != time.Unix(0, 0).UTC() {
			startTime = &t
		}
	}

	// Get end time
	var endTime *time.Time
	if req.EndTime != nil {
		t, err := ptypes.Timestamp(req.EndTime)
		log.Debugf("Converted end time proto %s to %s", req.EndTime, t)
		if err != nil {
			log.Errorf("Could not read end timestamp: %s", req.EndTime)
			return nil, status.Errorf(codes.InvalidArgument, "Invalid end timestamp")
		}
		if t != time.Unix(0, 0).UTC() {
			endTime = &t
		}
	}

	tags := make(map[string]string)

	// Copy the tags
	for k, v := range req.Tags {
		tags[k] = v
	}

	// Get page size
	pageSize := req.PageSize
	if pageSize == 0 {
		pageSize = defaultPageSize
	} else if pageSize > maxPageSize {
		pageSize = maxPageSize
	}

	// Get ordering
	var orderBy tsdb.Order
	if req.OrderBy == api.Order_ASCENDING {
		orderBy = tsdb.OrderAscending
	} else if req.OrderBy == api.Order_DESCENDING {
		orderBy = tsdb.OrderDescending
	} else {
		log.Errorf("unable to read ordering: %s", req.OrderBy)
		return nil, status.Error(codes.InvalidArgument, "Invalid order_by field")
	}

	// Query TSDB
	opts := []tsdb.QueryOption{
		tsdb.Tag("probe_configuration", util.Base64Encode(probeConfigurationUUID)),
		tsdb.Count(int(pageSize + 1)),
		tsdb.Offset(pageOffset),
		tsdb.OrderBy(orderBy),
	}
	if startTime != nil {
		opts = append(opts, tsdb.From(*startTime))
	}
	if endTime != nil {
		opts = append(opts, tsdb.To(*endTime))
	}
	for k, v := range tags {
		opts = append(opts, tsdb.Tag(k, v))
	}
	results, err := ts.GetResults(
		tpa.TargetProbeUUID,
		opts...,
	)
	if err != nil {
		log.Errorf("Could not retrieve results from TSDB: %s", err)
		return nil, status.Errorf(codes.Unknown, "No results found")
	}

	var nextPage bool
	if int32(len(results)) > pageSize {
		results = results[:pageSize]
		nextPage = true
	}

	res = &api.QueryResultsResponse{
		Results: make([]*api.Result, 0, len(results)),
	}
	for _, r := range results {
		ts, err := ptypes.TimestampProto(r.Timestamp)
		if err != nil {
			log.Warningf("Couldn't convert result timestamp %s to protobuf message. Skipping result...", r.Timestamp)
			continue
		}
		apiResult := api.Result{
			Check: path.Join(
				api.ResourceNameOrganization, util.Base64Encode(r.Organization),
				api.ResourceNameCheck, util.Base64Encode(r.Check),
			),
			Agent: path.Join(
				api.ResourceNameOrganization, util.Base64Encode(r.Organization),
				api.ResourceNameAgent, util.Base64Encode(r.Agent),
			),
			ProbeConfiguration: path.Join(
				api.ResourceNameOrganization, util.Base64Encode(r.Organization),
				api.ResourceNameAgent, util.Base64Encode(r.Agent),
				api.ResourceNameProbeConfiguration, util.Base64Encode(r.ProbeConfiguration),
			),
			Timestamp: ts,
		}
		if !r.Error {
			fields := make(map[string]*any.Any)
			for k, v := range r.Fields {
				f, err := wrapany.Wrap(v)
				if err != nil {
					log.Warningf("Couldn't wrap field (%s)%v to Any message: %s. Skipping field...", k, v, err)
					continue
				}
				fields[k] = f
			}
			apiResult.Fields = fields
			apiResult.Tags = r.Tags
		} else {
			apiResult.Error = r.Error
			apiResult.ErrorMessage = r.ErrorMessage
		}
		res.Results = append(res.Results, &apiResult)
	}

	if nextPage {
		// Generate next page token
		res.NextPageToken, err = util.NextPageToken(req, pageOffset+len(results))
		if err != nil {
			log.Warning("Failed to generate next page token")
			// Return values but don't set page token
			res.NextPageToken = ""
		}
	}

	return res, nil
}

// AddResults receives results from an agent for a probe
func (s *server) AddResults(ctx context.Context, req *api.AddResultsRequest) (*api.AddResultsResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, 45*time.Second)
	defer cancel()

	err := s.dataPipe.ProcessResults(ctx, req.Results)
	if err != nil {
		if err == context.DeadlineExceeded {
			log.Errorf("Request timed out.")
			return nil, status.Error(codes.DeadlineExceeded, "Request timed out")
		}
		log.Errorf("Request failed: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	return &api.AddResultsResponse{Results: req.Results}, nil
}
