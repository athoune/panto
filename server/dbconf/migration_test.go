// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package dbconf

import (
	"testing"

	"github.com/loderunner/goose"
	_ "github.com/mattn/go-sqlite3" // import SQLite just to reference driver

	"gitlab.com/pantomath-io/panto/util"
)

const (
	dbPath = "/tmp/pantotest.sqlite"
)

func TestGetDBConfVersion(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	// valid DB
	_, err := GetVersion(testDB)
	if err != nil {
		t.Fatalf("Unable to get the DB version: %v", err)
	}
}

func TestDBConfIsVersion(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	version, err := GetVersion(testDB)
	if err != nil {
		t.Fatalf("Unable to get the DB version")
	}

	ok, err := IsVersion(testDB, version)
	if err != nil {
		t.Fatalf("Unable to compare DB version: %v", err)
	}
	if !ok {
		t.Fatalf("Version should be %d", version)
	}
}

func TestDBConfListPendingMigrations(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	version, err := GetVersion(testDB)
	if err != nil {
		t.Fatalf("Unable to get the DB version")
	}

	// Test success
	list, err := ListPendingMigrations(version, version)
	if err != nil {
		t.Fatalf("Unable to list pending migrations: %v", err)
	}
	if len(list) > 0 {
		t.Fatalf("Migrations list should be empty")
	}

	// Test success
	list, err = ListPendingMigrations(version, version)
	if err != nil {
		t.Fatalf("Unable to list pending migrations: %v", err)
	}
	for i, migration := range list {
		if i < len(list) {
			if migration.Version > list[i+1].Version {
				t.Fatalf("Migrations list should be sorted")
			}
		}
	}
}

func TestDBConfMigrate(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	err := goose.SetDialect(testDB.getDialect())
	if err != nil {
		t.Fatalf("failed to set DB dialect: %s", err)
	}

	err = goose.Reset(testDB.getClient(), "")
	if err != nil {
		t.Fatalf("Unable to prepare test - reset db: %v", err)
	}

	// Test success
	err = Migrate(testDB)
	if err != nil {
		t.Fatalf("Unable to migrate DB (%s): %v", dbPath, err)
	}
}

func TestDBConfMigrateTo(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	version, err := GetVersion(testDB)
	if err != nil {
		t.Fatalf("Unable to get the DB version")
	}

	err = goose.SetDialect(testDB.getDialect())
	if err != nil {
		t.Fatalf("failed to set DB dialect: %s", err)
	}

	err = goose.Reset(testDB.getClient(), "")
	if err != nil {
		t.Fatalf("Unable to prepare test - reset db: %v", err)
	}

	// Test success
	err = MigrateTo(testDB, version)
	if err != nil {
		t.Fatalf("Unable to migrate DB (%s): %v", dbPath, err)
	}

	version, err = GetVersion(testDB)
	if err != nil {
		t.Fatalf("Unable to get the DB version")
	}
	if version == 0 {
		t.Fatalf("Version shouldn't be default value 0")
	}
}
