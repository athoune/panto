// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package dbconf

import (
	"database/sql"
	"fmt"
	"strings"
	"time"

	"github.com/google/uuid"
)

// An Agent is the representation of an agent in the DB
type Agent struct {
	ID               int
	UUID             uuid.UUID
	OrganizationUUID uuid.UUID
	DisplayName      string
	TsConfiguration  time.Time
	TsCreated        time.Time
	TsUpdated        time.Time
	TsDeleted        time.Time
}

// ListAgents returns all Agents from the DBConf
func (db *SQLiteDB) ListAgents(organizationUUID uuid.UUID, pageSize int32, pageOffset uint64) (agents []*Agent, err error) {
	req := new(strings.Builder)
	fmt.Fprint(req, `
		SELECT a.id, a.uuid, a.display_name, o.uuid, a.ts_configuration, a.ts_created, a.ts_updated, a.ts_deleted
		FROM agent AS a, organization AS o
		WHERE a.organization = o.id AND a.ts_deleted IS NULL AND o.ts_deleted IS NULL
			AND o.uuid = ?
	`)

	if pageSize > 0 || pageOffset > 0 {
		// if pageSize is 0, returns all the results (starting from pageOffset)
		if pageSize == 0 {
			pageSize = -1
		}
		fmt.Fprintf(req, " LIMIT %d OFFSET %d", pageSize, pageOffset)
	}

	rows, err := db.client.Query(req.String(), organizationUUID)
	if err != nil {
		return nil, fmt.Errorf("error running the query: %s", err)
	}
	defer rows.Close()

	agents = make([]*Agent, 0)
	for rows.Next() {
		a, err := scanAgent(rows)
		if err != nil {
			log.Errorf("unable to scan row: %s", err)
			continue
		}

		agents = append(agents, a)
	}
	err = rows.Err()
	if err != nil {
		if err == sql.ErrNoRows {
			return []*Agent{}, nil
		}
		log.Errorf("issue in the query: %s", err)
	}

	return
}

// GetAgent returns an Agent in the DB based on its UUID and its parent organization's UUID
func (db *SQLiteDB) GetAgent(organizationUUID, agentUUID uuid.UUID) (*Agent, error) {
	o, err := scanAgent(db.client.QueryRow(`
		SELECT a.id, a.uuid, a.display_name, o.uuid, a.ts_configuration, a.ts_created, a.ts_updated, a.ts_deleted
		FROM agent AS a, organization AS o
		WHERE a.organization = o.id AND a.ts_deleted IS NULL
			AND o.uuid = ? AND a.uuid = ?
	`, organizationUUID, agentUUID))
	if err != nil {
		if err == sql.ErrNoRows {
			// Return nil, nil to indicate no error but no result (404 Not Found)
			return nil, nil
		}
		return nil, fmt.Errorf("failed to retrieve Agent: %s", err)
	}

	return o, nil
}

// CreateAgent creates a new Agent in the DB
func (db *SQLiteDB) CreateAgent(agent *Agent) (*Agent, error) {
	if len(agent.DisplayName) == 0 {
		return nil, fmt.Errorf("missing agent display name")
	}

	u := uuid.New()
	_, err := db.client.Exec(
		`INSERT INTO agent (uuid, display_name, organization)
		VALUES (?,?,
			(SELECT id FROM organization WHERE ts_deleted IS NULL AND uuid = ?)
		)`,
		u, agent.DisplayName, agent.OrganizationUUID)
	if err != nil {
		return nil, fmt.Errorf("error running the query: %s", err)
	}

	return db.GetAgent(agent.OrganizationUUID, u)
}

// UpdateAgent updates an existing Agent in the DB
func (db *SQLiteDB) UpdateAgent(agent *Agent) (*Agent, error) {
	args := []interface{}{}
	req := new(strings.Builder)

	fmt.Fprint(req, "UPDATE agent SET ")

	// User-supplied fields to change
	if agent.DisplayName != "" {
		fmt.Fprint(req, "display_name=?, ")
		args = append(args, agent.DisplayName)
	}

	// Change update timestamp
	fmt.Fprintf(req, `ts_updated=(strftime('%%s', 'now')) `)

	// Agent & Organization constraint
	fmt.Fprint(req, `WHERE uuid=? AND organization = (SELECT id FROM organization WHERE ts_deleted IS NULL AND uuid=?) `)
	args = append(args, agent.UUID, agent.OrganizationUUID)

	// Soft delete constraint
	fmt.Fprint(req, `AND ts_deleted IS NULL`)

	res, err := db.client.Exec(req.String(), args...)
	if err != nil {
		return nil, fmt.Errorf("error running the query: %s", err)
	}
	if rowsAffected, err := res.RowsAffected(); err == nil && rowsAffected < 1 {
		return nil, fmt.Errorf("failed to update agent")
	}

	return db.GetAgent(agent.OrganizationUUID, agent.UUID)
}

// DeleteAgent deletes an Agent and all linked entities (TargetProbeAgent) from the DB
func (db *SQLiteDB) DeleteAgent(organizationUUID, agentUUID uuid.UUID) error {
	tx, err := db.client.Begin()
	if err != nil {
		return fmt.Errorf("failed to begin transaction: %s", err)
	}
	// Defer rollback. If transaction is already done, sql.ErrTxDone is silently ignored.
	defer tx.Rollback()

	// Delete linked target_probe_agent entries
	_, err = tx.Exec(`UPDATE target_probe_agent 
		SET ts_deleted=(strftime('%s', 'now')) 
		WHERE agent = (SELECT a.id FROM agent AS a, organization AS o WHERE a.organization=o.id AND a.uuid=? AND o.uuid=?)
		AND ts_deleted IS NULL`,
		agentUUID, organizationUUID)
	if err != nil {
		return fmt.Errorf("failed to delete linked TargetProbeAgents: %s", err)
	}

	// Delete agent
	_, err = tx.Exec(`UPDATE agent 
		SET ts_deleted=(strftime('%s', 'now'))
		WHERE id = (SELECT a.id FROM agent AS a, organization AS o WHERE a.organization=o.id AND a.uuid=? AND o.uuid=?)
		AND ts_deleted IS NULL`,
		agentUUID, organizationUUID)
	if err != nil {
		return fmt.Errorf("failed to delete agent: %s", err)
	}

	err = tx.Commit()
	if err != nil {
		return fmt.Errorf("failed to commit transaction: %s", err)
	}

	return nil
}

func scanAgent(r row) (*Agent, error) {
	var a Agent
	var tsCreated, tsUpdated, tsDeleted, tsConfiguration NullTime
	err := r.Scan(
		&a.ID,
		&a.UUID,
		&a.DisplayName,
		&a.OrganizationUUID,
		&tsConfiguration,
		&tsCreated,
		&tsUpdated,
		&tsDeleted,
	)
	if err != nil {
		return nil, err
	}
	if tsConfiguration.Valid {
		a.TsConfiguration = tsConfiguration.Time
	} else {
		a.TsConfiguration = time.Time{}
	}
	if tsCreated.Valid {
		a.TsCreated = tsCreated.Time
	} else {
		a.TsCreated = time.Time{}
	}
	if tsUpdated.Valid {
		a.TsUpdated = tsUpdated.Time
	} else {
		a.TsUpdated = time.Time{}
	}
	if tsDeleted.Valid {
		a.TsDeleted = tsDeleted.Time
	} else {
		a.TsDeleted = time.Time{}
	}
	return &a, nil
}
