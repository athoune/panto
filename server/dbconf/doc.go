// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package dbconf manages everything related to the SQL
// database. This database will be used to store and retrieve
// configurations.
/*
	The convention is to have:

	* one type per DB object;
	* a bunch of functions that returns one (list of) object(s) type to get objects;
	* a bunch of receiver functions that interact with the database (Save(), Delete()...)


	The "list" functions need 2 parameters: pageSize and pageOffset. Setting pageSize to 0
	means "return all results". The pageOffset is just an offset, independently of the size.
	The different combinations of pageSize and pageOffset are:

	| `pageSize` | `pageOffset` | SQL                  | Results             |
	| ---------- | ------------ | -------------------- | ------------------- |
	| 0          | 0            |                      | 0..∞ (_N_ results)  |
	| 1          | 0            | `LIMIT 1`            | 0..0 (1 result)     |
	| 0          | 10           | `LIMIT -1 OFFSET 10` | 10..∞ (_M_ results) |
	| 1          | 10           | `LIMIT 1 OFFSET 10`  | 10..10 (1 result)   |
*/
package dbconf
