// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package dbconf

import (
	"reflect"
	"testing"
	"time"

	sqlmock "github.com/DATA-DOG/go-sqlmock"

	"github.com/google/uuid"
)

func TestListAgents(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}

	now := time.Now()
	a0 := Agent{
		ID:               1,
		UUID:             testAgentUUID,
		DisplayName:      "Agent Zero",
		OrganizationUUID: testOrganizationUUID,
		TsConfiguration:  now,
		TsCreated:        now,
	}
	a1 := Agent{
		ID:               2,
		UUID:             uuid.New(),
		DisplayName:      "Agent One",
		OrganizationUUID: testOrganizationUUID,
		TsConfiguration:  now.Add(time.Hour),
		TsCreated:        now.Add(time.Hour),
	}

	rows := sqlmock.NewRows([]string{
		"a.id", "a.uuid", "a.display_name", "o.uuid", "a.ts_configuration", "a.ts_created", "a.ts_updated", "a.ts_deleted",
	}).
		AddRow(a0.ID, a0.UUID.String(), a0.DisplayName, a0.OrganizationUUID.String(), a0.TsConfiguration, a0.TsCreated, nil, nil).
		AddRow(a1.ID, a1.UUID.String(), a1.DisplayName, a1.OrganizationUUID.String(), a1.TsConfiguration, a1.TsCreated, nil, nil)
	mock.ExpectQuery(`SELECT a\.id, a\.uuid, a\.display_name, o\.uuid, a\.ts_configuration, a\.ts_created,
			a\.ts_updated, a\.ts_deleted
			FROM agent AS a, organization AS o 
			WHERE a\.organization = o\.id AND a\.ts_deleted IS NULL AND o\.ts_deleted IS NULL AND o\.uuid = ?`).
		WithArgs(testOrganizationUUID).
		WillReturnRows(rows)
	agents, err := mockDb.ListAgents(testOrganizationUUID, 0, 0)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(agents) != 2 {
		t.Fatalf("expected %d results, got %d", 2, len(agents))
	}
	if !reflect.DeepEqual(agents[0], &a0) {
		t.Fatalf("expected %v, got %v", a0, *agents[0])
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}

	rows = sqlmock.NewRows([]string{
		"a.id", "a.uuid", "a.display_name", "o.uuid", "a.ts_configuration", "a.ts_created", "a.ts_updated", "a.ts_deleted",
	}).AddRow(a1.ID, a1.UUID.String(), a1.DisplayName, a1.OrganizationUUID.String(), a1.TsConfiguration, a1.TsCreated, nil, nil)
	mock.ExpectQuery(`SELECT a\.id, a\.uuid, a\.display_name, o\.uuid, a\.ts_configuration, a\.ts_created,
		a\.ts_updated, a\.ts_deleted
		FROM agent AS a, organization AS o 
		WHERE a\.organization = o\.id AND a\.ts_deleted IS NULL AND o\.ts_deleted IS NULL AND o\.uuid = \? LIMIT -1 OFFSET 1`).
		WithArgs(testOrganizationUUID).
		WillReturnRows(rows)
	agents, err = mockDb.ListAgents(testOrganizationUUID, 0, 1)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(agents) != 1 {
		t.Fatalf("expected %d results, got %d", 1, len(agents))
	}
	if !reflect.DeepEqual(agents[0], &a1) {
		t.Fatalf("expected %v, got %v", a1, *agents[0])
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}

	rows = sqlmock.NewRows([]string{
		"a.id", "a.uuid", "a.display_name", "o.uuid", "a.ts_configuration", "a.ts_created", "a.ts_updated", "a.ts_deleted",
	}).AddRow(a0.ID, a0.UUID.String(), a0.DisplayName, a0.OrganizationUUID.String(), a0.TsConfiguration, a0.TsCreated, nil, nil)
	mock.ExpectQuery(`SELECT a\.id, a\.uuid, a\.display_name, o\.uuid, a\.ts_configuration, a\.ts_created,
		a\.ts_updated, a\.ts_deleted
		FROM agent AS a, organization AS o 
		WHERE a\.organization = o\.id AND a\.ts_deleted IS NULL AND o\.ts_deleted IS NULL AND o\.uuid = \? LIMIT 1 OFFSET 0`).
		WithArgs(testOrganizationUUID).
		WillReturnRows(rows)
	agents, err = mockDb.ListAgents(testOrganizationUUID, 1, 0)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(agents) != 1 {
		t.Fatalf("expected %d results, got %d", 1, len(agents))
	}
	if !reflect.DeepEqual(agents[0], &a0) {
		t.Fatalf("expected %v, got %v", a0, *agents[0])
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}

	rows = sqlmock.NewRows([]string{
		"a.id", "a.uuid", "a.display_name", "o.uuid", "a.ts_configuration", "a.ts_created", "a.ts_updated", "a.ts_deleted",
	}).AddRow(a0.ID, a0.UUID.String(), a0.DisplayName, a0.OrganizationUUID.String(), a0.TsConfiguration, a0.TsCreated, nil, nil)
	mock.ExpectQuery(`SELECT a\.id, a\.uuid, a\.display_name, o\.uuid, a\.ts_configuration, a\.ts_created,
		a\.ts_updated, a\.ts_deleted
		FROM agent AS a, organization AS o 
		WHERE a\.organization = o\.id AND a\.ts_deleted IS NULL AND o\.ts_deleted IS NULL AND o\.uuid = \? LIMIT 1 OFFSET 1`).
		WithArgs(testOrganizationUUID).
		WillReturnRows(rows)
	agents, err = mockDb.ListAgents(testOrganizationUUID, 1, 1)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(agents) != 1 {
		t.Fatalf("expected %d results, got %d", 1, len(agents))
	}
	if !reflect.DeepEqual(agents[0], &a0) {
		t.Fatalf("expected %v, got %v", a0, *agents[0])
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}

func TestGetAgent(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}

	now := time.Now()
	expect := Agent{
		ID:               1,
		UUID:             testAgentUUID,
		DisplayName:      "Agent Zero",
		OrganizationUUID: testOrganizationUUID,
		TsConfiguration:  now,
		TsCreated:        now,
	}
	rows := sqlmock.NewRows([]string{
		"a.id", "a.uuid", "a.display_name", "o.uuid", "a.ts_configuration", "a.ts_created", "a.ts_updated", "a.ts_deleted",
	}).
		AddRow(expect.ID, expect.UUID.String(), expect.DisplayName, expect.OrganizationUUID.String(), expect.TsConfiguration, expect.TsCreated, nil, nil)
	mock.ExpectQuery(`SELECT a\.id, a\.uuid, a\.display_name, o\.uuid, a\.ts_configuration, a\.ts_created, a\.ts_updated, a\.ts_deleted
		FROM agent AS a, organization AS o
		WHERE a\.organization = o\.id AND a\.ts_deleted IS NULL AND o\.uuid = \? AND a\.uuid = \?`).
		WithArgs(testOrganizationUUID, testAgentUUID).
		WillReturnRows(rows)

	got, err := mockDb.GetAgent(testOrganizationUUID, testAgentUUID)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if !reflect.DeepEqual(&expect, got) {
		t.Fatalf("expected %v, got %v", expect, *got)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}

	rows = sqlmock.NewRows([]string{
		"a.id", "a.uuid", "a.display_name", "o.uuid", "a.ts_configuration", "a.ts_created", "a.ts_updated", "a.ts_deleted",
	})

	u0 := uuid.New()
	u1 := uuid.New()
	mock.ExpectQuery(`SELECT a\.id, a\.uuid, a\.display_name, o\.uuid, a\.ts_configuration, a\.ts_created, a\.ts_updated, a\.ts_deleted
		FROM agent AS a, organization AS o
		WHERE a\.organization = o\.id AND a\.ts_deleted IS NULL AND o\.uuid = \? AND a\.uuid = \?`).
		WithArgs(u0, u1).
		WillReturnRows(rows)

	got, err = mockDb.GetAgent(u0, u1)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if got != nil {
		t.Fatalf("expected nil, got %v", *got)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}

func TestCreateAgent(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	agent := Agent{
		DisplayName:      "Agent Zero",
		OrganizationUUID: testOrganizationUUID,
	}

	uuidArg := StoreArg()
	mock.ExpectExec(`INSERT INTO agent \(uuid, display_name, organization\)
	VALUES \(\?,\?,
		\(SELECT id FROM organization WHERE ts_deleted IS NULL AND uuid = \?\)
	\)`).
		WithArgs(uuidArg, agent.DisplayName, agent.OrganizationUUID).
		WillReturnResult(sqlmock.NewResult(1, 1))

	now := time.Now()
	rows := sqlmock.NewRows([]string{
		"a.id", "a.uuid", "a.display_name", "o.uuid", "a.ts_configuration", "a.ts_created", "a.ts_updated", "a.ts_deleted",
	}).
		AddRow(1, testAgentUUID.String(), "Agent Zero", testOrganizationUUID.String(), now, now, nil, nil)
	mock.ExpectQuery(`SELECT a\.id, a\.uuid, a\.display_name, o\.uuid, a\.ts_configuration, a\.ts_created, a\.ts_updated, a\.ts_deleted
			FROM agent AS a, organization AS o
			WHERE a\.organization = o\.id AND a\.ts_deleted IS NULL AND o\.uuid = \? AND a\.uuid = \?`).
		WithArgs(testOrganizationUUID, uuidArg).
		WillReturnRows(rows)

	got, err := mockDb.CreateAgent(&agent)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if got.ID != 1 ||
		got.UUID != testAgentUUID ||
		got.DisplayName != "Agent Zero" ||
		got.OrganizationUUID != testOrganizationUUID ||
		got.TsConfiguration != now ||
		got.TsCreated != now {
		t.Fatalf("unexpected result %v", *got)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}

func TestUpdateAgent(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	agent := Agent{
		UUID:             testAgentUUID,
		OrganizationUUID: testOrganizationUUID,
		DisplayName:      "Agent One",
	}

	mock.ExpectExec(`UPDATE agent SET display_name=\?, ts_updated=\(strftime\('%s', 'now'\)\)
		WHERE uuid=\? AND organization = \(SELECT id FROM organization WHERE ts_deleted IS NULL AND uuid=\?\)`).
		WithArgs(agent.DisplayName, agent.UUID, agent.OrganizationUUID).
		WillReturnResult(sqlmock.NewResult(1, 1))

	uuidArg := StoreArg()
	now := time.Now()
	rows := sqlmock.NewRows([]string{
		"a.id", "a.uuid", "a.display_name", "o.uuid", "a.ts_configuration", "a.ts_created", "a.ts_updated", "a.ts_deleted",
	}).
		AddRow(1, testAgentUUID.String(), "Agent One", testOrganizationUUID.String(), now, now, nil, nil)
	mock.ExpectQuery(`SELECT a\.id, a\.uuid, a\.display_name, o\.uuid, a\.ts_configuration, a\.ts_created, a\.ts_updated, a\.ts_deleted
			FROM agent AS a, organization AS o
			WHERE a\.organization = o\.id AND a\.ts_deleted IS NULL AND o\.uuid = \? AND a\.uuid = \?`).
		WithArgs(testOrganizationUUID, uuidArg).
		WillReturnRows(rows)

	got, err := mockDb.UpdateAgent(&agent)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	u, ok := uuidArg.Value().(string)
	if !ok ||
		u != testAgentUUID.String() ||
		got.ID != 1 ||
		got.UUID != testAgentUUID ||
		got.DisplayName != "Agent One" ||
		got.OrganizationUUID != testOrganizationUUID ||
		got.TsConfiguration != now ||
		got.TsCreated != now {
		t.Fatalf("unexpected result %v", *got)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}

func TestDeleteAgent(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	mock.ExpectBegin()
	mock.ExpectExec(`UPDATE target_probe_agent 
		SET ts_deleted=\(strftime\('%s', 'now'\)\) 
		WHERE agent =
		\(SELECT a\.id FROM agent AS a, organization AS o WHERE a\.organization=o\.id AND a\.uuid=\? AND o\.uuid=\?\)`).
		WithArgs(testAgentUUID, testOrganizationUUID).
		WillReturnResult(sqlmock.NewResult(1, 5))
	mock.ExpectExec(`UPDATE agent 
		SET ts_deleted=\(strftime\('%s', 'now'\)\)
		WHERE id =
		\(SELECT a\.id FROM agent AS a, organization AS o WHERE a\.organization=o\.id AND a\.uuid=\? AND o\.uuid=\?\)`).
		WithArgs(testAgentUUID, testOrganizationUUID).
		WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectCommit()
	err = mockDb.DeleteAgent(testOrganizationUUID, testAgentUUID)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}
