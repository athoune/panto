// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package dbconf

import (
	"reflect"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
)

func TestListTagsByTarget(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	now := time.Now()
	t0 := Tag{
		ID:               1,
		OrganizationUUID: testOrganizationUUID,
		Slug:             "tag",
		DisplayName:      "Tag",
		Note:             "This is a note.",
		Color:            "#2F4C99",
		TsCreated:        now,
	}
	t1 := Tag{
		ID:               2,
		OrganizationUUID: testOrganizationUUID,
		Slug:             "tag-2",
		DisplayName:      "Tag",
		Note:             "This is another note.",
		Color:            "#00A994",
		TsCreated:        now.Add(time.Hour),
	}

	rows := sqlmock.NewRows([]string{
		"tag.id", "o.uuid", "tag.slug", "tag.display_name", "tag.note", "tag.color", "tag.ts_created", "tag.ts_updated", "tag.ts_deleted",
	}).
		AddRow(t0.ID, t0.OrganizationUUID.String(), t0.Slug, t0.DisplayName, t0.Note, t0.Color, t0.TsCreated, nil, nil).
		AddRow(t1.ID, t1.OrganizationUUID.String(), t1.Slug, t1.DisplayName, t1.Note, t1.Color, t1.TsCreated, nil, nil)
	mock.ExpectQuery(`
		SELECT DISTINCT tag\.id, o\.uuid, tag\.slug, tag\.display_name, tag\.note, tag\.color, tag\.ts_created, tag\.ts_updated, tag\.ts_deleted
		FROM organization AS o, tag, target, target_tag
	`).WithArgs(testOrganizationUUID, testTargetUUID).WillReturnRows(rows)
	tags, err := mockDb.ListTagsByTarget(testOrganizationUUID, testTargetUUID, 0, 0)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(tags) != 2 {
		t.Fatalf("expected %d results, got %d", 2, len(tags))
	}
	if !reflect.DeepEqual(tags[0], &t0) {
		t.Fatalf("expected %v, got %v", t0, *tags[0])
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}

func TestListTargetsByTag(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	now := time.Now()
	t0 := Target{
		ID:               1,
		UUID:             testTargetUUID,
		OrganizationUUID: testOrganizationUUID,
		Address:          "target.pantomath.io",
		Note:             "Note about the target",
		TsCreated:        now,
	}
	t1 := Target{
		ID:               2,
		UUID:             testTargetUUID,
		OrganizationUUID: testOrganizationUUID,
		Address:          "target-1.pantomath.io",
		Note:             "Note about the target",
		TsCreated:        now.Add(time.Hour),
	}

	rows := sqlmock.NewRows([]string{
		"target.id", "target.uuid", "o.uuid", "target.address", "target.note", "target.ts_created", "target.ts_updated", "target.ts_deleted",
	}).
		AddRow(t0.ID, t0.UUID.String(), t0.OrganizationUUID.String(), t0.Address, t0.Note, t0.TsCreated, nil, nil).
		AddRow(t1.ID, t1.UUID.String(), t1.OrganizationUUID.String(), t1.Address, t1.Note, t1.TsCreated, nil, nil)
	mock.ExpectQuery(`SELECT DISTINCT target\.id, target\.uuid, o\.uuid, target\.address, target\.note, target\.ts_created, target\.ts_updated, target\.ts_deleted
		FROM organization AS o, target, tag, target_tag`).
		WithArgs(testOrganizationUUID, "tag").
		WillReturnRows(rows)
	targets, err := mockDb.ListTargetsByTag(testOrganizationUUID, "tag", 0, 0)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(targets) != 2 {
		t.Fatalf("expected %d results, got %d", 2, len(targets))
	}
	if !reflect.DeepEqual(targets[0], &t0) {
		t.Fatalf("expected %v, got %v", t0, *targets[0])
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}

func TestLinkTagsToTarget(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	mock.ExpectBegin()
	mock.ExpectExec(`DELETE FROM target_tag`).
		WithArgs(testTargetUUID, testOrganizationUUID).
		WillReturnResult(sqlmock.NewResult(1, 8))
	mock.ExpectExec(`INSERT INTO target_tag \(target, tag\) VALUES`).
		WithArgs(testTargetUUID, testOrganizationUUID, "tag", testOrganizationUUID).
		WillReturnResult(sqlmock.NewResult(0, 1))
	mock.ExpectCommit()

	err = mockDb.LinkTagsToTarget(true, testOrganizationUUID, testTargetUUID, "tag")
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}

func TestUnlinkTagsFromTarget(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	mock.ExpectBegin()
	mock.ExpectExec(`DELETE FROM target_tag`).
		WithArgs(testTargetUUID, testOrganizationUUID, "tag", testOrganizationUUID).
		WillReturnResult(sqlmock.NewResult(0, 1))
	mock.ExpectCommit()

	err = mockDb.UnlinkTagsFromTarget(testOrganizationUUID, testTargetUUID, "tag")
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}
