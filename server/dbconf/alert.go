// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package dbconf

import (
	"database/sql"
	"fmt"
	"strings"
	"time"

	"github.com/google/uuid"

	"gitlab.com/pantomath-io/panto/util"
)

// An Alert is the representation of an alert in the DB
type Alert struct {
	ID               int
	UUID             uuid.UUID
	OrganizationUUID uuid.UUID
	ChannelUUID      uuid.UUID
	Type             string
	Configuration    map[string]interface{}
	TsCreated        time.Time
	TsUpdated        time.Time
	TsDeleted        time.Time
}

// ListAlerts returns all Alerts from the DBConf
func (db *SQLiteDB) ListAlerts(organizationUUID uuid.UUID, pageSize int32, pageOffset uint64) (alerts []*Alert, err error) {
	req := new(strings.Builder)
	fmt.Fprint(req, `
		SELECT a.id, a.uuid, o.uuid, c.uuid, a.type, a.configuration, a.ts_created, a.ts_updated, a.ts_deleted, ref.configuration_template
		FROM alert AS a, organization AS o, channel AS c, alert_type AS ref
		WHERE a.channel = c.id AND c.organization = o.id AND a.type = ref.label AND a.ts_deleted IS NULL AND c.ts_deleted IS NULL AND o.ts_deleted IS NULL
			AND o.uuid = ?
	`)

	if pageSize > 0 || pageOffset > 0 {
		// if pageSize is 0, returns all the results (starting from pageOffset)
		if pageSize == 0 {
			pageSize = -1
		}
		fmt.Fprintf(req, " LIMIT %d OFFSET %d", pageSize, pageOffset)
	}

	rows, err := db.client.Query(req.String(), organizationUUID)
	if err != nil {
		return nil, fmt.Errorf("error running the query: %s", err)
	}
	defer rows.Close()

	alerts = make([]*Alert, 0)
	for rows.Next() {
		a, err := scanAlert(rows)
		if err != nil {
			log.Errorf("unable to scan row: %s", err)
			continue
		}

		alerts = append(alerts, a)
	}
	err = rows.Err()
	if err != nil {
		if err == sql.ErrNoRows {
			return []*Alert{}, nil
		}
		log.Errorf("issue in the query: %s", err)
	}

	return
}

// GetAlert returns an Alert in the DB based on its UUID and its parent organization's UUID
func (db *SQLiteDB) GetAlert(organizationUUID, alertUUID uuid.UUID) (*Alert, error) {
	o, err := scanAlert(db.client.QueryRow(`
		SELECT a.id, a.uuid, o.uuid, c.uuid, a.type, a.configuration, a.ts_created, a.ts_updated, a.ts_deleted, ref.configuration_template
		FROM alert AS a, organization AS o, channel AS c, alert_type AS ref
		WHERE a.channel = c.id AND c.organization = o.id AND a.type = ref.label AND a.ts_deleted IS NULL
			AND o.uuid = ? AND a.uuid = ?
	`, organizationUUID, alertUUID))
	if err != nil {
		if err == sql.ErrNoRows {
			// Return nil, nil to indicate no error but no result (404 Not Found)
			return nil, nil
		}
		return nil, fmt.Errorf("failed to retrieve Alert: %s", err)
	}

	return o, nil
}

// CreateAlert creates a new Alert in the DB
func (db *SQLiteDB) CreateAlert(alert *Alert) (*Alert, error) {
	if alert.Configuration == nil {
		return nil, fmt.Errorf("missing alert configuration")
	}
	configuration, err := db.marshalAlertConfiguration(alert)
	if err != nil {
		return nil, err
	}

	u := uuid.New()
	_, err = db.client.Exec(
		`INSERT INTO alert (uuid, type, configuration, channel)
		VALUES (?,?,?,
			(SELECT c.id FROM channel AS c, organization AS o WHERE c.organization = o.id AND o.ts_deleted IS NULL AND c.ts_deleted IS NULL
					AND c.uuid = ? AND o.uuid = ?)
		)`,
		u, alert.Type, configuration, alert.ChannelUUID, alert.OrganizationUUID)
	if err != nil {
		return nil, fmt.Errorf("error running the query: %s", err)
	}

	return db.GetAlert(alert.OrganizationUUID, u)
}

// UpdateAlert updates an existing Alert in the DB
func (db *SQLiteDB) UpdateAlert(alert *Alert) (*Alert, error) {
	// Specify both type and config OR none.
	if alert.Type == "" && alert.Configuration != nil {
		return nil, fmt.Errorf("missing Channel type")
	}
	if alert.Type != "" && alert.Configuration == nil {
		return nil, fmt.Errorf("missing Channel config")
	}

	args := []interface{}{}
	req := new(strings.Builder)

	fmt.Fprint(req, "UPDATE alert SET ")

	// User-supplied fields to change
	if alert.ChannelUUID != (uuid.UUID{}) {
		fmt.Fprint(req, "channel=(SELECT c.id FROM channel AS c, organization AS o WHERE c.organization = o.id AND c.ts_deleted IS NULL AND o.ts_deleted IS NULL AND c.uuid = ? AND o.uuid = ?), ")
		args = append(args, alert.ChannelUUID, alert.OrganizationUUID)
	}

	if alert.Type != "" && alert.Configuration != nil {
		configuration, err := db.marshalAlertConfiguration(alert)
		if err != nil {
			return nil, err
		}
		fmt.Fprint(req, "type=?, configuration=?, ")
		args = append(args, alert.Type, configuration)
	}

	// Change update timestamp
	fmt.Fprintf(req, `ts_updated=(strftime('%%s', 'now')) `)

	// Alert, Channel & Organization constraint
	fmt.Fprint(req, `WHERE id=(
		SELECT a.id 
		FROM alert AS a, organization AS o, channel AS c 
		WHERE a.channel = c.id AND c.organization = o.id AND o.ts_deleted IS NULL AND c.ts_deleted IS NULL
			AND a.uuid = ? AND o.uuid = ?
	) `)
	args = append(args, alert.UUID, alert.OrganizationUUID)

	// Soft delete constraint
	fmt.Fprint(req, `AND ts_deleted IS NULL`)

	res, err := db.client.Exec(req.String(), args...)
	if err != nil {
		return nil, fmt.Errorf("error running the query: %s", err)
	}
	if rowsAffected, err := res.RowsAffected(); err == nil && rowsAffected < 1 {
		return nil, fmt.Errorf("failed to update alert")
	}

	return db.GetAlert(alert.OrganizationUUID, alert.UUID)
}

// DeleteAlert deletes an Alert and all linked entities (TargetProbeAlert) from the DB
func (db *SQLiteDB) DeleteAlert(organizationUUID, alertUUID uuid.UUID) error {
	if err := db.UnlinkTargetProbesFromAlert(organizationUUID, alertUUID); err != nil {
		return fmt.Errorf("failed to unlink TargetProbes from Alert: %s", err)
	}

	_, err := db.client.Exec(`UPDATE alert 
		SET ts_deleted=(strftime('%s', 'now'))
		WHERE id = (
			SELECT a.id FROM alert AS a, channel AS c, organization AS o 
			WHERE a.channel = c.id AND c.organization = o.id AND a.uuid = ? AND o.uuid = ?
		) AND ts_deleted IS NULL`,
		alertUUID, organizationUUID)
	if err != nil {
		return fmt.Errorf("failed to delete alert: %s", err)
	}

	return nil
}

func scanAlert(r row) (*Alert, error) {
	var a Alert
	var tsCreated, tsUpdated, tsDeleted NullTime
	var configuration, template NullBytes
	err := r.Scan(
		&a.ID,
		&a.UUID,
		&a.OrganizationUUID,
		&a.ChannelUUID,
		&a.Type,
		&configuration,
		&tsCreated,
		&tsUpdated,
		&tsDeleted,
		&template,
	)
	if err != nil {
		return nil, err
	}
	var def []util.Parameter
	if template.Valid {
		if err = util.UnmarshalJSON(template.Bytes, &def); err != nil {
			return nil, fmt.Errorf("couldn't unmarshal AlertType configuration template: %s", err)
		}
	} else {
		return nil, fmt.Errorf("AlertType configuration template is invalid")
	}
	if configuration.Valid {
		if a.Configuration, err = util.UnmarshalAndValidateParameters(configuration.Bytes, def); err != nil {
			return nil, fmt.Errorf("invalid Alert configuration: %s", err)
		}
	} else {
		return nil, fmt.Errorf("Alert config is invalid")
	}
	if tsCreated.Valid {
		a.TsCreated = tsCreated.Time
	} else {
		a.TsCreated = time.Time{}
	}
	if tsUpdated.Valid {
		a.TsUpdated = tsUpdated.Time
	} else {
		a.TsUpdated = time.Time{}
	}
	if tsDeleted.Valid {
		a.TsDeleted = tsDeleted.Time
	} else {
		a.TsDeleted = time.Time{}
	}
	return &a, nil
}

func (db *SQLiteDB) marshalAlertConfiguration(alert *Alert) ([]byte, error) {
	var b NullBytes
	err := db.client.QueryRow(`SELECT configuration_template FROM alert_type WHERE label = ?`, alert.Type).Scan(&b)
	if err != nil {
		return nil, err
	}
	var ref []util.Parameter
	if b.Valid {
		if err = util.UnmarshalJSON(b.Bytes, &ref); err != nil {
			return nil, fmt.Errorf("couldn't unmarshal AlertType configuration template: %s", err)
		}
	} else {
		return nil, fmt.Errorf("AlertType configuration template is invalid")
	}

	configuration, err := util.MarshalJSON(alert.Configuration)
	if err != nil {
		return nil, fmt.Errorf("couldn't marshal Alert configuration to JSON: %s", err)
	}

	_, err = util.UnmarshalAndValidateParameters(configuration, ref)
	if err != nil {
		return nil, fmt.Errorf("invalid configuration: %s", err)
	}

	return configuration, nil
}
