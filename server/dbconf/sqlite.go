// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package dbconf

import (
	"context"
	"database/sql"
	"database/sql/driver"
	"os"
	"regexp"
	"strings"

	// import SQLite just to reference driver
	"github.com/mattn/go-sqlite3"
)

// SQLiteMinimumVersion is the SQLite library version required
// Note: the version used is the one embedded in the library, not in the system
// see github.com/mattn/go-sqlite3
const SQLiteMinimumVersion = 3025000

// SQLiteDB is a concrete implementation of `sql.DB`
// that uses Sqlite as a SQL database.
type SQLiteDB struct {
	client *sql.DB
	path   string
}

var whitespaceRegexp *regexp.Regexp

type logDriver struct {
	sqlite3.SQLiteDriver
}

func init() {
	libVersion, libVersionNumber, _ := sqlite3.Version()
	if SQLiteMinimumVersion > libVersionNumber {
		log.Fatalf("Panto was built with an outdated version of SQLite (%s). SQLite version >= 3.25.0 required.", libVersion)
	}

	whitespaceRegexp = regexp.MustCompile(`\s+`)

	connectHook := func(conn *sqlite3.SQLiteConn) error {
		return conn.RegisterFunc(
			"regexp",
			func(re, s string) (bool, error) {
				return regexp.MatchString(re, s)
			},
			true)
	}

	sql.Register("sqlite3-regexp",
		&sqlite3.SQLiteDriver{
			ConnectHook: connectHook,
		})
	sql.Register("log-sqlite3-regexp", &logDriver{
		SQLiteDriver: sqlite3.SQLiteDriver{
			ConnectHook: connectHook,
		},
	})

}

// NewSQLiteDB creates a new connection to an Sqlite DB
// The `path` argument is the path to the database
// file.
func NewSQLiteDB(path string) (ConfigurationDB, error) {
	var err error
	var sqldb SQLiteDB
	var driver string

	debugEnv := strings.ToLower(os.Getenv("PANTO_DEBUG_SQL"))

	if debugEnv == "y" || debugEnv == "yes" {
		driver = "log-sqlite3-regexp"
	} else {
		driver = "sqlite3-regexp"
	}
	sqldb.client, err = sql.Open(driver, path)
	if err != nil {
		return nil, err
	}

	sqldb.path = path

	return &sqldb, nil
}

// Close closes the database connection
func (db *SQLiteDB) Close() error {
	return db.client.Close()
}

func (db *SQLiteDB) getClient() *sql.DB {
	return db.client
}

func (db *SQLiteDB) getDialect() string {
	return "sqlite3"
}

func (d *logDriver) Open(name string) (driver.Conn, error) {
	c, err := d.SQLiteDriver.Open(name)
	if err != nil {
		return nil, err
	}
	return &logConn{SQLiteConn: c.(*sqlite3.SQLiteConn)}, nil
}

type logConn struct {
	*sqlite3.SQLiteConn
}

func (c *logConn) ExecContext(ctx context.Context, query string, args []driver.NamedValue) (driver.Result, error) {
	log.Debugf(`execing "%s", args: %s`, normalizeQuery(query), args)
	return c.SQLiteConn.ExecContext(ctx, query, args)
}
func (c *logConn) QueryContext(ctx context.Context, query string, args []driver.NamedValue) (driver.Rows, error) {
	log.Debugf(`querying "%s" with args: %s`, normalizeQuery(query), args)
	return c.SQLiteConn.QueryContext(ctx, query, args)
}

func normalizeQuery(query string) string {
	query = strings.TrimSpace(query)
	query = whitespaceRegexp.ReplaceAllString(query, " ")
	return query
}
