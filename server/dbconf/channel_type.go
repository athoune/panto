// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package dbconf

import (
	"database/sql"
	"fmt"
	"strings"

	"gitlab.com/pantomath-io/panto/util"
)

// ChannelType is the representation of a channel_type in the DB
type ChannelType struct {
	Label                 string
	ConfigurationTemplate []util.Parameter
}

// ListChannelTypes returns all the ChannelTypes from the DBConf
func (db *SQLiteDB) ListChannelTypes(pageSize int32, pageOffset uint64) (channelTypes []*ChannelType, err error) {
	req := new(strings.Builder)
	fmt.Fprint(req, `SELECT label, configuration_template FROM channel_type`)

	if pageSize > 0 || pageOffset > 0 {
		// if pageSize is 0, returns all the results (starting from pageOffset)
		if pageSize == 0 {
			pageSize = -1
		}
		fmt.Fprintf(req, " LIMIT %d OFFSET %d", pageSize, pageOffset)
	}

	rows, err := db.client.Query(req.String())
	if err != nil {
		return nil, fmt.Errorf("error running the query: %s", err)
	}
	defer rows.Close()

	channelTypes = make([]*ChannelType, 0)
	for rows.Next() {
		p, err := scanChannelType(rows)
		if err != nil {
			log.Errorf("unable to scan row: %s", err)
			continue
		}

		channelTypes = append(channelTypes, p)
	}
	err = rows.Err()
	if err != nil {
		if err == sql.ErrNoRows {
			return []*ChannelType{}, nil
		}
		log.Errorf("issue in the query: %s", err)
	}

	return
}

// GetChannelType returns a ChannelType in the DB based on its label
func (db *SQLiteDB) GetChannelType(label string) (*ChannelType, error) {
	ct, err := scanChannelType(db.client.QueryRow(`
		SELECT label, configuration_template
		FROM channel_type
		WHERE label = ?
	`, label))
	if err != nil {
		if err == sql.ErrNoRows {
			// Return nil, nil to indicate no error but no result (404 Not Found)
			return nil, nil
		}
		return nil, fmt.Errorf("failed to retrieve Probe: %s", err)
	}

	return ct, nil
}

func scanChannelType(r row) (*ChannelType, error) {
	var ct ChannelType
	var config NullBytes
	err := r.Scan(
		&ct.Label,
		&config,
	)
	if err != nil {
		return nil, err
	}
	if config.Valid {
		if err = util.UnmarshalJSON(config.Bytes, &ct.ConfigurationTemplate); err != nil {
			return nil, fmt.Errorf("couldn't unmarshal ChannelType configuration: %s", err)
		}
	} else {
		return nil, fmt.Errorf("ChannelType configuration is nil")
	}

	return &ct, nil
}
