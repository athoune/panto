// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package dbconf

import (
	"encoding/json"
	"reflect"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/google/uuid"

	"gitlab.com/pantomath-io/panto/util"
)

func TestListProbes(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	p0 := Probe{
		ID:          42,
		UUID:        testProbeUUID,
		Label:       "ping",
		DisplayName: "Ping",
		Description: "ICMP for the masses",
		ConfigurationTemplate: []util.Parameter{{
			Name:        "count",
			Type:        "int",
			Description: "number of packets",
			Mandatory:   false,
			Default:     int64(4),
		}},
		TsCreated: time.Now().AddDate(-1, 0, 0),
	}
	configTemplate0, err := json.Marshal(p0.ConfigurationTemplate)
	if err != nil {
		panic(err)
	}
	metrics0 := []byte(`{"metrics":[{"name":"avg","type":"int","description":"Average time"}],"tags":[{"name":"host","description":"The host that was pinged"}]}`)
	err = json.Unmarshal(
		metrics0,
		&p0.Metrics,
	)
	if err != nil {
		panic(err)
	}
	graphs0 := []byte(`{"graphs":[]}`)
	err = json.Unmarshal(
		graphs0,
		&p0.Graphs,
	)
	if err != nil {
		panic(err)
	}
	p1 := Probe{
		ID:          42,
		UUID:        uuid.New(),
		Label:       "pong",
		DisplayName: "Pong",
		Description: "ICMP for the mosses",
		ConfigurationTemplate: []util.Parameter{{
			Name:        "count",
			Type:        "int",
			Description: "number of pockets",
			Mandatory:   false,
			Default:     int64(4),
		}},
		TsCreated: time.Now().AddDate(-1, 0, 0),
	}
	configTemplate1, err := json.Marshal(p1.ConfigurationTemplate)
	if err != nil {
		panic(err)
	}
	metrics1 := []byte(`{"metrics":[{"name":"ovg","type":"int","description":"Overoge time"}],"tags":[{"name":"host","description":"The host that was ponged"}]}`)
	err = json.Unmarshal(
		metrics1,
		&p1.Metrics,
	)
	if err != nil {
		panic(err)
	}
	graphs1 := []byte(`{"graphs":[{"title":"RTT","type":"lines","metrics":["min","max","avg"],"tags":[]}]}`)
	err = json.Unmarshal(
		graphs1,
		&p1.Graphs,
	)
	if err != nil {
		panic(err)
	}

	rows := sqlmock.NewRows([]string{"id", "uuid", "label", "display_name", "description", "configuration_template", "metrics", "graphs", "ts_created", "ts_updated", "ts_deleted"}).
		AddRow(p0.ID, p0.UUID.String(), p0.Label, p0.DisplayName, p0.Description, configTemplate0, metrics0, graphs0, p0.TsCreated, nil, nil).
		AddRow(p1.ID, p1.UUID.String(), p1.Label, p1.DisplayName, p1.Description, configTemplate1, metrics1, graphs1, p1.TsCreated, nil, nil)

	mock.ExpectQuery(`SELECT id, uuid, label, display_name, description, configuration_template, metrics, graphs, ts_created, ts_updated, ts_deleted
		FROM probe
		WHERE ts_deleted IS NULL`).
		WillReturnRows(rows)

	probes, err := mockDb.ListProbes(0, 0)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(probes) != 2 {
		t.Fatalf("expected %d results, got %d", 2, len(probes))
	}
	if !reflect.DeepEqual(probes[0], &p0) {
		t.Fatalf("expected %v, got %v", p0, *probes[0])
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}

	rows = sqlmock.NewRows([]string{"id", "uuid", "label", "display_name", "description", "configuration_template", "metrics", "graphs", "ts_created", "ts_updated", "ts_deleted"}).
		AddRow(p1.ID, p1.UUID.String(), p1.Label, p1.DisplayName, p1.Description, configTemplate1, metrics1, graphs1, p1.TsCreated, nil, nil)
	mock.ExpectQuery(`SELECT id, uuid, label, display_name, description, configuration_template, metrics, graphs, ts_created, ts_updated, ts_deleted
		FROM probe
		WHERE ts_deleted IS NULL LIMIT -1 OFFSET 1`).
		WillReturnRows(rows)
	probes, err = mockDb.ListProbes(0, 1)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(probes) != 1 {
		t.Fatalf("expected %d results, got %d", 1, len(probes))
	}
	if !reflect.DeepEqual(probes[0], &p1) {
		t.Fatalf("expected %v, got %v", p1, *probes[0])
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}

	rows = sqlmock.NewRows([]string{"id", "uuid", "label", "display_name", "description", "configuration_template", "metrics", "graphs", "ts_created", "ts_updated", "ts_deleted"}).
		AddRow(p0.ID, p0.UUID.String(), p0.Label, p0.DisplayName, p0.Description, configTemplate0, metrics0, graphs0, p0.TsCreated, nil, nil)
	mock.ExpectQuery(`SELECT id, uuid, label, display_name, description, configuration_template, metrics, graphs, ts_created, ts_updated, ts_deleted
		FROM probe
		WHERE ts_deleted IS NULL LIMIT 1 OFFSET 0`).
		WillReturnRows(rows)
	probes, err = mockDb.ListProbes(1, 0)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(probes) != 1 {
		t.Fatalf("expected %d results, got %d", 1, len(probes))
	}
	if !reflect.DeepEqual(probes[0], &p0) {
		t.Fatalf("expected %v, got %v", p0, *probes[0])
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}

	rows = sqlmock.NewRows([]string{"id", "uuid", "label", "display_name", "description", "configuration_template", "metrics", "graphs", "ts_created", "ts_updated", "ts_deleted"}).
		AddRow(p0.ID, p0.UUID.String(), p0.Label, p0.DisplayName, p0.Description, configTemplate0, metrics0, graphs0, p0.TsCreated, nil, nil)
	mock.ExpectQuery(`SELECT id, uuid, label, display_name, description, configuration_template, metrics, graphs, ts_created, ts_updated, ts_deleted
		FROM probe
		WHERE ts_deleted IS NULL LIMIT 1 OFFSET 1`).
		WillReturnRows(rows)
	probes, err = mockDb.ListProbes(1, 1)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(probes) != 1 {
		t.Fatalf("expected %d results, got %d", 1, len(probes))
	}
	if !reflect.DeepEqual(probes[0], &p0) {
		t.Fatalf("expected %v, got %v", p0, *probes[0])
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}

func TestGetProbe(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	expect := Probe{
		ID:          42,
		UUID:        testProbeUUID,
		Label:       "ping",
		DisplayName: "Ping",
		Description: "ICMP for the masses",
		ConfigurationTemplate: []util.Parameter{{
			Name:        "count",
			Type:        "int",
			Description: "number of pockets",
			Mandatory:   false,
			Default:     int64(4),
		}},
		TsCreated: time.Now().AddDate(-1, 0, 0),
	}
	configTemplate0, err := json.Marshal(expect.ConfigurationTemplate)
	if err != nil {
		panic(err)
	}
	metrics0 := []byte(`{"metrics":[{"name":"avg","type":"int","description":"Average time"}],"tags":[{"name":"host","description":"The host that was pinged"}]}`)
	err = json.Unmarshal(
		metrics0,
		&expect.Metrics,
	)
	if err != nil {
		panic(err)
	}
	graphs0 := []byte(`{"graphs":[{"title":"RTT","type":"lines","metrics":["min","max","avg"],"tags":[]}]}`)
	err = json.Unmarshal(
		graphs0,
		&expect.Graphs,
	)
	if err != nil {
		panic(err)
	}

	rows := sqlmock.NewRows([]string{"id", "uuid", "label", "display_name", "description", "configuration_template", "metrics", "graphs", "ts_created", "ts_updated", "ts_deleted"}).
		AddRow(expect.ID, expect.UUID.String(), expect.Label, expect.DisplayName, expect.Description, configTemplate0, metrics0, graphs0, expect.TsCreated, nil, nil)

	mock.ExpectQuery(`SELECT id, uuid, label, display_name, description, configuration_template, metrics, graphs, ts_created, ts_updated, ts_deleted
		FROM probe
		WHERE uuid = \? AND ts_deleted IS NULL`).
		WithArgs(testProbeUUID).
		WillReturnRows(rows)

	got, err := mockDb.GetProbe(testProbeUUID)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if !reflect.DeepEqual(got, &expect) {
		t.Fatalf("expected %v, got %v", expect, *got)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}
