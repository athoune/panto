// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package dbconf

import (
	"database/sql"
	"fmt"
	"strings"

	"github.com/google/uuid"
)

// ListTargetProbesByAlert returns all the TargetProbe linked to an Alert from the DBConf
func (db *SQLiteDB) ListTargetProbesByAlert(organizationUUID, alertUUID uuid.UUID, pageSize int32, pageOffset uint64) (targetProbes []*TargetProbe, err error) {
	req := new(strings.Builder)
	fmt.Fprint(req, `
		SELECT tp.id, tp.uuid, t.uuid, p.uuid, o.uuid, tp.state_type, tp.state_configuration, tp.ts_created, tp.ts_updated, tp.ts_deleted, ref.configuration_template
		FROM target_probe AS tp, target AS t, probe AS p, organization AS o, state_type AS ref, target_probe_alert as tpa, alert as a
		WHERE tp.target = t.id AND tp.probe = p.id AND t.organization = o.id AND tp.id = tpa.target_probe AND a.id = tpa.alert AND tp.state_type = ref.label
			AND tp.ts_deleted IS NULL AND t.ts_deleted IS NULL AND p.ts_deleted IS NULL AND o.ts_deleted IS NULL AND a.ts_deleted IS NULL
			AND o.uuid = ? AND a.uuid = ?
	`)

	if pageSize > 0 || pageOffset > 0 {
		// if pageSize is 0, returns all the results (starting from pageOffset)
		if pageSize == 0 {
			pageSize = -1
		}
		fmt.Fprintf(req, " LIMIT %d OFFSET %d", pageSize, pageOffset)
	}

	rows, err := db.client.Query(req.String(), organizationUUID, alertUUID)
	if err != nil {
		return nil, fmt.Errorf("error running the query: %s", err)
	}
	defer rows.Close()

	targetProbes = make([]*TargetProbe, 0)
	for rows.Next() {
		tp, err := scanTargetProbe(rows)
		if err != nil {
			log.Errorf("unable to scan row: %s", err)
			continue
		}

		targetProbes = append(targetProbes, tp)
	}
	err = rows.Err()
	if err != nil {
		if err == sql.ErrNoRows {
			return []*TargetProbe{}, nil
		}
		log.Errorf("issue in the query: %s", err)
	}

	return
}

// ListAlertsByTargetProbe returns all the Alerts linked to a TargetProbe from the DBConf
func (db *SQLiteDB) ListAlertsByTargetProbe(organizationUUID, targetProbeUUID uuid.UUID, pageSize int32, pageOffset uint64) ([]*Alert, error) {
	req := new(strings.Builder)
	fmt.Fprint(req, `
		SELECT a.id, a.uuid, o.uuid, c.uuid, a.type, a.configuration, a.ts_created, a.ts_updated, a.ts_deleted, ref.configuration_template
		FROM alert AS a, organization AS o, channel AS c, alert_type AS ref, target_probe AS tp, target_probe_alert AS tpa
		WHERE a.channel = c.id AND c.organization = o.id AND a.type = ref.label 
		    AND tpa.target_probe = tp.id AND tpa.alert = a.id
			AND a.ts_deleted IS NULL AND c.ts_deleted IS NULL AND o.ts_deleted IS NULL
			AND o.uuid = ? AND tp.uuid = ?
	`)

	if pageSize > 0 || pageOffset > 0 {
		// if pageSize is 0, returns all the results (starting from pageOffset)
		if pageSize == 0 {
			pageSize = -1
		}
		fmt.Fprintf(req, " LIMIT %d OFFSET %d", pageSize, pageOffset)
	}

	rows, err := db.client.Query(req.String(), organizationUUID, targetProbeUUID)
	if err != nil {
		return nil, fmt.Errorf("error running the query: %s", err)
	}
	defer rows.Close()

	alerts := make([]*Alert, 0)
	for rows.Next() {
		a, err := scanAlert(rows)
		if err != nil {
			log.Errorf("unable to scan row: %s", err)
			continue
		}

		alerts = append(alerts, a)
	}
	err = rows.Err()
	if err != nil {
		if err == sql.ErrNoRows {
			return []*Alert{}, nil
		}
		log.Errorf("issue in the query: %s", err)
	}

	return alerts, nil
}

// LinkTargetProbesToAlert connects a number of target_probes to an alert in the DB
// Set force to true to also remove all pre-existing links to alert
func (db *SQLiteDB) LinkTargetProbesToAlert(force bool, organizationUUID, alertUUID uuid.UUID, targetProbes ...uuid.UUID) error {
	tx, err := db.client.Begin()

	if err != nil {
		return fmt.Errorf("failed to begin transaction: %s", err)
	}
	// Defer rollback. If transaction is already done, sql.ErrTxDone is silently ignored.
	defer tx.Rollback()

	// Remove all pre-existing links
	if force {
		_, err = tx.Exec(`
			DELETE FROM target_probe_alert WHERE alert = (
				SELECT a.id FROM alert AS a, channel AS c, organization AS o
					WHERE a.channel = c.id AND c.organization = o.id AND a.uuid = ? AND o.UUID = ?)
		`, alertUUID, organizationUUID)
		if err != nil {
			return fmt.Errorf("failed to delete pre-existing target_probes: %s", err)
		}
	}

	for _, tpUUID := range targetProbes {
		res, err := tx.Exec(`
		INSERT INTO target_probe_alert (target_probe, alert) VALUES (
			(SELECT tp.id FROM target_probe AS tp, target AS t, organization AS o
				WHERE tp.target = t.id AND t.organization = o.id
					AND tp.ts_deleted IS NULL AND t.ts_deleted IS NULL AND o.ts_deleted IS NULL
					AND tp.uuid = ? AND o.UUID = ?),
			(SELECT a.id FROM alert AS a, channel AS c, organization AS o
				WHERE a.channel = c.id AND c.organization = o.id
					AND a.ts_deleted IS NULL AND c.ts_deleted IS NULL AND o.ts_deleted IS NULL
					AND a.uuid = ? AND o.UUID = ?)
		)
	`, tpUUID, organizationUUID, alertUUID, organizationUUID)
		if err != nil {
			return fmt.Errorf("query error: %s", err)
		}
		if n, _ := res.RowsAffected(); n == 0 {
			return fmt.Errorf("failed to insert link")
		}
	}

	err = tx.Commit()
	if err != nil {
		return fmt.Errorf("failed to commit transaction: %s", err)
	}

	return nil
}

// UnlinkTargetProbesFromAlert removes all existing links to alert
func (db *SQLiteDB) UnlinkTargetProbesFromAlert(organizationUUID, alertUUID uuid.UUID) error {
	// Remove all existing links
	_, err := db.client.Exec(`
		DELETE FROM target_probe_alert WHERE alert = (
			SELECT a.id FROM alert AS a, channel AS c, organization AS o
				WHERE a.channel = c.id AND c.organization = o.id AND a.uuid = ? AND o.UUID = ?)
	`, alertUUID, organizationUUID)
	if err != nil {
		return fmt.Errorf("SQL execution error: %s", err)
	}

	return nil
}
