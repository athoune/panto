// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package dbconf

import (
	"database/sql"
	"fmt"
	"strings"
	"time"

	"github.com/google/uuid"

	"gitlab.com/pantomath-io/panto/util"
)

// TargetProbeAgent is a representation of a target_probe_agent in the DB
type TargetProbeAgent struct {
	UUID               uuid.UUID
	OrganizationUUID   uuid.UUID
	AgentUUID          uuid.UUID
	TargetProbeUUID    uuid.UUID
	ProbeLabel         string
	ProbeConfiguration map[string]interface{}
	Schedule           time.Duration
	TsCreated          time.Time
	TsUpdated          time.Time
	TsDeleted          time.Time
}

// ListTargetProbeAgents returns all TargetProbeAgents for a given organization and agent
func (db *SQLiteDB) ListTargetProbeAgents(organizationUUID, agentUUID uuid.UUID, pageSize int32, pageOffset uint64) ([]*TargetProbeAgent, error) {
	req := new(strings.Builder)
	fmt.Fprint(req, `
		SELECT tpa.uuid, o.uuid, a.uuid, tp.uuid, p.label, tpa.probe_configuration, tpa.schedule, p.configuration_template, tpa.ts_created, tpa.ts_updated, tpa.ts_deleted
		FROM target_probe_agent AS tpa, target_probe AS tp, agent AS a, organization AS o, probe AS p
		WHERE tpa.target_probe = tp.id AND tpa.agent = a.id AND a.organization = o.id AND tp.probe = p.id
			AND tpa.ts_deleted IS NULL AND tp.ts_deleted IS NULL AND a.ts_deleted IS NULL AND o.ts_deleted IS NULL AND p.ts_deleted IS NULL
			AND o.uuid = ? AND a.uuid = ?
	`)

	if pageSize > 0 || pageOffset > 0 {
		// if pageSize is 0, returns all the results (starting from pageOffset)
		if pageSize == 0 {
			pageSize = -1
		}
		fmt.Fprintf(req, " LIMIT %d OFFSET %d", pageSize, pageOffset)
	}

	rows, err := db.client.Query(req.String(), organizationUUID, agentUUID)
	if err != nil {
		return nil, fmt.Errorf("error running the query: %s", err)
	}
	defer rows.Close()

	targetProbeAgents := make([]*TargetProbeAgent, 0)
	for rows.Next() {
		tpa, err := scanTargetProbeAgent(rows)
		if err != nil {
			log.Errorf("unable to scan row: %s", err)
			continue
		}

		targetProbeAgents = append(targetProbeAgents, tpa)
	}
	err = rows.Err()
	if err != nil {
		if err == sql.ErrNoRows {
			return []*TargetProbeAgent{}, nil
		}
		log.Errorf("issue in the query: %s", err)
	}

	return targetProbeAgents, nil
}

// ListTargetProbeAgentsForOrganization returns all TargetProbeAgents for a given organization
func (db *SQLiteDB) ListTargetProbeAgentsForOrganization(organizationUUID uuid.UUID, checkUUIDs []uuid.UUID, pageSize int32, pageOffset uint64) ([]*TargetProbeAgent, error) {
	req := new(strings.Builder)
	args := []interface{}{}

	fmt.Fprint(req, `
		SELECT tpa.uuid, o.uuid, a.uuid, tp.uuid, p.label, tpa.probe_configuration, tpa.schedule, p.configuration_template, tpa.ts_created, tpa.ts_updated, tpa.ts_deleted
		FROM target_probe_agent AS tpa, target_probe AS tp, agent AS a, organization AS o, probe AS p
		WHERE tpa.target_probe = tp.id AND tpa.agent = a.id AND a.organization = o.id AND tp.probe = p.id
			AND tpa.ts_deleted IS NULL AND tp.ts_deleted IS NULL AND a.ts_deleted IS NULL AND o.ts_deleted IS NULL AND p.ts_deleted IS NULL
			AND o.uuid = ?
	`)
	args = append(args, organizationUUID)

	if len(checkUUIDs) > 0 {
		s := strings.Repeat("?,", len(checkUUIDs))
		fmt.Fprintf(req, " AND tp.UUID IN (%s)", s[:len(s)-1])
		for _, u := range checkUUIDs {
			args = append(args, u)
		}
	}

	if pageSize > 0 || pageOffset > 0 {
		// if pageSize is 0, returns all the results (starting from pageOffset)
		if pageSize == 0 {
			pageSize = -1
		}
		fmt.Fprintf(req, " LIMIT %d OFFSET %d", pageSize, pageOffset)
	}

	rows, err := db.client.Query(req.String(), args...)
	if err != nil {
		return nil, fmt.Errorf("error running the query: %s", err)
	}
	defer rows.Close()

	targetProbeAgents := make([]*TargetProbeAgent, 0)
	for rows.Next() {
		tpa, err := scanTargetProbeAgent(rows)
		if err != nil {
			log.Errorf("unable to scan row: %s", err)
			continue
		}

		targetProbeAgents = append(targetProbeAgents, tpa)
	}
	err = rows.Err()
	if err != nil {
		if err == sql.ErrNoRows {
			return []*TargetProbeAgent{}, nil
		}
		log.Errorf("issue in the query: %s", err)
	}

	return targetProbeAgents, nil
}

// ListTargetProbeAgentsAll returns all TargetProbeAgents
func (db *SQLiteDB) ListTargetProbeAgentsAll(pageSize int32, pageOffset uint64) ([]*TargetProbeAgent, error) {
	req := new(strings.Builder)
	fmt.Fprint(req, `
		SELECT tpa.uuid, o.uuid, a.uuid, tp.uuid, p.label, tpa.probe_configuration, tpa.schedule, p.configuration_template, tpa.ts_created, tpa.ts_updated, tpa.ts_deleted
		FROM target_probe_agent AS tpa, target_probe AS tp, agent AS a, organization AS o, probe AS p
		WHERE tpa.target_probe = tp.id AND tpa.agent = a.id AND a.organization = o.id AND tp.probe = p.id
			AND tpa.ts_deleted IS NULL AND tp.ts_deleted IS NULL AND a.ts_deleted IS NULL AND o.ts_deleted IS NULL AND p.ts_deleted IS NULL
	`)

	if pageSize > 0 || pageOffset > 0 {
		if pageSize == 0 {
			pageSize = -1
		}
		fmt.Fprintf(req, " LIMIT %d OFFSET %d", pageSize, pageOffset)
	}

	rows, err := db.client.Query(req.String())
	if err != nil {
		return nil, fmt.Errorf("error running the query: %s", err)
	}
	defer rows.Close()

	targetProbeAgents := make([]*TargetProbeAgent, 0)
	for rows.Next() {
		tpa, err := scanTargetProbeAgent(rows)
		if err != nil {
			log.Errorf("unable to scan row: %s", err)
			continue
		}

		targetProbeAgents = append(targetProbeAgents, tpa)
	}
	err = rows.Err()
	if err != nil {
		if err == sql.ErrNoRows {
			return []*TargetProbeAgent{}, nil
		}
		log.Errorf("issue in the query: %s", err)
	}

	return targetProbeAgents, nil
}

// GetTargetProbeAgent returns a TargetProbeAgent according to its UUID and parent organization & agent
func (db *SQLiteDB) GetTargetProbeAgent(organizationUUID, agentUUID, targetProbeAgentUUID uuid.UUID) (*TargetProbeAgent, error) {
	t, err := scanTargetProbeAgent(db.client.QueryRow(`
		SELECT tpa.uuid, o.uuid, a.uuid, tp.uuid, p.label, tpa.probe_configuration, tpa.schedule, p.configuration_template, tpa.ts_created, tpa.ts_updated, tpa.ts_deleted
		FROM target_probe_agent AS tpa, target_probe AS tp, agent AS a, organization AS o, probe AS p
		WHERE tpa.target_probe = tp.id AND tpa.agent = a.id AND a.organization = o.id AND tp.probe = p.id AND tpa.ts_deleted IS NULL
			AND tpa.uuid = ? AND o.uuid = ? AND a.uuid = ?
	`, targetProbeAgentUUID, organizationUUID, agentUUID))
	if err != nil {
		if err == sql.ErrNoRows {
			// Return nil, nil to indicate no error but no result (404 Not Found)
			return nil, nil
		}
		return nil, fmt.Errorf("failed to retrieve TargetProbeAgent: %s", err)
	}

	return t, nil
}

// CreateTargetProbeAgent inserts a new TargetProbeAgent into the DB
func (db *SQLiteDB) CreateTargetProbeAgent(targetProbeAgent *TargetProbeAgent) (*TargetProbeAgent, error) {
	if targetProbeAgent.Schedule <= 0 {
		return nil, fmt.Errorf("invalid TargetProbeAgent schedule")
	}
	configuration, err := db.marshalProbeConfiguration(targetProbeAgent)
	if err != nil {
		return nil, fmt.Errorf("invalid TargetProbeAgent configuration: %s", err)
	}

	u := uuid.New()
	res, err := db.client.Exec(
		`INSERT INTO target_probe_agent (uuid, probe_configuration, schedule, agent, target_probe)
		VALUES (?,?,?,
			(SELECT a.id FROM agent AS a, organization AS o
				WHERE a.organization = o.id AND a.ts_deleted IS NULL AND o.ts_deleted IS NULL
					AND a.uuid = ? AND o.uuid = ?),
			(SELECT tp.id FROM target_probe AS tp, target AS t, organization AS o
				WHERE tp.target = t.id AND t.organization = o.id AND tp.ts_deleted IS NULL AND t.ts_deleted IS NULL AND o.ts_deleted IS NULL
					AND tp.uuid = ? AND o.uuid = ?)
		)`,
		u, configuration, targetProbeAgent.Schedule,
		targetProbeAgent.AgentUUID, targetProbeAgent.OrganizationUUID,
		targetProbeAgent.TargetProbeUUID, targetProbeAgent.OrganizationUUID)
	if err != nil {
		return nil, fmt.Errorf("error running the query: %s", err)
	}
	if r, err := res.RowsAffected(); err != nil || r < 1 {
		return nil, fmt.Errorf("error inserting target_probe_agent: %s", err)
	}

	// update field `ts_configuration` in `agent` object
	_, err = db.client.Exec(
		`UPDATE agent SET ts_configuration=(strftime('%%s', 'now'))
		WHERE id = (SELECT a.id FROM agent AS a, organization AS o
			WHERE a.organization = o.id AND a.ts_deleted IS NULL AND o.ts_deleted IS NULL
				AND a.uuid = ? AND o.uuid = ?)`,
		targetProbeAgent.AgentUUID, targetProbeAgent.OrganizationUUID)
	if err != nil {
		return nil, fmt.Errorf("error running the query: %s", err)
	}

	return db.GetTargetProbeAgent(targetProbeAgent.OrganizationUUID, targetProbeAgent.AgentUUID, u)
}

// UpdateTargetProbeAgent updates the writable fields of the TargetProbeAgent
func (db *SQLiteDB) UpdateTargetProbeAgent(targetProbeAgent *TargetProbeAgent) (*TargetProbeAgent, error) {
	if targetProbeAgent.Schedule < 0 {
		return nil, fmt.Errorf("invalid TargetProbeAgent schedule")
	}

	args := []interface{}{}
	req := new(strings.Builder)

	fmt.Fprint(req, "UPDATE target_probe_agent SET ")

	// User-supplied fields to change
	if targetProbeAgent.Schedule > 0 {
		fmt.Fprint(req, `schedule = ?, `)
		args = append(args, targetProbeAgent.Schedule)
	}
	if targetProbeAgent.ProbeConfiguration != nil {
		configuration, err := db.marshalProbeConfiguration(targetProbeAgent)
		if err != nil {
			return nil, fmt.Errorf("invalid probe configuration parameters: %s", err)
		}
		fmt.Fprint(req, `probe_configuration = ?, `)
		args = append(args, configuration)
	}

	// Change update timestamp
	fmt.Fprintf(req, `ts_updated=(strftime('%%s', 'now')) `)

	// TargetProbe & Agent & Organization constraint
	fmt.Fprint(req, `WHERE uuid = ?
		AND agent = (SELECT a.id 
					 FROM agent AS a, target_probe_agent AS tpa, organization AS o 
					 WHERE tpa.agent = a.id AND a.organization = o.id AND a.ts_deleted IS NULL AND o.ts_deleted IS NULL
					 	AND tpa.uuid = ? AND a.uuid = ? AND o.uuid = ?)
	`)
	args = append(args,
		targetProbeAgent.UUID,
		targetProbeAgent.UUID, targetProbeAgent.AgentUUID, targetProbeAgent.OrganizationUUID,
	)
	// Soft delete constraint
	fmt.Fprint(req, `AND ts_deleted IS NULL`)

	_, err := db.client.Exec(req.String(), args...)
	if err != nil {
		return nil, fmt.Errorf("error running the query: %s", err)
	}

	// update field `ts_configuration` in `agent` object
	res, err := db.client.Exec(
		`UPDATE agent SET ts_configuration=(strftime('%%s', 'now'))
		WHERE id = (SELECT a.id FROM agent AS a, organization AS o
			WHERE a.organization = o.id AND a.ts_deleted IS NULL AND o.ts_deleted IS NULL
				AND a.uuid = ? AND o.uuid = ?)`,
		targetProbeAgent.AgentUUID, targetProbeAgent.OrganizationUUID)
	if err != nil {
		return nil, fmt.Errorf("error running the query: %s", err)
	}
	if rowsAffected, err := res.RowsAffected(); err == nil && rowsAffected < 1 {
		return nil, fmt.Errorf("failed to update target_probe_agent")
	}

	return db.GetTargetProbeAgent(targetProbeAgent.OrganizationUUID, targetProbeAgent.AgentUUID, targetProbeAgent.UUID)
}

// DeleteTargetProbeAgent removes a TargetProbeAgent from the DB
func (db *SQLiteDB) DeleteTargetProbeAgent(organizationUUID, agentUUID, targetProbeAgentUUID uuid.UUID) error {
	_, err := db.client.Exec(`
		UPDATE target_probe_agent
		SET ts_deleted=(strftime('%s', 'now'))
		WHERE uuid = ?
			AND agent = (SELECT a.id FROM agent AS a, target_probe_agent AS tpa, organization AS o
				WHERE tpa.agent = a.id AND a.organization = o.id AND tpa.uuid = ? AND a.uuid = ? AND o.uuid = ?)
	`, targetProbeAgentUUID, targetProbeAgentUUID, agentUUID, organizationUUID)
	if err != nil {
		return fmt.Errorf("failed to delete TargetProbeAgent: %s", err)
	}
	return nil
}

func scanTargetProbeAgent(r row) (*TargetProbeAgent, error) {
	var tpa TargetProbeAgent
	var tsCreated, tsUpdated, tsDeleted NullTime
	var schedule sql.NullInt64
	var configuration, template NullBytes
	err := r.Scan(
		&tpa.UUID,
		&tpa.OrganizationUUID,
		&tpa.AgentUUID,
		&tpa.TargetProbeUUID,
		&tpa.ProbeLabel,
		&configuration,
		&schedule,
		&template,
		&tsCreated,
		&tsUpdated,
		&tsDeleted,
	)
	if err != nil {
		return nil, err
	}
	var def []util.Parameter
	if template.Valid {
		if err = util.UnmarshalJSON(template.Bytes, &def); err != nil {
			return nil, fmt.Errorf("couldn't unmarshal Probe configuration template: %s", err)
		}
	} else {
		return nil, fmt.Errorf("Probe configuration template is invalid")
	}
	if configuration.Valid {
		if tpa.ProbeConfiguration, err = util.UnmarshalAndValidateParameters(configuration.Bytes, def); err != nil {
			return nil, fmt.Errorf("invalid configuration: %s", err)
		}
	} else {
		return nil, fmt.Errorf("Probe config is invalid")
	}
	if schedule.Valid {
		tpa.Schedule = time.Duration(schedule.Int64)
	} else {
		return nil, fmt.Errorf("invalid TargetProbeAgent schedule")
	}
	if tsCreated.Valid {
		tpa.TsCreated = tsCreated.Time
	} else {
		tpa.TsCreated = time.Time{}
	}
	if tsUpdated.Valid {
		tpa.TsUpdated = tsUpdated.Time
	} else {
		tpa.TsUpdated = time.Time{}
	}
	if tsDeleted.Valid {
		tpa.TsDeleted = tsDeleted.Time
	} else {
		tpa.TsDeleted = time.Time{}
	}
	return &tpa, nil
}

func (db *SQLiteDB) marshalProbeConfiguration(tpa *TargetProbeAgent) ([]byte, error) {
	if tpa.TargetProbeUUID == (uuid.UUID{}) {
		// If UUID is missing get TPA first
		tpaFromDb, err := db.GetTargetProbeAgent(tpa.OrganizationUUID, tpa.AgentUUID, tpa.UUID)
		if err != nil {
			return nil, err
		}
		if tpaFromDb == nil {
			return nil, fmt.Errorf("target_probe_agent not found")
		}
		tpa.TargetProbeUUID = tpaFromDb.TargetProbeUUID
	}
	tp, err := db.GetTargetProbe(tpa.OrganizationUUID, tpa.TargetProbeUUID)
	if err != nil {
		return nil, err
	}
	if tp == nil {
		return nil, fmt.Errorf("target_probe not found")
	}
	p, err := db.GetProbe(tp.ProbeUUID)
	if err != nil {
		return nil, err
	}
	if p == nil {
		return nil, fmt.Errorf("probe not found")
	}

	detail, err := util.MarshalJSON(tpa.ProbeConfiguration)
	if err != nil {
		return nil, fmt.Errorf("couldn't marshal channel config to JSON: %s", err)
	}
	_, err = util.UnmarshalAndValidateParameters(detail, p.ConfigurationTemplate)
	if err != nil {
		return nil, fmt.Errorf("invalid configuration: %s", err)
	}

	return detail, nil
}
