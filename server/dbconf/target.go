// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package dbconf

import (
	"database/sql"
	"fmt"
	"strings"
	"time"

	"github.com/google/uuid"
)

// A Target is the representation of a target in the DB
type Target struct {
	ID               int
	UUID             uuid.UUID
	OrganizationUUID uuid.UUID
	Address          string
	Note             string
	TsCreated        time.Time
	TsUpdated        time.Time
	TsDeleted        time.Time
}

// ListTargets returns all Targets from the DBConf
func (db *SQLiteDB) ListTargets(organizationUUID uuid.UUID, pageSize int32, pageOffset uint64) (targets []*Target, err error) {
	req := new(strings.Builder)
	fmt.Fprint(req, `
		SELECT t.id, t.uuid, o.uuid, t.address, t.note, t.ts_created, t.ts_updated, t.ts_deleted
		FROM target AS t, organization AS o
		WHERE t.organization = o.id AND t.ts_deleted IS NULL AND o.ts_deleted IS NULL AND o.uuid = ?
	`)

	if pageSize > 0 || pageOffset > 0 {
		// if pageSize is 0, returns all the results (starting from pageOffset)
		if pageSize == 0 {
			pageSize = -1
		}
		fmt.Fprintf(req, " LIMIT %d OFFSET %d", pageSize, pageOffset)
	}

	rows, err := db.client.Query(req.String(), organizationUUID)
	if err != nil {
		return nil, fmt.Errorf("error running the query: %s", err)
	}
	defer rows.Close()

	targets = make([]*Target, 0)
	for rows.Next() {
		t, err := scanTarget(rows)
		if err != nil {
			log.Errorf("unable to scan row: %s", err)
			continue
		}

		targets = append(targets, t)
	}
	err = rows.Err()
	if err != nil {
		if err == sql.ErrNoRows {
			return []*Target{}, nil
		}
		log.Errorf("issue in the query: %s", err)
	}

	return
}

// GetTarget returns a Target in the DB based on its UUID and its parent organization's UUID
func (db *SQLiteDB) GetTarget(organizationUUID, targetUUID uuid.UUID) (*Target, error) {
	o, err := scanTarget(db.client.QueryRow(`
		SELECT t.id, t.uuid, o.uuid, t.address, t.note, t.ts_created, t.ts_updated, t.ts_deleted
		FROM target AS t, organization AS o
		WHERE t.organization = o.id AND t.ts_deleted IS NULL
			AND o.uuid = ? AND t.uuid = ?
	`, organizationUUID, targetUUID))
	if err != nil {
		if err == sql.ErrNoRows {
			// Return nil, nil to indicate no error but no result (404 Not Found)
			return nil, nil
		}
		return nil, fmt.Errorf("failed to retrieve Target: %s", err)
	}

	return o, nil
}

// CreateTarget creates a new Target in the DB
func (db *SQLiteDB) CreateTarget(target *Target) (*Target, error) {
	if len(target.Address) == 0 {
		return nil, fmt.Errorf("missing target address")
	}

	u := uuid.New()
	_, err := db.client.Exec(
		`INSERT INTO target (uuid, address, note, organization)
		VALUES (?,?,?,
			(SELECT id FROM organization WHERE ts_deleted IS NULL AND uuid = ?)
		)`,
		u, target.Address, target.Note, target.OrganizationUUID)
	if err != nil {
		return nil, fmt.Errorf("error running the query: %s", err)
	}

	return db.GetTarget(target.OrganizationUUID, u)
}

// UpdateTarget updates an existing Target in the DB
func (db *SQLiteDB) UpdateTarget(target *Target) (*Target, error) {
	args := []interface{}{}
	req := new(strings.Builder)

	fmt.Fprint(req, "UPDATE target SET ")

	// User-supplied fields to change
	if target.Address != "" {
		fmt.Fprint(req, "address=?, ")
		args = append(args, target.Address)
	}
	fmt.Fprint(req, "note=?, ")
	args = append(args, target.Note)

	// Change update timestamp
	fmt.Fprintf(req, `ts_updated = (strftime('%%s', 'now')) `)

	// Target & Organization constraint
	fmt.Fprint(req, `WHERE uuid=? AND organization = (SELECT o.id FROM organization AS o, target AS t WHERE t.organization = o.id AND o.ts_deleted IS NULL AND o.uuid=?) `)
	args = append(args, target.UUID, target.OrganizationUUID)

	// Soft delete constraint
	fmt.Fprint(req, `AND ts_deleted IS NULL`)

	res, err := db.client.Exec(req.String(), args...)
	if err != nil {
		return nil, fmt.Errorf("error running the query: %s", err)
	}
	if rowsAffected, err := res.RowsAffected(); err == nil && rowsAffected < 1 {
		return nil, fmt.Errorf("failed to update target")
	}

	return db.GetTarget(target.OrganizationUUID, target.UUID)
}

// DeleteTarget deletes a Target and all linked entities (TargetProbe, TargetProbeAgent) from the DB
func (db *SQLiteDB) DeleteTarget(organizationUUID, targetUUID uuid.UUID) error {
	tx, err := db.client.Begin()
	if err != nil {
		return fmt.Errorf("failed to begin transaction: %s", err)
	}
	// Defer rollback. If transaction is already done, sql.ErrTxDone is silently ignored.
	defer tx.Rollback()

	// Delete linked target_probe_agent entries
	_, err = tx.Exec(`UPDATE target_probe_agent
		SET ts_deleted=(strftime('%s', 'now')) 
		WHERE target_probe =
			(SELECT tp.id FROM target_probe AS tp, target AS t, organization AS o
			WHERE t.organization=o.id AND tp.target=t.id
				AND t.uuid=? AND o.uuid=?)
		AND ts_deleted IS NULL`,
		targetUUID, organizationUUID)
	if err != nil {
		return fmt.Errorf("failed to delete linked TargetProbeAgents: %s", err)
	}

	// Delete linked target_probe entries
	_, err = tx.Exec(`UPDATE target_probe
		SET ts_deleted=(strftime('%s', 'now')) 
		WHERE target =
			(SELECT t.id FROM target AS t, organization AS o
			WHERE t.organization=o.id
				AND t.uuid=? AND o.uuid=?)
		AND ts_deleted IS NULL`,
		targetUUID, organizationUUID)
	if err != nil {
		return fmt.Errorf("failed to delete linked TargetProbes: %s", err)
	}

	// Delete target
	_, err = tx.Exec(`UPDATE target 
		SET ts_deleted=(strftime('%s', 'now'))
		WHERE id = 
			(SELECT t.id FROM target AS t, organization AS o
			WHERE t.organization=o.id
				AND t.uuid=? AND o.uuid=?)
		AND ts_deleted IS NULL`,
		targetUUID, organizationUUID)
	if err != nil {
		return fmt.Errorf("failed to delete Target: %s", err)
	}

	err = tx.Commit()
	if err != nil {
		return fmt.Errorf("failed to commit transaction: %s", err)
	}

	return nil
}

func scanTarget(r row) (*Target, error) {
	var a Target
	var tsCreated, tsUpdated, tsDeleted NullTime
	var note sql.NullString
	err := r.Scan(
		&a.ID,
		&a.UUID,
		&a.OrganizationUUID,
		&a.Address,
		&note,
		&tsCreated,
		&tsUpdated,
		&tsDeleted,
	)
	if err != nil {
		return nil, err
	}
	if tsCreated.Valid {
		a.TsCreated = tsCreated.Time
	} else {
		a.TsCreated = time.Time{}
	}
	if tsUpdated.Valid {
		a.TsUpdated = tsUpdated.Time
	} else {
		a.TsUpdated = time.Time{}
	}
	if tsDeleted.Valid {
		a.TsDeleted = tsDeleted.Time
	} else {
		a.TsDeleted = time.Time{}
	}
	if note.Valid {
		a.Note = note.String
	}
	return &a, nil
}
