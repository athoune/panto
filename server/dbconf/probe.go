// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package dbconf

import (
	"database/sql"
	"fmt"
	"strings"
	"time"

	"github.com/google/uuid"

	"gitlab.com/pantomath-io/panto/util"
)

// Metrics describes the fields and tags of a Probe's results
type Metrics struct {
	Metrics []struct {
		Name        string
		Type        string
		Description string
	}
	Tags []struct {
		Name        string
		Description string
	}
}

// Graphs describes the graphs to render a Probe
type Graphs struct {
	Graphs []struct {
		Title   string   // name of the graph
		Type    string   // type of graph (lines, stacked-lines)
		Format  string   // unit formatter for the metrics
		Metrics []string // list of metrics to graph
		Tags    []string // list of tags to group the metrics on (and to duplicate graphs)
	}
}

// Probe is the representation of a probe in the DB
type Probe struct {
	ID                    int
	UUID                  uuid.UUID
	Label                 string
	DisplayName           string
	Description           string
	ConfigurationTemplate []util.Parameter
	Metrics               Metrics
	Graphs                Graphs
	TsCreated             time.Time
	TsUpdated             time.Time
	TsDeleted             time.Time
}

// ListProbes returns all Probes from the DBConf
func (db *SQLiteDB) ListProbes(pageSize int32, pageOffset uint64) (probes []*Probe, err error) {
	req := new(strings.Builder)
	fmt.Fprint(req, `
		SELECT id, uuid, label, display_name, description, configuration_template, metrics, graphs, ts_created, ts_updated, ts_deleted
		FROM probe
		WHERE ts_deleted IS NULL
	`)

	if pageSize > 0 || pageOffset > 0 {
		// if pageSize is 0, returns all the results (starting from pageOffset)
		if pageSize == 0 {
			pageSize = -1
		}
		fmt.Fprintf(req, " LIMIT %d OFFSET %d", pageSize, pageOffset)
	}

	rows, err := db.client.Query(req.String())
	if err != nil {
		return nil, fmt.Errorf("error running the query: %s", err)
	}
	defer rows.Close()

	probes = make([]*Probe, 0)
	for rows.Next() {
		p, err := scanProbe(rows)
		if err != nil {
			log.Errorf("unable to scan row: %s", err)
			continue
		}

		probes = append(probes, p)
	}
	err = rows.Err()
	if err != nil {
		if err == sql.ErrNoRows {
			return []*Probe{}, nil
		}
		log.Errorf("issue in the query: %s", err)
	}

	return
}

// GetProbe returns a Probe in the DB based on its UUID
func (db *SQLiteDB) GetProbe(probeUUID uuid.UUID) (*Probe, error) {
	p, err := scanProbe(db.client.QueryRow(`
		SELECT id, uuid, label, display_name, description, configuration_template, metrics, graphs, ts_created, ts_updated, ts_deleted
		FROM probe
		WHERE uuid = ? AND ts_deleted IS NULL
	`, probeUUID))
	if err != nil {
		if err == sql.ErrNoRows {
			// Return nil, nil to indicate no error but no result (404 Not Found)
			return nil, nil
		}
		return nil, fmt.Errorf("failed to retrieve Probe: %s", err)
	}

	return p, nil
}

func scanProbe(r row) (*Probe, error) {
	var p Probe
	var configTemplate, metrics, graphs NullBytes
	var tsCreated, tsUpdated, tsDeleted NullTime
	err := r.Scan(
		&p.ID,
		&p.UUID,
		&p.Label,
		&p.DisplayName,
		&p.Description,
		&configTemplate,
		&metrics,
		&graphs,
		&tsCreated,
		&tsUpdated,
		&tsDeleted,
	)
	if err != nil {
		return nil, err
	}
	if configTemplate.Valid {
		if err = util.UnmarshalJSON(configTemplate.Bytes, &p.ConfigurationTemplate); err != nil {
			return nil, fmt.Errorf("couldn't unmarshal probe configuration_template: %s", err)
		}
	}
	if metrics.Valid {
		if err = util.UnmarshalJSON(metrics.Bytes, &p.Metrics); err != nil {
			return nil, fmt.Errorf("couldn't unmarshal probe metrics: %s", err)
		}
	}
	if graphs.Valid {
		if err = util.UnmarshalJSON(graphs.Bytes, &p.Graphs); err != nil {
			return nil, fmt.Errorf("couldn't unmarshal probe graphs: %s", err)
		}
	}
	if tsCreated.Valid {
		p.TsCreated = tsCreated.Time
	} else {
		p.TsCreated = time.Time{}
	}
	if tsUpdated.Valid {
		p.TsUpdated = tsUpdated.Time
	} else {
		p.TsUpdated = time.Time{}
	}
	if tsDeleted.Valid {
		p.TsDeleted = tsDeleted.Time
	} else {
		p.TsDeleted = time.Time{}
	}

	return &p, nil
}
