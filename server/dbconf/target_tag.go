// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package dbconf

import (
	"database/sql"
	"fmt"
	"strings"

	"github.com/google/uuid"
)

// ListTagsByTarget returns all Tags linked to a given target from the DB
func (db *SQLiteDB) ListTagsByTarget(organizationUUID, targetUUID uuid.UUID, pageSize int32, pageOffset uint64) ([]*Tag, error) {
	req := new(strings.Builder)
	fmt.Fprint(req, `
		SELECT DISTINCT tag.id, o.uuid, tag.slug, tag.display_name, tag.note, tag.color, tag.ts_created, tag.ts_updated, tag.ts_deleted
		FROM organization AS o, tag, target, target_tag
		WHERE tag.organization = o.id AND target.organization = o.id AND target.id = target_tag.target AND tag.id = target_tag.tag
			AND tag.ts_deleted IS NULL AND o.ts_deleted IS NULL
			AND o.uuid = ? AND target.uuid = ?
	`)

	if pageSize > 0 || pageOffset > 0 {
		// if pageSize is 0, returns all the results (starting from pageOffset)
		if pageSize == 0 {
			pageSize = -1
		}
		fmt.Fprintf(req, " LIMIT %d OFFSET %d", pageSize, pageOffset)
	}

	rows, err := db.client.Query(req.String(), organizationUUID, targetUUID)
	if err != nil {
		return nil, fmt.Errorf("error running the query: %s", err)
	}
	defer rows.Close()

	tags := make([]*Tag, 0)
	for rows.Next() {
		t, err := scanTag(rows)
		if err != nil {
			log.Errorf("unable to scan row: %s", err)
			continue
		}

		tags = append(tags, t)
	}
	err = rows.Err()
	if err != nil {
		if err == sql.ErrNoRows {
			return []*Tag{}, nil
		}
		log.Errorf("issue in the query: %s", err)
	}

	return tags, nil
}

// ListTargetsByTag returns all Targets linked to a given tag from the DB
func (db *SQLiteDB) ListTargetsByTag(organizationUUID uuid.UUID, tagSlug string, pageSize int32, pageOffset uint64) ([]*Target, error) {
	req := new(strings.Builder)
	fmt.Fprint(req, `
		SELECT DISTINCT target.id, target.uuid, o.uuid, target.address, target.note, target.ts_created, target.ts_updated, target.ts_deleted
		FROM organization AS o, target, tag, target_tag
		WHERE tag.organization = o.id AND target.organization = o.id AND target.id = target_tag.target AND tag.id = target_tag.tag
			AND target.ts_deleted IS NULL AND tag.ts_deleted IS NULL AND o.ts_deleted IS NULL
			AND o.uuid = ? AND tag.slug = ?
	`)

	if pageSize > 0 || pageOffset > 0 {
		// if pageSize is 0, returns all the results (starting from pageOffset)
		if pageSize == 0 {
			pageSize = -1
		}
		fmt.Fprintf(req, " LIMIT %d OFFSET %d", pageSize, pageOffset)
	}

	rows, err := db.client.Query(req.String(), organizationUUID, tagSlug)
	if err != nil {
		return nil, fmt.Errorf("error running the query: %s", err)
	}
	defer rows.Close()

	targets := make([]*Target, 0)
	for rows.Next() {
		t, err := scanTarget(rows)
		if err != nil {
			log.Errorf("unable to scan row: %s", err)
			continue
		}

		targets = append(targets, t)
	}
	err = rows.Err()
	if err != nil {
		if err == sql.ErrNoRows {
			return []*Target{}, nil
		}
		log.Errorf("issue in the query: %s", err)
	}

	return targets, nil
}

// LinkTagsToTarget connects a number of tags to a target in the DB
// Set force to true to also remove all pre-existing links to target
func (db *SQLiteDB) LinkTagsToTarget(force bool, organizationUUID, targetUUID uuid.UUID, tagSlugs ...string) error {
	tx, err := db.client.Begin()

	if err != nil {
		return fmt.Errorf("failed to begin transaction: %s", err)
	}
	// Defer rollback. If transaction is already done, sql.ErrTxDone is silently ignored.
	defer tx.Rollback()

	// Remove all pre-existing links
	if force {
		_, err = tx.Exec(`
			DELETE FROM target_tag WHERE 
				target = (SELECT target.id FROM target, organization AS o
					WHERE target.organization = o.id AND target.ts_deleted IS NULL AND o.ts_deleted IS NULL AND target.uuid = ? AND o.UUID = ?)
		`, targetUUID, organizationUUID)
		if err != nil {
			return fmt.Errorf("failed to delete pre-existing tags: %s", err)
		}
	}

	for _, tagName := range tagSlugs {
		res, err := tx.Exec(`
		INSERT INTO target_tag (target, tag) VALUES (
			(SELECT target.id FROM target, organization AS o
				WHERE target.organization = o.id AND target.ts_deleted IS NULL AND o.ts_deleted IS NULL AND target.uuid = ? AND o.UUID = ?),
			(SELECT tag.id FROM tag, organization AS o
				WHERE tag.organization = o.id AND tag.ts_deleted IS NULL AND o.ts_deleted IS NULL AND tag.slug = ? AND o.UUID = ?)
		)
	`, targetUUID, organizationUUID, tagName, organizationUUID)
		if err != nil {
			return fmt.Errorf("query error: %s", err)
		}
		if n, _ := res.RowsAffected(); n == 0 {
			return fmt.Errorf("failed to insert link")
		}
	}

	err = tx.Commit()
	if err != nil {
		return fmt.Errorf("failed to commit transaction: %s", err)
	}

	return nil
}

// UnlinkTagsFromTarget disconnects a tag from a target in the DB
func (db *SQLiteDB) UnlinkTagsFromTarget(organizationUUID, targetUUID uuid.UUID, tagSlugs ...string) error {
	tx, err := db.client.Begin()

	if err != nil {
		return fmt.Errorf("failed to begin transaction: %s", err)
	}
	// Defer rollback. If transaction is already done, sql.ErrTxDone is silently ignored.
	defer tx.Rollback()

	for _, tagName := range tagSlugs {
		res, err := tx.Exec(`
		DELETE FROM target_tag WHERE 
			target = (SELECT target.id FROM target, organization AS o WHERE target.uuid = ? AND o.UUID = ? AND target.organization = o.id)
			AND tag = (SELECT tag.id FROM tag, organization AS o WHERE tag.slug = ? AND o.UUID = ? AND tag.organization = o.id)
	`, targetUUID, organizationUUID, tagName, organizationUUID)
		if err != nil {
			return fmt.Errorf("query error: %s", err)
		}
		if n, _ := res.RowsAffected(); n == 0 {
			return fmt.Errorf("failed to delete link")
		}
	}

	err = tx.Commit()
	if err != nil {
		return fmt.Errorf("failed to commit transaction: %s", err)
	}

	return nil
}
