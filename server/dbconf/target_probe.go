// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package dbconf

import (
	"database/sql"
	"fmt"
	"strings"
	"time"

	"github.com/google/uuid"

	"gitlab.com/pantomath-io/panto/util"
)

// TargetProbe is the representation of the target_probe in the DB
type TargetProbe struct {
	ID                 int
	UUID               uuid.UUID
	TargetUUID         uuid.UUID
	ProbeUUID          uuid.UUID
	OrganizationUUID   uuid.UUID
	StateType          string
	StateConfiguration map[string]interface{}
	TsCreated          time.Time
	TsUpdated          time.Time
	TsDeleted          time.Time
}

// ListTargetProbes returns all the TargetProbe from the DBConf
func (db *SQLiteDB) ListTargetProbes(organizationUUID uuid.UUID, targetUUIDs, probeUUIDs []uuid.UUID, pageSize int32, pageOffset uint64) (targetProbes []*TargetProbe, err error) {
	req := new(strings.Builder)
	args := []interface{}{}

	fmt.Fprint(req, `
		SELECT tp.id, tp.uuid, t.uuid, p.uuid, o.uuid, tp.state_type, tp.state_configuration, tp.ts_created, tp.ts_updated, tp.ts_deleted, ref.configuration_template
		FROM target_probe AS tp LEFT JOIN state_type AS ref ON tp.state_type = ref.label,
			target AS t, probe AS p, organization AS o
		WHERE tp.target = t.id AND tp.probe = p.id AND t.organization = o.id AND tp.ts_deleted IS NULL AND o.ts_deleted IS NULL AND p.ts_deleted IS NULL
			AND o.uuid = ?
	`)
	args = append(args, organizationUUID)

	if len(targetUUIDs) > 0 {
		s := strings.Repeat("?,", len(targetUUIDs))
		fmt.Fprintf(req, " AND t.UUID IN (%s)", s[:len(s)-1])
		for _, u := range targetUUIDs {
			args = append(args, u)
		}
	}

	if len(probeUUIDs) > 0 {
		s := strings.Repeat("?,", len(probeUUIDs))
		fmt.Fprintf(req, " AND p.UUID IN (%s)", s[:len(s)-1])
		for _, u := range probeUUIDs {
			args = append(args, u)
		}
	}

	if pageSize > 0 || pageOffset > 0 {
		// if pageSize is 0, returns all the results (starting from pageOffset)
		if pageSize == 0 {
			pageSize = -1
		}
		fmt.Fprintf(req, " LIMIT %d OFFSET %d", pageSize, pageOffset)
	}

	rows, err := db.client.Query(req.String(), args...)
	if err != nil {
		return nil, fmt.Errorf("error running the query: %s", err)
	}
	defer rows.Close()

	targetProbes = make([]*TargetProbe, 0)
	for rows.Next() {
		tp, err := scanTargetProbe(rows)
		if err != nil {
			log.Errorf("unable to scan row: %s", err)
			continue
		}

		targetProbes = append(targetProbes, tp)
	}
	err = rows.Err()
	if err != nil {
		if err == sql.ErrNoRows {
			return []*TargetProbe{}, nil
		}
		log.Errorf("issue in the query: %s", err)
	}

	return
}

// GetTargetProbe returns a TargetProbes in the DB based on its UUID and its parent organization's UUID
func (db *SQLiteDB) GetTargetProbe(organizationUUID, targetprobeUUID uuid.UUID) (*TargetProbe, error) {
	o, err := scanTargetProbe(db.client.QueryRow(`
		SELECT tp.id, tp.uuid, t.uuid, p.uuid, o.uuid, tp.state_type, tp.state_configuration, tp.ts_created, tp.ts_updated, tp.ts_deleted, ref.configuration_template
		FROM target_probe AS tp LEFT JOIN state_type AS ref ON tp.state_type = ref.label,
			target AS t, probe AS p, organization AS o
		WHERE tp.target = t.id AND tp.probe = p.id AND t.organization = o.id AND tp.ts_deleted IS NULL
			AND o.uuid = ? AND tp.uuid = ?
	`, organizationUUID, targetprobeUUID))
	if err != nil {
		if err == sql.ErrNoRows {
			// Return nil, nil to indicate no error but no result (404 Not Found)
			return nil, nil
		}
		return nil, fmt.Errorf("failed to retrieve TargetProbe: %s", err)
	}

	return o, nil
}

// CreateTargetProbe creates a new TargetProbe in the DB
func (db *SQLiteDB) CreateTargetProbe(tp *TargetProbe) (*TargetProbe, error) {
	if tp.StateConfiguration == nil {
		return nil, fmt.Errorf("missing state configuration")
	}
	config, err := db.marshalTargetProbeStateConfiguration(tp)
	if err != nil {
		return nil, err
	}

	u := uuid.New()
	_, err = db.client.Exec(
		`INSERT INTO target_probe (uuid, target, probe, state_type, state_configuration)
		VALUES (?,
			(SELECT t.id FROM target AS t, organization as o WHERE t.organization = o.id AND t.ts_deleted IS NULL AND o.ts_deleted IS NULL AND t.uuid = ? AND o.uuid = ?),
			(SELECT p.id FROM probe AS p WHERE p.ts_deleted IS NULL AND p.uuid = ?),
			?, ?
		)`,
		u, tp.TargetUUID, tp.OrganizationUUID, tp.ProbeUUID, tp.StateType, config)
	if err != nil {
		return nil, fmt.Errorf("error running the query: %s", err)
	}

	return db.GetTargetProbe(tp.OrganizationUUID, u)
}

// UpdateTargetProbe updates an existing TargetProbe in the DB
func (db *SQLiteDB) UpdateTargetProbe(tp *TargetProbe) (*TargetProbe, error) {
	args := []interface{}{}
	req := new(strings.Builder)

	fmt.Fprint(req, "UPDATE target_probe SET ")

	// User-supplied fields to change
	if tp.TargetUUID != (uuid.UUID{}) {
		fmt.Fprint(req, "target = (SELECT t.id FROM target AS t, organization AS o WHERE t.organization = o.id AND t.ts_deleted IS NULL AND o.ts_deleted IS NULL AND t.uuid = ? AND o.uuid = ?), ")
		args = append(args, tp.TargetUUID, tp.OrganizationUUID)
	}

	if tp.ProbeUUID != (uuid.UUID{}) {
		fmt.Fprint(req, "probe = (SELECT id FROM probe WHERE ts_deleted IS NULL AND uuid = ?), ")
		args = append(args, tp.ProbeUUID)
	}

	if tp.StateType != "" {
		fmt.Fprint(req, "state_type=?, ")
		args = append(args, tp.StateType)
	}

	if tp.StateConfiguration != nil {
		config, err := db.marshalTargetProbeStateConfiguration(tp)
		if err != nil {
			return nil, err
		}
		fmt.Fprint(req, "state_configuration=?, ")
		args = append(args, config)
	}

	// Change update timestamp
	fmt.Fprintf(req, `ts_updated = (strftime('%%s', 'now')) `)

	// TargetProbe constraint
	fmt.Fprint(req, `WHERE id = (
		SELECT tp.id
		FROM target_probe AS tp, target AS t, organization AS o
		WHERE tp.target=t.id AND t.organization=o.id AND tp.ts_deleted IS NULL AND o.ts_deleted IS NULL AND t.ts_deleted IS NULL
			AND tp.uuid = ? AND o.uuid = ?
	)`)
	args = append(args, tp.UUID, tp.OrganizationUUID)

	// Soft delete constraint
	fmt.Fprint(req, ` AND ts_deleted IS NULL`)

	res, err := db.client.Exec(req.String(), args...)
	if err != nil {
		return nil, fmt.Errorf("error running the query: %s", err)
	}
	if rowsAffected, err := res.RowsAffected(); err == nil && rowsAffected < 1 {
		return nil, fmt.Errorf("failed to update target_probe")
	}

	return db.GetTargetProbe(tp.OrganizationUUID, tp.UUID)
}

// DeleteTargetProbe deletes a TargetProbe and all linked entities (TargetProbeAgent) from the DB
func (db *SQLiteDB) DeleteTargetProbe(organizationUUID, targetprobeUUID uuid.UUID) error {
	// Soft delete all the target_probe_agent entites
	_, err := db.client.Exec(`UPDATE target_probe_agent
		SET ts_deleted=(strftime('%s', 'now'))
		WHERE target_probe = (
			SELECT tp.id FROM target_probe AS tp, target AS t, organization AS o 
			WHERE tp.target = t.id AND t.organization = o.id AND tp.uuid = ? AND o.uuid = ?
		) AND ts_deleted IS NULL`,
		targetprobeUUID, organizationUUID)
	if err != nil {
		return fmt.Errorf("failed to delete target_probe_agent: %s", err)
	}

	_, err = db.client.Exec(`UPDATE target_probe 
		SET ts_deleted=(strftime('%s', 'now'))
		WHERE id = (
			SELECT tp.id FROM target_probe AS tp, target AS t, organization AS o 
			WHERE tp.target = t.id AND t.organization = o.id AND tp.uuid = ? AND o.uuid = ?
		) AND ts_deleted IS NULL`,
		targetprobeUUID, organizationUUID)
	if err != nil {
		return fmt.Errorf("failed to delete target_probe: %s", err)
	}

	return nil
}

func scanTargetProbe(r row) (*TargetProbe, error) {
	var tp TargetProbe
	var tsCreated, tsUpdated, tsDeleted NullTime
	var config, template NullBytes
	var stateType sql.NullString
	err := r.Scan(
		&tp.ID,
		&tp.UUID,
		&tp.TargetUUID,
		&tp.ProbeUUID,
		&tp.OrganizationUUID,
		&stateType,
		&config,
		&tsCreated,
		&tsUpdated,
		&tsDeleted,
		&template,
	)
	if err != nil {
		return nil, err
	}
	// If state_type is NULL, ignore everything about state
	if stateType.Valid {
		tp.StateType = stateType.String
		var def map[string]interface{}
		if template.Valid {
			if err = util.UnmarshalJSON(template.Bytes, &def); err != nil {
				return nil, fmt.Errorf("couldn't unmarshal StateRef details: %s", err)
			}
		}
		if config.Valid {
			if err = util.UnmarshalJSON(config.Bytes, &tp.StateConfiguration); err != nil {
				return nil, fmt.Errorf("couldn't unmarshal TargetProbe state configuration: %s", err)
			}
		} else {
			return nil, fmt.Errorf("TargetProbe state configuration is invalid")
		}
		// if template.Valid {
		// 	// TODO: `tp.Values` needs to be validated against `def`
		// 	// the validation should consist in field presence and type reflection
		// }
	}
	if tsCreated.Valid {
		tp.TsCreated = tsCreated.Time
	} else {
		tp.TsCreated = time.Time{}
	}
	if tsUpdated.Valid {
		tp.TsUpdated = tsUpdated.Time
	} else {
		tp.TsUpdated = time.Time{}
	}
	if tsDeleted.Valid {
		tp.TsDeleted = tsDeleted.Time
	} else {
		tp.TsDeleted = time.Time{}
	}
	return &tp, nil
}

func (db *SQLiteDB) marshalTargetProbeStateConfiguration(tp *TargetProbe) ([]byte, error) {
	if tp.StateType != "" {
		// get state values template from DB
		var b NullBytes
		err := db.client.QueryRow(`SELECT configuration_template FROM state_type WHERE label = ?`, tp.StateType).Scan(&b)
		if err != nil {
			return nil, err
		}
		var ref map[string]interface{}
		if b.Valid {
			if err = util.UnmarshalJSON(b.Bytes, &ref); err != nil {
				return nil, fmt.Errorf("couldn't unmarshal StateType configuration template: %s", err)
			}
		} else {
			return nil, fmt.Errorf("StateType configuration template is invalid")
		}
		// TODO: `tp.StateConfiguration` needs to be validated against `ref`
		// the validation should consist in field presence and type reflection
	} else if len(tp.StateConfiguration) > 0 {
		// a state values without a definition is invalid
		return nil, fmt.Errorf("missing state type")
	}

	config, err := util.MarshalJSON(tp.StateConfiguration)
	if err != nil {
		return nil, fmt.Errorf("couldn't marshal state configuration to JSON: %s", err)
	}

	return config, nil
}
