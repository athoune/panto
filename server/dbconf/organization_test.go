// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package dbconf

import (
	"reflect"
	"testing"
	"time"

	sqlmock "github.com/DATA-DOG/go-sqlmock"

	"github.com/google/uuid"
)

func TestListOrganizations(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	u := uuid.New()
	ts := time.Now().AddDate(-1, 0, 0)
	rows := sqlmock.NewRows([]string{"id", "uuid", "name", "ts_created", "ts_updated", "ts_deleted"}).
		AddRow(42, testOrganizationUUID.String(), "pantomath", ts, nil, nil).
		AddRow(43, u.String(), "pantomath-2", ts, nil, nil)

	org0 := Organization{
		ID: 42, UUID: testOrganizationUUID, Name: "pantomath",
		TsCreated: ts, TsUpdated: time.Time{}, TsDeleted: time.Time{},
	}
	org1 := Organization{
		ID: 43, UUID: u, Name: "pantomath-2",
		TsCreated: ts, TsUpdated: time.Time{}, TsDeleted: time.Time{},
	}

	mock.ExpectQuery(`SELECT id, uuid, name, ts_created, ts_updated, ts_deleted
		FROM organization
		WHERE ts_deleted IS NULL`).
		WillReturnRows(rows)

	orgs, err := mockDb.ListOrganizations(0, 0)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(orgs) != 2 {
		t.Fatalf("expected %d results, got %d", 2, len(orgs))
	}
	if !reflect.DeepEqual(orgs[0], &org0) {
		t.Fatalf("expected %v, got %v", org0, *orgs[0])
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}

	rows = sqlmock.NewRows([]string{"id", "uuid", "name", "ts_created", "ts_updated", "ts_deleted"}).
		AddRow(43, u.String(), "pantomath-2", ts, nil, nil)
	mock.ExpectQuery(`SELECT id, uuid, name, ts_created, ts_updated, ts_deleted
		FROM organization
		WHERE ts_deleted IS NULL LIMIT -1 OFFSET 1`).
		WillReturnRows(rows)
	orgs, err = mockDb.ListOrganizations(0, 1)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(orgs) != 1 {
		t.Fatalf("expected %d results, got %d", 1, len(orgs))
	}
	if !reflect.DeepEqual(orgs[0], &org1) {
		t.Fatalf("expected %v, got %v", org1, *orgs[0])
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}

	rows = sqlmock.NewRows([]string{"id", "uuid", "name", "ts_created", "ts_updated", "ts_deleted"}).
		AddRow(42, testOrganizationUUID.String(), "pantomath", ts, nil, nil)
	mock.ExpectQuery(`SELECT id, uuid, name, ts_created, ts_updated, ts_deleted
	FROM organization
	WHERE ts_deleted IS NULL LIMIT 1 OFFSET 0`).
		WillReturnRows(rows)
	orgs, err = mockDb.ListOrganizations(1, 0)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(orgs) != 1 {
		t.Fatalf("expected %d results, got %d", 1, len(orgs))
	}
	if !reflect.DeepEqual(orgs[0], &org0) {
		t.Fatalf("expected %v, got %v", org0, *orgs[0])
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}

	rows = sqlmock.NewRows([]string{"id", "uuid", "name", "ts_created", "ts_updated", "ts_deleted"}).
		AddRow(42, testOrganizationUUID.String(), "pantomath", ts, nil, nil)
	mock.ExpectQuery(`SELECT id, uuid, name, ts_created, ts_updated, ts_deleted
	FROM organization
	WHERE ts_deleted IS NULL LIMIT 1 OFFSET 1`).
		WillReturnRows(rows)
	orgs, err = mockDb.ListOrganizations(1, 1)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(orgs) != 1 {
		t.Fatalf("expected %d results, got %d", 1, len(orgs))
	}
	if !reflect.DeepEqual(orgs[0], &org0) {
		t.Fatalf("expected %v, got %v", org0, *orgs[0])
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}

func TestGetOrganization(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	ts := time.Now().AddDate(-1, 0, 0)
	rows := sqlmock.NewRows([]string{"id", "uuid", "name", "ts_created", "ts_updated", "ts_deleted"}).
		AddRow(42, testOrganizationUUID.String(), "pantomath", ts, nil, nil)
	expect := Organization{
		ID: 42, UUID: testOrganizationUUID, Name: "pantomath",
		TsCreated: ts, TsUpdated: time.Time{}, TsDeleted: time.Time{},
	}

	mock.ExpectQuery(`
		SELECT id, uuid, name, ts_created, ts_updated, ts_deleted
		FROM organization
		WHERE ts_deleted IS NULL AND uuid = \?
	`).WithArgs(testOrganizationUUID).
		WillReturnRows(rows)

	got, err := mockDb.GetOrganization(testOrganizationUUID)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if !reflect.DeepEqual(&expect, got) {
		t.Fatalf("expected %v, got %v", expect, *got)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}

	rows = sqlmock.NewRows([]string{"id", "uuid", "name", "ts_created", "ts_updated", "ts_deleted"})

	u := uuid.New()
	mock.ExpectQuery(`
		SELECT id, uuid, name, ts_created, ts_updated, ts_deleted
		FROM organization
		WHERE ts_deleted IS NULL AND uuid = \?
	`).WithArgs(u).
		WillReturnRows(rows)

	got, err = mockDb.GetOrganization(u)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if got != nil {
		t.Fatalf("expected nil, got %v", *got)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}

func TestCreateOrganization(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	org := Organization{
		Name: "pantomath",
	}

	uuidArg := StoreArg()
	now := time.Now()
	rows := sqlmock.NewRows([]string{"id", "uuid", "name", "ts_created", "ts_updated", "ts_deleted"}).
		AddRow(1, testOrganizationUUID.String(), "pantomath", now, nil, nil)
	mock.ExpectExec(`INSERT INTO organization \(uuid, name\) VALUES \(\?,\?\)`).
		WithArgs(uuidArg, org.Name).
		WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectQuery(`SELECT id, uuid, name, ts_created, ts_updated, ts_deleted
		FROM organization
		WHERE ts_deleted IS NULL AND uuid = \?`).
		WithArgs(uuidArg).
		WillReturnRows(rows)

	got, err := mockDb.CreateOrganization(&org)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if got.ID != 1 ||
		got.Name != "pantomath" ||
		got.TsCreated != now {
		t.Fatalf("unexpected result %v", *got)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}

func TestUpdateOrganization(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	org := Organization{
		ID:   42,
		UUID: testOrganizationUUID,
		Name: "pantomath",
	}

	tsCreated := time.Now().AddDate(-1, 0, 0)
	tsUpdated := time.Now()
	rows := sqlmock.NewRows([]string{"id", "uuid", "name", "ts_created", "ts_updated", "ts_deleted"}).
		AddRow(org.ID, org.UUID.String(), org.Name, tsCreated, tsUpdated, nil)

	mock.ExpectExec(`UPDATE organization SET name=\?, ts_updated=\(strftime\('%s', 'now'\)\) WHERE ts_deleted IS NULL AND uuid=\?`).
		WithArgs(org.Name, org.UUID).
		WillReturnResult(sqlmock.NewResult(1, 1))

	expect := Organization{
		ID: 42, UUID: testOrganizationUUID, Name: "pantomath",
		TsCreated: tsCreated, TsUpdated: tsUpdated, TsDeleted: time.Time{},
	}
	mock.ExpectQuery(`
			SELECT id, uuid, name, ts_created, ts_updated, ts_deleted
			FROM organization
			WHERE ts_deleted IS NULL AND uuid = \?
		`).WithArgs(testOrganizationUUID).
		WillReturnRows(rows)

	got, err := mockDb.UpdateOrganization(&org)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if !reflect.DeepEqual(&expect, got) {
		t.Fatalf("expected %v, got %v", expect, *got)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}
