// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package dbconf

import (
	"fmt"
	"path/filepath"

	"github.com/loderunner/goose"
	_ "github.com/mattn/go-sqlite3" // import SQLite just to reference driver

	_ "gitlab.com/pantomath-io/panto/data/migrations" // import Panto migrations
)

// requiredVersion is the expected schema version.
var requiredVersion int64

func init() {
	migrations, err := goose.CollectMigrations("", 0, goose.MaxVersion)
	if err != nil {
		panic(err.Error())
	}
	if len(migrations) == 0 {
		panic("no migrations found")
	}

	requiredVersion = migrations[len(migrations)-1].Version
}

// Migration is the representation of a migration script
type Migration struct {
	Version int64
	Name    string
}

// GetVersion returns the migration version of a DB
func GetVersion(db ConfigurationDB) (version int64, err error) {
	if err = goose.SetDialect(db.getDialect()); err != nil {
		return
	}

	version, err = goose.GetDBVersion(db.getClient())
	if err != nil {
		return
	}

	return
}

// RequiredVersion returns the version of the DB expected by Panto
func RequiredVersion() (version int64) {
	return requiredVersion
}

// IsVersion checks if the DB Conf version matches the parameter
func IsVersion(db ConfigurationDB, version int64) (b bool, err error) {
	dbVersion, err := GetVersion(db)
	if err != nil {
		return
	}

	b = (dbVersion == version)
	return
}

// Migrate applies all the available migrations on the DB
func Migrate(db ConfigurationDB) (err error) {
	if err = goose.SetDialect(db.getDialect()); err != nil {
		return
	}

	err = goose.Up(db.getClient(), "")
	if err != nil {
		return err
	}
	return nil
}

// MigrateTo applies all the available migrations up to a specific version on the DB
func MigrateTo(db ConfigurationDB, version int64) (err error) {
	if err = goose.SetDialect(db.getDialect()); err != nil {
		return
	}

	err = goose.UpTo(db.getClient(), "", version)
	if err != nil {
		return err
	}
	return nil
}

// Downgrade reverts one migrations on the DB
func Downgrade(db ConfigurationDB) (err error) {
	if err = goose.SetDialect(db.getDialect()); err != nil {
		return
	}

	err = goose.Down(db.getClient(), "")
	if err != nil {
		return err
	}
	return nil
}

// ListPendingMigrations returns the list of all migrations available between 2 version
func ListPendingMigrations(from, to int64) (list []Migration, err error) {
	if to == 0 {
		to = -1
	}
	migrations, err := goose.CollectMigrations("", from, to)
	if err != nil {
		return
	}

	list = make([]Migration, len(migrations))
	for i, migration := range migrations {
		list[i] = Migration{
			Version: migration.Version,
			Name:    filepath.Base(migration.Source),
		}
	}

	return
}

// ListLastMigration returns the last migrations for a given version
func ListLastMigration(to int64) (list []Migration, err error) {
	if to == 0 {
		to = -1
	}
	migrations, err := goose.CollectMigrations("", 0, to)
	if err != nil {
		return
	}

	if len(migrations) < 1 {
		return nil, fmt.Errorf("there are no migration available")
	}

	migration := migrations[len(migrations)-1]

	list = make([]Migration, 1)
	list[0] = Migration{
		Version: migration.Version,
		Name:    filepath.Base(migration.Source),
	}

	return
}
