// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package dbconf

import (
	"reflect"
	"testing"
	"time"

	sqlmock "github.com/DATA-DOG/go-sqlmock"

	"github.com/google/uuid"
)

func TestListTargets(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	now := time.Now()
	t0 := Target{
		ID:               1,
		UUID:             testTargetUUID,
		OrganizationUUID: testOrganizationUUID,
		Address:          "target.pantomath.io",
		Note:             "Note about the target",
		TsCreated:        now,
	}
	t1 := Target{
		ID:               2,
		UUID:             testTargetUUID,
		OrganizationUUID: testOrganizationUUID,
		Address:          "target-1.pantomath.io",
		Note:             "Note about the target",
		TsCreated:        now.Add(time.Hour),
	}

	rows := sqlmock.NewRows([]string{
		"t.id", "t.uuid", "o.uuid", "t.address", "t.note", "t.ts_created", "t.ts_updated", "t.ts_deleted",
	}).
		AddRow(t0.ID, t0.UUID.String(), t0.OrganizationUUID.String(), t0.Address, t0.Note, t0.TsCreated, nil, nil).
		AddRow(t1.ID, t1.UUID.String(), t1.OrganizationUUID.String(), t1.Address, t1.Note, t1.TsCreated, nil, nil)
	mock.ExpectQuery(`SELECT t\.id, t\.uuid, o\.uuid, t\.address, t\.note, t\.ts_created, t\.ts_updated, t\.ts_deleted
		FROM target AS t, organization AS o
		WHERE t\.organization = o\.id AND t\.ts_deleted IS NULL AND o\.ts_deleted IS NULL AND o\.uuid = \?`).
		WithArgs(testOrganizationUUID).
		WillReturnRows(rows)
	targets, err := mockDb.ListTargets(testOrganizationUUID, 0, 0)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(targets) != 2 {
		t.Fatalf("expected %d results, got %d", 2, len(targets))
	}
	if !reflect.DeepEqual(targets[0], &t0) {
		t.Fatalf("expected %v, got %v", t0, *targets[0])
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}

	rows = sqlmock.NewRows([]string{
		"t.id", "t.uuid", "o.uuid", "t.address", "t.note", "t.ts_created", "t.ts_updated", "t.ts_deleted",
	}).AddRow(t1.ID, t1.UUID.String(), t1.OrganizationUUID.String(), t1.Address, t1.Note, t1.TsCreated, nil, nil)
	mock.ExpectQuery(`SELECT t\.id, t\.uuid, o\.uuid, t\.address, t\.note, t\.ts_created, t\.ts_updated, t\.ts_deleted
		FROM target AS t, organization AS o
		WHERE t\.organization = o\.id AND t\.ts_deleted IS NULL AND o\.ts_deleted IS NULL AND o\.uuid = \? LIMIT -1 OFFSET 1`).
		WithArgs(testOrganizationUUID).
		WillReturnRows(rows)
	targets, err = mockDb.ListTargets(testOrganizationUUID, 0, 1)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(targets) != 1 {
		t.Fatalf("expected %d results, got %d", 1, len(targets))
	}
	if !reflect.DeepEqual(targets[0], &t1) {
		t.Fatalf("expected %v, got %v", t1, *targets[0])
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}

	rows = sqlmock.NewRows([]string{
		"t.id", "t.uuid", "o.uuid", "t.address", "t.note", "t.ts_created", "t.ts_updated", "t.ts_deleted",
	}).AddRow(t0.ID, t0.UUID.String(), t0.OrganizationUUID.String(), t0.Address, t0.Note, t0.TsCreated, nil, nil)
	mock.ExpectQuery(`SELECT t\.id, t\.uuid, o\.uuid, t\.address, t\.note, t\.ts_created, t\.ts_updated, t\.ts_deleted
		FROM target AS t, organization AS o
		WHERE t\.organization = o\.id AND t\.ts_deleted IS NULL AND o\.ts_deleted IS NULL AND o\.uuid = \? LIMIT 1 OFFSET 0`).
		WithArgs(testOrganizationUUID).
		WillReturnRows(rows)
	targets, err = mockDb.ListTargets(testOrganizationUUID, 1, 0)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(targets) != 1 {
		t.Fatalf("expected %d results, got %d", 1, len(targets))
	}
	if !reflect.DeepEqual(targets[0], &t0) {
		t.Fatalf("expected %v, got %v", t0, *targets[0])
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}

	rows = sqlmock.NewRows([]string{
		"t.id", "t.uuid", "o.uuid", "t.address", "t.note", "t.ts_created", "t.ts_updated", "t.ts_deleted",
	}).AddRow(t0.ID, t0.UUID.String(), t0.OrganizationUUID.String(), t0.Address, t0.Note, t0.TsCreated, nil, nil)
	mock.ExpectQuery(`SELECT t\.id, t\.uuid, o\.uuid, t\.address, t\.note, t\.ts_created, t\.ts_updated, t\.ts_deleted
		FROM target AS t, organization AS o
		WHERE t\.organization = o\.id AND t\.ts_deleted IS NULL AND o\.ts_deleted IS NULL AND o\.uuid = \? LIMIT 1 OFFSET 1`).
		WithArgs(testOrganizationUUID).
		WillReturnRows(rows)
	targets, err = mockDb.ListTargets(testOrganizationUUID, 1, 1)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(targets) != 1 {
		t.Fatalf("expected %d results, got %d", 1, len(targets))
	}
	if !reflect.DeepEqual(targets[0], &t0) {
		t.Fatalf("expected %v, got %v", t0, *targets[0])
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}

func TestGetTarget(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	now := time.Now()
	expect := Target{
		ID:               1,
		UUID:             testTargetUUID,
		OrganizationUUID: testOrganizationUUID,
		Address:          "target.pantomath.io",
		Note:             "Note about the target",
		TsCreated:        now,
	}
	rows := sqlmock.NewRows([]string{
		"t.id", "t.uuid", "o.uuid", "t.address", "t.note", "t.ts_created", "t.ts_updated", "t.ts_deleted",
	}).AddRow(expect.ID, expect.UUID.String(), expect.OrganizationUUID.String(), expect.Address, expect.Note, expect.TsCreated, nil, nil)
	mock.ExpectQuery(`SELECT t\.id, t\.uuid, o\.uuid, t\.address, t\.note, t\.ts_created, t\.ts_updated, t\.ts_deleted
		FROM target AS t, organization AS o
		WHERE t\.organization = o\.id AND t\.ts_deleted IS NULL AND o\.uuid = \? AND t\.uuid = \?`).
		WithArgs(testOrganizationUUID, testTargetUUID).
		WillReturnRows(rows)

	got, err := mockDb.GetTarget(testOrganizationUUID, testTargetUUID)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if !reflect.DeepEqual(&expect, got) {
		t.Fatalf("expected %v, got %v", expect, *got)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}

	rows = sqlmock.NewRows([]string{
		"t.id", "t.uuid", "o.uuid", "t.address", "t.note", "t.ts_created", "t.ts_updated", "t.ts_deleted",
	})

	u0 := uuid.New()
	u1 := uuid.New()
	mock.ExpectQuery(`SELECT t\.id, t\.uuid, o\.uuid, t\.address, t\.note, t\.ts_created, t\.ts_updated, t\.ts_deleted
		FROM target AS t, organization AS o
		WHERE t\.organization = o\.id AND t\.ts_deleted IS NULL AND o\.uuid = \? AND t\.uuid = \?`).
		WithArgs(u0, u1).
		WillReturnRows(rows)

	got, err = mockDb.GetTarget(u0, u1)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if got != nil {
		t.Fatalf("expected nil, got %v", *got)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}

func TestCreateTarget(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	target := Target{
		OrganizationUUID: testOrganizationUUID,
		Address:          "target.pantomath.io",
		Note:             "Note about the target",
	}

	uuidArg := StoreArg()
	mock.ExpectExec(`INSERT INTO target \(uuid, address, note, organization\)
	VALUES \(\?,\?,\?,
		\(SELECT id FROM organization WHERE ts_deleted IS NULL AND uuid = \?\)
	\)`).
		WithArgs(uuidArg, target.Address, target.Note, target.OrganizationUUID).
		WillReturnResult(sqlmock.NewResult(1, 1))

	now := time.Now()
	rows := sqlmock.NewRows([]string{
		"t.id", "t.uuid", "o.uuid", "t.address", "t.note", "t.ts_created", "t.ts_updated", "t.ts_deleted",
	}).
		AddRow(1, testTargetUUID.String(), testOrganizationUUID.String(), target.Address, target.Note, now, nil, nil)
	mock.ExpectQuery(`SELECT t\.id, t\.uuid, o\.uuid, t\.address, t\.note, t\.ts_created, t\.ts_updated, t\.ts_deleted
		FROM target AS t, organization AS o
		WHERE t\.organization = o\.id AND t\.ts_deleted IS NULL AND o\.uuid = \? AND t\.uuid = \?`).
		WithArgs(testOrganizationUUID, uuidArg).
		WillReturnRows(rows)

	got, err := mockDb.CreateTarget(&target)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if got.ID != 1 ||
		got.UUID != testTargetUUID ||
		got.Address != target.Address ||
		got.Note != target.Note ||
		got.OrganizationUUID != testOrganizationUUID ||
		got.TsCreated != now {
		t.Fatalf("unexpected result %v", *got)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}

func TestUpdateTarget(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	target := Target{
		UUID:             testTargetUUID,
		OrganizationUUID: testOrganizationUUID,
		Address:          "target.pantomath.io",
		Note:             "Note about the target",
	}

	mock.ExpectExec(`UPDATE target SET address=\?, note=\?, ts_updated = \(strftime\('%s', 'now'\)\)
		WHERE uuid=\? AND organization = \(SELECT o\.id FROM organization AS o, target AS t WHERE t\.organization = o\.id AND o\.ts_deleted IS NULL AND o\.uuid=\?\)`).
		WithArgs(target.Address, target.Note, target.UUID, target.OrganizationUUID).
		WillReturnResult(sqlmock.NewResult(1, 1))

	uuidArg := StoreArg()
	now := time.Now()
	rows := sqlmock.NewRows([]string{
		"t.id", "t.uuid", "o.uuid", "t.address", "t.note", "t.ts_created", "t.ts_updated", "t.ts_deleted",
	}).AddRow(1, testTargetUUID.String(), testOrganizationUUID.String(), target.Address, target.Note, now, nil, nil)
	mock.ExpectQuery(`SELECT t\.id, t\.uuid, o\.uuid, t\.address, t\.note, t\.ts_created, t\.ts_updated, t\.ts_deleted
		FROM target AS t, organization AS o
		WHERE t\.organization = o\.id AND t\.ts_deleted IS NULL AND o\.uuid = \? AND t\.uuid = \?`).
		WithArgs(testOrganizationUUID, uuidArg).
		WillReturnRows(rows)

	got, err := mockDb.UpdateTarget(&target)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	u, ok := uuidArg.Value().(string)
	if !ok ||
		u != testTargetUUID.String() ||
		got.ID != 1 ||
		got.UUID != testTargetUUID ||
		got.Address != target.Address ||
		got.Note != target.Note ||
		got.OrganizationUUID != testOrganizationUUID ||
		got.TsCreated != now {
		t.Fatalf("unexpected result %v", *got)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}

func TestDeleteTarget(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	mock.ExpectBegin()
	mock.ExpectExec(`UPDATE target_probe_agent
		SET ts_deleted=\(strftime\('%s', 'now'\)\) 
		WHERE target_probe =
			\(SELECT tp.id FROM target_probe AS tp, target AS t, organization AS o WHERE t\.organization=o\.id AND tp\.target=t\.id AND t\.uuid=\? AND o\.uuid=\?\)`).
		WithArgs(testTargetUUID, testOrganizationUUID).
		WillReturnResult(sqlmock.NewResult(1, 5))
	mock.ExpectExec(`UPDATE target_probe
			SET ts_deleted=\(strftime\('%s', 'now'\)\) 
			WHERE target =
				\(SELECT t\.id FROM target AS t, organization AS o WHERE t\.organization=o\.id AND t\.uuid=\? AND o\.uuid=\?\)`).
		WithArgs(testTargetUUID, testOrganizationUUID).
		WillReturnResult(sqlmock.NewResult(1, 2))
	mock.ExpectExec(`UPDATE target
		SET ts_deleted=\(strftime\('%s', 'now'\)\) 
		WHERE id =
			\(SELECT t\.id FROM target AS t, organization AS o WHERE t\.organization=o\.id AND t\.uuid=\? AND o\.uuid=\?\)`).
		WithArgs(testTargetUUID, testOrganizationUUID).
		WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectCommit()
	err = mockDb.DeleteTarget(testOrganizationUUID, testTargetUUID)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}
