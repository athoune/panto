// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package dbconf

import (
	"reflect"
	"testing"
	"time"

	sqlmock "github.com/DATA-DOG/go-sqlmock"

	"github.com/google/uuid"
)

const (
	emailTemplate = `[{"name": "to", "type":"string", "mandatory": true}]`
	slackTemplate = `[{"name": "channel", "type":"string", "mandatory": true}]`
)

func TestListChannels(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	now := time.Now()
	c0 := Channel{
		ID:               1,
		UUID:             testChannelUUID,
		OrganizationUUID: testOrganizationUUID,
		DisplayName:      "channel1",
		Type:             "email",
		Configuration:    map[string]interface{}{"to": "barbes@rochechouart.paris"},
		TsCreated:        now,
	}
	c1 := Channel{
		ID:               2,
		UUID:             uuid.New(),
		OrganizationUUID: testOrganizationUUID,
		DisplayName:      "channel2",
		Type:             "slack",
		Configuration:    map[string]interface{}{"channel": "#alert"},
		TsCreated:        now.Add(time.Hour),
	}

	rows := sqlmock.NewRows([]string{
		"c.id", "c.uuid", "o.uuid", "c.display_name", "c.type", "c.ts_created", "c.ts_updated", "c.ts_deleted", "c.configuration", "ct.configuration_template",
	}).
		AddRow(c0.ID, c0.UUID.String(), c0.OrganizationUUID.String(), c0.DisplayName, c0.Type, c0.TsCreated, nil, nil, []byte(`{"to":"barbes@rochechouart.paris"}`), []byte(emailTemplate)).
		AddRow(c1.ID, c1.UUID.String(), c1.OrganizationUUID.String(), c1.DisplayName, c1.Type, c1.TsCreated, nil, nil, []byte(`{"channel": "#alert"}`), []byte(slackTemplate))
	mock.ExpectQuery(`SELECT c\.id, c\.uuid, o\.uuid, c\.display_name, c\.type, c\.ts_created, c\.ts_updated, c\.ts_deleted,
		c\.configuration, ct\.configuration_template
		FROM channel AS c, organization AS o, channel_type AS ct
		WHERE c\.organization = o\.id AND c\.type = ct\.label AND c\.ts_deleted IS NULL AND o\.ts_deleted IS NULL AND o\.uuid = \?`).
		WithArgs(testOrganizationUUID).
		WillReturnRows(rows)
	channels, err := mockDb.ListChannels(testOrganizationUUID, 0, 0)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(channels) != 2 {
		t.Fatalf("expected %d results, got %d", 2, len(channels))
	}
	if !reflect.DeepEqual(channels[0], &c0) {
		t.Fatalf("expected %v, got %v", c0, *channels[0])
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}

	rows = sqlmock.NewRows([]string{
		"c.id", "c.uuid", "o.uuid", "c.display_name", "c.type", "c.ts_created", "c.ts_updated", "c.ts_deleted", "c.configuration", "ct.configuration_template",
	}).AddRow(c1.ID, c1.UUID.String(), c1.OrganizationUUID.String(), c1.DisplayName, c1.Type, c1.TsCreated, nil, nil, []byte(`{"channel": "#alert"}`), []byte(slackTemplate))
	mock.ExpectQuery(`SELECT c\.id, c\.uuid, o\.uuid, c\.display_name, c\.type, c\.ts_created, c\.ts_updated, c\.ts_deleted,
		c\.configuration, ct\.configuration_template
		FROM channel AS c, organization AS o, channel_type AS ct
		WHERE c\.organization = o\.id AND c\.type = ct\.label AND c\.ts_deleted IS NULL AND o\.ts_deleted IS NULL AND o\.uuid = \? LIMIT -1 OFFSET 1`).
		WithArgs(testOrganizationUUID).
		WillReturnRows(rows)
	channels, err = mockDb.ListChannels(testOrganizationUUID, 0, 1)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(channels) != 1 {
		t.Fatalf("expected %d results, got %d", 1, len(channels))
	}
	if !reflect.DeepEqual(channels[0], &c1) {
		t.Fatalf("expected %v, got %v", c1, *channels[0])
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}

	rows = sqlmock.NewRows([]string{
		"c.id", "c.uuid", "o.uuid", "c.display_name", "c.type", "c.ts_created", "c.ts_updated", "c.ts_deleted", "c.configuration", "ct.configuration_template",
	}).AddRow(c0.ID, c0.UUID.String(), c0.OrganizationUUID.String(), c0.DisplayName, c0.Type, c0.TsCreated, nil, nil, []byte(`{"to":"barbes@rochechouart.paris"}`), []byte(emailTemplate))
	mock.ExpectQuery(`SELECT c\.id, c\.uuid, o\.uuid, c\.display_name, c\.type, c\.ts_created, c\.ts_updated, c\.ts_deleted,
		c\.configuration, ct\.configuration_template
		FROM channel AS c, organization AS o, channel_type AS ct
		WHERE c\.organization = o\.id AND c\.type = ct\.label AND c\.ts_deleted IS NULL AND o\.ts_deleted IS NULL AND o\.uuid = \? LIMIT 1 OFFSET 0`).
		WithArgs(testOrganizationUUID).
		WillReturnRows(rows)
	channels, err = mockDb.ListChannels(testOrganizationUUID, 1, 0)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(channels) != 1 {
		t.Fatalf("expected %d results, got %d", 1, len(channels))
	}
	if !reflect.DeepEqual(channels[0], &c0) {
		t.Fatalf("expected %v, got %v", c0, *channels[0])
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}

	rows = sqlmock.NewRows([]string{
		"c.id", "c.uuid", "o.uuid", "c.display_name", "c.type", "c.ts_created", "c.ts_updated", "c.ts_deleted", "c.configuration", "ct.configuration_template",
	}).AddRow(c0.ID, c0.UUID.String(), c0.OrganizationUUID.String(), c0.DisplayName, c0.Type, c0.TsCreated, nil, nil, []byte(`{"to":"barbes@rochechouart.paris"}`), []byte(emailTemplate))
	mock.ExpectQuery(`SELECT c\.id, c\.uuid, o\.uuid, c\.display_name, c\.type, c\.ts_created, c\.ts_updated, c\.ts_deleted,
		c\.configuration, ct\.configuration_template
		FROM channel AS c, organization AS o, channel_type AS ct
		WHERE c\.organization = o\.id AND c\.type = ct\.label AND c\.ts_deleted IS NULL AND o\.ts_deleted IS NULL AND o\.uuid = \? LIMIT 1 OFFSET 1`).
		WithArgs(testOrganizationUUID).
		WillReturnRows(rows)
	channels, err = mockDb.ListChannels(testOrganizationUUID, 1, 1)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(channels) != 1 {
		t.Fatalf("expected %d results, got %d", 1, len(channels))
	}
	if !reflect.DeepEqual(channels[0], &c0) {
		t.Fatalf("expected %v, got %v", c0, *channels[0])
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}

func TestGetChannel(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	now := time.Now()
	expect := Channel{
		ID:               1,
		UUID:             testChannelUUID,
		OrganizationUUID: testOrganizationUUID,
		DisplayName:      "channel name",
		Type:             "email",
		Configuration:    map[string]interface{}{"to": "barbes@rochechouart.paris"},
		TsCreated:        now,
	}
	rows := sqlmock.NewRows([]string{
		"c.id", "c.uuid", "o.uuid", "c.display_name", "c.type", "c.ts_created", "c.ts_updated", "c.ts_deleted", "c.configuration", "ct.configuration_template",
	}).AddRow(expect.ID, expect.UUID.String(), expect.OrganizationUUID.String(), expect.DisplayName, expect.Type, expect.TsCreated, nil, nil, []byte(`{"to":"barbes@rochechouart.paris"}`), []byte(emailTemplate))
	mock.ExpectQuery(`SELECT c\.id, c\.uuid, o\.uuid, c\.display_name, c\.type, c\.ts_created, c\.ts_updated, c\.ts_deleted,
		c\.configuration, ct\.configuration_template
		FROM channel AS c, organization AS o, channel_type AS ct
		WHERE c\.organization = o\.id AND c\.type = ct\.label AND c\.ts_deleted IS NULL AND o\.uuid = \? AND c\.uuid = \?`).
		WithArgs(testOrganizationUUID, testChannelUUID).
		WillReturnRows(rows)

	got, err := mockDb.GetChannel(testOrganizationUUID, testChannelUUID)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if !reflect.DeepEqual(&expect, got) {
		t.Fatalf("expected %v, got %v", expect, *got)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}

	u0 := uuid.New()
	u1 := uuid.New()
	rows = sqlmock.NewRows([]string{
		"c.id", "c.uuid", "o.uuid", "c.display_name", "c.type", "c.ts_created", "c.ts_updated", "c.ts_deleted", "c.configuration", "ct.configuration_template",
	})
	mock.ExpectQuery(`SELECT c\.id, c\.uuid, o\.uuid, c\.display_name, c\.type, c\.ts_created, c\.ts_updated, c\.ts_deleted,
		c\.configuration, ct\.configuration_template
		FROM channel AS c, organization AS o, channel_type AS ct
		WHERE c\.organization = o\.id AND c\.type = ct\.label AND c\.ts_deleted IS NULL AND o\.uuid = \? AND c\.uuid = \?`).
		WithArgs(u0.String(), u1.String()).
		WillReturnRows(rows)

	got, err = mockDb.GetChannel(u0, u1)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if got != nil {
		t.Fatalf("expected nil, got %v", *got)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}

func TestCreateChannel(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	channel := Channel{
		OrganizationUUID: testOrganizationUUID,
		DisplayName:      "channel name",
		Type:             "email",
		Configuration:    map[string]interface{}{"to": "barbes@rochechouart.paris"},
	}

	rows := sqlmock.NewRows([]string{"label", "configuration_template"}).AddRow("email", []byte(emailTemplate))
	mock.ExpectQuery(`SELECT label, configuration_template
		FROM channel_type
		WHERE label = \?`).WithArgs("email").WillReturnRows(rows)

	uuidArg := StoreArg()
	mock.ExpectExec(`INSERT INTO channel \(uuid, display_name, type, configuration, organization\)
	VALUES \(\?,\?,\?,\?,
		\(SELECT id FROM organization WHERE ts_deleted IS NULL AND uuid = \?\)
	\)`).
		WithArgs(uuidArg, "channel name", channel.Type, []byte(`{"to":"barbes@rochechouart.paris"}`), channel.OrganizationUUID).
		WillReturnResult(sqlmock.NewResult(1, 1))

	now := time.Now()
	rows = sqlmock.NewRows([]string{
		"c.id", "c.uuid", "o.uuid", "c.display_name", "c.type", "c.ts_created", "c.ts_updated", "c.ts_deleted", "c.configuration", "ct.configuration_template",
	}).AddRow(1, testChannelUUID.String(), testOrganizationUUID.String(), "channel name", "email", now, nil, nil, []byte(`{"to":"barbes@rochechouart.paris"}`), []byte(emailTemplate))
	mock.ExpectQuery(`SELECT c\.id, c\.uuid, o\.uuid, c\.display_name, c\.type, c\.ts_created, c\.ts_updated, c\.ts_deleted,
		c\.configuration, ct\.configuration_template
		FROM channel AS c, organization AS o, channel_type AS ct
		WHERE c\.organization = o\.id AND c\.type = ct\.label AND c\.ts_deleted IS NULL AND o\.uuid = \? AND c\.uuid = \?`).
		WithArgs(testOrganizationUUID, uuidArg).
		WillReturnRows(rows)

	got, err := mockDb.CreateChannel(&channel)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if got.ID != 1 ||
		got.UUID != testChannelUUID ||
		got.OrganizationUUID != testOrganizationUUID ||
		got.Type != "email" ||
		!reflect.DeepEqual(channel.Configuration, got.Configuration) ||
		got.TsCreated != now {
		t.Fatalf("unexpected result %v", *got)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}

func TestUpdateChannel(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	channel := Channel{
		UUID:             testChannelUUID,
		OrganizationUUID: testOrganizationUUID,
		DisplayName:      "channel name",
		Type:             "slack",
		Configuration:    map[string]interface{}{"channel": "#barbes-rochechouart"},
	}

	rows := sqlmock.NewRows([]string{"label", "configuration_template"}).AddRow("slack", []byte(slackTemplate))
	mock.ExpectQuery(`SELECT label, configuration_template
		FROM channel_type
		WHERE label = \?`).WithArgs("slack").WillReturnRows(rows)

	mock.ExpectExec(`UPDATE channel SET type=\?, configuration=\?, display_name=\?, ts_updated=\(strftime\('%s', 'now'\)\)
		WHERE uuid = \? AND organization = \(SELECT id FROM organization WHERE ts_deleted IS NULL AND uuid=\?\)`).
		WithArgs(channel.Type, []byte(`{"channel":"#barbes-rochechouart"}`), "channel name", testChannelUUID, testOrganizationUUID).
		WillReturnResult(sqlmock.NewResult(1, 1))

	uuidArg := StoreArg()
	now := time.Now()
	rows = sqlmock.NewRows([]string{
		"c.id", "c.uuid", "o.uuid", "c.display_name", "c.type", "c.ts_created", "c.ts_updated", "c.ts_deleted", "c.configuration", "ct.configuration_template",
	}).AddRow(1, testChannelUUID.String(), testOrganizationUUID.String(), "channel name", "slack", now, nil, nil, []byte(`{"channel":"#barbes-rochechouart"}`), []byte(slackTemplate))
	mock.ExpectQuery(`SELECT c\.id, c\.uuid, o\.uuid, c\.display_name, c\.type, c\.ts_created, c\.ts_updated, c\.ts_deleted,
		c\.configuration, ct\.configuration_template
		FROM channel AS c, organization AS o, channel_type AS ct
		WHERE c\.organization = o\.id AND c\.type = ct\.label AND c\.ts_deleted IS NULL AND o\.uuid = \? AND c\.uuid = \?`).
		WithArgs(testOrganizationUUID, uuidArg).
		WillReturnRows(rows)

	got, err := mockDb.UpdateChannel(&channel)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	u, ok := uuidArg.Value().(string)
	if !ok ||
		u != testChannelUUID.String() ||
		got.ID != 1 ||
		got.UUID != testChannelUUID ||
		got.OrganizationUUID != testOrganizationUUID ||
		got.Type != "slack" ||
		!reflect.DeepEqual(channel.Configuration, got.Configuration) ||
		got.TsCreated != now {
		t.Fatalf("unexpected result %v", *got)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}

func TestDeleteChannel(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	mock.ExpectExec(`UPDATE channel 
		SET ts_deleted=\(strftime\('%s', 'now'\)\)
		WHERE id = 
			\(SELECT c\.id FROM channel AS c, organization AS o WHERE c\.organization=o\.id AND c\.uuid=\? AND o\.uuid=\?\)`).
		WithArgs(testChannelUUID, testOrganizationUUID).
		WillReturnResult(sqlmock.NewResult(1, 1))

	err = mockDb.DeleteChannel(testOrganizationUUID, testChannelUUID)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}
