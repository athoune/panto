// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package dbconf

import (
	"encoding/json"
	"reflect"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"gitlab.com/pantomath-io/panto/util"
)

func TestListChannelTypes(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	ct0 := ChannelType{
		Label: "email",
		ConfigurationTemplate: []util.Parameter{{
			Name:        "dest",
			Type:        "string",
			Description: "foo@bar.com",
			Mandatory:   true,
			Placeholder: "",
			Default:     "",
		}},
	}
	config0, err := json.Marshal(ct0.ConfigurationTemplate)
	if err != nil {
		panic(err)
	}
	ct1 := ChannelType{
		Label: "slack",
		ConfigurationTemplate: []util.Parameter{{
			Name:        "dest",
			Type:        "string",
			Description: "foobar",
			Mandatory:   false,
			Placeholder: "",
			Default:     "",
		}},
	}
	config1, err := json.Marshal(ct1.ConfigurationTemplate)
	if err != nil {
		panic(err)
	}

	rows := sqlmock.NewRows([]string{"label", "configuration_template"}).
		AddRow(
			"email",
			config0,
		).
		AddRow(
			"slack",
			config1,
		)

	mock.ExpectQuery(`SELECT label, configuration_template FROM channel_type`).
		WillReturnRows(rows)

	channelTypes, err := mockDb.ListChannelTypes(0, 0)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(channelTypes) != 2 {
		t.Fatalf("expected %d results, got %d", 2, len(channelTypes))
	}
	if !reflect.DeepEqual(channelTypes[0], &ct0) {
		t.Fatalf("expected %v, got %v", ct0, *channelTypes[0])
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}

	rows = sqlmock.NewRows([]string{"label", "configuration_template"}).
		AddRow(
			"slack",
			config1,
		)
	mock.ExpectQuery(`SELECT label, configuration_template FROM channel_type LIMIT -1 OFFSET 1`).
		WillReturnRows(rows)
	channelTypes, err = mockDb.ListChannelTypes(0, 1)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(channelTypes) != 1 {
		t.Fatalf("expected %d results, got %d", 1, len(channelTypes))
	}
	if !reflect.DeepEqual(channelTypes[0], &ct1) {
		t.Fatalf("expected %v, got %v", ct1, *channelTypes[0])
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}

	rows = sqlmock.NewRows([]string{"label", "configuration_template"}).
		AddRow(
			"email",
			config0,
		)
	mock.ExpectQuery(`SELECT label, configuration_template FROM channel_type LIMIT 1 OFFSET 0`).
		WillReturnRows(rows)
	channelTypes, err = mockDb.ListChannelTypes(1, 0)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(channelTypes) != 1 {
		t.Fatalf("expected %d results, got %d", 1, len(channelTypes))
	}
	if !reflect.DeepEqual(channelTypes[0], &ct0) {
		t.Fatalf("expected %v, got %v", ct0, *channelTypes[0])
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}

	rows = sqlmock.NewRows([]string{"label", "configuration_template"}).
		AddRow(
			"email",
			config0,
		)
	mock.ExpectQuery(`SELECT label, configuration_template FROM channel_type LIMIT 1 OFFSET 1`).
		WillReturnRows(rows)
	channelTypes, err = mockDb.ListChannelTypes(1, 1)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(channelTypes) != 1 {
		t.Fatalf("expected %d results, got %d", 1, len(channelTypes))
	}
	if !reflect.DeepEqual(channelTypes[0], &ct0) {
		t.Fatalf("expected %v, got %v", ct0, *channelTypes[0])
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}

func TestGetChannelType(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	expect := ChannelType{
		Label: "email",
		ConfigurationTemplate: []util.Parameter{{
			Name:        "dest",
			Type:        "string",
			Description: "foo@bar.com",
			Mandatory:   true,
			Placeholder: "",
			Default:     "",
		}},
	}
	config, err := json.Marshal(expect.ConfigurationTemplate)
	if err != nil {
		panic(err)
	}

	rows := sqlmock.NewRows([]string{"label", "configuration_template"}).
		AddRow(
			"email",
			config,
		)

	mock.ExpectQuery(`SELECT label, configuration_template FROM channel_type WHERE label = \?`).
		WithArgs("email").
		WillReturnRows(rows)

	got, err := mockDb.GetChannelType("email")
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if !reflect.DeepEqual(got, &expect) {
		t.Fatalf("expected %v, got %v", expect, *got)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}
