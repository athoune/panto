// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"context"
	"path"

	"github.com/golang/protobuf/ptypes/empty"

	"gitlab.com/pantomath-io/panto/api"
	"gitlab.com/pantomath-io/panto/server/dbconf"
	"gitlab.com/pantomath-io/panto/server/services"
	"gitlab.com/pantomath-io/panto/util"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// ListTargets returns all the targets from an organization
func (s *server) ListTargets(ctx context.Context, req *api.ListTargetsRequest) (res *api.ListTargetsResponse, err error) {
	if len(req.Parent) == 0 {
		log.Errorf("missing parent Organization name")
		return nil, status.Error(codes.InvalidArgument, "Missing parent Organization name")
	}

	var pageOffset uint64
	if req.PageToken != "" {
		valid, err := util.ValidatePageToken(req.PageToken, req, &pageOffset)
		if err != nil {
			log.Errorf("Failed to validate page token: %s", err)
			return nil, status.Errorf(codes.InvalidArgument, "Invalid page token")
		}
		if !valid {
			log.Errorf("Page token does not match request")
			return nil, status.Errorf(codes.InvalidArgument, "Page token does not match request")
		}
	}

	pageSize := req.PageSize
	if pageSize == 0 {
		pageSize = defaultPageSize
	} else if pageSize > maxPageSize {
		pageSize = maxPageSize
	}

	// get the organization UUID from the parent field
	organizationUUID, err := api.ParseNameOrganization(req.Parent)
	if err != nil {
		log.Errorf("couldn't parse parent Organization name: %s", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid parent Organization name")
	}

	dbconf, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error.")
	}
	dbTargets, err := dbconf.ListTargets(organizationUUID, pageSize+1, pageOffset)
	if err != nil {
		log.Errorf("failed to list Targets: %s", err)
		return nil, status.Error(codes.NotFound, "No Targets found")
	}

	var nextPage bool
	if int32(len(dbTargets)) > pageSize {
		dbTargets = dbTargets[:pageSize]
		nextPage = true
	}

	targets := make([]*api.Target, len(dbTargets))

	for i, target := range dbTargets {
		var tags []*api.Tag
		dbTags, err := dbconf.ListTagsByTarget(organizationUUID, target.UUID, 0, 0)
		if err != nil {
			log.Errorf("failed to list Tags: %s", err)
		} else {
			for _, tag := range dbTags {
				tags = append(tags, &api.Tag{
					Name:        path.Join(api.ResourceNameOrganization, util.Base64Encode(organizationUUID), api.ResourceNameTag, tag.Slug),
					DisplayName: tag.DisplayName,
					Note:        tag.Note,
					Color:       tag.Color,
				})
			}
		}

		targets[i] = &api.Target{
			Name:    path.Join(api.ResourceNameOrganization, util.Base64Encode(organizationUUID), api.ResourceNameTarget, util.Base64Encode(target.UUID)),
			Address: target.Address,
			Note:    target.Note,
			Tags:    tags,
		}
	}

	res = &api.ListTargetsResponse{
		Targets: targets,
	}

	if nextPage {
		nextPageToken, err := util.NextPageToken(req, pageOffset+uint64(len(targets)))
		if err != nil {
			log.Warning("Failed to generate next page token")
			// Return values but don't set page token
			nextPageToken = ""
		}
		res.NextPageToken = nextPageToken
	}

	return
}

// GetTarget returns a specific Target.
func (s *server) GetTarget(ctx context.Context, req *api.GetTargetRequest) (res *api.Target, err error) {
	if len(req.Name) == 0 {
		log.Errorf("missing Target name")
		return nil, status.Error(codes.InvalidArgument, "Missing Target name")
	}

	// get the organization and target UUIDs from the name field
	organizationUUID, targetUUID, err := api.ParseNameTarget(req.Name)
	if err != nil {
		log.Errorf("couldn't parse Target name: %s", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid Target name")
	}

	dbconf, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}
	dbTarget, err := dbconf.GetTarget(organizationUUID, targetUUID)
	if err != nil {
		log.Errorf("failed to get Target: %s", err)
		return nil, status.Error(codes.Internal, "Target not found")
	}
	if dbTarget == nil {
		log.Errorf("target not found: %s", req.Name)
		return nil, status.Error(codes.NotFound, "Target not found")
	}

	var tags []*api.Tag
	dbTags, err := dbconf.ListTagsByTarget(organizationUUID, dbTarget.UUID, 0, 0)
	if err != nil {
		log.Errorf("failed to list Tags: %s", err)
	} else {
		for _, tag := range dbTags {
			tags = append(tags, &api.Tag{
				Name:        path.Join(api.ResourceNameOrganization, util.Base64Encode(organizationUUID), api.ResourceNameTag, tag.Slug),
				DisplayName: tag.DisplayName,
				Note:        tag.Note,
				Color:       tag.Color,
			})
		}
	}

	res = &api.Target{
		Name:    path.Join(api.ResourceNameOrganization, util.Base64Encode(organizationUUID), api.ResourceNameTarget, util.Base64Encode(dbTarget.UUID)),
		Address: dbTarget.Address,
		Note:    dbTarget.Note,
		Tags:    tags,
	}

	return
}

// CreateTarget insert a new Target in an Organization
func (s *server) CreateTarget(ctx context.Context, req *api.CreateTargetRequest) (res *api.Target, err error) {
	if len(req.Parent) == 0 {
		log.Errorf("missing parent Organization name")
		return nil, status.Error(codes.InvalidArgument, "Missing parent Organization name")
	}

	// get the organization UUID from the parent field
	organizationUUID, err := api.ParseNameOrganization(req.Parent)
	if err != nil {
		log.Errorf("couldn't parse parent Organization name: %s", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid parent Organization name")
	}

	db, err := services.GetDBConf()
	if err != nil {
		log.Errorf("couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	if req.GetTarget() == nil {
		log.Errorf("missing target object in request")
		return nil, status.Error(codes.InvalidArgument, "Missing Target")
	}

	if len(req.Target.GetAddress()) == 0 {
		log.Errorf("missing address in target object in request")
		return nil, status.Error(codes.InvalidArgument, "Missing address")
	}

	dbTarget, err := db.CreateTarget(&dbconf.Target{
		OrganizationUUID: organizationUUID,
		Address:          req.Target.Address,
		Note:             req.Target.Note,
	})
	if err != nil {
		log.Errorf("failed to create Target: %s", err)
		return nil, status.Error(codes.Internal, "Couldn't create Target")
	}

	// Link tags
	var tags []*api.Tag
	tagNames := make([]string, 0, len(req.Target.Tags))
	for _, tag := range req.Target.Tags {
		tagOrganizationUUID, tagName, err := api.ParseNameTag(tag.Name)
		if err != nil {
			log.Errorf("unable to extract tag name (%s)", err)
			continue
		}
		if tagOrganizationUUID != organizationUUID {
			log.Errorf("can't assign tag from different organization")
			continue
		}
		tagNames = append(tagNames, tagName)
	}
	err = db.LinkTagsToTarget(false, organizationUUID, dbTarget.UUID, tagNames...)
	if err != nil {
		log.Errorf("failed to link tags to target: %s", err)
	} else {
		dbTags, err := db.ListTagsByTarget(organizationUUID, dbTarget.UUID, 0, 0)
		if err != nil {
			log.Errorf("failed to retrieve tags for target: %s", err)
		} else {
			for _, t := range dbTags {
				tags = append(tags, &api.Tag{
					Name:        path.Join(api.ResourceNameOrganization, util.Base64Encode(organizationUUID), api.ResourceNameTag, t.Slug),
					DisplayName: t.DisplayName,
					Note:        t.Note,
					Color:       t.Color,
				})
			}
		}
	}

	res = &api.Target{
		Name:    path.Join(api.ResourceNameOrganization, util.Base64Encode(organizationUUID), api.ResourceNameTarget, util.Base64Encode(dbTarget.UUID)),
		Address: dbTarget.Address,
		Note:    dbTarget.Note,
		Tags:    tags,
	}

	return
}

// UpdateTarget updates an existing Target and returns it.
func (s *server) UpdateTarget(ctx context.Context, req *api.UpdateTargetRequest) (res *api.Target, err error) {
	if len(req.Target.Name) == 0 {
		log.Errorf("missing Target name")
		return nil, status.Error(codes.InvalidArgument, "Missing target name")
	}

	// get the organization and target UUIDs from the name field
	organizationUUID, targetUUID, err := api.ParseNameTarget(req.Target.Name)
	if err != nil {
		log.Errorf("couldn't parse Target name: %s", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid Target name")
	}

	db, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	updateTarget := &dbconf.Target{
		UUID:             targetUUID,
		OrganizationUUID: organizationUUID,
	}

	if req.UpdateMask != nil && util.Contains(req.UpdateMask.Paths, "name") {
		return nil, status.Error(codes.InvalidArgument, "Target name is read-only")
	}

	if req.UpdateMask == nil || util.Contains(req.UpdateMask.Paths, "address") {
		if req.Target.GetAddress() == "" {
			return nil, status.Error(codes.InvalidArgument, "Missing Target address")
		}
		updateTarget.Address = req.Target.GetAddress()
	}

	if req.UpdateMask == nil || util.Contains(req.UpdateMask.GetPaths(), "note") {
		updateTarget.Note = req.Target.GetNote()
	}

	if req.UpdateMask == nil || util.Contains(req.UpdateMask.Paths, "tags") {
		tagNames := make([]string, 0, len(req.Target.Tags))
		for _, tag := range req.Target.Tags {
			tagOrganizationUUID, tagName, err := api.ParseNameTag(tag.Name)
			if err != nil {
				log.Errorf("unable to extract tag name (%s)", err)
				continue
			}
			if tagOrganizationUUID != organizationUUID {
				log.Errorf("can't assign tag from different organization")
				continue
			}
			tagNames = append(tagNames, tagName)
		}
		err = db.LinkTagsToTarget(true, organizationUUID, targetUUID, tagNames...)
		if err != nil {
			log.Errorf("failed to link tags to target: %s", err)
			return nil, status.Errorf(codes.Internal, "Couldn't update Target")
		}
	}

	updateTarget, err = db.UpdateTarget(updateTarget)
	if err != nil {
		log.Errorf("failed to update Target: %s", err)
		return nil, status.Error(codes.Internal, "Couldn't update Target")
	}

	var tags []*api.Tag
	dbTags, err := db.ListTagsByTarget(organizationUUID, updateTarget.UUID, 0, 0)
	if err != nil {
		log.Errorf("failed to list Tags: %s", err)
	} else {
		for _, tag := range dbTags {
			tags = append(tags, &api.Tag{
				Name:        path.Join(api.ResourceNameOrganization, util.Base64Encode(organizationUUID), api.ResourceNameTag, tag.Slug),
				DisplayName: tag.DisplayName,
				Note:        tag.Note,
				Color:       tag.Color,
			})
		}
	}

	res = &api.Target{
		Name:    path.Join(api.ResourceNameOrganization, util.Base64Encode(updateTarget.OrganizationUUID), api.ResourceNameTarget, util.Base64Encode(updateTarget.UUID)),
		Address: updateTarget.Address,
		Note:    updateTarget.Note,
		Tags:    tags,
	}

	return
}

// DeleteTarget deletes an existing Target, corresponding Checks and ProbeConfigurations and unlinks it from Tags.
func (s *server) DeleteTarget(ctx context.Context, req *api.DeleteTargetRequest) (res *empty.Empty, err error) {
	if len(req.Name) == 0 {
		log.Errorf("missing target name")
		return nil, status.Error(codes.InvalidArgument, "Missing Target name")
	}

	// get the organization and target UUIDs from the name field
	organizationUUID, targetUUID, err := api.ParseNameTarget(req.Name)
	if err != nil {
		log.Errorf("couldn't parse Target name: %s", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid Target name")
	}

	db, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	err = db.DeleteTarget(organizationUUID, targetUUID)
	if err != nil {
		log.Errorf("failed to delete Target: %s", err)
		return nil, status.Error(codes.Internal, "Couldn't delete Target")
	}

	res = &empty.Empty{}
	return
}
