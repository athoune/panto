package server

import (
	"context"
	"reflect"
	"strings"

	"github.com/golang/protobuf/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"

	"gitlab.com/pantomath-io/panto/api"
)

func deprecated389(deprecated map[string]struct{}, msg proto.Message) {
	// #389 deprecates state_type and state_configuration in Check and
	// replaces them with type and configuration
	if check, ok := msg.(*api.Check); ok {
		if check.Type == "" && check.StateType != "" {
			check.Type = check.StateType
			check.StateType = ""
			deprecated["Check.state_type"] = struct{}{}
		}
		if check.Configuration == "" && check.StateConfiguration != "" {
			check.Configuration = check.StateConfiguration
			check.StateConfiguration = ""
			deprecated["Check.state_configuration"] = struct{}{}
		}
		return
	}

	// Recurse into all fields
	in := reflect.ValueOf(msg).Elem()
	for i := 0; i < in.NumField(); i++ {
		f := in.Field(i)
		if (f.Kind() == reflect.Ptr || f.Kind() == reflect.Interface) && !f.IsNil() {
			if subMsg, ok := in.Field(i).Interface().(proto.Message); ok && subMsg != nil {
				deprecated389(deprecated, subMsg)
			}
		}
	}
}

// deprecationInterceptor digs into the fields of a request and updates legacy fields. It also sets a header
// containing a list of deprecated fields it encountered
func deprecationInterceptor(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (res interface{}, err error) {
	msg, ok := req.(proto.Message)
	if !ok {
		log.Warningf("Request is not a protobuf message: %T", req)
		return handler(ctx, req)
	}

	// set of deprecated field names
	deprecated := make(map[string]struct{})

	deprecated389(deprecated, msg)

	if len(deprecated) > 0 {
		l := make([]string, 0, len(deprecated))
		for d := range deprecated {
			l = append(l, d)
		}

		grpc.SendHeader(
			ctx,
			metadata.New(map[string]string{
				"deprecated": strings.Join(l, ","),
			}),
		)
	}

	return handler(ctx, req)
}
