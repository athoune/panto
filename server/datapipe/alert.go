// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package datapipe

import (
	"bytes"
	"fmt"
	"time"

	"github.com/google/uuid"

	"gitlab.com/pantomath-io/panto/api"
	"gitlab.com/pantomath-io/panto/server/alert"
	"gitlab.com/pantomath-io/panto/server/dbconf"
	"gitlab.com/pantomath-io/panto/server/tsdb"
	"gitlab.com/pantomath-io/panto/util"
)

const (
	// AlertWaitingTime is the number of seconds to wait before sending another alert
	AlertWaitingTime time.Duration = 60 * time.Second
)

// Alert represents an alert at a certain time
type Alert struct {
	State       bool
	ProbeUUID   uuid.UUID
	TargetUUID  uuid.UUID
	AgentUUID   uuid.UUID
	ChannelUUID uuid.UUID
	Reason      string
}

// alertDefinition enum
const (
	StateChange      string = "state_change"
	StateRecurrence  string = "state_recurrence"
	StateCombination string = "state_combination"
	StateFlap        string = "state_flap"
)

// GetAlert evaluates an Alert.State based on the configuration
func GetAlert(result tsdb.Result, state *State, dbAlert *dbconf.Alert, ts tsdb.TSDB) (*Alert, error) {
	// Check input
	if len(result.Check) == 0 {
		return nil, fmt.Errorf("empty target_probe UUID")
	}
	if len(result.Probe) == 0 {
		return nil, fmt.Errorf("empty probe UUID")
	}
	if len(result.Target) == 0 {
		return nil, fmt.Errorf("empty target UUID")
	}
	if len(result.Agent) == 0 {
		return nil, fmt.Errorf("empty agent UUID")
	}
	if state.Value == StateValue(api.State_UNKNOWN) {
		return nil, fmt.Errorf("unknown State")
	}

	var alert = &Alert{
		ProbeUUID:   result.Probe,
		TargetUUID:  result.Target,
		AgentUUID:   result.Agent,
		ChannelUUID: dbAlert.ChannelUUID,
	}

	// Based on DB alert, evaluate alert state
	switch dbAlert.Type {
	case StateChange:
		var err error
		alert.State, alert.Reason, err = checkStateChange(result, state, ts)
		if err != nil {
			return nil, fmt.Errorf("failed to evaluate state: %s", err)
		}
	case StateRecurrence:
		var err error
		recurrence, ok := dbAlert.Configuration["recurrence"]
		if !ok {
			return nil, fmt.Errorf("unable to read recurrence configuration")
		}
		r, ok := recurrence.(float64)
		if !ok {
			return nil, fmt.Errorf("recurrence count is not an integer")
		}
		alert.State, alert.Reason, err = checkStateRecurrence(int(r), result, state, ts)
		if err != nil {
			return nil, fmt.Errorf("failed to evaluate state: %s", err)
		}
	case StateFlap:
		var err error
		occurrence, ok := dbAlert.Configuration["occurrence"]
		if !ok {
			return nil, fmt.Errorf("unable to read occurrence configuration")
		}
		o, ok := occurrence.(float64)
		if !ok {
			return nil, fmt.Errorf("occurrence count is not an integer")
		}
		tolerance, ok := dbAlert.Configuration["tolerance"]
		if !ok {
			return nil, fmt.Errorf("unable to read tolerance configuration")
		}
		t, ok := tolerance.(float64)
		if !ok {
			return nil, fmt.Errorf("tolerance count is not an integer")
		}
		alert.State, alert.Reason, err = checkStateFlap(int(o), int(t), result, state, ts)
		if err != nil {
			return nil, fmt.Errorf("failed to evaluate state: %s", err)
		}
	default:
		return nil, fmt.Errorf("unknown alert definition (%s)", dbAlert.Type)
	}

	return alert, nil
}

// checkStateChange compares the current state with previous state
func checkStateChange(result tsdb.Result, state *State, ts tsdb.TSDB) (bool, string, error) {
	var reason string

	lastStates, err := ts.GetStates(
		[]uuid.UUID{result.Check},
		tsdb.Tag("agent", util.Base64Encode(result.Agent)),
		tsdb.Count(1),
		tsdb.OrderBy(tsdb.OrderDescending),
	)
	if err != nil {
		return false, "", err
	}
	stateChange := !(StateValue(lastStates[0].State) == state.Value)
	if stateChange {
		reason = fmt.Sprintf("State was %d and is now %d", lastStates[0].State, state.Value)
	}
	return stateChange, reason, nil
}

// checkStateRecurrence checks if the current state lasts, and is not ok
func checkStateRecurrence(recurrence int, result tsdb.Result, state *State, ts tsdb.TSDB) (bool, string, error) {
	log.Debug("Checking state recurrence alert conditions")

	if state.Value == StateValueOK {
		return false, "", nil
	}

	lastStates, err := ts.GetStates(
		[]uuid.UUID{result.Check},
		tsdb.Tag("agent", util.Base64Encode(result.Agent)),
		tsdb.Count(1),
		tsdb.OrderBy(tsdb.OrderDescending),
	)
	if err != nil {
		return false, "", err
	}

	var debugLog bytes.Buffer
	for _, s := range lastStates {
		debugLog.WriteString(fmt.Sprintf("%d ", s.State))
	}
	log.Debugf("Got last states: %s", debugLog.String())

	for _, previousState := range lastStates {
		if previousState.State == int(api.State_OK) {
			return false, "", nil
		}
	}
	reason := fmt.Sprintf("State is %d and is not OK for at least %d occurrence(s)", state.Value, recurrence)
	return true, reason, nil
}

// checkStateFlap checks if the state flaps more than X times over the last occurrences
func checkStateFlap(occurrence, tolerance int, result tsdb.Result, state *State, ts tsdb.TSDB) (bool, string, error) {
	log.Debug("Checking state flap alert conditions")

	lastStates, err := ts.GetStates(
		[]uuid.UUID{result.Check},
		tsdb.Tag("agent", util.Base64Encode(result.Agent)),
		tsdb.Count(occurrence),
		tsdb.OrderBy(tsdb.OrderDescending),
	)
	if err != nil {
		return false, "", err
	}

	var debugLog bytes.Buffer
	for _, s := range lastStates {
		debugLog.WriteString(fmt.Sprintf("%d ", s.State))
	}
	log.Debugf("Got last states: %s", debugLog.String())

	if len(lastStates) < occurrence {
		return false, "", nil
	}

	var stateChange int
	for i, previousState := range lastStates[:len(lastStates)-1] {
		if previousState.State != lastStates[i+1].State {
			stateChange++
		}
	}

	if stateChange <= tolerance {
		return false, "", nil
	}

	reason := fmt.Sprintf("State is %d and flapped %d time(s) over the last %d occurence(s)", state.Value, stateChange, occurrence)
	return true, reason, nil
}

// SendAlert actually sends the Alert based on configuration and business rules. Returns true if the alert was actually
// sent, and false otherwise. Note that the alert can not be sent for a variety of reasons that don't designate an error
// in the system. As such, (false, nil) is a valid result.
func (a *Alert) SendAlert(result tsdb.Result, state *State, db dbconf.ConfigurationDB, ts tsdb.TSDB) (bool, error) {
	// check previous Alert sent timestamp
	previousAlert, err := ts.GetAlertEvents([]uuid.UUID{result.ProbeConfiguration}, tsdb.Count(1), tsdb.OrderBy(tsdb.OrderDescending))
	if err != nil {
		return false, err
	}
	if len(previousAlert) > 0 {
		lastTimestamp := previousAlert[0].Timestamp
		elapsed := time.Since(lastTimestamp)
		if elapsed < AlertWaitingTime {
			log.Debugf("Got last alert, sent %d sec ago (i.e. too early to spam)", elapsed/time.Second)
			return false, nil
		}
	}

	// TODO: check mute

	// get Channel configuration for this alert
	channel, err := db.GetChannel(result.Organization, a.ChannelUUID)
	if err != nil {
		return false, err
	}

	agent, err := db.GetAgent(result.Organization, result.Agent)
	if err != nil {
		return false, err
	}

	target, err := db.GetTarget(result.Organization, result.Target)
	if err != nil {
		return false, err
	}
	targetName := target.Address
	if target.Address == "localhost" || target.Address == "127.0.0.1" || target.Address == "::1" {
		targetName = agent.DisplayName
	}

	probe, err := db.GetProbe(result.Probe)
	if err != nil {
		return false, err
	}

	err = alert.DispatchAlert(alert.Alert{
		ChannelType:   channel.Type,
		ChannelConfig: channel.Configuration,
		Vars: alert.TemplateVars{
			Title:       state.Value.String() + " alert",
			Target:      targetName,
			Agent:       agent.DisplayName,
			Probe:       probe.DisplayName,
			State:       state.Value.String(),
			Reason:      state.Reason,
			Message:     a.Reason,
			OK:          state.Value == StateValueOK,
			Critical:    state.Value == StateValueCritical,
			Warning:     state.Value == StateValueWarning,
			MissingData: state.Value == StateValueMissingData,
		},
	})
	if err != nil {
		return false, err
	}

	return true, nil
}
