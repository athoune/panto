// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package datapipe

import (
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"gitlab.com/pantomath-io/panto/api"
	"gitlab.com/pantomath-io/panto/server/tsdb"
	"gitlab.com/pantomath-io/panto/util"
)

// One minute time window
const historyWindow = time.Duration(6e+10)

// History is the States definition for a history check
type History struct {
	Interval string `json:"interval"`
	States   []struct {
		State      string `json:"state"`
		Match      match  `json:"match"`
		Conditions []struct {
			Field     string   `json:"field"`
			Operator  operator `json:"operator"`
			Tolerance float64  `json:"tolerance"`
			Reason    string   `json:"reason"`
		} `json:"conditions"`
		Tags []struct {
			Name     string   `json:"name"`
			Operator operator `json:"operator"`
			Value    string   `json:"value"`
		} `json:"tags"`
	} `json:"states"`
}

// checkResultHistory tests the alert status (ok, warn, crit)
func checkResultHistory(jsonDef []byte, result tsdb.Result, ts tsdb.TSDB) (*State, error) {
	var historyDef History

	// Check input
	if len(jsonDef) == 0 {
		return nil, fmt.Errorf("empty reference")
	}
	if len(result.Fields) == 0 {
		return nil, fmt.Errorf("empty fields")
	}

	err := json.Unmarshal(jsonDef, &historyDef)
	if err != nil {
		return nil, fmt.Errorf("unable to parse JSON: %s", err)
	}

	results := make(map[string]*conditionResult)

	// Get reference history value
	historyResult, err := getHistoryResult(historyDef, result, ts)
	if err != nil {
		return nil, err
	}
	if historyResult == nil {
		return &State{
			Value:  StateValue(api.State_UNKNOWN),
			Reason: "State could not be calculated: no historical value to compare to",
		}, nil
	}
	log.Debugf("Got closest past value: %v", historyResult)

States:
	for _, state := range historyDef.States {
		if len(state.Conditions) == 0 {
			log.Debug("state has no conditions, skipping...")
			continue States
		}
		for _, tag := range state.Tags {
			v, ok := result.Tags[tag.Name]
			if !ok {
				continue States
			}
			ok, err = compareString(v, tag.Value, tag.Operator)
			if err != nil {
				log.Errorf("unable to evaluate tag condition: %s", err)
				continue States
			}
			if !ok {
				continue States
			}
		}
		condResult := make([]bool, len(state.Conditions))
		reasons := make([]string, 0, len(state.Conditions))
		for i, condition := range state.Conditions {
			rField, ok := result.Fields[condition.Field]
			if !ok {
				return nil, fmt.Errorf("unable to find the condition field in the new result")
			}
			hField, ok := historyResult.Fields[condition.Field]
			if !ok {
				return nil, fmt.Errorf("unable to find the condition field in the history result")
			}
			condResult[i], err = compareHistoryValues(rField, hField, condition.Operator, condition.Tolerance)
			if err != nil {
				return nil, err
			}
			if condResult[i] {
				reasons = append(reasons, condition.Reason)
			}
		}

		// Add the result array to the global map
		stateResult := &conditionResult{}
		switch state.Match {
		case MatchAny:
			stateResult.Result = any(condResult)
		case MatchAll:
			stateResult.Result = all(condResult)
		default:
			return nil, fmt.Errorf("Unable to cope Match configuration (%s)", state.Match)
		}
		stateResult.Reason = strings.Join(reasons, "\n")
		results[state.State] = stateResult
	}

	return combineStates(results)
}

// getHistoryResult retrieves the value closest to the reference time minus the interval, inside a
// time window (defined in `historyWindow`). If no such result has been found, `nil` is returned
// with no error.
func getHistoryResult(history History, r tsdb.Result, ts tsdb.TSDB) (*tsdb.Result, error) {

	interval, err := time.ParseDuration(history.Interval)
	if err != nil {
		return nil, fmt.Errorf("Could not parse time interval \"%s\": %s", history.Interval, err)
	}

	historyTime := r.Timestamp.Add(-interval)
	from := historyTime.Add(-historyWindow / 2)
	to := historyTime.Add(historyWindow / 2)
	results, err := ts.GetResults(
		r.Check,
		tsdb.From(from),
		tsdb.To(to),
		tsdb.Tag("agent", util.Base64Encode(r.Agent)),
	)

	if err != nil {
		return nil, fmt.Errorf("Couldn't retrieve results from TSDB: %s", err)
	}

	if len(results) == 0 {
		return nil, nil
	}

	log.Debugf("Got results: %v", results)

	// Results are ordered by time, so just advance from 0 until we overstep the reference time.
	// If this becomes a performance issue at some point, we can probably do a faster binary search of the
	// closest time.
	res := &results[0]
	if res.Timestamp.After(historyTime) {
		// First result is already later than reference time
		return res, nil
	}

	for i := 1; i < len(results); i++ {
		next := &results[i]
		if next.Timestamp.After(historyTime) {
			// Next result oversteps ref time, return the closest of the two
			if historyTime.Sub(res.Timestamp) < next.Timestamp.Sub(historyTime) {
				return res, nil
			}
			return next, nil
		}

		res = next
	}

	return res, nil
}

// compareHistoryValues prepares values before sending them on to the generic compareValues
func compareHistoryValues(r, h interface{}, op operator, tol float64) (bool, error) {
	// If history type is numeric, add or subtract tolerance for correct comparison type
	switch h2 := h.(type) {
	case float64:
		d := h2 * tol
		switch op {
		case OpGreaterThan, OpLowerEqual:
			return compareValues(r, h2+d, op)
		case OpLowerThan, OpGreaterEqual:
			return compareValues(r, h2-d, op)
		case OpEqual:
			lt, err := compareValues(r, h2-d, OpLowerThan)
			if err != nil {
				return false, err
			}
			gt, err := compareValues(r, h2+d, OpGreaterThan)
			if err != nil {
				return false, err
			}
			return (!lt && !gt), nil
		case OpNotEqual:
			lt, err := compareValues(r, h2-d, OpLowerThan)
			if err != nil {
				return false, err
			}
			gt, err := compareValues(r, h2+d, OpGreaterThan)
			if err != nil {
				return false, err
			}
			return (lt || gt), nil
		}
	case int64:
		d := int64(float64(h2) * tol)
		switch op {
		case OpGreaterThan, OpLowerEqual:
			return compareValues(r, h2+d, op)
		case OpLowerThan, OpGreaterEqual:
			return compareValues(r, h2-d, op)
		case OpEqual:
			lt, err := compareValues(r, h2-d, OpLowerThan)
			if err != nil {
				return false, err
			}
			gt, err := compareValues(r, h2+d, OpGreaterThan)
			if err != nil {
				return false, err
			}
			return (!lt && !gt), nil
		case OpNotEqual:
			lt, err := compareValues(r, h2-d, OpLowerThan)
			if err != nil {
				return false, err
			}
			gt, err := compareValues(r, h2+d, OpGreaterThan)
			if err != nil {
				return false, err
			}
			return (lt || gt), nil
		}
	}
	return compareValues(r, h, op)
}
