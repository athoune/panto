// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package datapipe

import (
	"testing"
	"time"

	"gitlab.com/pantomath-io/panto/server/tsdb"
)

func TestComputeTrend(t *testing.T) {
	now := time.Now()
	type testCase struct {
		r, t   tsdb.Result
		f      string
		i      string
		expect float64
	}
	cases := []testCase{
		{
			r:      tsdb.Result{Entry: tsdb.Entry{Timestamp: now}, Fields: map[string]interface{}{"f": 10}},
			t:      tsdb.Result{Entry: tsdb.Entry{Timestamp: now.Add(-5 * time.Minute)}, Fields: map[string]interface{}{"f": 0}},
			f:      "f",
			i:      "1m",
			expect: 2.0,
		},
		{
			r:      tsdb.Result{Entry: tsdb.Entry{Timestamp: now}, Fields: map[string]interface{}{"f": 0}},
			t:      tsdb.Result{Entry: tsdb.Entry{Timestamp: now.Add(-5 * time.Minute)}, Fields: map[string]interface{}{"f": 10}},
			f:      "f",
			i:      "1m",
			expect: -2.0,
		},
		{
			r:      tsdb.Result{Entry: tsdb.Entry{Timestamp: now}, Fields: map[string]interface{}{"f": 10}},
			t:      tsdb.Result{Entry: tsdb.Entry{Timestamp: now.Add(-5 * time.Minute)}, Fields: map[string]interface{}{"f": 10}},
			f:      "f",
			i:      "1m",
			expect: 0.0,
		},
		{
			r:      tsdb.Result{Entry: tsdb.Entry{Timestamp: now}, Fields: map[string]interface{}{"f": 10}},
			t:      tsdb.Result{Entry: tsdb.Entry{Timestamp: now.Add(-5 * time.Minute)}, Fields: map[string]interface{}{"f": 0}},
			f:      "f",
			i:      "5m",
			expect: 10.0,
		},
		{
			r:      tsdb.Result{Entry: tsdb.Entry{Timestamp: now}, Fields: map[string]interface{}{"f": 10}},
			t:      tsdb.Result{Entry: tsdb.Entry{Timestamp: now.Add(-5 * time.Minute)}, Fields: map[string]interface{}{"f": 0}},
			f:      "f",
			i:      "1h",
			expect: 120.0,
		},
	}

	for _, c := range cases {
		got, _ := computeTrend(c.r, c.t, c.f, c.i)
		if got != c.expect {
			t.Errorf("failed to compute trend: got %v, expected %v", got, c.expect)
		}
	}
}
