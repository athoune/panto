// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package datapipe

import (
	"testing"
)

type historyTestCase struct {
	r, h interface{}
	op   operator
	tol  float64
	res  bool
}

func TestCompareHistoryValues(t *testing.T) {
	cases := []historyTestCase{
		// int64, OpEqual
		{int64(1000), int64(1000), OpEqual, 0.0, true},
		{int64(1000), int64(1000), OpEqual, .1, true},
		{int64(990), int64(1000), OpEqual, .1, true},
		{int64(990), int64(1000), OpEqual, .01, true},
		{int64(990), int64(1000), OpEqual, .001, false},
		{int64(1010), int64(1000), OpEqual, .1, true},
		{int64(1010), int64(1000), OpEqual, .01, true},
		{int64(1010), int64(1000), OpEqual, .001, false},
		// int64, OpNotEqual
		{int64(1000), int64(1000), OpNotEqual, 0.0, false},
		{int64(1000), int64(1000), OpNotEqual, .1, false},
		{int64(990), int64(1000), OpNotEqual, .1, false},
		{int64(990), int64(1000), OpNotEqual, .01, false},
		{int64(990), int64(1000), OpNotEqual, .001, true},
		{int64(1010), int64(1000), OpNotEqual, .1, false},
		{int64(1010), int64(1000), OpNotEqual, .01, false},
		{int64(1010), int64(1000), OpNotEqual, .001, true},
		// int64, OpLowerThan
		{int64(1000), int64(1000), OpLowerThan, 0.0, false},
		{int64(1000), int64(1000), OpLowerThan, .1, false},
		{int64(990), int64(1000), OpLowerThan, .1, false},
		{int64(990), int64(1000), OpLowerThan, .01, false},
		{int64(990), int64(1000), OpLowerThan, .001, true},
		{int64(1010), int64(1000), OpLowerThan, .1, false},
		{int64(1010), int64(1000), OpLowerThan, .01, false},
		{int64(1010), int64(1000), OpLowerThan, .001, false},
		// int64, OpLowerEqual
		{int64(1000), int64(1000), OpLowerEqual, 0.0, true},
		{int64(1000), int64(1000), OpLowerEqual, .1, true},
		{int64(990), int64(1000), OpLowerEqual, .1, true},
		{int64(990), int64(1000), OpLowerEqual, .01, true},
		{int64(990), int64(1000), OpLowerEqual, .001, true},
		{int64(1010), int64(1000), OpLowerEqual, .1, true},
		{int64(1010), int64(1000), OpLowerEqual, .01, true},
		{int64(1010), int64(1000), OpLowerEqual, .001, false},
		// int64, OpGreaterThan
		{int64(1000), int64(1000), OpGreaterThan, 0.0, false},
		{int64(1000), int64(1000), OpGreaterThan, .1, false},
		{int64(990), int64(1000), OpGreaterThan, .1, false},
		{int64(990), int64(1000), OpGreaterThan, .01, false},
		{int64(990), int64(1000), OpGreaterThan, .001, false},
		{int64(1010), int64(1000), OpGreaterThan, .1, false},
		{int64(1010), int64(1000), OpGreaterThan, .01, false},
		{int64(1010), int64(1000), OpGreaterThan, .001, true},
		// int64, OpGreaterEqual
		{int64(1000), int64(1000), OpGreaterEqual, 0.0, true},
		{int64(1000), int64(1000), OpGreaterEqual, .1, true},
		{int64(990), int64(1000), OpGreaterEqual, .1, true},
		{int64(990), int64(1000), OpGreaterEqual, .01, true},
		{int64(990), int64(1000), OpGreaterEqual, .001, false},
		{int64(1010), int64(1000), OpGreaterEqual, .1, true},
		{int64(1010), int64(1000), OpGreaterEqual, .01, true},
		{int64(1010), int64(1000), OpGreaterEqual, .001, true},
		// float64, OpEqual
		{float64(1000), float64(1000), OpEqual, 0.0, true},
		{float64(1000), float64(1000), OpEqual, .1, true},
		{float64(990), float64(1000), OpEqual, .1, true},
		{float64(990), float64(1000), OpEqual, .01, true},
		{float64(990), float64(1000), OpEqual, .001, false},
		{float64(1010), float64(1000), OpEqual, .1, true},
		{float64(1010), float64(1000), OpEqual, .01, true},
		{float64(1010), float64(1000), OpEqual, .001, false},
		// float64, OpNotEqual
		{float64(1000), float64(1000), OpNotEqual, 0.0, false},
		{float64(1000), float64(1000), OpNotEqual, .1, false},
		{float64(990), float64(1000), OpNotEqual, .1, false},
		{float64(990), float64(1000), OpNotEqual, .01, false},
		{float64(990), float64(1000), OpNotEqual, .001, true},
		{float64(1010), float64(1000), OpNotEqual, .1, false},
		{float64(1010), float64(1000), OpNotEqual, .01, false},
		{float64(1010), float64(1000), OpNotEqual, .001, true},
		// float64, OpLowerThan
		{float64(1000), float64(1000), OpLowerThan, 0.0, false},
		{float64(1000), float64(1000), OpLowerThan, .1, false},
		{float64(990), float64(1000), OpLowerThan, .1, false},
		{float64(990), float64(1000), OpLowerThan, .01, false},
		{float64(990), float64(1000), OpLowerThan, .001, true},
		{float64(1010), float64(1000), OpLowerThan, .1, false},
		{float64(1010), float64(1000), OpLowerThan, .01, false},
		{float64(1010), float64(1000), OpLowerThan, .001, false},
		// float64, OpLowerEqual
		{float64(1000), float64(1000), OpLowerEqual, 0.0, true},
		{float64(1000), float64(1000), OpLowerEqual, .1, true},
		{float64(990), float64(1000), OpLowerEqual, .1, true},
		{float64(990), float64(1000), OpLowerEqual, .01, true},
		{float64(990), float64(1000), OpLowerEqual, .001, true},
		{float64(1010), float64(1000), OpLowerEqual, .1, true},
		{float64(1010), float64(1000), OpLowerEqual, .01, true},
		{float64(1010), float64(1000), OpLowerEqual, .001, false},
		// float64, OpGreaterThan
		{float64(1000), float64(1000), OpGreaterThan, 0.0, false},
		{float64(1000), float64(1000), OpGreaterThan, .1, false},
		{float64(990), float64(1000), OpGreaterThan, .1, false},
		{float64(990), float64(1000), OpGreaterThan, .01, false},
		{float64(990), float64(1000), OpGreaterThan, .001, false},
		{float64(1010), float64(1000), OpGreaterThan, .1, false},
		{float64(1010), float64(1000), OpGreaterThan, .01, false},
		{float64(1010), float64(1000), OpGreaterThan, .001, true},
		// float64, OpGreaterEqual
		{float64(1000), float64(1000), OpGreaterEqual, 0.0, true},
		{float64(1000), float64(1000), OpGreaterEqual, .1, true},
		{float64(990), float64(1000), OpGreaterEqual, .1, true},
		{float64(990), float64(1000), OpGreaterEqual, .01, true},
		{float64(990), float64(1000), OpGreaterEqual, .001, false},
		{float64(1010), float64(1000), OpGreaterEqual, .1, true},
		{float64(1010), float64(1000), OpGreaterEqual, .01, true},
		{float64(1010), float64(1000), OpGreaterEqual, .001, true},
	}
	for _, c := range cases {
		res, err := compareHistoryValues(c.r, c.h, c.op, c.tol)

		if err != nil {
			// Error found
			t.Errorf("Failed to compare %#v %s %#v: %s", c.r, c.op, c.h, err)
		} else {
			if res != c.res {
				// Unexpected result
				t.Errorf("Invalid result comparing %#v %s %#v with tolerance %f: got %v, expected %v", c.r, c.op, c.h, c.tol, res, c.res)
			}
		}
	}
}
