// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package datapipe

import "testing"

func TestAny(t *testing.T) {
	{
		res := any([]bool{true})
		if !res {
			t.Error("any true should be true")
		}
	}
	{
		res := any([]bool{true, false})
		if !res {
			t.Error("any true/false should be true")
		}
	}
	{
		res := any([]bool{false, false})
		if res {
			t.Error("any false/false should be false")
		}
	}
	{
		res := any([]bool{})
		if res {
			t.Error("any empty should be false")
		}
	}
}

func TestAll(t *testing.T) {
	{
		res := all([]bool{true})
		if !res {
			t.Error("all true should be true")
		}
	}
	{
		res := all([]bool{true, false})
		if res {
			t.Error("all true/false should be false")
		}
	}
	{
		res := all([]bool{false, false})
		if res {
			t.Error("all false/false should be false")
		}
	}
	{
		res := all([]bool{})
		if !res {
			t.Error("all empty should be false")
		}
	}
}

type compareTestCase struct {
	lhs, rhs interface{}
	op       operator
	res      bool
	err      bool
}

func TestCompareValues(t *testing.T) {
	cases := []compareTestCase{
		// int64 cases
		{int64(1000), int64(1000), OpEqual, true, false},
		{int64(1000), int64(999), OpEqual, false, false},
		{int64(1000), int64(1000), OpNotEqual, false, false},
		{int64(1000), int64(999), OpNotEqual, true, false},
		{int64(1000), int64(1000), OpLowerThan, false, false},
		{int64(1000), int64(999), OpLowerThan, false, false},
		{int64(999), int64(1000), OpLowerThan, true, false},
		{int64(1000), int64(1000), OpLowerEqual, true, false},
		{int64(1000), int64(999), OpLowerEqual, false, false},
		{int64(999), int64(1000), OpLowerEqual, true, false},
		{int64(1000), int64(1000), OpGreaterThan, false, false},
		{int64(1000), int64(999), OpGreaterThan, true, false},
		{int64(999), int64(1000), OpGreaterThan, false, false},
		{int64(1000), int64(1000), OpGreaterEqual, true, false},
		{int64(1000), int64(999), OpGreaterEqual, true, false},
		{int64(999), int64(1000), OpGreaterEqual, false, false},
		{int64(1000), int64(1000), OpMatch, false, true},
		{int64(1000), int64(1000), OpNotMatch, false, true},

		// float64 cases
		{float64(1000), float64(1000), OpEqual, true, false},
		{float64(1000), float64(999), OpEqual, false, false},
		{float64(1000), float64(1000), OpNotEqual, false, false},
		{float64(1000), float64(999), OpNotEqual, true, false},
		{float64(1000), float64(1000), OpLowerThan, false, false},
		{float64(1000), float64(999), OpLowerThan, false, false},
		{float64(999), float64(1000), OpLowerThan, true, false},
		{float64(1000), float64(1000), OpLowerEqual, true, false},
		{float64(1000), float64(999), OpLowerEqual, false, false},
		{float64(999), float64(1000), OpLowerEqual, true, false},
		{float64(1000), float64(1000), OpGreaterThan, false, false},
		{float64(1000), float64(999), OpGreaterThan, true, false},
		{float64(999), float64(1000), OpGreaterThan, false, false},
		{float64(1000), float64(1000), OpGreaterEqual, true, false},
		{float64(1000), float64(999), OpGreaterEqual, true, false},
		{float64(999), float64(1000), OpGreaterEqual, false, false},
		{float64(1000), float64(1000), OpMatch, false, true},
		{float64(1000), float64(1000), OpNotMatch, false, true},

		// bool cases
		{true, true, OpEqual, true, false},
		{true, false, OpEqual, false, false},
		{true, true, OpNotEqual, false, false},
		{true, false, OpNotEqual, true, false},
		{true, true, OpLowerThan, false, true},
		{true, true, OpLowerEqual, false, true},
		{true, true, OpGreaterThan, false, true},
		{true, true, OpGreaterEqual, false, true},
		{true, true, OpMatch, false, true},
		{true, true, OpNotMatch, false, true},

		// string cases
		{"Barbès", "Barbès", OpEqual, true, false},
		{"Barbès", "Rochechouart", OpEqual, false, false},
		{"Barbès", "Barbès", OpNotEqual, false, false},
		{"Barbès", "Rochechouart", OpNotEqual, true, false},
		{"Barbès", "Barbès", OpLowerThan, false, true},
		{"Barbès", "Barbès", OpLowerEqual, false, true},
		{"Barbès", "Barbès", OpGreaterThan, false, true},
		{"Barbès", "Barbès", OpGreaterEqual, false, true},
		{"Barbès", "Barbès", OpMatch, true, false},
		{"Barbès", "^Barbès$", OpMatch, true, false},
		{"Barbès", "Bar", OpMatch, true, false},
		{"Barbès", "Bar(bès)+", OpMatch, true, false},
		{"Barbès", "Bar(bèsse)+", OpMatch, false, false},
		{"Barbès", "^Bar", OpMatch, true, false},
		{"Barbès", "^Bar$", OpMatch, false, false},
		{"Barbès", "bès", OpMatch, true, false},
		{"Barbès", "bès$", OpMatch, true, false},
		{"Barbès", "^bès", OpMatch, false, false},
		{"Barbès", "(Barbès", OpMatch, false, true},
		{"Barbès", "Barbès", OpNotMatch, false, false},
		{"Barbès", "^Barbès$", OpNotMatch, false, false},
		{"Barbès", "Bar", OpNotMatch, false, false},
		{"Barbès", "Bar(bès)+", OpNotMatch, false, false},
		{"Barbès", "Bar(bèsse)+", OpNotMatch, true, false},
		{"Barbès", "^Bar", OpNotMatch, false, false},
		{"Barbès", "^Bar$", OpNotMatch, true, false},
		{"Barbès", "bès", OpNotMatch, false, false},
		{"Barbès", "bès$", OpNotMatch, false, false},
		{"Barbès", "^bès", OpNotMatch, true, false},
		{"Barbès", "(Barbès", OpNotMatch, false, true},

		// error cases
		{"1", int64(1), OpEqual, false, true},
		{1, 1, OpEqual, false, true},
	}

	for _, c := range cases {
		res, err := compareValues(c.lhs, c.rhs, c.op)

		if !c.err {
			// Expect no error
			if err != nil {
				// Error found
				t.Errorf("Error comparing %#v %s %#v: %s", c.lhs, c.op, c.rhs, err)
			} else if c.res != res {
				// Unexpected result
				t.Errorf("Invalid result comparing %#v %s %#v: got %v, expected %v", c.lhs, c.op, c.rhs, res, c.res)
			}
			// No error, valid result, test is successful
		} else {
			// Expect error
			if err == nil {
				// No error found
				t.Errorf("Expected error comparing %#v %s %#v", c.lhs, c.op, c.rhs)
			}
		}
	}
}

type stateTestCase struct {
	sType string
	sConf string
	err   bool
}

func TestValidateStateConfigurationFormat(t *testing.T) {
	cases := []stateTestCase{
		{StateTypeNone, `{}`, false},
		{StateTypeNone, `{"states":{}}`, true}, // should be empty
		{StateTypeNone, `{"states":[]}`, true}, // should be empty
		{StateTypeNone, `{"foo"}`, true},       // should be empty

		{StateTypeThreshold, `{"states":[]}`, false},
		{StateTypeThreshold, `{"states":[{}]}`, false},
		{StateTypeThreshold, `{"states":{}}`, true}, // states should be an array
		{StateTypeThreshold, `{"states":[{"state":"critical"}]}`, false},
		{StateTypeThreshold, `{"states":[{"state":"critical","match":"all","conditions":[{"field":"field_name","operator":"lt","value":42,"reason":"reason of failure"}]}]}`, false},
		{StateTypeThreshold, `{"states":[{"state":"critical","match":"all","conditions":[{"field":"field_name","operator":"lt","value":42,"reason":"reason of failure"},{"field":"field_name","operator":"lt","value":-42,"reason":"reason of failure"}]}]}`, false},
		{StateTypeThreshold, `{"states":[{"state":"critical","match":"all","conditions":[{"field":"field_name","operator":"lt","value":42,"reason":"reason of failure"}]},{"state":"warning","match":"any","conditions":[{"field":"field_name","operator":"le","value":42,"reason":"reason of failure"}]}]}`, false},

		{StateTypeHistory, `{"states":[]}`, false},
		{StateTypeHistory, `{"states":[],"interval":"0"}`, false},
		{StateTypeHistory, `{"states":[],"interval":0}`, true}, // interval should be a String
		{StateTypeHistory, `{"states":[{}]}`, false},
		{StateTypeHistory, `{"states":{}}`, true}, // states should be an array
		{StateTypeHistory, `{"states":[{"state":"critical"}]}`, false},
		{StateTypeHistory, `{"states":[{"state":"critical","match":"all","conditions":[{"field":"field_name","operator":"lt","value":42,"reason":"reason of failure"}]}]}`, false},
		{StateTypeHistory, `{"states":[{"state":"critical","match":"all","conditions":[{"field":"field_name","operator":"lt","value":42,"reason":"reason of failure"},{"field":"field_name","operator":"lt","value":-42,"reason":"reason of failure"}]}]}`, false},
		{StateTypeHistory, `{"states":[{"state":"critical","match":"all","conditions":[{"field":"field_name","operator":"lt","value":42,"reason":"reason of failure"}]},{"state":"warning","match":"any","conditions":[{"field":"field_name","operator":"le","value":42,"reason":"reason of failure"}]}]}`, false},

		{StateTypeTrend, `{"states":[]}`, false},
		{StateTypeTrend, `{"states":[],"interval":"0"}`, false},
		{StateTypeTrend, `{"states":[],"interval":0}`, true}, // interval should be a String
		{StateTypeTrend, `{"states":[{}]}`, false},
		{StateTypeTrend, `{"states":{}}`, true}, // states should be an array
		{StateTypeTrend, `{"states":[{"state":"critical"}]}`, false},
		{StateTypeTrend, `{"states":[{"state":"critical","match":"all","conditions":[{"field":"field_name","operator":"lt","value":42,"reason":"reason of failure"}]}]}`, false},
		{StateTypeTrend, `{"states":[{"state":"critical","match":"all","conditions":[{"field":"field_name","operator":"lt","value":42,"reason":"reason of failure"},{"field":"field_name","operator":"lt","value":-42,"reason":"reason of failure"}]}]}`, false},
		{StateTypeTrend, `{"states":[{"state":"critical","match":"all","conditions":[{"field":"field_name","operator":"lt","value":42,"reason":"reason of failure"}]},{"state":"warning","match":"any","conditions":[{"field":"field_name","operator":"le","value":42,"reason":"reason of failure"}]}]}`, false},
	}

	for _, c := range cases {
		err := ValidateStateConfigurationFormat(c.sType, c.sConf)

		if !c.err {
			// expect no error
			if err != nil {
				t.Errorf("Unexpected error during validation (type: %s, conf: %s): %s", c.sType, c.sConf, err)
			}
		} else {
			// expect error
			if err == nil {
				t.Errorf("Expected error validating type %s for conf %s", c.sType, c.sConf)
			}
		}
	}
}
