// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package datapipe

import (
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"gitlab.com/pantomath-io/panto/api"
	"gitlab.com/pantomath-io/panto/server/tsdb"
	"gitlab.com/pantomath-io/panto/util"
)

// Trend is the States definition for a trend check
type Trend struct {
	Interval string `json:"interval"`
	States   []struct {
		State      string `json:"state"`
		Match      match  `json:"match"`
		Conditions []struct {
			Field      string      `json:"field"`
			Operator   operator    `json:"operator"`
			Difference interface{} `json:"value"`
			Reason     string      `json:"reason"`
		} `json:"conditions"`
		Tags []struct {
			Name     string   `json:"name"`
			Operator operator `json:"operator"`
			Value    string   `json:"value"`
		} `json:"tags"`
	} `json:"states"`
}

// checkResultTrend tests the alert status (ok, warn, crit)
func checkResultTrend(jsonDef []byte, result tsdb.Result, ts tsdb.TSDB) (*State, error) {
	var trendDef Trend

	// Check input
	if len(jsonDef) == 0 {
		return nil, fmt.Errorf("empty reference")
	}
	if len(result.Fields) == 0 {
		return nil, fmt.Errorf("empty fields")
	}

	err := json.Unmarshal(jsonDef, &trendDef)
	if err != nil {
		return nil, fmt.Errorf("unable to parse JSON: %s", err)
	}

	results := make(map[string]*conditionResult)

	// Get reference trend value
	trendResult, err := getTrendResult(result, ts)
	if err != nil {
		return nil, err
	}
	if trendResult == nil {
		return &State{
			Value:  StateValue(api.State_UNKNOWN),
			Reason: "State could not be calculated: no previous value to compare to",
		}, nil
	}
	log.Debugf("Got previous value: %v", trendResult)

States:
	for _, state := range trendDef.States {
		if len(state.Conditions) == 0 {
			log.Debug("state has no conditions, skipping...")
			continue States
		}
		for _, tag := range state.Tags {
			v, ok := result.Tags[tag.Name]
			if !ok {
				continue States
			}
			ok, err = compareString(v, tag.Value, tag.Operator)
			if err != nil {
				log.Errorf("unable to evaluate tag condition: %s", err)
				continue States
			}
			if !ok {
				continue States
			}
		}
		condResult := make([]bool, len(state.Conditions))
		reasons := make([]string, 0, len(state.Conditions))
		for i, condition := range state.Conditions {
			t, err := computeTrend(result, *trendResult, condition.Field, trendDef.Interval)
			if err != nil {
				return nil, err
			}
			log.Debugf(
				"trend: (%s)%v -> (%s)%v = %v in %s",
				trendResult.Timestamp,
				trendResult.Fields[condition.Field],
				result.Timestamp,
				result.Fields[condition.Field],
				t,
				trendDef.Interval,
			)

			condResult[i], err = compareValues(t, condition.Difference, condition.Operator)
			if err != nil {
				return nil, err
			}
			if condResult[i] {
				reasons = append(reasons, condition.Reason)
			}
		}

		// Add the result array to the global map
		stateResult := &conditionResult{}
		switch state.Match {
		case MatchAny:
			stateResult.Result = any(condResult)
		case MatchAll:
			stateResult.Result = all(condResult)
		default:
			return nil, fmt.Errorf("Unable to cope Match configuration (%s)", state.Match)
		}
		stateResult.Reason = strings.Join(reasons, "\n")
		results[state.State] = stateResult
	}

	return combineStates(results)
}

// getTrendResult retrieves the most recent value up to and including the reference time minus the interval. If no such
// result has been found, `nil` is returned with no error.
func getTrendResult(r tsdb.Result, ts tsdb.TSDB) (*tsdb.Result, error) {
	results, err := ts.GetResults(
		r.Check,
		tsdb.Count(1),
		tsdb.OrderBy(tsdb.OrderDescending),
		tsdb.To(r.Timestamp),
		tsdb.Tag("agent", util.Base64Encode(r.Agent)),
	)

	if err != nil {
		return nil, fmt.Errorf("Couldn't retrieve results from TSDB: %s", err)
	}

	if len(results) == 0 {
		return nil, nil
	}

	return &results[0], nil
}

func computeTrend(result, trendResult tsdb.Result, field string, interval string) (res float64, err error) {
	intvl, err := time.ParseDuration(interval)
	if err != nil {
		return res, fmt.Errorf("Could not parse time interval \"%s\": %s", interval, err)
	}
	rField, ok := result.Fields[field]
	if !ok {
		return res, fmt.Errorf("unable to find the condition field in the new result")
	}
	tField, ok := trendResult.Fields[field]
	if !ok {
		return res, fmt.Errorf("unable to find the condition field in the trend result")
	}

	r, err := toFloat(rField)
	if err != nil {
		return
	}
	t, err := toFloat(tField)
	if err != nil {
		return
	}

	return (r - t) * (intvl.Seconds() / result.Timestamp.Sub(trendResult.Timestamp).Seconds()), nil
}

// toFloat converts any number to float64
func toFloat(n interface{}) (float64, error) {
	switch n := n.(type) {
	case uint:
		return float64(n), nil
	case uint8:
		return float64(n), nil
	case uint16:
		return float64(n), nil
	case uint32:
		return float64(n), nil
	case uint64:
		return float64(n), nil
	case int:
		return float64(n), nil
	case int8:
		return float64(n), nil
	case int16:
		return float64(n), nil
	case int32:
		return float64(n), nil
	case int64:
		return float64(n), nil
	case float32:
		return float64(n), nil
	case float64:
		return n, nil
	}

	return 0.0, fmt.Errorf("not a numeric type")
}
