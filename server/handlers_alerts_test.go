// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"context"
	"path"
	"testing"

	"gitlab.com/pantomath-io/panto/api"
	"gitlab.com/pantomath-io/panto/util"
	"google.golang.org/genproto/protobuf/field_mask"
)

func TestListAlerts(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	s := server{}

	req := &api.ListAlertsRequest{
		Parent: path.Join(api.ResourceNameOrganization, util.Base64Encode(organization)),
	}
	res, err := s.ListAlerts(context.Background(), req)
	if err != nil {
		t.Fatalf("Failed to list alerts: %s", err)
	} else {
		if len(res.Alerts) != 10 {
			t.Fatalf("Unexpected alerts count: got %d, expected %d", len(res.Alerts), 10)
		}
		if res.NextPageToken != "" {
			t.Fatalf("Unexpected next page token: %s", res.NextPageToken)
		}
	}
}

func TestListAlertsWithNesting(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	s := server{}

	req := &api.ListAlertsRequest{
		Parent:       path.Join(api.ResourceNameOrganization, util.Base64Encode(organization)),
		ChildrenMask: &field_mask.FieldMask{Paths: []string{"checks", "channel"}},
	}
	res, err := s.ListAlerts(context.Background(), req)
	if err != nil {
		t.Fatalf("Failed to list alerts: %s", err)
	} else {
		if len(res.Alerts) != 10 {
			t.Fatalf("Unexpected alerts count: got %d, expected %d", len(res.Alerts), 10)
		} else {
			if res.Alerts[0].Children == nil {
				t.Fatalf("Expected children, got nothing ¯\\_(ツ)_/¯")
			}
			if len(res.Alerts[0].Children.Checks) == 0 {
				t.Fatalf("Expected children Checks, got nothing ¯\\_(ツ)_/¯")
			}
			const channelName = "organizations/KTCCC60hRgWDtGnLxb_I9Q/channels/AjBVqs9hTB2ph2EWyDoMuQ"
			if res.Alerts[0].Children.Channel == nil {
				t.Fatalf("expected Channel child")
			}
			if res.Alerts[0].Children.Channel.Name != channelName {
				t.Fatalf("expected %s, got %s", channelName, res.Alerts[0].Children.Channel.Name)
			}
		}
	}
}

func TestListAlertsPagination(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	s := server{}

	// Test pagination
	req := &api.ListAlertsRequest{
		Parent:   path.Join(api.ResourceNameOrganization, util.Base64Encode(organization)),
		PageSize: 4,
	}
	res, err := s.ListAlerts(context.Background(), req)
	if err != nil {
		t.Fatalf("Failed to list alerts: %s", err)
	} else {
		if len(res.Alerts) != 4 {
			t.Fatalf("Unexpected alerts count: got %d, expected %d", len(res.Alerts), 4)
		}
		if res.NextPageToken == "" {
			t.Fatalf("Missing next page token: %s", res.NextPageToken)
		}
	}

	// Page 2
	page2Token := res.NextPageToken
	req.PageToken = res.NextPageToken
	res, err = s.ListAlerts(context.Background(), req)
	if err != nil {
		t.Fatalf("Failed to list alerts: %s", err)
	} else {
		if len(res.Alerts) != 4 {
			t.Fatalf("Unexpected alerts count: got %d, expected %d", len(res.Alerts), 4)
		}
		if res.NextPageToken == "" {
			t.Fatalf("Missing next page token: %s", res.NextPageToken)
		}
	}

	// Page 3
	req.PageToken = res.NextPageToken
	res, err = s.ListAlerts(context.Background(), req)
	if err != nil {
		t.Fatalf("Failed to list alerts: %s", err)
	} else {
		if len(res.Alerts) != 2 {
			t.Fatalf("Unexpected alerts count: got %d, expected %d", len(res.Alerts), 2)
		}
		if res.NextPageToken != "" {
			t.Fatalf("Unexpected next page token: %s", res.NextPageToken)
		}
	}

	// Invalid page token
	req.PageToken = page2Token[1:]
	_, err = s.ListAlerts(context.Background(), req)
	if err == nil {
		t.Fatalf("Successfully listed alerts with invalid token")
	}

	// Invalid request
	req.PageSize = 10
	req.PageToken = page2Token
	_, err = s.ListAlerts(context.Background(), req)
	if err == nil {
		t.Fatalf("Successfully listed alerts with invalid request")
	}
}
