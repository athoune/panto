// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"context"
	"path"
	"testing"

	"google.golang.org/genproto/protobuf/field_mask"

	"gitlab.com/pantomath-io/panto/api"
	"gitlab.com/pantomath-io/panto/util"
)

func TestQueryStates(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	s := server{}

	// Test basic query
	req := &api.QueryStatesRequest{
		Parent: path.Join(api.ResourceNameOrganization, util.Base64Encode(organization)),
		ProbeConfigurations: []string{
			path.Join(
				api.ResourceNameOrganization, util.Base64Encode(organization),
				api.ResourceNameAgent, "oKuOJ7EaTuSke81z3lYWDQ",
				api.ResourceNameProbeConfiguration, "D0Wcyw6CTleaz5-nLLgv6Q"),
		},
		// PageSize :,
		// PageToken:,
	}
	res, err := s.QueryStates(context.Background(), req)
	if err != nil {
		t.Errorf("Failed to query states: %s", err)
	} else {
		if len(res.States) != 1 {
			t.Errorf("Unexpected states count: got %d, expected %d", len(res.States), 1)
		}
	}

	// Test basic query with inexistent Agent
	req = &api.QueryStatesRequest{
		Parent: path.Join(api.ResourceNameOrganization, util.Base64Encode(organization)),
		ProbeConfigurations: []string{
			path.Join(
				api.ResourceNameOrganization, util.Base64Encode(organization),
				api.ResourceNameAgent, "______________________",
				api.ResourceNameProbeConfiguration, "D0Wcyw6CTleaz5-nLLgv6Q"),
		},
		// PageSize :,
		// PageToken:,
	}
	_, err = s.QueryStates(context.Background(), req)
	if err == nil {
		t.Errorf("QueryStates on inexistent ProbeConfiguration should return an error")
	}

	// Test basic query with ProbeConfiguration without State
	req = &api.QueryStatesRequest{
		Parent: path.Join(api.ResourceNameOrganization, util.Base64Encode(organization)),
		ProbeConfigurations: []string{
			path.Join(
				api.ResourceNameOrganization, util.Base64Encode(organization),
				api.ResourceNameAgent, "oKuOJ7EaTuSke81z3lYWDQ",
				api.ResourceNameProbeConfiguration, "FTVGDoa1RVcoZoPjFTXsTA"),
		},
		// PageSize :,
		// PageToken:,
	}
	res, err = s.QueryStates(context.Background(), req)
	if err != nil {
		t.Errorf("Failed to query states: %s", err)
	} else {
		if len(res.States) > 0 {
			t.Errorf("Unexpected states count: got %d, expected %d", len(res.States), 0)
			t.Errorf("%v", res.States)
		}
	}

}

func TestQueryStatesWithoutProbeConfiguration(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	s := server{}

	// Test basic query
	req := &api.QueryStatesRequest{
		Parent: path.Join(api.ResourceNameOrganization, util.Base64Encode(organization)),
		// PageSize :,
		// PageToken:,
	}
	res, err := s.QueryStates(context.Background(), req)
	if err != nil {
		t.Errorf("Failed to query states: %s", err)
	} else {
		if len(res.States) < 1 {
			t.Errorf("Unexpected states count: got %d, expected more than %d", len(res.States), 1)
		}
	}
}

func TestQueryStatesWithoutProbeConfigurationPagination(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	s := server{}

	// Test basic query
	req := &api.QueryStatesRequest{
		Parent:   path.Join(api.ResourceNameOrganization, util.Base64Encode(organization)),
		PageSize: 1,
		// PageToken:,
	}
	res, err := s.QueryStates(context.Background(), req)
	if err != nil {
		t.Errorf("Failed to query states: %s", err)
	} else {
		if len(res.States) != 1 {
			t.Errorf("Unexpected states count: got %d, expected %d", len(res.States), 1)
		}
	}

	if res.NextPageToken == "" {
		t.Errorf("Pagination did not work: received empty token")
	}

	req = &api.QueryStatesRequest{
		Parent:    path.Join(api.ResourceNameOrganization, util.Base64Encode(organization)),
		PageSize:  1,
		PageToken: res.NextPageToken,
	}
	res, err = s.QueryStates(context.Background(), req)
	if err != nil {
		t.Errorf("Failed to query states: %s", err)
	} else {
		if len(res.States) != 1 {
			t.Errorf("Unexpected states count: got %d, expected %d", len(res.States), 1)
		}
	}

	if res.NextPageToken != "" {
		t.Errorf("Last pagination should be empty: %s", res.NextPageToken)
	}
}

func TestQueryStatesPagination(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	s := server{}

	// Test basic query
	req := &api.QueryStatesRequest{
		Parent: path.Join(api.ResourceNameOrganization, util.Base64Encode(organization)),
		ProbeConfigurations: []string{
			path.Join(
				api.ResourceNameOrganization, util.Base64Encode(organization),
				api.ResourceNameAgent, "oKuOJ7EaTuSke81z3lYWDQ",
				api.ResourceNameProbeConfiguration, "D0Wcyw6CTleaz5-nLLgv6Q"),
		},
		PageSize: 1,
		// PageToken:,
	}
	res, err := s.QueryStates(context.Background(), req)
	if err != nil {
		t.Fatalf("Failed to query states: %s", err)
	} else {
		if len(res.States) < 1 {
			t.Errorf("Unexpected states count: got %d, expected %d", len(res.States), 1)
		}
	}

	if res.NextPageToken != "" {
		t.Errorf("Pagination did not worked, next page token should be empty, %s", res.NextPageToken)
	}
}

func TestQueryStatesWithNesting(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	s := server{}

	{
		req := &api.QueryStatesRequest{
			Parent:   path.Join(api.ResourceNameOrganization, util.Base64Encode(organization)),
			PageSize: 1,
		}
		res, err := s.QueryStates(context.Background(), req)
		if err != nil {
			t.Errorf("Failed to query states: %s", err)
		} else {
			if len(res.States) != 1 {
				t.Errorf("Unexpected states count: got %d, expected %d", len(res.States), 1)
			} else if res.States[0].Children != nil {
				t.Errorf("Unexpected children, got %#v", res.States[0].Children)
			}
		}
	}

	{
		req := &api.QueryStatesRequest{
			Parent:       path.Join(api.ResourceNameOrganization, util.Base64Encode(organization)),
			ChildrenMask: &field_mask.FieldMask{Paths: []string{"agent"}},
			PageSize:     1,
		}
		res, err := s.QueryStates(context.Background(), req)
		if err != nil {
			t.Errorf("Failed to query states: %s", err)
		} else {
			if len(res.States) != 1 {
				t.Errorf("Unexpected states count: got %d, expected %d", len(res.States), 1)
			} else if res.States[0].Children == nil || res.States[0].Children.Agent == nil {
				t.Errorf("Unexpected children, got %#v", res.States[0].Children)
			}
		}
	}

	{
		req := &api.QueryStatesRequest{
			Parent:       path.Join(api.ResourceNameOrganization, util.Base64Encode(organization)),
			ChildrenMask: &field_mask.FieldMask{Paths: []string{"agent", "check", "probe_configuration"}},
			PageSize:     1,
		}
		res, err := s.QueryStates(context.Background(), req)
		if err != nil {
			t.Errorf("Failed to query states: %s", err)
		} else {
			if len(res.States) != 1 {
				t.Errorf("Unexpected states count: got %d, expected %d", len(res.States), 1)
			} else if res.States[0].Children == nil ||
				res.States[0].Children.Agent == nil ||
				res.States[0].Children.Check == nil ||
				res.States[0].Children.ProbeConfiguration == nil {
				t.Errorf("Unexpected children, got %#v", res.States[0].Children)
			}
		}
	}
}
