// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"context"
	"path"

	"gitlab.com/pantomath-io/panto/api"
	"gitlab.com/pantomath-io/panto/server/dbconf"
	"gitlab.com/pantomath-io/panto/server/services"
	"gitlab.com/pantomath-io/panto/server/tsdb"
	"gitlab.com/pantomath-io/panto/util"

	"github.com/golang/protobuf/ptypes"
	"github.com/google/uuid"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// QueryStates returns the latest states for an agent and a check
func (s *server) QueryStates(ctx context.Context, req *api.QueryStatesRequest) (*api.QueryStatesResponse, error) {
	if len(req.Parent) == 0 {
		log.Errorf("missing parent Organization")
		return nil, status.Error(codes.InvalidArgument, "Missing parent Organization name")
	}
	// get the organization UUID from the parent field
	organizationUUID, err := api.ParseNameOrganization(req.Parent)
	if err != nil {
		log.Errorf("couldn't parse parent Organization name: %s", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid parent Organization name")
	}

	// validate nested children mask
	acceptedChildren := []string{"agent", "check", "probe_configuration"}
	if req.ChildrenMask != nil {
		for _, child := range req.ChildrenMask.Paths {
			if !util.Contains(acceptedChildren, child) {
				log.Errorf("invalid child in children_mask: %s", child)
				return nil, status.Errorf(codes.InvalidArgument, "Invalid field in children_mask: %s", child)
			}
		}
	}

	// Get TSDB
	ts, err := services.GetTSDB()
	if err != nil {
		log.Errorf("couldn't find tsdb service: %s", err)
		return nil, status.Errorf(codes.Internal, "Internal error")
	}

	// Get DBConf
	db, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	// Parse Page Token
	var stateOffset int
	if req.PageToken != "" {
		valid, err := util.ValidatePageToken(req.PageToken, req, &stateOffset)
		if err != nil {
			log.Errorf("Failed to validate page token: %s", err)
			return nil, status.Errorf(codes.InvalidArgument, "Invalid page token")
		}
		if !valid {
			log.Errorf("Page token does not match request")
			return nil, status.Errorf(codes.InvalidArgument, "Page token does not match request")
		}
	}

	// Get page size
	pageSize := int(req.PageSize)
	if pageSize == 0 {
		pageSize = defaultPageSize
	} else if pageSize > maxPageSize {
		pageSize = maxPageSize
	}

	// Assume this is the last page
	nextPage := false

	// Get all relevant probe configurations
	var tpas []*dbconf.TargetProbeAgent
	if len(req.ProbeConfigurations) > 0 {
		tpas = make([]*dbconf.TargetProbeAgent, 0, len(req.ProbeConfigurations))

		for _, pc := range req.ProbeConfigurations {
			var oUUID, agentUUID, probeConfigurationUUID uuid.UUID
			oUUID, agentUUID, probeConfigurationUUID, err = api.ParseNameProbeConfiguration(pc)
			if err != nil {
				log.Errorf("couldn't parse ProbeConfiguration name: %s (%v)", err, pc)
				return nil, status.Error(codes.InvalidArgument, "Invalid Probe Configuration name")
			}

			if oUUID != organizationUUID {
				log.Errorf("Organization in the parent and the probe_configuration do not match: (parent)%s / (probeconf)%s)", organizationUUID, oUUID)
				return nil, status.Error(codes.InvalidArgument, "Probe Configuration not in Organization")
			}

			tpa, err := db.GetTargetProbeAgent(organizationUUID, agentUUID, probeConfigurationUUID)
			if err != nil {
				log.Errorf("couldn't get the target_probe_agent: %s", err)
				return nil, status.Error(codes.Internal, "Probe Configuration not found")
			}
			if tpa == nil {
				log.Warningf("couldn't find the probe_configuration %s", tpa)
				return nil, status.Errorf(codes.InvalidArgument, "Probe Configuration not found")
			}
			tpas = append(tpas, tpa)
		}
	} else {
		// No filter on ProbeConfiguration, get all the TargetProbeAgent from the Organization
		tpas, err = db.ListTargetProbeAgentsForOrganization(
			organizationUUID,
			[]uuid.UUID{},
			-1,
			0,
		)
		if err != nil {
			log.Errorf("couldn't get probe configurations: %s", err)
			return nil, status.Error(codes.Internal, "Probe Configuration not found")
		}
	}

	states := make([]*api.State, 0, pageSize)
	if len(tpas) > 0 {
		checks := make(map[uuid.UUID]struct{})
		agents := make(map[string]struct{})
		for _, tpa := range tpas {
			checks[tpa.TargetProbeUUID] = struct{}{}
			agents[util.Base64Encode(tpa.AgentUUID)] = struct{}{}
		}

		c := make([]uuid.UUID, 0, len(checks))
		for k := range checks {
			c = append(c, k)
		}
		a := make([]string, 0, len(agents))
		for k := range agents {
			a = append(a, k)
		}
		tsdbStates, err := ts.GetStates(
			c,
			tsdb.Count(1),
			tsdb.OrderBy(tsdb.OrderDescending),
			tsdb.GroupBy("agent"),
			tsdb.Tag("agent", a...),
		)
		if err != nil {
			log.Errorf("Unable to retrieve states from TSDB: %s", err)
			return nil, status.Error(codes.Internal, "No states found")
		}

		if len(tsdbStates) > stateOffset {
		States:
			for _, tsdbState := range tsdbStates[stateOffset:] {
				if len(states) == pageSize {
					nextPage = true
					break
				}
				stateOffset++

				ts, err := ptypes.TimestampProto(tsdbState.Timestamp)
				if err != nil {
					log.Warningf("Couldn't convert result timestamp %s to protobuf message: %s. Skipping state...", tsdbState.Timestamp, err)
					continue States
				}
				apiState := &api.State{
					ProbeConfiguration: path.Join(
						api.ResourceNameOrganization, util.Base64Encode(organizationUUID),
						api.ResourceNameAgent, util.Base64Encode(tsdbState.Agent),
						api.ResourceNameProbeConfiguration, util.Base64Encode(tsdbState.ProbeConfiguration),
					),
					Check: path.Join(
						api.ResourceNameOrganization, util.Base64Encode(organizationUUID),
						api.ResourceNameCheck, util.Base64Encode(tsdbState.Check),
					),
					Agent: path.Join(
						api.ResourceNameOrganization, util.Base64Encode(organizationUUID),
						api.ResourceNameAgent, util.Base64Encode(tsdbState.Agent),
					),
					Timestamp: ts,
					Type:      api.State_Type(tsdbState.State),
					Reason:    tsdbState.Reason,
				}

				if req.ChildrenMask != nil && len(req.ChildrenMask.Paths) > 0 {
					apiState.Children = &api.State_Children{}
					// HACK: this is a bit redundant as we already retrieved the ProbeConfigurations
					// from the DB earlier up. In the spirit of keeping code simple and avoiding repetition
					// we call the handler here.
					if util.Contains(req.ChildrenMask.Paths, "probe_configuration") {
						apiState.Children.ProbeConfiguration, err = s.GetProbeConfiguration(
							ctx,
							&api.GetProbeConfigurationRequest{Name: apiState.ProbeConfiguration},
						)
						if err != nil {
							log.Warningf("Couldn't retrieve probe configuration: %s. Skipping state...", err)
							continue States
						}
					}
					if util.Contains(req.ChildrenMask.Paths, "agent") {
						apiState.Children.Agent, err = s.GetAgent(ctx, &api.GetAgentRequest{Name: apiState.Agent})
						if err != nil {
							log.Warningf("Couldn't retrieve agent: %s. Skipping state...", err)
							continue States
						}
					}
					if util.Contains(req.ChildrenMask.Paths, "check") {
						apiState.Children.Check, err = s.GetCheck(ctx, &api.GetCheckRequest{Name: apiState.Check})
						if err != nil {
							log.Warningf("Couldn't retrieve check: %s. Skipping state...", err)
							continue States
						}
					}
				}

				states = append(states, apiState)
			}
		}
	}

	res := &api.QueryStatesResponse{
		States: states,
	}

	// Generate next page token and return current page
	if nextPage {
		// Generate next page token
		res.NextPageToken, err = util.NextPageToken(req, stateOffset)
		if err != nil {
			log.Warning("Failed to generate next page token")
			// Return values but don't set page token
			res.NextPageToken = ""
		}
	}

	return res, nil
}
