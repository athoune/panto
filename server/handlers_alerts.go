// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"context"
	"encoding/json"
	"path"

	"gitlab.com/pantomath-io/panto/api"
	"gitlab.com/pantomath-io/panto/server/dbconf"
	"gitlab.com/pantomath-io/panto/server/services"
	"gitlab.com/pantomath-io/panto/util"

	"github.com/golang/protobuf/ptypes/empty"
	"github.com/google/uuid"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// ListAlerts returns all the Alerts associated to the parent Organization
func (s *server) ListAlerts(ctx context.Context, req *api.ListAlertsRequest) (res *api.ListAlertsResponse, err error) {
	if len(req.Parent) == 0 {
		log.Errorf("missing parent Organization")
		return nil, status.Error(codes.InvalidArgument, "Missing parent Organization name")
	}

	var pageOffset uint64
	if req.PageToken != "" {
		valid, err := util.ValidatePageToken(req.PageToken, req, &pageOffset)
		if err != nil {
			log.Errorf("Failed to validate page token: %s", err)
			return nil, status.Errorf(codes.InvalidArgument, "Invalid page token")
		}
		if !valid {
			log.Errorf("Page token does not match request")
			return nil, status.Errorf(codes.InvalidArgument, "Page token does not match request")
		}
	}

	pageSize := req.PageSize
	if pageSize == 0 {
		pageSize = defaultPageSize
	} else if pageSize > maxPageSize {
		pageSize = maxPageSize
	}

	// get the organization UUID from the parent field
	organizationUUID, err := api.ParseNameOrganization(req.Parent)
	if err != nil {
		log.Errorf("couldn't parse parent Organization name: %s", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid parent Organization name")
	}

	// validate nested children mask
	acceptedChildren := []string{"checks", "channel"}
	if req.ChildrenMask != nil {
		for _, child := range req.ChildrenMask.Paths {
			if !util.Contains(acceptedChildren, child) {
				log.Errorf("invalid field in children_mask: %s", child)
				return nil, status.Errorf(codes.InvalidArgument, "Invalid field in children_mask: %s", child)
			}
		}
	}

	dbconf, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}
	dbAlerts, err := dbconf.ListAlerts(organizationUUID, pageSize+1, pageOffset)
	if err != nil {
		log.Errorf("failed to list Alerts: %s", err)
		return nil, status.Error(codes.NotFound, "No Alerts found")
	}

	var nextPage bool
	if int32(len(dbAlerts)) > pageSize {
		dbAlerts = dbAlerts[:pageSize]
		nextPage = true
	}

	var alerts []*api.Alert
Alerts:
	for _, alert := range dbAlerts {
		dbChecks, err := dbconf.ListTargetProbesByAlert(organizationUUID, alert.UUID, 0, 0)
		if err != nil {
			log.Errorf("unable to get Checks for the Alert: %s", err)
			continue
		}
		var checks []string
		for _, check := range dbChecks {
			checks = append(checks, path.Join(api.ResourceNameOrganization, util.Base64Encode(organizationUUID), api.ResourceNameCheck, util.Base64Encode(check.UUID)))
		}

		alertConfig, err := json.Marshal(alert.Configuration)
		if err != nil {
			log.Errorf("unable to marshal configuration to JSON: %s", err)
			continue Alerts
		}

		apiAlert := &api.Alert{
			Name:          path.Join(api.ResourceNameOrganization, util.Base64Encode(organizationUUID), api.ResourceNameAlert, util.Base64Encode(alert.UUID)),
			Checks:        checks,
			Channel:       path.Join(api.ResourceNameOrganization, util.Base64Encode(organizationUUID), api.ResourceNameChannel, util.Base64Encode(alert.ChannelUUID)),
			Type:          alert.Type,
			Configuration: string(alertConfig),
		}

		if req.ChildrenMask != nil && len(req.ChildrenMask.Paths) > 0 {
			apiAlert.Children = &api.Alert_Children{}
			if util.Contains(req.ChildrenMask.Paths, "checks") {

				var checks []*api.Check
				for _, check := range dbChecks {
					config, err := json.Marshal(check.StateConfiguration)
					if err != nil {
						log.Errorf("unable to marshal state configuration to JSON: %s", err)
						continue
					}
					checks = append(checks, &api.Check{
						Name:          path.Join(api.ResourceNameOrganization, util.Base64Encode(organizationUUID), api.ResourceNameCheck, util.Base64Encode(check.UUID)),
						Target:        path.Join(api.ResourceNameOrganization, util.Base64Encode(organizationUUID), api.ResourceNameTarget, util.Base64Encode(check.TargetUUID)),
						Probe:         path.Join(api.ResourceNameProbe, util.Base64Encode(check.ProbeUUID)),
						Type:          check.StateType,
						Configuration: string(config),
					})
				}

				apiAlert.Children.Checks = checks
			}
			if util.Contains(req.ChildrenMask.Paths, "channel") {
				apiAlert.Children.Channel, err = s.GetChannel(ctx, &api.GetChannelRequest{Name: apiAlert.Channel})
				if err != nil {
					return nil, err
				}
			}
		}

		alerts = append(alerts, apiAlert)
	}

	res = &api.ListAlertsResponse{
		Alerts: alerts,
	}

	if nextPage {
		res.NextPageToken, err = util.NextPageToken(req, pageOffset+uint64(len(alerts)))
		if err != nil {
			log.Warning("Failed to generate next page token")
			// Return values but don't set page token
			res.NextPageToken = ""
		}
	}

	return res, nil
}

// GetAlert returns a specific Alert
func (s *server) GetAlert(ctx context.Context, req *api.GetAlertRequest) (res *api.Alert, err error) {
	if len(req.Name) == 0 {
		log.Errorf("missing Alert name")
		return nil, status.Error(codes.InvalidArgument, "Missing Alert name")
	}

	// get the organization and agent UUIDs from the name field
	organizationUUID, alertUUID, err := api.ParseNameAlert(req.Name)
	if err != nil {
		log.Errorf("couldn't parse Alert name: %s", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid Alert name")
	}

	// validate nested children mask
	acceptedChildren := []string{"checks", "channel"}
	if req.ChildrenMask != nil {
		for _, child := range req.ChildrenMask.Paths {
			if !util.Contains(acceptedChildren, child) {
				log.Errorf("invalid field in children_mask: %s", child)
				return nil, status.Errorf(codes.InvalidArgument, "Invalid field in children_mask: %s", child)
			}
		}
	}

	dbconf, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}
	dbAlert, err := dbconf.GetAlert(organizationUUID, alertUUID)
	if err != nil {
		log.Errorf("failed to get Alert: %s", err)
		return nil, status.Error(codes.Internal, "Alert not found")
	}
	if dbAlert == nil {
		log.Errorf("alert not found: %s", req.Name)
		return nil, status.Error(codes.NotFound, "Alert not found")
	}

	dbChecks, err := dbconf.ListTargetProbesByAlert(organizationUUID, alertUUID, 0, 0)
	if err != nil {
		log.Errorf("unable to get Checks for the Alert: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}
	checks := make([]string, 0, len(dbChecks))
	for _, check := range dbChecks {
		checks = append(checks, path.Join(api.ResourceNameOrganization, util.Base64Encode(organizationUUID), api.ResourceNameCheck, util.Base64Encode(check.UUID)))
	}

	alertConfig, err := json.Marshal(dbAlert.Configuration)
	if err != nil {
		log.Errorf("unable to marshal configuration to JSON: %s", err)
	}

	res = &api.Alert{
		Name:          path.Join(api.ResourceNameOrganization, util.Base64Encode(organizationUUID), api.ResourceNameAlert, util.Base64Encode(dbAlert.UUID)),
		Checks:        checks,
		Channel:       path.Join(api.ResourceNameOrganization, util.Base64Encode(organizationUUID), api.ResourceNameChannel, util.Base64Encode(dbAlert.ChannelUUID)),
		Type:          dbAlert.Type,
		Configuration: string(alertConfig),
	}

	if req.ChildrenMask != nil && len(req.ChildrenMask.Paths) > 0 {
		res.Children = &api.Alert_Children{}
		if util.Contains(req.ChildrenMask.Paths, "checks") {

			var checks []*api.Check
			for _, check := range dbChecks {
				config, err := json.Marshal(check.StateConfiguration)
				if err != nil {
					log.Errorf("unable to marshal state configuration to JSON: %s", err)
					continue
				}
				checks = append(checks, &api.Check{
					Name:          path.Join(api.ResourceNameOrganization, util.Base64Encode(organizationUUID), api.ResourceNameCheck, util.Base64Encode(check.UUID)),
					Target:        path.Join(api.ResourceNameOrganization, util.Base64Encode(organizationUUID), api.ResourceNameTarget, util.Base64Encode(check.TargetUUID)),
					Probe:         path.Join(api.ResourceNameProbe, util.Base64Encode(check.ProbeUUID)),
					Type:          check.StateType,
					Configuration: string(config),
				})
			}

			res.Children.Checks = checks
		}
		if util.Contains(req.ChildrenMask.Paths, "channel") {
			res.Children.Channel, err = s.GetChannel(ctx, &api.GetChannelRequest{Name: res.Channel})
			if err != nil {
				return nil, err
			}
		}
	}

	return
}

// CreateAlert insert a new Alert in an Organization
func (s *server) CreateAlert(ctx context.Context, req *api.CreateAlertRequest) (res *api.Alert, err error) {
	if len(req.Parent) == 0 {
		log.Errorf("missing parent Organization name")
		return nil, status.Error(codes.InvalidArgument, "Missing parent Organization name")
	}

	// get the organization UUID from the parent field
	organizationUUID, err := api.ParseNameOrganization(req.Parent)
	if err != nil {
		log.Errorf("couldn't parse parent Organization name: %s", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid parent Organization name")
	}

	db, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	if req.GetAlert() == nil {
		log.Errorf("missing Alert object in request")
		return nil, status.Error(codes.InvalidArgument, "Missing Alert")
	}

	if len(req.Alert.GetChecks()) == 0 {
		log.Errorf("missing Check in alert object in request")
		return nil, status.Error(codes.InvalidArgument, "Missing Check")
	}

	checkUUIDs := make([]uuid.UUID, len(req.Alert.GetChecks()))
	for i, check := range req.Alert.GetChecks() {
		_, checkUUID, err := api.ParseNameCheck(check)
		if err != nil {
			log.Errorf("couldn't parse Check name: %s", err)
			return nil, status.Error(codes.InvalidArgument, "Invalid Check name")
		}
		checkUUIDs[i] = checkUUID
	}

	if len(req.Alert.GetChannel()) == 0 {
		log.Errorf("missing Channel in alert object in request")
		return nil, status.Error(codes.InvalidArgument, "Missing Channel")
	}

	_, channelUUID, err := api.ParseNameChannel(req.Alert.Channel)
	if err != nil {
		log.Errorf("couldn't parse Channel name: %s", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid Channel name")
	}

	if len(req.Alert.GetType()) == 0 {
		log.Errorf("missing definition in alert object in request")
		return nil, status.Error(codes.InvalidArgument, "Missing Alert type")
	}

	var alertConfig map[string]interface{}
	if err = json.Unmarshal([]byte(req.Alert.GetConfiguration()), &alertConfig); err != nil {
		log.Errorf("couldn't unmarshal JSON configuration: %s", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid Alert configuration")
	}

	dbAlert, err := db.CreateAlert(&dbconf.Alert{
		OrganizationUUID: organizationUUID,
		ChannelUUID:      channelUUID,
		Type:             req.Alert.GetType(),
		Configuration:    alertConfig,
	})
	if err != nil {
		log.Errorf("failed to create Alert: %s", err)
		return nil, status.Error(codes.Internal, "Couldn't create Alert")
	}

	err = db.LinkTargetProbesToAlert(false, organizationUUID, dbAlert.UUID, checkUUIDs...)
	if err != nil {
		log.Errorf("unable to get Checks for the Alert: %s", err)
		return nil, status.Error(codes.Internal, "Couldn't connect Alert to Checks")
	}
	dbChecks, err := db.ListTargetProbesByAlert(organizationUUID, dbAlert.UUID, 0, 0)
	if err != nil {
		log.Errorf("unable to get Checks for the Alert: %s", err)
		return nil, status.Error(codes.Internal, "No Checks found")
	}
	checks := make([]string, len(dbChecks))
	for i, curcheck := range dbChecks {
		checks[i] = path.Join(
			api.ResourceNameOrganization, util.Base64Encode(organizationUUID),
			api.ResourceNameCheck, util.Base64Encode(curcheck.UUID),
		)
	}

	dbalertConfig, err := json.Marshal(dbAlert.Configuration)
	if err != nil {
		log.Errorf("unable to marshal configuration to JSON: %s", err)
	}

	res = &api.Alert{
		Name:          path.Join(api.ResourceNameOrganization, util.Base64Encode(organizationUUID), api.ResourceNameAlert, util.Base64Encode(dbAlert.UUID)),
		Checks:        checks,
		Channel:       path.Join(api.ResourceNameOrganization, util.Base64Encode(organizationUUID), api.ResourceNameChannel, util.Base64Encode(dbAlert.ChannelUUID)),
		Type:          dbAlert.Type,
		Configuration: string(dbalertConfig),
	}

	return
}

// UpdateAlert update an existing Alert
func (s *server) UpdateAlert(ctx context.Context, req *api.UpdateAlertRequest) (res *api.Alert, err error) {
	if len(req.Alert.Name) == 0 {
		log.Errorf("missing Alert name")
		return nil, status.Error(codes.InvalidArgument, "Missing Alert name")
	}

	// get the organization and agent UUIDs from the name field
	organizationUUID, alertUUID, err := api.ParseNameAlert(req.Alert.Name)
	if err != nil {
		log.Errorf("couldn't parse Alert name: %s", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid Alert name")
	}

	db, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	var checkUUIDs []uuid.UUID

	updateAlert := new(dbconf.Alert)
	updateAlert.UUID = alertUUID
	updateAlert.OrganizationUUID = organizationUUID

	if req.UpdateMask != nil && util.Contains(req.UpdateMask.Paths, "name") {
		return nil, status.Error(codes.InvalidArgument, "Alert name is read-only")
	}

	if req.UpdateMask == nil || util.Contains(req.UpdateMask.Paths, "channel") {
		_, channelUUID, err := api.ParseNameChannel(req.Alert.Channel)
		if err != nil {
			log.Errorf("unable to extract channel UUID: %s", err)
			return nil, status.Error(codes.InvalidArgument, "Invalid Channel name")
		}
		updateAlert.ChannelUUID = channelUUID
	}

	if req.UpdateMask == nil || util.Contains(req.UpdateMask.Paths, "type") {
		if req.Alert.GetType() == "" {
			return nil, status.Error(codes.InvalidArgument, "Missing Alert type")
		}
		updateAlert.Type = req.Alert.GetType()
	}

	if req.UpdateMask == nil || util.Contains(req.UpdateMask.Paths, "checks") {
		for _, check := range req.Alert.GetChecks() {
			_, checkUUID, err := api.ParseNameCheck(check)
			if err != nil {
				log.Errorf("unable to extract UUID from check name (%s)", err)
				return nil, status.Error(codes.InvalidArgument, "Invalid Check name")
			}
			checkUUIDs = append(checkUUIDs, checkUUID)
		}
	}

	if req.UpdateMask == nil || util.Contains(req.UpdateMask.Paths, "configuration") {
		var alertConfig map[string]interface{}
		if err = json.Unmarshal([]byte(req.Alert.GetConfiguration()), &alertConfig); err != nil {
			log.Errorf("couldn't unmarshal JSON configuration: %s", err)
			return nil, status.Error(codes.InvalidArgument, "Invalid Alert configuration")
		}
		updateAlert.Configuration = alertConfig
	}

	dbAlert, err := db.UpdateAlert(updateAlert)
	if err != nil {
		log.Errorf("failed to update Alert: %s", err)
		return nil, status.Error(codes.Internal, "Couldn't update Alert")
	}

	if len(checkUUIDs) > 0 {
		err := db.LinkTargetProbesToAlert(true, organizationUUID, dbAlert.UUID, checkUUIDs...)
		if err != nil {
			log.Errorf("unable to get Checks for the Alert: %s", err)
			return nil, status.Error(codes.Internal, "No Checks found")
		}
	}

	dbChecks, err := db.ListTargetProbesByAlert(organizationUUID, dbAlert.UUID, 0, 0)
	if err != nil {
		log.Errorf("unable to get Checks for the Alert: %s", err)
		return nil, status.Error(codes.Internal, "No Checks found")
	}
	checks := make([]string, len(dbChecks))
	for i, curcheck := range dbChecks {
		checks[i] = path.Join(
			api.ResourceNameOrganization, util.Base64Encode(organizationUUID),
			api.ResourceNameCheck, util.Base64Encode(curcheck.UUID),
		)
	}

	dbalertConfig, err := json.Marshal(dbAlert.Configuration)
	if err != nil {
		log.Errorf("unable to marshal configuration to JSON: %s", err)
	}

	res = &api.Alert{
		Name:          path.Join(api.ResourceNameOrganization, util.Base64Encode(organizationUUID), api.ResourceNameAlert, util.Base64Encode(dbAlert.UUID)),
		Checks:        checks,
		Channel:       path.Join(api.ResourceNameOrganization, util.Base64Encode(organizationUUID), api.ResourceNameChannel, util.Base64Encode(dbAlert.ChannelUUID)),
		Type:          dbAlert.Type,
		Configuration: string(dbalertConfig),
	}

	return
}

// DeleteAlert delets an existing Alert
func (s *server) DeleteAlert(ctx context.Context, req *api.DeleteAlertRequest) (res *empty.Empty, err error) {
	if len(req.Name) == 0 {
		log.Errorf("missing alert name")
		return nil, status.Error(codes.InvalidArgument, "Missing Alert name")
	}

	// get the organization and alert UUIDs from the name field
	organizationUUID, alertUUID, err := api.ParseNameAlert(req.Name)
	if err != nil {
		log.Errorf("couldn't parse A1ert name: %s", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid Alert name")
	}

	db, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	err = db.DeleteAlert(organizationUUID, alertUUID)
	if err != nil {
		log.Errorf("failed to delete Alert: %s", err)
		return nil, status.Error(codes.Internal, "Couldn't delete Alert")
	}

	res = &empty.Empty{}
	return
}
