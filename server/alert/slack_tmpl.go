// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package alert

const slackTemplateString = `{
	"username": "Panto",
{{if .OK -}}"icon_emoji": ":100:",
{{- else if .Critical -}}"icon_emoji": ":rotating_light:",
{{- else if .Warning -}}"icon_emoji": ":warning:",
{{- else if .MissingData -}}"icon_emoji": ":grey_question:",
{{- else -}}"icon_url": "https://i.imgur.com/LJBqo6z.png",
{{- end}}
	"attachments" : [
		{
			"title": "{{js .Title}}",
			{{if .OK -}}"color": "good",
			{{- else if .Critical -}}"color": "danger",
			{{- else if .Warning -}}"color": "warning",
			{{- end}}
			"fields": [
				{"title":"Target","value":"{{js .Target}}","short":true},
				{"title":"Probe","value":"{{js .Probe}}","short":true},
				{"title":"Agent","value":"{{js .Agent}}","short":true},
				{"title":"Message","value":"{{js .Reason}}","short":false}
			],
			"thumb_url": "https://i.imgur.com/LJBqo6z.png",
			"footer":"Panto by Pantomath",
			"footer_icon":"https://i.imgur.com/llkfZpG.png"
		}
	]
}`
