// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package alert

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"testing"

	"github.com/google/uuid"
)

func TestSlackTemplate(t *testing.T) {
	slackInit()
	if slackTemplate == nil {
		t.Fatal("Couldn't initialize slack alert template")
	}

	vars := TemplateVars{
		Title:    "CRITICAL alert",
		Target:   uuid.New().String(),
		Agent:    uuid.New().String(),
		Probe:    uuid.New().String(),
		State:    "CRITICAL",
		Reason:   "Test alert\nReason line 2",
		Message:  "Test alert is critical\nMessage line 2",
		Critical: true,
		Warning:  false,
	}
	var message bytes.Buffer
	err := slackTemplate.Execute(&message, vars)
	if err != nil {
		t.Fatalf("Couldn't execute slack alert template: %s", err)
	}
	ioutil.WriteFile("/tmp/panto-slack-alert.json", message.Bytes(), 0666)
	var m map[string]interface{}
	err = json.Unmarshal(message.Bytes(), &m)
	if err != nil {
		t.Fatalf("message is not valid JSON after template execution: %s", err)
	}
}
