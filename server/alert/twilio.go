// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package alert

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"sync"
	"text/template"
)

var twilioTemplate *template.Template
var twilioOnce sync.Once

func twilioInit() {
	var err error
	twilioTemplate = template.New("twilio")
	twilioTemplate, err = twilioTemplate.Parse(twilioTemplateString)
	if err != nil {
		log.Critical("Unable to parse Twilio template, no SMS alert will be sent")
		twilioTemplate = nil
	}
}

// SendTwilio post a message on a Twilio webhook
func SendTwilio(config map[string]interface{}, vars TemplateVars) error {
	twilioOnce.Do(twilioInit)

	if twilioTemplate == nil {
		log.Critical("Unable to parse Twilio template, no SMS alert will be sent")
		return fmt.Errorf("no Twilio template")
	}

	var message strings.Builder
	err := twilioTemplate.Execute(&message, vars)
	if err != nil {
		return fmt.Errorf("unable to execute template: %s", err)
	}

	accountSid, ok := config["account-sid"]
	if !ok {
		return fmt.Errorf("unable to get account SID from config")
	}
	accountSidStr, ok := accountSid.(string)
	if !ok {
		return fmt.Errorf("account SID is not a string")
	}

	token, ok := config["token"]
	if !ok {
		return fmt.Errorf("unable to get auth token from config")
	}
	tokenStr, ok := token.(string)
	if !ok {
		return fmt.Errorf("auth token is not a string")
	}

	var fromStr string
	if from, ok := config["from"]; ok {
		fromStr, ok = from.(string)
		if !ok {
			return fmt.Errorf("\"from\" is not a string")
		}
	}

	var messagingServiceSidStr string
	if messagingServiceSid, ok := config["messaging-service-sid"]; ok {
		messagingServiceSidStr, ok = messagingServiceSid.(string)
		if !ok {
			return fmt.Errorf("messaging service SID is not a string")
		}
	}

	if fromStr == "" && messagingServiceSidStr == "" {
		return fmt.Errorf("unable to get \"from\" number or messaging service SID from config")
	}

	to, ok := config["to"]
	if !ok {
		return fmt.Errorf("unable to get \"to\" number from config")
	}
	toStr, ok := to.(string)
	if !ok {
		return fmt.Errorf("\"to\" number is not a string")
	}

	form := url.Values{}
	if fromStr != "" {
		form.Set("From", fromStr)
	} else {
		form.Set("MessagingServiceSid", messagingServiceSidStr)
	}
	form.Set("To", toStr)
	form.Set("Body", message.String())

	req, err := http.NewRequest(
		"POST",
		"https://api.twilio.com/2010-04-01/Accounts/"+accountSidStr+"/Messages",
		strings.NewReader(form.Encode()),
	)
	if err != nil {
		return fmt.Errorf("unable to prepare POST request: %s", err)
	}
	req.SetBasicAuth(accountSidStr, tokenStr)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return fmt.Errorf("unable to send message to Twilio: %s", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusCreated {
		body, err := ioutil.ReadAll(resp.Body)
		if err == nil {
			log.Debugf("Twilio alert returned: %s", body)
		}
		return fmt.Errorf("error while POSTing Twilio alert: %s", resp.Status)
	}

	return nil
}
