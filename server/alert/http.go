// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package alert

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

// SendHTTP posts an alert on a custom HTTP endpoint
func SendHTTP(config map[string]interface{}, vars TemplateVars) error {
	msg, err := json.Marshal(vars)
	if err != nil {
		return fmt.Errorf("unable to marshal JSON: %s", err)
	}

	url, ok := config["url"]
	if !ok {
		return fmt.Errorf("missing URL in HTTP alert configuration")
	}
	urlStr, ok := url.(string)
	if !ok {
		return fmt.Errorf("URL is not a string")
	}

	req, err := http.NewRequest("POST", urlStr, bytes.NewBuffer(msg))
	if err != nil {
		return fmt.Errorf("unable to prepare POST request: %s", err)
	}
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return fmt.Errorf("unable to POST to URL: %s", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		body, err := ioutil.ReadAll(resp.Body)
		if err == nil {
			log.Debugf("Alert POST returned: %s", body)
		}
		return fmt.Errorf("error while POSTing HTTP alert: %s", resp.Status)
	}

	return nil
}
