// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package alert

import (
	"crypto/tls"
	"fmt"
	"html/template"
	"strings"
	"sync"

	"github.com/spf13/viper"
	gomail "gopkg.in/gomail.v2"
)

var emailTemplate *template.Template
var emailOnce sync.Once

func emailInit() {
	var err error
	emailTemplate = template.New("email")
	emailTemplate, err = emailTemplate.Parse(emailTemplateString)
	if err != nil {
		log.Critical("Unable to parse email template, no email alert will be sent")
		emailTemplate = nil
	}
}

// SendEmail dispatches an alert by e-mail
func SendEmail(config map[string]interface{}, vars TemplateVars) error {
	emailOnce.Do(emailInit)

	if emailTemplate == nil {
		log.Critical("Unable to parse email template, no email alert will be sent")
		return fmt.Errorf("no email template")
	}

	var message strings.Builder
	err := emailTemplate.Execute(&message, vars)
	if err != nil {
		return fmt.Errorf("unable to execute template: %s", err)
	}

	dest, ok := config["dest"]
	if !ok {
		return fmt.Errorf("unable to get config for mail destination")
	}
	destStr, ok := dest.(string)
	if !ok {
		return fmt.Errorf("destination is not a string")
	}

	m := gomail.NewMessage()
	m.SetHeader("From", viper.GetString("smtp.from"))
	m.SetHeader("To", destStr)
	m.SetHeader("Subject", vars.Title+" on "+vars.Target)
	m.SetBody("text/html", message.String())

	// optional CC mail
	ccmail, ok := config["cc"]
	if ok {
		ccmailStr, ok := ccmail.(string)
		if !ok {
			return fmt.Errorf("Cc: field is not a string")
		}
		m.SetAddressHeader("Cc", ccmailStr, "")
	}

	d := gomail.NewDialer(
		viper.GetString("smtp.server"),
		viper.GetInt("smtp.port"),
		viper.GetString("smtp.username"),
		viper.GetString("smtp.password"),
	)
	d.TLSConfig = &tls.Config{
		InsecureSkipVerify: true,
		ServerName:         viper.GetString("smtp.server"),
	}

	return d.DialAndSend(m)
}
