// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package alert

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"sync"
	"text/template"
)

var slackTemplate *template.Template
var slackOnce sync.Once

func slackInit() {
	var err error
	slackTemplate = template.New("slack")
	slackTemplate, err = slackTemplate.Parse(slackTemplateString)
	if err != nil {
		log.Critical("Unable to parse Slack template, no Slack alert will be sent")
		slackTemplate = nil
	}
}

// SendSlack post a message on a Slack webhook
func SendSlack(config map[string]interface{}, vars TemplateVars) error {
	slackOnce.Do(slackInit)

	if slackTemplate == nil {
		log.Critical("Unable to parse Slack template, no Slack alert will be sent")
		return fmt.Errorf("no Slack template")
	}

	var message bytes.Buffer
	err := slackTemplate.Execute(&message, vars)
	if err != nil {
		return fmt.Errorf("unable to execute template: %s", err)
	}

	webhookURL, ok := config["webhook_url"]
	if !ok {
		return fmt.Errorf("unable to get config for Slack webhook URL")
	}
	webhookURLStr, ok := webhookURL.(string)
	if !ok {
		return fmt.Errorf("webhook URL is not a string")
	}

	req, err := http.NewRequest("POST", webhookURLStr, &message)
	if err != nil {
		return fmt.Errorf("unable to prepare POST request: %s", err)
	}
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return fmt.Errorf("unable to send message to Slack: %s", err)
	}
	defer resp.Body.Close()

	body, _ := ioutil.ReadAll(resp.Body)
	if string(body) != "ok" {
		return fmt.Errorf("error while sending message to Slack: %s", string(body))
	}

	return nil
}
