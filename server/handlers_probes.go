// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"context"
	"path"

	"gitlab.com/pantomath-io/panto/api"
	"gitlab.com/pantomath-io/panto/server/services"
	"gitlab.com/pantomath-io/panto/util"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// ListProbes returns all the probes
func (s *server) ListProbes(ctx context.Context, req *api.ListProbesRequest) (res *api.ListProbesResponse, err error) {
	var pageOffset uint64
	if req.PageToken != "" {
		valid, err := util.ValidatePageToken(req.PageToken, req, &pageOffset)
		if err != nil {
			log.Errorf("Failed to validate page token: %s", err)
			return nil, status.Errorf(codes.InvalidArgument, "Invalid page token")
		}
		if !valid {
			log.Errorf("Page token does not match request")
			return nil, status.Errorf(codes.InvalidArgument, "Page token does not match request")
		}
	}

	pageSize := req.PageSize
	if pageSize == 0 {
		pageSize = defaultPageSize
	} else if pageSize > maxPageSize {
		pageSize = maxPageSize
	}

	dbconf, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}
	dbprobes, err := dbconf.ListProbes(pageSize+1, pageOffset)
	if err != nil {
		log.Errorf("failed to list Probes: %s", err)
		return nil, status.Error(codes.NotFound, "No Probes found")
	}

	var nextPage bool
	if int32(len(dbprobes)) > pageSize {
		dbprobes = dbprobes[:pageSize]
		nextPage = true
	}

	probes := make([]*api.Probe, len(dbprobes))
	for i, probe := range dbprobes {
		// ConfigurationTemplate is a repeated field of type ProbeParameter
		curParameters := make([]*api.Probe_Parameter, len(probe.ConfigurationTemplate))
		for j, param := range probe.ConfigurationTemplate {
			curParameters[j] = &api.Probe_Parameter{
				Name:        param.Name,
				Type:        param.Type,
				Description: param.Description,
				Mandatory:   param.Mandatory,
			}
			if param.Placeholder != nil {
				curParameters[j].Placeholder, err = api.NewStruct(param.Placeholder)
				if err != nil {
					log.Warningf("%s", err)
					log.Warningf("invalid placeholder for parameter %s[%s]: %v", probe.Label, param.Name, param.Placeholder)
				}
			}
			if param.Default != nil {
				curParameters[j].Default, err = api.NewStruct(param.Default)
				if err != nil {
					log.Warningf("%s", err)
					log.Warningf("invalid default for parameter %s[%s]: %v", probe.Label, param.Name, param.Default)
				}
			}
		}
		metrics := make([]*api.Probe_Metric, len(probe.Metrics.Metrics))
		for j, m := range probe.Metrics.Metrics {
			metrics[j] = &api.Probe_Metric{
				Name:        m.Name,
				Type:        m.Type,
				Description: m.Description,
			}
		}
		tags := make([]*api.Probe_Tag, len(probe.Metrics.Tags))
		for j, t := range probe.Metrics.Tags {
			tags[j] = &api.Probe_Tag{
				Name:        t.Name,
				Description: t.Description,
			}
		}
		graphs := make([]*api.Probe_Graph, len(probe.Graphs.Graphs))
		for k, g := range probe.Graphs.Graphs {
			graphs[k] = &api.Probe_Graph{
				Title:      g.Title,
				Type:       g.Type,
				UnitFormat: g.Format,
				Metrics:    g.Metrics,
				Tags:       g.Tags,
			}
		}
		probes[i] = &api.Probe{
			Name:                  path.Join(api.ResourceNameProbe, util.Base64Encode(probe.UUID)),
			ProbeName:             probe.Label,
			DisplayName:           probe.DisplayName,
			Description:           probe.Description,
			ConfigurationTemplate: curParameters,
			Metrics:               metrics,
			Tags:                  tags,
			Graphs:                graphs,
		}
	}

	res = &api.ListProbesResponse{
		Probes: probes,
	}

	if nextPage {
		res.NextPageToken, err = util.NextPageToken(req, pageOffset+uint64(len(probes)))
		if err != nil {
			log.Warning("Failed to generate next page token")
			// Return values but don't set page token
			res.NextPageToken = ""
		}
	}

	return
}

// GetProbe returns a named Probe resource
func (s *server) GetProbe(ctx context.Context, req *api.GetProbeRequest) (*api.Probe, error) {
	if len(req.GetName()) == 0 {
		log.Errorf("missing Probe name")
		return nil, status.Error(codes.InvalidArgument, "Missing Probe name")
	}

	// get the probe UUID from the name
	probeUUID, err := api.ParseNameProbe(req.Name)
	if err != nil {
		log.Errorf("couldn't parse Probe name: %s", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid Probe name")
	}

	dbconf, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}
	probe, err := dbconf.GetProbe(probeUUID)
	if err != nil {
		log.Errorf("failed to get probe %s (%s)", probeUUID, err)
		return nil, status.Error(codes.Internal, "Probe not found")
	}
	if probe == nil {
		log.Errorf("probe not found: %s", req.Name)
		return nil, status.Error(codes.NotFound, "Probe not found")
	}

	// ConfigurationTemplate is a repeated field of type ProbeParameter
	curParameters := make([]*api.Probe_Parameter, len(probe.ConfigurationTemplate))
	for i, param := range probe.ConfigurationTemplate {
		curParameters[i] = &api.Probe_Parameter{
			Name:        param.Name,
			Type:        param.Type,
			Description: param.Description,
			Mandatory:   param.Mandatory,
		}
		if param.Placeholder != nil {
			curParameters[i].Placeholder, err = api.NewStruct(param.Placeholder)
			if err != nil {
				log.Warningf("%s", err)
				log.Warningf("invalid placeholder for parameter %s[%s]: %v", probe.Label, param.Name, param.Placeholder)
			}
		}
		if param.Default != nil {
			curParameters[i].Default, err = api.NewStruct(param.Default)
			if err != nil {
				log.Warningf("%s", err)
				log.Warningf("invalid default for parameter %s[%s]: %v", probe.Label, param.Name, param.Default)
			}
		}
	}
	metrics := make([]*api.Probe_Metric, len(probe.Metrics.Metrics))
	for i, m := range probe.Metrics.Metrics {
		metrics[i] = &api.Probe_Metric{
			Name:        m.Name,
			Type:        m.Type,
			Description: m.Description,
		}
	}
	tags := make([]*api.Probe_Tag, len(probe.Metrics.Tags))
	for i, t := range probe.Metrics.Tags {
		tags[i] = &api.Probe_Tag{
			Name:        t.Name,
			Description: t.Description,
		}
	}
	graphs := make([]*api.Probe_Graph, len(probe.Graphs.Graphs))
	for k, g := range probe.Graphs.Graphs {
		graphs[k] = &api.Probe_Graph{
			Title:      g.Title,
			Type:       g.Type,
			UnitFormat: g.Format,
			Metrics:    g.Metrics,
			Tags:       g.Tags,
		}
	}

	response := &api.Probe{
		Name:                  path.Join(api.ResourceNameProbe, util.Base64Encode(probe.UUID)),
		ProbeName:             probe.Label,
		DisplayName:           probe.DisplayName,
		Description:           probe.Description,
		ConfigurationTemplate: curParameters,
		Metrics:               metrics,
		Tags:                  tags,
		Graphs:                graphs,
	}

	return response, nil
}
