// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"context"
	"encoding/json"
	"path"
	"time"

	"gitlab.com/pantomath-io/panto/api"
	"gitlab.com/pantomath-io/panto/server/dbconf"
	"gitlab.com/pantomath-io/panto/server/services"
	"gitlab.com/pantomath-io/panto/util"

	"github.com/golang/protobuf/ptypes"
	durpb "github.com/golang/protobuf/ptypes/duration"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/google/uuid"
	"google.golang.org/genproto/googleapis/rpc/errdetails"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// ListProbeConfigurations returns the full configuration for an agent
func (s *server) ListProbeConfigurations(
	ctx context.Context,
	request *api.ListProbeConfigurationsRequest,
) (response *api.ListProbeConfigurationsResponse, err error) {
	if len(request.Parent) == 0 {
		log.Errorf("missing parent Agent")
		return nil, status.Error(codes.InvalidArgument, "Missing parent Agent")
	}
	log.Debugf("Received configuration request for agent %s", request.Parent)

	var pageOffset uint64
	if request.PageToken != "" {
		valid, err := util.ValidatePageToken(request.PageToken, request, &pageOffset)
		if err != nil {
			log.Errorf("Failed to validate page token: %s", err)
			return nil, status.Errorf(codes.InvalidArgument, "Invalid page token")
		}
		if !valid {
			log.Errorf("Page token does not match request")
			return nil, status.Errorf(codes.InvalidArgument, "Page token does not match request")
		}
	}

	pageSize := request.PageSize
	if pageSize == 0 {
		pageSize = defaultPageSize
	} else if pageSize > maxPageSize {
		pageSize = maxPageSize
	}

	// get the agent UUID from the parent field
	organizationUUID, agentUUID, err := api.ParseNameAgent(request.Parent)
	if err != nil {
		log.Errorf("couldn't parse parent Agent name: %s", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid parent Agent name")
	}

	db, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}
	tpas, err := db.ListTargetProbeAgents(organizationUUID, agentUUID, pageSize+1, pageOffset)
	if err != nil {
		log.Errorf("failed to list ProbeConfigurations: %s", err)
		return nil, status.Error(codes.NotFound, "No Probe Configurations found")
	}

	var nextPage bool
	if int32(len(tpas)) > pageSize {
		tpas = tpas[:pageSize]
		nextPage = true
	}

	configurations := make([]*api.ProbeConfiguration, len(tpas))

	for i, tpa := range tpas {
		configuration := &api.ProbeConfiguration{
			Name: path.Join(
				api.ResourceNameOrganization, util.Base64Encode(organizationUUID),
				api.ResourceNameAgent, util.Base64Encode(agentUUID),
				api.ResourceNameProbeConfiguration, util.Base64Encode(tpa.UUID),
			),
			Check: path.Join(
				api.ResourceNameOrganization, util.Base64Encode(organizationUUID),
				api.ResourceNameCheck, util.Base64Encode(tpa.TargetProbeUUID),
			),
			ProbeLabel: tpa.ProbeLabel,
			Schedule:   ptypes.DurationProto(tpa.Schedule),
		}

		if request.WithTarget {
			tp, err := db.GetTargetProbe(organizationUUID, tpa.TargetProbeUUID)
			if err != nil {
				log.Errorf("unable to get relative TargetProbe: %s", err)
				continue
			}
			t, err := db.GetTarget(organizationUUID, tp.TargetUUID)
			if err != nil {
				log.Errorf("unable to get relative Target: %s", err)
				continue
			}
			var tags []*api.Tag
			dbTags, err := db.ListTagsByTarget(organizationUUID, t.UUID, 0, 0)
			if err != nil {
				log.Errorf("failed to list Tags: %s", err)
			} else {
				for _, tag := range dbTags {
					tags = append(tags, &api.Tag{
						Name:        path.Join(api.ResourceNameOrganization, util.Base64Encode(organizationUUID), api.ResourceNameTag, tag.Slug),
						DisplayName: tag.DisplayName,
						Note:        tag.Note,
						Color:       tag.Color,
					})
				}
			}

			configuration.Target = &api.Target{
				Name: path.Join(
					api.ResourceNameOrganization, util.Base64Encode(organizationUUID),
					api.ResourceNameTarget, util.Base64Encode(t.UUID),
				),
				Address: t.Address,
				Note:    t.Note,
				Tags:    tags,
			}
		}

		probeConfig, err := json.Marshal(tpa.ProbeConfiguration)
		if err != nil {
			log.Errorf("unable to marshal configuration to JSON: %s", err)
			continue
		}
		configuration.Configuration = string(probeConfig)

		configurations[i] = configuration
	}

	response = &api.ListProbeConfigurationsResponse{
		ProbeConfigurations: configurations,
	}

	if nextPage {
		response.NextPageToken, err = util.NextPageToken(request, pageOffset+uint64(len(configurations)))
		if err != nil {
			log.Warning("Failed to generate next page token")
			// Return values but don't set page token
			response.NextPageToken = ""
		}
	}

	return response, nil
}

// GetProbeConfiguration returns the configuration for a specific probe on a given agent.
func (s *server) GetProbeConfiguration(
	ctx context.Context,
	req *api.GetProbeConfigurationRequest,
) (*api.ProbeConfiguration, error) {
	if len(req.Name) == 0 {
		log.Error("missing probe configuration name")
		return nil, status.Error(codes.InvalidArgument, "Missing Probe Configuration name")
	}

	// get the Agent and ProbeConfiguration UUIDs from the name field
	organizationUUID, agentUUID, probeConfigurationUUID, err := api.ParseNameProbeConfiguration(req.Name)
	if err != nil {
		log.Errorf("couldn't parse ProbeConfiguration name: %s", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid Probe Configuration name")
	}

	db, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	tpa, err := db.GetTargetProbeAgent(organizationUUID, agentUUID, probeConfigurationUUID)
	if err != nil {
		log.Errorf("couldn't get TargetProbeAgent: %s", err)
		return nil, status.Error(codes.Internal, "Probe Configuration not found")
	}
	if tpa == nil {
		log.Errorf("probe_configuration not found: %s", req.Name)
		return nil, status.Error(codes.NotFound, "Probe Configuration not found")
	}

	res := api.ProbeConfiguration{
		Name: req.Name,
		Check: path.Join(
			api.ResourceNameOrganization, util.Base64Encode(organizationUUID),
			api.ResourceNameCheck, util.Base64Encode(tpa.TargetProbeUUID),
		),
		ProbeLabel: tpa.ProbeLabel,
		Schedule:   ptypes.DurationProto(tpa.Schedule),
	}

	probeConfig, err := json.Marshal(tpa.ProbeConfiguration)
	if err != nil {
		log.Errorf("unable to marshal configuration to JSON: %s", err)
		return nil, status.Error(codes.Internal, "Invalid Probe Configuration")
	}
	res.Configuration = string(probeConfig)

	if req.WithTarget {
		tp, err := db.GetTargetProbe(organizationUUID, tpa.TargetProbeUUID)
		if err != nil {
			log.Errorf("unable to get relative TargetProbe: %s", err)
			return nil, status.Error(codes.Internal, "Target not found")
		}
		t, err := db.GetTarget(organizationUUID, tp.TargetUUID)
		if err != nil {
			log.Errorf("unable to get relative Target: %s", err)
			return nil, status.Error(codes.Internal, "Target not found")
		}
		var tags []*api.Tag
		dbTags, err := db.ListTagsByTarget(organizationUUID, t.UUID, 0, 0)
		if err != nil {
			log.Errorf("failed to list Tags: %s", err)
		} else {
			for _, tag := range dbTags {
				tags = append(tags, &api.Tag{
					Name:        path.Join(api.ResourceNameOrganization, util.Base64Encode(organizationUUID), api.ResourceNameTag, tag.Slug),
					DisplayName: tag.DisplayName,
					Note:        tag.Note,
					Color:       tag.Color,
				})
			}
		}

		res.Target = &api.Target{
			Name: path.Join(
				api.ResourceNameOrganization, util.Base64Encode(organizationUUID),
				api.ResourceNameTarget, util.Base64Encode(t.UUID),
			),
			Address: t.Address,
			Note:    t.Note,
			Tags:    tags,
		}
	}

	return &res, nil
}

// CreateProbeConfiguration creates a configuration for a specific probe to be run by the Agent.
func (s *server) CreateProbeConfiguration(
	ctx context.Context,
	req *api.CreateProbeConfigurationRequest,
) (*api.ProbeConfiguration, error) {

	db, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	if len(req.Parent) == 0 {
		log.Error("missing parent agent")
		return nil, status.Error(codes.InvalidArgument, "Missing parent Agent name")
	}

	// get the agent UUID from the parent field
	organizationUUID, agentUUID, err := api.ParseNameAgent(req.Parent)
	if err != nil {
		log.Errorf("couldn't parse parent Agent name: %s", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid parent Agent name")
	}

	pc := req.ProbeConfiguration
	if pc == nil {
		log.Error("missing probe configuration")
		return nil, status.Error(codes.InvalidArgument, "Missing Probe Configuration")
	}

	fieldViolations := make([]*errdetails.BadRequest_FieldViolation, 0)

	oUUID, checkUUID, errDetail := validateProbeConfigurationCheck(pc.Check)
	if errDetail != nil {
		fieldViolations = append(fieldViolations, errDetail)
	} else if oUUID != organizationUUID {
		return nil, status.Error(codes.InvalidArgument, "Parent Agent and Check are not in the same Organization")
	}

	schedule, errDetail := validateProbeConfigurationSchedule(pc.Schedule)
	if errDetail != nil {
		fieldViolations = append(fieldViolations, errDetail)
	}

	var config map[string]interface{}
	if checkUUID != (uuid.UUID{}) {
		tp, err := db.GetTargetProbe(organizationUUID, checkUUID)
		if err != nil || tp == nil {
			log.Errorf("couldn't get Check \"%s\": %s", pc.Check, err)
			return nil, status.Error(codes.FailedPrecondition, "Check not found")
		}
		p, err := db.GetProbe(tp.ProbeUUID)
		if err != nil || p == nil {
			log.Errorf("couldn't get Probe %s: %s", tp.ProbeUUID, err)
			return nil, status.Error(codes.FailedPrecondition, "Probe not found")
		}
		var errDetails []*errdetails.BadRequest_FieldViolation
		config, errDetails = validateProbeConfigurationConfiguration(pc.Configuration, p.ConfigurationTemplate)
		if errDetails != nil {
			fieldViolations = append(fieldViolations, errDetails...)
		}
	}

	if len(fieldViolations) > 0 {
		s := status.New(codes.InvalidArgument, "Invalid Probe Configuration")
		s2, err := s.WithDetails(&errdetails.BadRequest{FieldViolations: fieldViolations})
		if err != nil {
			log.Errorf("failure adding validation details to error")
			return nil, s.Err()
		}
		return nil, s2.Err()
	}

	tpa, err := db.CreateTargetProbeAgent(&dbconf.TargetProbeAgent{
		OrganizationUUID:   organizationUUID,
		AgentUUID:          agentUUID,
		TargetProbeUUID:    checkUUID,
		ProbeConfiguration: config,
		Schedule:           schedule,
	})
	if err != nil || tpa == nil {
		log.Errorf("failed to create target_probe_agent: %s", err)
		return nil, status.Error(codes.Internal, "Couldn't create Probe Configuration")
	}

	pConfig, err := json.Marshal(tpa.ProbeConfiguration)
	if err != nil {
		log.Errorf("unable to marshal configuration to JSON: %s", err)
		return nil, status.Error(codes.Internal, "Invalid configuration")
	}

	res := api.ProbeConfiguration{
		Name: path.Join(
			api.ResourceNameOrganization, util.Base64Encode(tpa.OrganizationUUID),
			api.ResourceNameAgent, util.Base64Encode(tpa.AgentUUID),
			api.ResourceNameProbeConfiguration, util.Base64Encode(tpa.UUID),
		),
		Check: path.Join(
			api.ResourceNameOrganization, util.Base64Encode(tpa.OrganizationUUID),
			api.ResourceNameCheck, util.Base64Encode(tpa.TargetProbeUUID),
		),
		Configuration: string(pConfig),
		ProbeLabel:    tpa.ProbeLabel,
		Schedule:      ptypes.DurationProto(tpa.Schedule),
	}

	return &res, nil
}

// UpdateProbeConfiguration update a configuration for a specific probe to be run by the Agent.
func (s *server) UpdateProbeConfiguration(ctx context.Context, req *api.UpdateProbeConfigurationRequest) (*api.ProbeConfiguration, error) {
	db, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	pc := req.ProbeConfiguration
	if pc == nil {
		log.Error("missing probe configuration")
		return nil, status.Error(codes.InvalidArgument, "Missing Probe Configuration")
	}

	fieldViolations := make([]*errdetails.BadRequest_FieldViolation, 0)

	organizationUUID, agentUUID, probeConfigurationUUID, errDetail := validateProbeConfigurationName(pc.Name)
	if errDetail != nil {
		fieldViolations = append(fieldViolations, errDetail)
	}

	if req.UpdateMask != nil && util.Contains(req.UpdateMask.Paths, "name") {
		return nil, status.Error(codes.InvalidArgument, "Probe Configuration name is read-only")
	}
	if req.UpdateMask != nil && util.Contains(req.UpdateMask.Paths, "check") {
		return nil, status.Error(codes.InvalidArgument, "Check name is read-only")
	}

	updateTPA := &dbconf.TargetProbeAgent{
		UUID:             probeConfigurationUUID,
		OrganizationUUID: organizationUUID,
		AgentUUID:        agentUUID,
	}

	oUUID, checkUUID, errDetail := validateProbeConfigurationCheck(pc.Check)
	if errDetail != nil {
		fieldViolations = append(fieldViolations, errDetail)
	} else if oUUID != organizationUUID {
		return nil, status.Error(codes.InvalidArgument, "Probe Configuration and Check are not in the same Organization")
	}

	if req.UpdateMask == nil || util.Contains(req.UpdateMask.Paths, "configuration") {
		tp, err := db.GetTargetProbe(organizationUUID, checkUUID)
		if err != nil || tp == nil {
			log.Errorf("couldn't get Check \"%s\": %s", pc.Check, err)
			return nil, status.Error(codes.FailedPrecondition, "Check not found")
		}
		p, err := db.GetProbe(tp.ProbeUUID)
		if err != nil || p == nil {
			log.Errorf("couldn't get Probe %s: %s", tp.ProbeUUID, err)
			return nil, status.Error(codes.FailedPrecondition, "Probe not found")
		}
		config, errDetails := validateProbeConfigurationConfiguration(pc.Configuration, p.ConfigurationTemplate)
		if errDetails != nil {
			fieldViolations = append(fieldViolations, errDetails...)
		}
		updateTPA.ProbeConfiguration = config
	}

	if req.UpdateMask == nil || util.Contains(req.UpdateMask.Paths, "schedule") {
		schedule, errDetail := validateProbeConfigurationSchedule(pc.Schedule)
		if errDetail != nil {
			fieldViolations = append(fieldViolations, errDetail)
		}
		updateTPA.Schedule = schedule
	}

	if len(fieldViolations) > 0 {
		s := status.New(codes.InvalidArgument, "Invalid Probe Configuration")
		s2, err := s.WithDetails(&errdetails.BadRequest{FieldViolations: fieldViolations})
		if err != nil {
			log.Errorf("failure adding validation details to error")
			return nil, s.Err()
		}
		return nil, s2.Err()
	}

	tpa, err := db.UpdateTargetProbeAgent(updateTPA)
	if err != nil {
		log.Errorf("%s", err)
		return nil, status.Error(codes.Internal, "Couldn't update Probe Configuration")
	}

	pConfig, err := json.Marshal(tpa.ProbeConfiguration)
	if err != nil {
		log.Errorf("unable to marshal probe_configuration to JSON: %s", err)
		return nil, status.Error(codes.Internal, "Invalid configuration")
	}

	res := api.ProbeConfiguration{
		Name: path.Join(
			api.ResourceNameOrganization, util.Base64Encode(tpa.OrganizationUUID),
			api.ResourceNameAgent, util.Base64Encode(tpa.AgentUUID),
			api.ResourceNameProbeConfiguration, util.Base64Encode(tpa.UUID),
		),
		Check: path.Join(
			api.ResourceNameOrganization, util.Base64Encode(tpa.OrganizationUUID),
			api.ResourceNameCheck, util.Base64Encode(tpa.TargetProbeUUID),
		),
		Configuration: string(pConfig),
		ProbeLabel:    tpa.ProbeLabel,
		Schedule:      ptypes.DurationProto(tpa.Schedule),
	}

	return &res, nil
}

// DeleteProbeConfiguration deletes the configuration for a specific probe on a given agent.
func (s *server) DeleteProbeConfiguration(ctx context.Context, req *api.DeleteProbeConfigurationRequest) (*empty.Empty, error) {
	db, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	if len(req.Name) == 0 {
		log.Error("missing ProbeConfiguration name")
		return nil, status.Error(codes.InvalidArgument, "Missing Probe Configuration name")
	}

	// get the Agent and ProbeConfiguration UUIDs from the name field
	organizationUUID, agentUUID, probeConfigurationUUID, err := api.ParseNameProbeConfiguration(req.Name)
	if err != nil {
		log.Errorf("couldn't parse ProbeConfiguration name: %s", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid Probe Configuration name")
	}

	err = db.DeleteTargetProbeAgent(organizationUUID, agentUUID, probeConfigurationUUID)
	if err != nil {
		log.Errorf("failed to delete target_probe_agent: %s", err)
		return nil, status.Error(codes.Internal, "Couldn't delete Probe Configuration")
	}

	return &empty.Empty{}, nil
}

// QueryProbeConfigurations returns the probeconfigurations for a list of given Checks in the Parent Organisation.
func (s *server) QueryProbeConfigurations(
	ctx context.Context,
	req *api.QueryProbeConfigurationsRequest,
) (response *api.QueryProbeConfigurationsResponse, err error) {
	if len(req.Organization) == 0 {
		log.Errorf("missing parent Organization name")
		return nil, status.Error(codes.InvalidArgument, "Missing parent Organization name")
	}
	log.Debugf("Received a query probe_configuration request")

	// get the Organization UUIDs from the name field
	organizationUUID, err := api.ParseNameOrganization(req.Organization)
	if err != nil {
		log.Errorf("couldn't parse Organization name: %s", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid parent Organization name")
	}

	var pageOffset uint64
	if req.PageToken != "" {
		valid, err := util.ValidatePageToken(req.PageToken, req, &pageOffset)
		if err != nil {
			log.Errorf("Failed to validate page token: %s", err)
			return nil, status.Errorf(codes.InvalidArgument, "Invalid page token")
		}
		if !valid {
			log.Errorf("Page token does not match request")
			return nil, status.Errorf(codes.InvalidArgument, "Page token does not match request")
		}
	}

	pageSize := req.PageSize
	if pageSize == 0 {
		pageSize = defaultPageSize
	} else if pageSize > maxPageSize {
		pageSize = maxPageSize
	}

	// get the optional constraints on checks
	checks := make([]uuid.UUID, len(req.Checks))
	for i, c := range req.Checks {
		_, cUUID, err := api.ParseNameCheck(c)
		if err != nil {
			log.Errorf("couldn't parse Check name: %s", err)
			return nil, status.Error(codes.InvalidArgument, "Invalid Check name")
		}
		checks[i] = cUUID
	}

	db, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}
	tpas, err := db.ListTargetProbeAgentsForOrganization(organizationUUID, checks, pageSize+1, pageOffset)
	if err != nil {
		log.Errorf("failed to list ProbeConfigurations: %s", err)
		return nil, status.Error(codes.NotFound, "No Probe Configurations found")
	}

	var nextPage bool
	if int32(len(tpas)) > pageSize {
		tpas = tpas[:pageSize]
		nextPage = true
	}

	configurations := make([]*api.ProbeConfiguration, len(tpas))

	for i, tpa := range tpas {
		configuration := &api.ProbeConfiguration{
			Name: path.Join(
				api.ResourceNameOrganization, util.Base64Encode(organizationUUID),
				api.ResourceNameAgent, util.Base64Encode(tpa.AgentUUID),
				api.ResourceNameProbeConfiguration, util.Base64Encode(tpa.UUID),
			),
			Check: path.Join(
				api.ResourceNameOrganization, util.Base64Encode(organizationUUID),
				api.ResourceNameCheck, util.Base64Encode(tpa.TargetProbeUUID),
			),
			ProbeLabel: tpa.ProbeLabel,
			Schedule:   ptypes.DurationProto(tpa.Schedule),
		}

		probeConfig, err := json.Marshal(tpa.ProbeConfiguration)
		if err != nil {
			log.Errorf("unable to marshal configuration to JSON: %s", err)
			continue
		}
		configuration.Configuration = string(probeConfig)

		configurations[i] = configuration
	}

	response = &api.QueryProbeConfigurationsResponse{
		ProbeConfigurations: configurations,
	}

	if nextPage {
		response.NextPageToken, err = util.NextPageToken(req, pageOffset+uint64(len(configurations)))
		if err != nil {
			log.Warning("Failed to generate next page token")
			// Return values but don't set page token
			response.NextPageToken = ""
		}
	}

	return response, nil
}

func newFieldViolation(field, description string) *errdetails.BadRequest_FieldViolation {
	return &errdetails.BadRequest_FieldViolation{
		Field:       field,
		Description: description,
	}
}

func validateProbeConfigurationName(name string) (uuid.UUID, uuid.UUID, uuid.UUID, *errdetails.BadRequest_FieldViolation) {
	if len(name) == 0 {
		log.Errorf("missing Probe Configuration name")
		return uuid.UUID{}, uuid.UUID{}, uuid.UUID{}, newFieldViolation("name", "name is required")
	}

	orgUUID, agentUUID, pcUUID, err := api.ParseNameProbeConfiguration(name)
	if err != nil {
		log.Errorf("Probe Configuration name is invalid")
		return uuid.UUID{}, uuid.UUID{}, uuid.UUID{}, newFieldViolation("name", "name is invalid")
	}

	return orgUUID, agentUUID, pcUUID, nil
}

func validateProbeConfigurationCheck(check string) (uuid.UUID, uuid.UUID, *errdetails.BadRequest_FieldViolation) {
	if len(check) == 0 {
		log.Errorf("missing Check")
		return uuid.UUID{}, uuid.UUID{}, newFieldViolation("check", "check is required")
	}

	orgUUID, checkUUID, err := api.ParseNameCheck(check)
	if err != nil {
		log.Errorf("Check name is invalid")
		return uuid.UUID{}, uuid.UUID{}, newFieldViolation("check", "check is invalid")
	}

	return orgUUID, checkUUID, nil
}

func validateProbeConfigurationSchedule(schedule *durpb.Duration) (time.Duration, *errdetails.BadRequest_FieldViolation) {
	if schedule == nil {
		log.Errorf("missing schedule")
		return 0, newFieldViolation("schedule", "schedule is required")
	}

	sched, err := ptypes.Duration(schedule)
	if err != nil {
		log.Errorf("invalid schedule: %s", err)
		return 0, newFieldViolation("schedule", "invalid schedule")
	}

	// TODO: improve business rules about schedule (min time, max time, etc.)
	if sched.Nanoseconds() <= 0 {
		log.Errorf("schedule must be greater than zero", err)
		return 0, newFieldViolation("schedule", "schedule must be greater than zero")
	}

	return sched, nil
}

func validateProbeConfigurationConfiguration(configuration string, configurationTemplate []util.Parameter) (map[string]interface{}, []*errdetails.BadRequest_FieldViolation) {
	if len(configuration) == 0 {
		log.Errorf("missing configuration")
		return nil, []*errdetails.BadRequest_FieldViolation{newFieldViolation("configuration", "configuration is required")}
	}

	var config map[string]interface{}
	var err error
	if config, err = util.UnmarshalAndValidateParameters([]byte(configuration), configurationTemplate); err != nil {
		if err, ok := err.(*util.ParameterValidationError); ok {
			log.Errorf("invalid parameters in configuration: %s", err)
			fv := make([]*errdetails.BadRequest_FieldViolation, len(err.Errors))
			for i, d := range err.Errors {
				fv[i] = newFieldViolation("configuration."+d.Name, d.Message)
			}
			return nil, fv
		}
		log.Errorf("couldn't unmarshal JSON configuration: %s", err)
		return nil, []*errdetails.BadRequest_FieldViolation{newFieldViolation("configuration", "configuration is not a valid JSON object")}
	}

	// TODO: Validate probe configuration in probe context
	//       => call Configure(config) on probe

	return config, nil
}
