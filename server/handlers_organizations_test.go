// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"context"
	"encoding/json"
	"fmt"
	"reflect"
	"testing"

	"gitlab.com/pantomath-io/panto/api"
	"gitlab.com/pantomath-io/panto/util"
)

func TestListOrganizations(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	s := server{}

	req := &api.ListOrganizationsRequest{}
	res, err := s.ListOrganizations(context.Background(), req)
	if err != nil {
		t.Fatalf("Failed to list organizations: %s", err)
	} else {
		if len(res.Organizations) != 10 {
			t.Fatalf("Unexpected organizations count: got %d, expected %d", len(res.Organizations), 10)
		}
		for i, o := range res.Organizations {
			expect := fmt.Sprintf("organization%d", i)
			if o.DisplayName != expect {
				t.Fatalf("Unexpected organization at index %d: got %v, expected %v", i, o.DisplayName, expect)
			}
		}
		if res.NextPageToken != "" {
			t.Fatalf("Unexpected next page token: %s", res.NextPageToken)
		}
	}
}

func TestListOrganizationsPagination(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	s := server{}

	// Test pagination
	req := &api.ListOrganizationsRequest{
		PageSize: 4,
	}
	res, err := s.ListOrganizations(context.Background(), req)
	if err != nil {
		t.Fatalf("Failed to list organizations: %s", err)
	} else {
		if len(res.Organizations) != 4 {
			t.Fatalf("Unexpected organizations count: got %d, expected %d", len(res.Organizations), 4)
		}
		for i, o := range res.Organizations {
			expect := fmt.Sprintf("organization%d", i)
			if o.DisplayName != expect {
				t.Fatalf("Unexpected organization at index %d: got %v, expected %v", i, o.DisplayName, expect)
			}
		}
		if res.NextPageToken == "" {
			t.Fatalf("Missing next page token: %s", res.NextPageToken)
		}
	}

	// Page 2
	page2Token := res.NextPageToken
	req.PageToken = res.NextPageToken
	res, err = s.ListOrganizations(context.Background(), req)
	if err != nil {
		t.Fatalf("Failed to list organizations: %s", err)
	} else {
		if len(res.Organizations) != 4 {
			t.Fatalf("Unexpected organizations count: got %d, expected %d", len(res.Organizations), 4)
		}
		for i, o := range res.Organizations {
			expect := fmt.Sprintf("organization%d", i+4)
			if o.DisplayName != expect {
				t.Fatalf("Unexpected organization at index %d: got %v, expected %v", i, o.DisplayName, expect)
			}
		}
		if res.NextPageToken == "" {
			t.Fatalf("Missing next page token: %s", res.NextPageToken)
		}
	}

	// Page 3
	req.PageToken = res.NextPageToken
	res, err = s.ListOrganizations(context.Background(), req)
	if err != nil {
		t.Fatalf("Failed to list organizations: %s", err)
	} else {
		if len(res.Organizations) != 2 {
			t.Fatalf("Unexpected organizations count: got %d, expected %d", len(res.Organizations), 2)
		}
		for i, o := range res.Organizations {
			expect := fmt.Sprintf("organization%d", i+8)
			if o.DisplayName != expect {
				t.Fatalf("Unexpected organization at index %d: got %v, expected %v", i, o.DisplayName, expect)
			}
		}
		if res.NextPageToken != "" {
			t.Fatalf("Unexpected next page token: %s", res.NextPageToken)
		}
	}

	// Invalid page token
	req.PageToken = page2Token[1:]
	_, err = s.ListOrganizations(context.Background(), req)
	if err == nil {
		t.Fatalf("Successfully listed organizations with invalid token")
	}

	// Invalid request
	req.PageSize = 10
	req.PageToken = page2Token
	_, err = s.ListOrganizations(context.Background(), req)
	if err == nil {
		t.Fatalf("Successfully listed organizations with invalid request")
	}
}
func TestGetOrganization(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	s := server{}

	req := &api.GetOrganizationRequest{Name: "organizations/KTCCC60hRgWDtGnLxb_I9Q"}
	res, err := s.GetOrganization(context.Background(), req)
	if err != nil {
		t.Fatalf("Failed to get organization: %s", err)
	} else {
		expect := api.Organization{
			Name:        "organizations/KTCCC60hRgWDtGnLxb_I9Q",
			DisplayName: "organization0",
		}
		if !reflect.DeepEqual(res, &expect) {
			r, _ := json.Marshal(res)
			e, _ := json.Marshal(expect)
			t.Fatalf("expected %s, got %s", string(e), string(r))
		}
	}
}
