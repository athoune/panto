// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"context"
	"fmt"
	"path"

	"github.com/golang/protobuf/ptypes"
	"github.com/google/uuid"
	"gitlab.com/pantomath-io/panto/api"
	"gitlab.com/pantomath-io/panto/server/services"
	"gitlab.com/pantomath-io/panto/server/tsdb"
	"gitlab.com/pantomath-io/panto/util"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// QueryNotifications returns all latest Notifications for ProbeConfiguration(s) (or the whole Organization if no ProbeConfiguration provided).
func (s *server) QueryNotifications(ctx context.Context, req *api.QueryNotificationsRequest) (res *api.QueryNotificationsResponse, err error) {
	if len(req.Parent) == 0 {
		log.Errorf("missing parent Organization")
		return nil, status.Error(codes.InvalidArgument, "Missing parent Organization")
	}
	// get the organization UUID from the parent field
	organizationUUID, err := api.ParseNameOrganization(req.Parent)
	if err != nil {
		log.Errorf("couldn't parse parent Organization name: %s", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid parent Organization name")
	}

	// validate nested children mask
	acceptedChildren := []string{"agent", "check"}
	if req.ChildrenMask != nil {
		for _, child := range req.ChildrenMask.Paths {
			if !util.Contains(acceptedChildren, child) {
				log.Errorf("invalid field in children_mask: %s", child)
				return nil, status.Errorf(codes.InvalidArgument, "Invalid field in children_mask: %s", child)
			}
		}
	}

	// Get TSDB
	ts, err := services.GetTSDB()
	if err != nil {
		log.Errorf("couldn't find tsdb service: %s", err)
		return nil, status.Errorf(codes.Internal, "Internal error")
	}

	// Get DBConf
	db, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	// Parse Page Token
	var pageOffset int
	if req.PageToken != "" {
		valid, err := util.ValidatePageToken(req.PageToken, req, &pageOffset)
		if err != nil {
			log.Errorf("Failed to validate page token: %s", err)
			return nil, status.Errorf(codes.InvalidArgument, "Invalid page token")
		}
		if !valid {
			log.Errorf("Page token does not match request")
			return nil, status.Errorf(codes.InvalidArgument, "Page token does not match request")
		}
	}

	// Get page size
	pageSize := int(req.PageSize)
	if pageSize == 0 {
		pageSize = defaultPageSize
	} else if pageSize > maxPageSize {
		pageSize = maxPageSize
	}

	// Get check-agent pairs
	var probeConfs []uuid.UUID
	var checks []uuid.UUID
	var agents []string

	if len(req.ProbeConfigurations) > 0 {
		if len(req.ProbeConfigurations) < pageOffset {
			log.Errorf("Invalid token, page offset is larger than number of probe_configuration.")
			return nil, status.Errorf(codes.InvalidArgument, "Page token does not match request")
		}

		probeConfs = make([]uuid.UUID, len(req.ProbeConfigurations[pageOffset:]))

		checks = make([]uuid.UUID, len(req.ProbeConfigurations[pageOffset:]))
		agents = make([]string, len(req.ProbeConfigurations[pageOffset:]))

		for i, tpa := range req.ProbeConfigurations[pageOffset:] {
			oUUID, agentUUID, probeConfigurationUUID, err := api.ParseNameProbeConfiguration(tpa)
			if err != nil {
				log.Errorf("couldn't parse ProbeConfiguration name: %s (%v)", err, tpa)
				return nil, status.Error(codes.InvalidArgument, "Invalid Probe Configuration name")
			}
			if oUUID != organizationUUID {
				log.Errorf("Agent is not in Organization")
				return nil, status.Error(codes.InvalidArgument, "Agent is not in Organization")
			}

			probeConfs[i] = probeConfigurationUUID

			probeConfiguration, err := db.GetTargetProbeAgent(organizationUUID, agentUUID, probeConfigurationUUID)
			if err != nil {
				log.Errorf("couldn't get the probe_configuration_agent: %s", err)
				return nil, status.Error(codes.Internal, "Probe Configuration not found")
			}
			if probeConfiguration != nil {
				checks[i] = probeConfiguration.TargetProbeUUID
				agents[i] = util.Base64Encode(probeConfiguration.AgentUUID)
			} else {
				log.Debugf("couldn't find the probe_configuration_agent")
				return nil, status.Error(codes.NotFound, "Probe Configuration not found")
			}
		}
	} else {
		// No filter on ProbeConfiguration, get all the TargetProbeAgent from the Organization (with no Check filter)
		tpas, err := db.ListTargetProbeAgentsForOrganization(organizationUUID, []uuid.UUID{}, 0, 0)
		if err != nil {
			log.Errorf("couldn't get ProbeConfiguration name: %s", err)
			return nil, status.Error(codes.InvalidArgument, "No Probe Configurations found")
		}

		probeConfs = make([]uuid.UUID, len(tpas))
		checks = make([]uuid.UUID, len(tpas))
		agents = make([]string, len(tpas))

		for i, tpa := range tpas {
			probeConfs[i] = tpa.UUID
			checks[i] = tpa.TargetProbeUUID
			agents[i] = util.Base64Encode(tpa.AgentUUID)
		}
	}

	notifications, err := ts.GetAlertEvents(
		probeConfs,
		tsdb.Count(1),
		tsdb.OrderBy(tsdb.OrderDescending),
		tsdb.GroupBy("agent"),
		tsdb.Tag("agent", agents...),
	)
	if err != nil {
		log.Errorf("Unable to retrieve alert events from TSDB (%s)", err)
		return nil, status.Error(codes.Internal, "No Notifications found")
	}
	if len(notifications) == 0 {
		log.Warningf("No alertevent retrieved from TSDB")
		return &api.QueryNotificationsResponse{}, nil
	}

	// stripping the result with pagesize and page offset
	// TODO: implement an optimized version of this pagination to avoid retrieving too much data from TSDB
	if len(notifications) < pageOffset {
		log.Errorf("Not enough alertevent retrieved from TSDB (%d) for the offset (%d)", len(notifications), pageOffset)
		return nil, status.Error(codes.InvalidArgument, "Invalid page offset")
	}
	notifications = notifications[pageOffset:]
	var nextPage bool
	if len(notifications) > pageSize {
		notifications = notifications[:pageSize]
		nextPage = true
	}

	// Prepare response
	res = &api.QueryNotificationsResponse{
		Notifications: make([]*api.Notification, len(notifications)),
	}

	for i, n := range notifications {
		ts, err := ptypes.TimestampProto(n.Timestamp)
		if err != nil {
			log.Warningf("Couldn't convert result timestamp %s to protobuf message (%s). Skipping alertevent...", err, n.Timestamp)
			continue
		}

		var event api.Event
		switch n.Event {
		case tsdb.EventValueNotified:
			event = api.Event_NOTIFIED
		case tsdb.EventValueAcknowledged:
			event = api.Event_ACKNOWLEDGED
		case tsdb.EventValueSnoozed:
			event = api.Event_SNOOZED
		default:
			log.Warningf("Unable to convert Event in the API: %v", n.Event)
			continue
		}

		res.Notifications[i] = &api.Notification{
			Check:              path.Join(api.ResourceNameOrganization, util.Base64Encode(organizationUUID), api.ResourceNameCheck, util.Base64Encode(n.Check)),
			Agent:              path.Join(api.ResourceNameOrganization, util.Base64Encode(organizationUUID), api.ResourceNameAgent, util.Base64Encode(n.Agent)),
			ProbeConfiguration: path.Join(api.ResourceNameOrganization, util.Base64Encode(organizationUUID), api.ResourceNameAgent, util.Base64Encode(n.Agent), api.ResourceNameProbeConfiguration, util.Base64Encode(n.ProbeConfiguration)),
			Timestamp:          ts,
			Event:              event,
		}

		if req.ChildrenMask != nil && len(req.ChildrenMask.Paths) > 0 {
			res.Notifications[i].Children = &api.Notification_Children{}
			if util.Contains(req.ChildrenMask.Paths, "agent") {
				res.Notifications[i].Children.Agent, err = s.GetAgent(ctx, &api.GetAgentRequest{Name: res.Notifications[i].Agent})
				if err != nil {
					log.Warningf("Couldn't retrieve agent: %s. Skipping Notification...", err)
				}
			}
			if util.Contains(req.ChildrenMask.Paths, "check") {
				res.Notifications[i].Children.Check, err = s.GetCheck(ctx, &api.GetCheckRequest{Name: res.Notifications[i].Check})
				if err != nil {
					log.Warningf("Couldn't retrieve check: %s. Skipping Notification...", err)
				}
			}
		}
	}

	// Page size reached, generate next page token and return current page
	if nextPage {
		// Generate next page token
		res.NextPageToken, err = util.NextPageToken(req, pageOffset+len(notifications))
		if err != nil {
			log.Warning("Failed to generate next page token")
			// Return values but don't set page token
			res.NextPageToken = ""
		}
		return res, nil
	}

	return res, nil
}

// AddNotification adds a Notification for a ProbeConfiguration.
func (s *server) AddNotification(ctx context.Context, req *api.AddNotificationRequest) (res *api.Notification, err error) {
	if len(req.Parent) == 0 {
		log.Errorf("missing parent Organization name")
		return nil, status.Error(codes.InvalidArgument, "Missing parent Organization name")
	}

	// get the organization UUID from the parent field
	organizationUUID, err := api.ParseNameOrganization(req.Parent)
	if err != nil {
		log.Errorf("couldn't parse parent Organization name: %s", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid parent Organization name")
	}

	// Get TSDB
	ts, err := services.GetTSDB()
	if err != nil {
		log.Errorf("couldn't find tsdb service: %s", err)
		return nil, status.Errorf(codes.Internal, "Internal error")
	}

	// Get DBConf
	db, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	// check input
	if len(req.ProbeConfiguration) == 0 {
		log.Errorf("missing ProbeConfiguration name")
		return nil, status.Error(codes.InvalidArgument, "Missing Probe Configuration name")
	}

	oUUID, agentUUID, probeConfigurationUUID, err := api.ParseNameProbeConfiguration(req.ProbeConfiguration)
	if err != nil {
		log.Errorf("couldn't parse ProbeConfiguration name: %s (%v)", err, req.ProbeConfiguration)
		return nil, status.Error(codes.InvalidArgument, "Invalid Probe Configuration name")
	}
	if oUUID != organizationUUID {
		log.Errorf("ProbeConfiguration is not in Organization")
		return nil, status.Error(codes.InvalidArgument, "Probe Configuration is not in Organization")
	}

	tpa, err := db.GetTargetProbeAgent(organizationUUID, agentUUID, probeConfigurationUUID)
	if err != nil {
		log.Errorf("couldn't get the probe_configuration_agent: %s", err)
		return nil, status.Error(codes.Internal, "Probe Configuration not found")
	}
	tp, err := db.GetTargetProbe(organizationUUID, tpa.TargetProbeUUID)
	if err != nil {
		log.Errorf("couldn't get the target_probe: %s", err)
		return nil, status.Error(codes.Internal, "Check not found")
	}

	notification := tsdb.AlertEvent{
		Entry: tsdb.Entry{
			Organization:       organizationUUID,
			ProbeConfiguration: probeConfigurationUUID,
			Check:              tpa.TargetProbeUUID,
			Target:             tp.TargetUUID,
			Probe:              tp.ProbeUUID,
			Agent:              agentUUID,
		},
		Event: tsdb.EventValue(req.Event),
	}

	err = ts.AddAlertEvent(notification)
	if err != nil {
		log.Errorf("unable to create the AlertEvent: %s", err)
		return nil, status.Error(codes.Internal, "Couldn't create Notification")
	}

	tsNotifs, err := ts.GetAlertEvents(
		[]uuid.UUID{probeConfigurationUUID},
		tsdb.Count(1),
		tsdb.OrderBy(tsdb.OrderDescending),
		tsdb.GroupBy("agent"),
		tsdb.Tag("agent", util.Base64Encode(agentUUID)),
	)
	if err != nil || len(tsNotifs) == 0 {
		log.Errorf("Unable to retrieve alert event from TSDB (%s)", err)
		return nil, status.Error(codes.Internal, "Couldn't create Notification")
	}

	t, err := ptypes.TimestampProto(tsNotifs[0].Timestamp)
	if err != nil {
		log.Errorf("Could not convert notification timestamp (%s): %s", tsNotifs[0], err)
		return nil, fmt.Errorf("Invalid timestamp")
	}

	res = &api.Notification{
		Check:              path.Join(api.ResourceNameOrganization, util.Base64Encode(organizationUUID), api.ResourceNameCheck, util.Base64Encode(tsNotifs[0].Check)),
		Agent:              path.Join(api.ResourceNameOrganization, util.Base64Encode(organizationUUID), api.ResourceNameAgent, util.Base64Encode(tsNotifs[0].Agent)),
		ProbeConfiguration: path.Join(api.ResourceNameOrganization, util.Base64Encode(organizationUUID), api.ResourceNameAgent, util.Base64Encode(tsNotifs[0].Agent), api.ResourceNameProbeConfiguration, util.Base64Encode(tsNotifs[0].ProbeConfiguration)),
		Timestamp:          t,
		Event:              api.Event(tsNotifs[0].Event),
	}

	return
}
