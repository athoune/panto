// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"context"
	"encoding/json"
	"path"

	"gitlab.com/pantomath-io/panto/api"
	"gitlab.com/pantomath-io/panto/server/dbconf"
	"gitlab.com/pantomath-io/panto/server/services"
	"gitlab.com/pantomath-io/panto/util"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// ListChannelTypes returns all the ChannelTypes
func (s *server) ListChannelTypes(ctx context.Context, req *api.ListChannelTypesRequest) (res *api.ListChannelTypesResponse, err error) {
	var pageOffset uint64
	if req.PageToken != "" {
		valid, err := util.ValidatePageToken(req.PageToken, req, &pageOffset)
		if err != nil {
			log.Errorf("Failed to validate page token: %s", err)
			return nil, status.Errorf(codes.InvalidArgument, "Invalid page token")
		}
		if !valid {
			log.Errorf("Page token does not match request")
			return nil, status.Errorf(codes.InvalidArgument, "Page token does not match request")
		}
	}

	pageSize := req.PageSize
	if pageSize == 0 {
		pageSize = defaultPageSize
	} else if pageSize > maxPageSize {
		pageSize = maxPageSize
	}

	dbconf, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}
	dbChannelTypes, err := dbconf.ListChannelTypes(pageSize+1, pageOffset)
	if err != nil {
		log.Errorf("failed to list ChannelTypes: %s", err)
		return nil, status.Error(codes.NotFound, "No Channel Type found")
	}

	var nextPage bool
	if int32(len(dbChannelTypes)) > pageSize {
		dbChannelTypes = dbChannelTypes[:pageSize]
		nextPage = true
	}

	cts := make([]*api.ChannelType, len(dbChannelTypes))
	for i, ct := range dbChannelTypes {
		// Parameters is a repeated field of type ProbeParameter
		var curParameters []*api.ChannelType_Parameter
		for _, parameter := range ct.ConfigurationTemplate {
			curParameters = append(curParameters, &api.ChannelType_Parameter{
				Name:        parameter.Name,
				Type:        parameter.Type,
				Description: parameter.Description,
				Mandatory:   parameter.Mandatory,
			})
		}

		cts[i] = &api.ChannelType{
			Name:          path.Join(api.ResourceNameChannelType, ct.Label),
			Label:         ct.Label,
			Configuration: curParameters,
		}
	}

	res = &api.ListChannelTypesResponse{
		Channeltypes: cts,
	}

	if nextPage {
		nextPageToken, err := util.NextPageToken(req, pageOffset+uint64(len(cts)))
		if err != nil {
			log.Warning("Failed to generate next page token")
			// Return values but don't set page token
			nextPageToken = ""
		}
		res.NextPageToken = nextPageToken
	}

	return
}

// ListChannels returns all the Channels associated to the parent Organization
func (s *server) ListChannels(ctx context.Context, req *api.ListChannelsRequest) (res *api.ListChannelsResponse, err error) {
	if len(req.Parent) == 0 {
		log.Errorf("missing parent Organization name")
		return nil, status.Error(codes.InvalidArgument, "Missing parent Organization name")
	}

	var pageOffset uint64
	if req.PageToken != "" {
		valid, err := util.ValidatePageToken(req.PageToken, req, &pageOffset)
		if err != nil {
			log.Errorf("Failed to validate page token: %s", err)
			return nil, status.Errorf(codes.InvalidArgument, "Invalid page token")
		}
		if !valid {
			log.Errorf("Page token does not match request")
			return nil, status.Errorf(codes.InvalidArgument, "Page token does not match request")
		}
	}

	pageSize := req.PageSize
	if pageSize == 0 {
		pageSize = defaultPageSize
	} else if pageSize > maxPageSize {
		pageSize = maxPageSize
	}

	// get the organization UUID from the parent field
	organizationUUID, err := api.ParseNameOrganization(req.Parent)
	if err != nil {
		log.Errorf("couldn't parse parent Organization name: %s", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid parent Organization name")
	}

	// validate nested children mask
	acceptedChildren := []string{"channel_type"}
	if req.ChildrenMask != nil {
		for _, child := range req.ChildrenMask.Paths {
			if !util.Contains(acceptedChildren, child) {
				log.Errorf("invalid field in children_mask: %s", child)
				return nil, status.Errorf(codes.InvalidArgument, "Invalid field in children_mask: %s", child)
			}
		}
	}

	dbconf, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}
	dbChannels, err := dbconf.ListChannels(organizationUUID, pageSize+1, pageOffset)
	if err != nil {
		log.Errorf("failed to list Channels: %s", err)
		return nil, status.Error(codes.NotFound, "No Channels found")
	}

	var nextPage bool
	if int32(len(dbChannels)) > pageSize {
		dbChannels = dbChannels[:pageSize]
		nextPage = true
	}

	var channels []*api.Channel
Channels:
	for _, channel := range dbChannels {
		channelConfig, err := json.Marshal(channel.Configuration)
		if err != nil {
			log.Errorf("unable to marshal configuration to JSON: %s", err)
			continue Channels
		}

		chanType, err := dbconf.GetChannelType(channel.Type)
		if err != nil {
			log.Errorf("couldn't get the channel type %s: %s", channel.Type, err)
			return nil, status.Error(codes.Internal, "Channel Type not found")
		}

		apiChan := &api.Channel{
			Name: path.Join(
				api.ResourceNameOrganization, util.Base64Encode(organizationUUID),
				api.ResourceNameChannel, util.Base64Encode(channel.UUID),
			),
			ChannelType:   path.Join(api.ResourceNameChannelType, chanType.Label),
			DisplayName:   channel.DisplayName,
			Configuration: string(channelConfig),
		}

		if req.ChildrenMask != nil && len(req.ChildrenMask.Paths) > 0 {
			apiChan.Children = &api.Channel_Children{}
			if util.Contains(req.ChildrenMask.Paths, "channel_type") {
				// Parameters is a repeated field of type ProbeParameter
				var curParameters []*api.ChannelType_Parameter
				for _, parameter := range chanType.ConfigurationTemplate {
					curParameters = append(curParameters, &api.ChannelType_Parameter{
						Name:        parameter.Name,
						Type:        parameter.Type,
						Description: parameter.Description,
						Mandatory:   parameter.Mandatory,
					})
				}

				apiChan.Children.ChannelType = &api.ChannelType{
					Name:          path.Join(api.ResourceNameChannelType, chanType.Label),
					Label:         chanType.Label,
					Configuration: curParameters,
				}
			}
		}

		channels = append(channels, apiChan)
	}

	res = &api.ListChannelsResponse{
		Channels: channels,
	}

	if nextPage {
		res.NextPageToken, err = util.NextPageToken(req, pageOffset+uint64(len(channels)))
		if err != nil {
			log.Warning("Failed to generate next page token")
			// Return values but don't set page token
			res.NextPageToken = ""
		}
	}

	return
}

// GetChannel returns a specific Channel
func (s *server) GetChannel(ctx context.Context, req *api.GetChannelRequest) (*api.Channel, error) {
	if len(req.Name) == 0 {
		log.Errorf("missing Channel name")
		return nil, status.Error(codes.InvalidArgument, "Missing Channel name")
	}

	// get the organization and channel UUIDs from the name field
	organizationUUID, channelUUID, err := api.ParseNameChannel(req.Name)
	if err != nil {
		log.Errorf("couldn't parse Channel name: %s", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid Channel name")
	}

	// validate nested children mask
	acceptedChildren := []string{"channel_type"}
	if req.ChildrenMask != nil {
		for _, child := range req.ChildrenMask.Paths {
			if !util.Contains(acceptedChildren, child) {
				log.Errorf("invalid field in children_mask: %s", child)
				return nil, status.Errorf(codes.InvalidArgument, "Invalid field in children_mask: %s", child)
			}
		}
	}

	dbconf, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}
	dbChannel, err := dbconf.GetChannel(organizationUUID, channelUUID)
	if err != nil {
		log.Errorf("failed to get Channel: %s", err)
		return nil, status.Error(codes.Internal, "Channel not found")
	}

	channelConfig, err := json.Marshal(dbChannel.Configuration)
	if err != nil {
		log.Errorf("unable to marshal configuration to JSON: %s", err)
		return nil, status.Error(codes.Internal, "Invalid Channel configuration")
	}

	chanType, err := dbconf.GetChannelType(dbChannel.Type)
	if err != nil {
		log.Errorf("couldn't get the channel type %s: %s", dbChannel.Type, err)
		return nil, status.Error(codes.Internal, "Channel Type not found")
	}

	res := api.Channel{
		Name: path.Join(
			api.ResourceNameOrganization, util.Base64Encode(organizationUUID),
			api.ResourceNameChannel, util.Base64Encode(dbChannel.UUID),
		),
		ChannelType:   path.Join(api.ResourceNameChannelType, chanType.Label),
		DisplayName:   dbChannel.DisplayName,
		Configuration: string(channelConfig),
	}

	if req.ChildrenMask != nil && len(req.ChildrenMask.Paths) > 0 {
		res.Children = &api.Channel_Children{}
		if util.Contains(req.ChildrenMask.Paths, "channel_type") {
			// Parameters is a repeated field of type ProbeParameter
			var curParameters []*api.ChannelType_Parameter
			for _, parameter := range chanType.ConfigurationTemplate {
				curParameters = append(curParameters, &api.ChannelType_Parameter{
					Name:        parameter.Name,
					Type:        parameter.Type,
					Description: parameter.Description,
					Mandatory:   parameter.Mandatory,
				})
			}

			res.Children.ChannelType = &api.ChannelType{
				Name:          path.Join(api.ResourceNameChannelType, chanType.Label),
				Label:         chanType.Label,
				Configuration: curParameters,
			}
		}
	}

	return &res, nil
}

// CreateChannel inserts a new Channel in an Organization
func (s *server) CreateChannel(ctx context.Context, req *api.CreateChannelRequest) (res *api.Channel, err error) {
	if len(req.Parent) == 0 {
		log.Errorf("missing parent Organization")
		return nil, status.Error(codes.InvalidArgument, "Missing parent Organization name")
	}

	// get the organization UUID from the parent field
	organizationUUID, err := api.ParseNameOrganization(req.Parent)
	if err != nil {
		log.Errorf("couldn't parse parent Organization name: %s", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid parent Organization name")
	}

	db, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	if req.GetChannel() == nil {
		log.Errorf("missing channel object in request")
		return nil, status.Error(codes.InvalidArgument, "Missing Channel")
	}

	var channelConfig map[string]interface{}
	if err = json.Unmarshal([]byte(req.Channel.GetConfiguration()), &channelConfig); err != nil {
		log.Errorf("couldn't unmarshal JSON configuration: %s", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid Channel configuration")
	}

	dbChannel := &dbconf.Channel{
		OrganizationUUID: organizationUUID,
		Type:             req.Channel.ChannelType,
		DisplayName:      req.Channel.DisplayName,
		Configuration:    channelConfig,
	}
	dbChannel, err = db.CreateChannel(dbChannel)
	if err != nil {
		log.Errorf("failed to create Channel: %s", err)
		return nil, status.Error(codes.Internal, "Couldn't create Channel")
	}

	dbchannelConfig, err := json.Marshal(dbChannel.Configuration)
	if err != nil {
		log.Errorf("unable to marshal configuration to JSON: %s", err)
		return nil, status.Error(codes.Internal, "Invalid Channel configuration")
	}

	chanType, err := db.GetChannelType(req.Channel.ChannelType)
	if err != nil {
		log.Errorf("couldn't get the channel type %s: %s", req.Channel.ChannelType, err)
		return nil, status.Error(codes.Internal, "Channel Type not found")
	}

	res = &api.Channel{
		Name: path.Join(
			api.ResourceNameOrganization, util.Base64Encode(organizationUUID),
			api.ResourceNameChannel, util.Base64Encode(dbChannel.UUID),
		),
		ChannelType:   path.Join(api.ResourceNameChannelType, chanType.Label),
		DisplayName:   req.Channel.DisplayName,
		Configuration: string(dbchannelConfig),
	}

	return
}
