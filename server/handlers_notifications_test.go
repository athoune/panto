// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"context"
	"path"
	"strings"
	"testing"

	"gitlab.com/pantomath-io/panto/api"
	"gitlab.com/pantomath-io/panto/util"
	"google.golang.org/genproto/protobuf/field_mask"
)

func TestQueryNotifications(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	s := server{}

	// Test basic query
	req := &api.QueryNotificationsRequest{
		Parent: path.Join(api.ResourceNameOrganization, "KTCCC60hRgWDtGnLxb_I9Q"),
		ProbeConfigurations: []string{
			path.Join(
				api.ResourceNameOrganization, "KTCCC60hRgWDtGnLxb_I9Q",
				api.ResourceNameAgent, "oKuOJ7EaTuSke81z3lYWDQ",
				api.ResourceNameProbeConfiguration, "D0Wcyw6CTleaz5-nLLgv6Q",
			),
		},
		// PageSize :,
		// PageToken:,
	}
	res, err := s.QueryNotifications(context.Background(), req)
	if err != nil {
		t.Errorf("Failed to query notifications: %s", err)
	} else {
		if len(res.Notifications) != 1 {
			t.Errorf("Unexpected notifications count: got %d, expected %d", len(res.Notifications), 1)
		}
	}

	// Test basic query with inexistent Agent
	req = &api.QueryNotificationsRequest{
		Parent: path.Join(api.ResourceNameOrganization, "KTCCC60hRgWDtGnLxb_I9Q"),
		ProbeConfigurations: []string{
			path.Join(
				api.ResourceNameOrganization, "KTCCC60hRgWDtGnLxb_I9Q",
				api.ResourceNameAgent, "______________________",
				api.ResourceNameProbeConfiguration, "D0Wcyw6CTleaz5-nLLgv6Q",
			),
		},
		// PageSize :,
		// PageToken:,
	}
	_, err = s.QueryNotifications(context.Background(), req)
	if err == nil {
		t.Errorf("QueryNotifications on inexistent Agent should return an error")
	}

	// Test basic query with ProbeConfiguration without Notification
	req = &api.QueryNotificationsRequest{
		Parent: path.Join(api.ResourceNameOrganization, "KTCCC60hRgWDtGnLxb_I9Q"),
		ProbeConfigurations: []string{
			path.Join(
				api.ResourceNameOrganization, "KTCCC60hRgWDtGnLxb_I9Q",
				api.ResourceNameAgent, "oKuOJ7EaTuSke81z3lYWDQ",
				api.ResourceNameProbeConfiguration, "FTVGDoa1RVcoZoPjFTXsTA",
			),
		},
		// PageSize :,
		// PageToken:,
	}
	res, err = s.QueryNotifications(context.Background(), req)
	if err != nil {
		t.Errorf("Failed to query notifications: %s", err)
	} else {
		if len(res.Notifications) > 0 {
			t.Errorf("Unexpected notifications count: got %d, expected %d", len(res.Notifications), 0)
			t.Errorf("%v", res.Notifications)
		}
	}
}

func TestQueryNotificationsWithNesting(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	s := server{}

	// Test without nesting
	req := &api.QueryNotificationsRequest{
		Parent: path.Join(api.ResourceNameOrganization, "KTCCC60hRgWDtGnLxb_I9Q"),
		ProbeConfigurations: []string{
			path.Join(
				api.ResourceNameOrganization, "KTCCC60hRgWDtGnLxb_I9Q",
				api.ResourceNameAgent, "oKuOJ7EaTuSke81z3lYWDQ",
				api.ResourceNameProbeConfiguration, "D0Wcyw6CTleaz5-nLLgv6Q",
			),
		},
		// PageSize :,
		// PageToken:,
	}
	res, err := s.QueryNotifications(context.Background(), req)
	if err != nil {
		t.Errorf("Failed to query notifications: %s", err)
	} else {
		if len(res.Notifications) != 1 {
			t.Errorf("Unexpected notifications count: got %d, expected %d", len(res.Notifications), 1)
		} else if res.Notifications[0].Children != nil {
			t.Errorf("Unexpected children, got %#v", res.Notifications[0].Children)
		}
	}

	// Test with nesting
	req = &api.QueryNotificationsRequest{
		Parent: path.Join(api.ResourceNameOrganization, "KTCCC60hRgWDtGnLxb_I9Q"),
		ProbeConfigurations: []string{
			path.Join(
				api.ResourceNameOrganization, "KTCCC60hRgWDtGnLxb_I9Q",
				api.ResourceNameAgent, "oKuOJ7EaTuSke81z3lYWDQ",
				api.ResourceNameProbeConfiguration, "D0Wcyw6CTleaz5-nLLgv6Q",
			),
		},
		ChildrenMask: &field_mask.FieldMask{Paths: []string{"agent"}},
		// PageSize :,
		// PageToken:,
	}
	res, err = s.QueryNotifications(context.Background(), req)
	if err != nil {
		t.Errorf("Failed to query notifications: %s", err)
	} else {
		if len(res.Notifications) != 1 {
			t.Errorf("Unexpected notifications count: got %d, expected %d", len(res.Notifications), 1)
		} else if res.Notifications[0].Children == nil || res.Notifications[0].Children.Agent == nil {
			t.Errorf("Unexpected children, got %#v", res.Notifications[0].Children)
		}
	}
}

func TestQueryNotificationsWithoutProbeConfiguration(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	s := server{}

	// Test basic query
	req := &api.QueryNotificationsRequest{
		Parent: path.Join(api.ResourceNameOrganization, "KTCCC60hRgWDtGnLxb_I9Q"),
		// PageSize :,
		// PageToken:,
	}
	res, err := s.QueryNotifications(context.Background(), req)
	if err != nil {
		t.Errorf("Failed to query notifications: %s", err)
	} else {
		if len(res.Notifications) < 1 {
			t.Errorf("Unexpected notifications count: got %d, expected more than %d", len(res.Notifications), 1)
		}
	}
}

func TestQueryNotificationsWithoutProbeConfigurationPagination(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	s := server{}
	overallResults := 1

	// Test basic query
	req := &api.QueryNotificationsRequest{
		Parent:   path.Join(api.ResourceNameOrganization, "KTCCC60hRgWDtGnLxb_I9Q"),
		PageSize: 1,
		// PageToken:,
	}
	res, err := s.QueryNotifications(context.Background(), req)
	if err != nil {
		t.Errorf("Failed to query notifications: %s", err)
	} else {
		if len(res.Notifications) != 1 {
			t.Errorf("Unexpected notifications count: got %d, expected %d", len(res.Notifications), 1)
		}
	}

	npt := res.NextPageToken
	for npt != "" {
		overallResults++
		t.Logf("[%d] Next Page Token: %s", overallResults+1, npt)

		// Page i
		req = &api.QueryNotificationsRequest{
			Parent:    path.Join(api.ResourceNameOrganization, "KTCCC60hRgWDtGnLxb_I9Q"),
			PageSize:  1,
			PageToken: npt,
		}
		res, err = s.QueryNotifications(context.Background(), req)
		if err != nil {
			t.Errorf("Failed to query notifications: %s", err)
		} else {
			if len(res.Notifications) != 1 {
				t.Errorf("Unexpected notifications count: got %d, expected %d", len(res.Notifications), 1)
			}
		}
		npt = res.NextPageToken
	}

	if overallResults < 2 {
		t.Errorf("Unexpected notifications overall count: got %d, expected %d", overallResults, 2)
	}
}

func TestQueryNotificationsPagination(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	s := server{}

	// Test basic query
	req := &api.QueryNotificationsRequest{
		Parent: path.Join(api.ResourceNameOrganization, "KTCCC60hRgWDtGnLxb_I9Q"),
		ProbeConfigurations: []string{
			path.Join(
				api.ResourceNameOrganization, "KTCCC60hRgWDtGnLxb_I9Q",
				api.ResourceNameAgent, "oKuOJ7EaTuSke81z3lYWDQ",
				api.ResourceNameProbeConfiguration, "D0Wcyw6CTleaz5-nLLgv6Q",
			),
		},
		PageSize: 1,
		// PageToken:,
	}
	res, err := s.QueryNotifications(context.Background(), req)
	if err != nil {
		t.Fatalf("Failed to query notifications: %s", err)
	} else {
		if len(res.Notifications) < 1 {
			t.Errorf("Unexpected notifications count: got %d, expected %d", len(res.Notifications), 1)
		}
	}

	if res.NextPageToken != "" {
		t.Errorf("Pagination did not worked, next page token should be empty, %s", res.NextPageToken)
	}
}

func TestAddNotification(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live testing...")
	}

	s := server{}

	// Test basic query
	req := &api.AddNotificationRequest{
		Parent: path.Join(api.ResourceNameOrganization, "KTCCC60hRgWDtGnLxb_I9Q"),
		ProbeConfiguration: path.Join(
			api.ResourceNameOrganization, "KTCCC60hRgWDtGnLxb_I9Q",
			api.ResourceNameAgent, "oKuOJ7EaTuSke81z3lYWDQ",
			api.ResourceNameProbeConfiguration, "ql4J5d_jS0eJ40R6jNGuow",
		),
		Event: api.Event_ACKNOWLEDGED,
	}

	res, err := s.AddNotification(context.Background(), req)
	if err != nil {
		t.Errorf("Failed to add notifications: %s", err)
	}

	if !strings.HasPrefix(res.Check, path.Join(api.ResourceNameOrganization, "KTCCC60hRgWDtGnLxb_I9Q")) ||
		!strings.HasPrefix(res.Agent, path.Join(api.ResourceNameOrganization, "KTCCC60hRgWDtGnLxb_I9Q")) ||
		res.ProbeConfiguration != path.Join(api.ResourceNameOrganization, "KTCCC60hRgWDtGnLxb_I9Q", api.ResourceNameAgent, "oKuOJ7EaTuSke81z3lYWDQ", api.ResourceNameProbeConfiguration, "ql4J5d_jS0eJ40R6jNGuow") ||
		res.Event != api.Event_ACKNOWLEDGED {
		t.Errorf("Unexpected result: got %v, expected %v", res, req)
	}

}
