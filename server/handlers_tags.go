// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"context"
	"path"

	"github.com/golang/protobuf/ptypes/empty"

	"gitlab.com/pantomath-io/panto/api"
	"gitlab.com/pantomath-io/panto/server/dbconf"
	"gitlab.com/pantomath-io/panto/server/services"
	"gitlab.com/pantomath-io/panto/util"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// ListTags returns all the Tags associated to the parent Organization
func (s *server) ListTags(ctx context.Context, req *api.ListTagsRequest) (res *api.ListTagsResponse, err error) {
	if len(req.Parent) == 0 {
		log.Errorf("missing parent Organization name")
		return nil, status.Error(codes.InvalidArgument, "Missing parent Organization name")
	}

	var pageOffset uint64
	if req.PageToken != "" {
		valid, err := util.ValidatePageToken(req.PageToken, req, &pageOffset)
		if err != nil {
			log.Errorf("Failed to validate page token: %s", err)
			return nil, status.Errorf(codes.InvalidArgument, "Invalid page token")
		}
		if !valid {
			log.Errorf("Page token does not match request")
			return nil, status.Errorf(codes.InvalidArgument, "Page token does not match request")
		}
	}

	pageSize := req.PageSize
	if pageSize == 0 {
		pageSize = defaultPageSize
	} else if pageSize > maxPageSize {
		pageSize = maxPageSize
	}

	// get the organization UUID from the parent field
	organizationUUID, err := api.ParseNameOrganization(req.Parent)
	if err != nil {
		log.Errorf("couldn't parse parent Organization name: %s", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid parent Organization name")
	}

	dbconf, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}
	dbTags, err := dbconf.ListTags(organizationUUID, pageSize+1, pageOffset)
	if err != nil {
		log.Errorf("failed to list Tags: %s", err)
		return nil, status.Error(codes.NotFound, "No Tags found")
	}

	var nextPage bool
	if int32(len(dbTags)) > pageSize {
		dbTags = dbTags[:pageSize]
		nextPage = true
	}

	tags := make([]*api.Tag, len(dbTags))

	for i, tag := range dbTags {
		tags[i] = &api.Tag{
			Name:        path.Join(api.ResourceNameOrganization, util.Base64Encode(organizationUUID), api.ResourceNameTag, tag.Slug),
			DisplayName: tag.DisplayName,
			Note:        tag.Note,
			Color:       tag.Color,
		}
	}

	res = &api.ListTagsResponse{
		Tags: tags,
	}

	if nextPage {
		res.NextPageToken, err = util.NextPageToken(req, pageOffset+uint64(len(tags)))
		if err != nil {
			log.Warning("Failed to generate next page token")
			// Return values but don't set page token
			res.NextPageToken = ""
		}
	}

	return
}

// GetTag returns a specific Tag
func (s *server) GetTag(ctx context.Context, req *api.GetTagRequest) (res *api.Tag, err error) {
	// check input
	if len(req.Name) == 0 {
		log.Errorf("missing tag Name")
		return nil, status.Error(codes.InvalidArgument, "Missing Tag name")
	}

	// get the organization and target UUIDs from the name field
	organizationUUID, tagName, err := api.ParseNameTag(req.Name)
	if err != nil {
		log.Errorf("couldn't parse Tag name: %s", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid Tag name")
	}

	db, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	dbTag, err := db.GetTag(organizationUUID, tagName)
	if err != nil {
		log.Errorf("failed to get Tag: %s", err)
		return nil, status.Error(codes.Internal, "Tag not found")
	}
	if dbTag == nil {
		log.Errorf("tag not found: %s", req.Name)
		return nil, status.Error(codes.NotFound, "Tag not found")
	}

	res = &api.Tag{
		Name:        path.Join(api.ResourceNameOrganization, util.Base64Encode(organizationUUID), api.ResourceNameTag, dbTag.Slug),
		DisplayName: dbTag.DisplayName,
		Note:        dbTag.Note,
		Color:       dbTag.Color,
	}

	return
}

// CreateTag inserts a new Tag in an Organization
func (s *server) CreateTag(ctx context.Context, req *api.CreateTagRequest) (res *api.Tag, err error) {
	if len(req.Parent) == 0 {
		log.Errorf("missing parent Organization name")
		return nil, status.Error(codes.InvalidArgument, "Missing parent Organization name")
	}

	// get the organization UUID from the parent field
	organizationUUID, err := api.ParseNameOrganization(req.Parent)
	if err != nil {
		log.Errorf("couldn't parse parent Organization name: %s", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid parent Organization name")
	}

	db, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	if req.GetTag() == nil {
		log.Errorf("missing tag object in request")
		return nil, status.Error(codes.InvalidArgument, "Missing Tag")
	}

	dbTag, err := db.CreateTag(&dbconf.Tag{
		OrganizationUUID: organizationUUID,
		DisplayName:      req.Tag.GetDisplayName(),
		Note:             req.Tag.GetNote(),
		Color:            req.Tag.GetColor(),
	})
	if err != nil {
		log.Errorf("failed to create Tag: %s", err)
		return nil, status.Error(codes.Internal, "Couldn't create Tag")
	}

	res = &api.Tag{
		Name:        path.Join(api.ResourceNameOrganization, util.Base64Encode(dbTag.OrganizationUUID), api.ResourceNameTag, dbTag.Slug),
		DisplayName: dbTag.DisplayName,
		Note:        dbTag.Note,
		Color:       dbTag.Color,
	}

	return
}

// UpdateTag updates an existing Tag
func (s *server) UpdateTag(ctx context.Context, req *api.UpdateTagRequest) (res *api.Tag, err error) {
	if len(req.Tag.Name) == 0 {
		log.Errorf("missing tag name")
		return nil, status.Error(codes.InvalidArgument, "Missing Tag name")
	}

	// get the organization and target UUIDs from the name field
	organizationUUID, tagName, err := api.ParseNameTag(req.Tag.Name)
	if err != nil {
		log.Errorf("couldn't parse Tag name: %s", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid Tag name")
	}

	db, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	updateTag := &dbconf.Tag{
		DisplayName:      tagName,
		OrganizationUUID: organizationUUID,
	}

	if req.UpdateMask != nil && util.Contains(req.UpdateMask.Paths, "name") {
		return nil, status.Error(codes.InvalidArgument, "Tag name is read-only")
	}

	if req.UpdateMask == nil || util.Contains(req.UpdateMask.Paths, "display_name") {
		if req.Tag.GetDisplayName() == "" {
			return nil, status.Error(codes.InvalidArgument, "Missing Tag display name.")
		}
		updateTag.DisplayName = req.Tag.GetDisplayName()
	}

	if req.UpdateMask == nil || util.Contains(req.UpdateMask.Paths, "note") {
		updateTag.Note = req.Tag.GetNote()
	}

	if req.UpdateMask == nil || util.Contains(req.UpdateMask.Paths, "color") {
		if req.Tag.GetColor() == "" {
			return nil, status.Error(codes.InvalidArgument, "Missing Tag color")
		}
		updateTag.Color = req.Tag.GetColor()
	}

	dbTag, err := db.UpdateTag(updateTag)
	if err != nil {
		log.Errorf("failed to update Tag: %s", err)
		return nil, status.Error(codes.Internal, "Couldn't update Tag")
	}

	res = &api.Tag{
		Name:        path.Join(api.ResourceNameOrganization, util.Base64Encode(dbTag.OrganizationUUID), api.ResourceNameTag, dbTag.Slug),
		DisplayName: dbTag.DisplayName,
		Note:        dbTag.Note,
		Color:       dbTag.Color,
	}

	return
}

// DeleteTag deletes an existing Tag and unlink it from Target
func (s *server) DeleteTag(ctx context.Context, req *api.DeleteTagRequest) (res *empty.Empty, err error) {
	if len(req.Name) == 0 {
		log.Errorf("missing tag name")
		return nil, status.Error(codes.InvalidArgument, "Missing Tag name")
	}

	// get the organization and target UUIDs from the name field
	organizationUUID, tagName, err := api.ParseNameTag(req.Name)
	if err != nil {
		log.Errorf("couldn't parse Tag name: %s", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid Tag name")
	}

	db, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	err = db.DeleteTag(organizationUUID, tagName)
	if err != nil {
		log.Errorf("failed to delete Tag: %s", err)
		return nil, status.Error(codes.Internal, "Couldn't delete Tag")
	}

	res = &empty.Empty{}
	return
}
