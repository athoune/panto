// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"context"
	"encoding/json"
	"fmt"
	"testing"

	"github.com/golang/protobuf/proto"

	"gitlab.com/pantomath-io/panto/api"
	"gitlab.com/pantomath-io/panto/util"
)

func TestListTags(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	s := server{}

	req := &api.ListTagsRequest{
		Parent: "organizations/KTCCC60hRgWDtGnLxb_I9Q",
	}
	res, err := s.ListTags(context.Background(), req)
	if err != nil {
		t.Fatalf("Failed to list tags: %s", err)
	} else {
		if len(res.Tags) != 10 {
			t.Fatalf("Unexpected tags count: got %d, expected %d", len(res.Tags), 10)
		}
		for i, tag := range res.Tags {
			expect := fmt.Sprintf("organizations/KTCCC60hRgWDtGnLxb_I9Q/tags/tag-%d", i)
			if tag.Name != expect {
				t.Fatalf("Unexpected tag at index %d: got %v, expected %v", i, tag.Name, expect)
			}
		}
		if res.NextPageToken != "" {
			t.Fatalf("Unexpected next page token: %s", res.NextPageToken)
		}
	}
}

func TestListTagsPagination(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	s := server{}

	// Test pagination
	req := &api.ListTagsRequest{
		Parent:   "organizations/KTCCC60hRgWDtGnLxb_I9Q",
		PageSize: 4,
	}
	res, err := s.ListTags(context.Background(), req)
	if err != nil {
		t.Fatalf("Failed to list tags: %s", err)
	} else {
		if len(res.Tags) != 4 {
			t.Fatalf("Unexpected tags count: got %d, expected %d", len(res.Tags), 4)
		}
		for i, tag := range res.Tags {
			expect := fmt.Sprintf("organizations/KTCCC60hRgWDtGnLxb_I9Q/tags/tag-%d", i)
			if tag.Name != expect {
				t.Fatalf("Unexpected tag at index %d: got %v, expected %v", i, tag.Name, expect)
			}
		}
		if res.NextPageToken == "" {
			t.Fatalf("Missing next page token: %s", res.NextPageToken)
		}
	}

	// Page 2
	page2Token := res.NextPageToken
	req.PageToken = res.NextPageToken
	res, err = s.ListTags(context.Background(), req)
	if err != nil {
		t.Fatalf("Failed to list tags: %s", err)
	} else {
		if len(res.Tags) != 4 {
			t.Fatalf("Unexpected tags count: got %d, expected %d", len(res.Tags), 4)
		}
		for i, tag := range res.Tags {
			expect := fmt.Sprintf("organizations/KTCCC60hRgWDtGnLxb_I9Q/tags/tag-%d", i+4)
			if tag.Name != expect {
				t.Fatalf("Unexpected tag at index %d: got %v, expected %v", i, tag.Name, expect)
			}
		}
		if res.NextPageToken == "" {
			t.Fatalf("Missing next page token: %s", res.NextPageToken)
		}
	}

	// Page 3
	req.PageToken = res.NextPageToken
	res, err = s.ListTags(context.Background(), req)
	if err != nil {
		t.Fatalf("Failed to list tags: %s", err)
	} else {
		if len(res.Tags) != 2 {
			t.Fatalf("Unexpected tags count: got %d, expected %d", len(res.Tags), 2)
		}
		for i, tag := range res.Tags {
			expect := fmt.Sprintf("organizations/KTCCC60hRgWDtGnLxb_I9Q/tags/tag-%d", i+8)
			if tag.Name != expect {
				t.Fatalf("Unexpected tag at index %d: got %v, expected %v", i, tag.Name, expect)
			}
		}
		if res.NextPageToken != "" {
			t.Fatalf("Unexpected next page token: %s", res.NextPageToken)
		}
	}

	// Invalid page token
	req.PageToken = page2Token[1:]
	_, err = s.ListTags(context.Background(), req)
	if err == nil {
		t.Fatalf("Successfully listed tags with invalid token")
	}

	// Invalid request
	req.PageSize = 10
	req.PageToken = page2Token
	_, err = s.ListTags(context.Background(), req)
	if err == nil {
		t.Fatalf("Successfully listed tags with invalid request")
	}
}

func TestCreateTag(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	s := server{}

	req := &api.CreateTagRequest{
		Parent: "organizations/KTCCC60hRgWDtGnLxb_I9Q",
		Tag: &api.Tag{
			DisplayName: "Tag",
			Color:       "#1234aa",
		},
	}

	res, err := s.CreateTag(context.Background(), req)
	if err != nil {
		t.Fatalf("unexpected error creating tag: %s", err)
	}

	expect := &api.Tag{
		Name:        "organizations/KTCCC60hRgWDtGnLxb_I9Q/tags/tag-10",
		DisplayName: "Tag",
		Note:        "",
		Color:       "#1234aa",
	}

	if !proto.Equal(res, expect) {
		resJSON, _ := json.Marshal(res)
		expectJSON, _ := json.Marshal(expect)
		t.Fatalf("expected %s, got %s", expectJSON, resJSON)
	}
}
