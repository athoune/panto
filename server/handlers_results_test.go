// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"context"
	"path"
	"testing"
	"time"

	"github.com/golang/protobuf/proto"
	"github.com/golang/protobuf/ptypes"

	"gitlab.com/pantomath-io/panto/api"
	"gitlab.com/pantomath-io/panto/util"
)

func TestQueryResults(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	s := server{}

	// Test basic query
	req := &api.QueryResultsRequest{
		Parent: path.Join(
			api.ResourceNameOrganization, util.Base64Encode(organization),
		),
		ProbeConfiguration: path.Join(
			api.ResourceNameOrganization, util.Base64Encode(organization),
			api.ResourceNameAgent, util.Base64Encode(agent1),
			api.ResourceNameProbeConfiguration, util.Base64Encode(probeConfiguration1),
		),
		// StartTime:,
		// EndTime  :,
		// Tags     :,
		// OrderBy  :,
		// PageSize :,
		// PageToken:,
	}
	res, err := s.QueryResults(context.Background(), req)
	if err != nil {
		t.Fatalf("Failed to query results: %s", err)
	} else {
		if len(res.Results) != defaultPageSize {
			t.Fatalf("Unexpected results count: got %d, expected %d", len(res.Results), defaultPageSize)
		}
		for i, r := range res.Results {
			if !proto.Equal(r, results[probeConfiguration1][i]) {
				t.Fatalf("Unexpected result at index %d: got %v, expected %v", i, r, results[probeConfiguration1][i])
				break
			}
		}
	}

	// Test query with start time
	ts, _ := ptypes.TimestampProto(startTime.Add(2 * time.Hour))
	req = &api.QueryResultsRequest{
		Parent: path.Join(
			api.ResourceNameOrganization, util.Base64Encode(organization),
		),
		ProbeConfiguration: path.Join(
			api.ResourceNameOrganization, util.Base64Encode(organization),
			api.ResourceNameAgent, util.Base64Encode(agent1),
			api.ResourceNameProbeConfiguration, util.Base64Encode(probeConfiguration1),
		),
		// Agent: ,
		StartTime: ts,
		// EndTime  :,
		// Tags     :,
		// OrderBy  :,
		// PageSize :,
		// PageToken:,
	}
	res, err = s.QueryResults(context.Background(), req)
	if err != nil {
		t.Fatalf("Failed to query results: %s", err)
	} else {
		for i, r := range res.Results {
			if len(res.Results) != defaultPageSize {
				t.Fatalf("Unexpected results count: got %d, expected %d", len(res.Results), defaultPageSize)
			}
			if !proto.Equal(r, results[probeConfiguration1][i+4]) {
				t.Fatalf("Unexpected result at index %d: got %v, expected %v", i, r, results[probeConfiguration1][i+4])
				break
			}
		}
	}

	// Test query with end time
	req = &api.QueryResultsRequest{
		Parent: path.Join(
			api.ResourceNameOrganization, util.Base64Encode(organization),
		),
		ProbeConfiguration: path.Join(
			api.ResourceNameOrganization, util.Base64Encode(organization),
			api.ResourceNameAgent, util.Base64Encode(agent1),
			api.ResourceNameProbeConfiguration, util.Base64Encode(probeConfiguration1),
		),
		// Agent: ,
		// StartTime: ,
		EndTime: ts,
		// Tags     :,
		// OrderBy  :,
		// PageSize :,
		// PageToken:,
	}
	res, err = s.QueryResults(context.Background(), req)
	if err != nil {
		t.Fatalf("Failed to query results: %s", err)
	} else {
		if len(res.Results) != 4 {
			t.Fatalf("Unexpected results count: got %d, expected %d", len(res.Results), 4)
		}
		for i, r := range res.Results {
			if !proto.Equal(r, results[probeConfiguration1][i]) {
				t.Fatalf("Unexpected result at index %d: got %v, expected %v", i, r, results[probeConfiguration1][i])
				break
			}
		}
	}

	// Test query with tags
	req = &api.QueryResultsRequest{
		Parent: path.Join(
			api.ResourceNameOrganization, util.Base64Encode(organization),
		),
		ProbeConfiguration: path.Join(
			api.ResourceNameOrganization, util.Base64Encode(organization),
			api.ResourceNameAgent, util.Base64Encode(agent1),
			api.ResourceNameProbeConfiguration, util.Base64Encode(probeConfiguration1),
		),
		// Agent: ,
		// StartTime: ,
		// EndTime: ,
		Tags: map[string]string{"first": "true"},
		// OrderBy  :,
		// PageSize :,
		// PageToken:,
	}
	res, err = s.QueryResults(context.Background(), req)
	if err != nil {
		t.Fatalf("Failed to query results: %s", err)
	} else {
		if len(res.Results) != 1 {
			t.Fatalf("Unexpected results count: got %d, expected %d", len(res.Results), 1)
		}
		for i, r := range res.Results {
			if !proto.Equal(r, results[probeConfiguration1][i]) {
				t.Fatalf("Unexpected result at index %d: got %v, expected %v", i, r, results[probeConfiguration1][i])
				break
			}
		}
	}

	// Test query with ordering
	req = &api.QueryResultsRequest{
		Parent: path.Join(
			api.ResourceNameOrganization, util.Base64Encode(organization),
		),
		ProbeConfiguration: path.Join(
			api.ResourceNameOrganization, util.Base64Encode(organization),
			api.ResourceNameAgent, util.Base64Encode(agent1),
			api.ResourceNameProbeConfiguration, util.Base64Encode(probeConfiguration1),
		),
		// Agent    :,
		// StartTime:,
		// EndTime  :,
		// Tags     :,
		OrderBy: api.Order_ASCENDING,
		// PageSize :,
		// PageToken:,
	}
	res, err = s.QueryResults(context.Background(), req)
	if err != nil {
		t.Fatalf("Failed to query results: %s", err)
	} else {
		if len(res.Results) != defaultPageSize {
			t.Fatalf("Unexpected results count: got %d, expected %d", len(res.Results), defaultPageSize)
		}
		for i, r := range res.Results {
			if !proto.Equal(r, results[probeConfiguration1][i]) {
				t.Fatalf("Unexpected result at index %d: got %v, expected %v", i, r, results[probeConfiguration1][i])
				break
			}
		}
	}
	req = &api.QueryResultsRequest{
		Parent: path.Join(
			api.ResourceNameOrganization, util.Base64Encode(organization),
		),
		ProbeConfiguration: path.Join(
			api.ResourceNameOrganization, util.Base64Encode(organization),
			api.ResourceNameAgent, util.Base64Encode(agent1),
			api.ResourceNameProbeConfiguration, util.Base64Encode(probeConfiguration1),
		),
		// Agent    :,
		// StartTime:,
		// EndTime  :,
		// Tags     :,
		OrderBy: api.Order_DESCENDING,
		// PageSize :,
		// PageToken:,
	}
	res, err = s.QueryResults(context.Background(), req)
	if err != nil {
		t.Fatalf("Failed to query results: %s", err)
	} else {
		if len(res.Results) != defaultPageSize {
			t.Fatalf("Unexpected results count: got %d, expected %d", len(res.Results), defaultPageSize)
		}
		for i, r := range res.Results {
			if !proto.Equal(r, results[probeConfiguration1][len(results[probeConfiguration1])-i-1]) {
				t.Fatalf("Unexpected result at index %d: got %s, expected %s", i, r, results[probeConfiguration1][len(results[probeConfiguration1])-i-1])
				break
			}
		}
	}

	// Test pagination
	req = &api.QueryResultsRequest{
		Parent: path.Join(
			api.ResourceNameOrganization, util.Base64Encode(organization),
		),
		ProbeConfiguration: path.Join(
			api.ResourceNameOrganization, util.Base64Encode(organization),
			api.ResourceNameAgent, util.Base64Encode(agent1),
			api.ResourceNameProbeConfiguration, util.Base64Encode(probeConfiguration1),
		),
		// Agent: ,
		// StartTime:,
		// EndTime  :,
		// Tags     :,
		// OrderBy  :,
		PageSize: 1,
		// PageToken:,
	}
	res, err = s.QueryResults(context.Background(), req)
	if err != nil {
		t.Fatalf("Failed to query results: %s", err)
	} else {
		if len(res.Results) != 1 {
			t.Fatalf("Unexpected results count: got %d, expected %d", len(res.Results), 1)
		}
		if !proto.Equal(res.Results[0], results[probeConfiguration1][0]) {
			t.Fatalf("Unexpected result: got %v, expected %v", res.Results[0], results[probeConfiguration1][0])
		}
	}
	req = &api.QueryResultsRequest{
		Parent: path.Join(
			api.ResourceNameOrganization, util.Base64Encode(organization),
		),
		ProbeConfiguration: path.Join(
			api.ResourceNameOrganization, util.Base64Encode(organization),
			api.ResourceNameAgent, util.Base64Encode(agent1),
			api.ResourceNameProbeConfiguration, util.Base64Encode(probeConfiguration1),
		),
		// Agent: ,
		// StartTime:,
		// EndTime  :,
		// Tags     :,
		// OrderBy  :,
		PageSize:  1,
		PageToken: res.NextPageToken,
	}
	res, err = s.QueryResults(context.Background(), req)
	if err != nil {
		t.Fatalf("Failed to query results: %s", err)
	} else {
		if len(res.Results) != 1 {
			t.Fatalf("Unexpected results count: got %d, expected %d", len(res.Results), 1)
		}
		if !proto.Equal(res.Results[0], results[probeConfiguration1][1]) {
			t.Fatalf("Unexpected result: got %v, expected %v", res.Results[0], results[probeConfiguration1][1])
		}
	}
	req = &api.QueryResultsRequest{
		Parent: path.Join(
			api.ResourceNameOrganization, util.Base64Encode(organization),
		),
		ProbeConfiguration: path.Join(
			api.ResourceNameOrganization, util.Base64Encode(organization),
			api.ResourceNameAgent, util.Base64Encode(agent1),
			api.ResourceNameProbeConfiguration, util.Base64Encode(probeConfiguration1),
		),
		// Agent: ,
		// StartTime:,
		// EndTime  :,
		// Tags     :,
		// OrderBy  :,
		PageSize: 1E9,
		// PageToken:,
	}
	res, err = s.QueryResults(context.Background(), req)
	if err != nil {
		t.Fatalf("Failed to query results: %s", err)
	} else {
		if len(res.Results) != maxPageSize {
			t.Fatalf("Unexpected results count: got %d, expected %d", len(res.Results), 1)
		}
	}
}
