package server

import (
	"reflect"
	"testing"

	"gitlab.com/pantomath-io/panto/api"
)

func TestDeprecated389(t *testing.T) {
	{
		got := &api.Check{
			Type:          "none",
			Configuration: "{}",
		}
		expect := &api.Check{
			Type:          "none",
			Configuration: "{}",
		}
		m := make(map[string]struct{})
		expectMap := map[string]struct{}{}
		deprecated389(m, got)
		if !reflect.DeepEqual(expect, got) {
			t.Fatalf("expected %s, got %s", expect, got)
		}
		if !reflect.DeepEqual(expectMap, m) {
			t.Fatalf("expected deprecation list %s, got %s", expectMap, m)
		}
	}

	{
		got := &api.Check{
			StateType:     "none",
			Configuration: "{}",
		}
		expect := &api.Check{
			Type:          "none",
			Configuration: "{}",
		}
		m := make(map[string]struct{})
		expectMap := map[string]struct{}{"Check.state_type": {}}
		deprecated389(m, got)
		if !reflect.DeepEqual(expect, got) {
			t.Fatalf("expected %s, got %s", expect, got)
		}
		if !reflect.DeepEqual(expectMap, m) {
			t.Fatalf("expected deprecation list %s, got %s", expectMap, m)
		}
	}

	{
		got := &api.Check{
			StateType:          "none",
			StateConfiguration: "{}",
		}
		expect := &api.Check{
			Type:          "none",
			Configuration: "{}",
		}
		m := make(map[string]struct{})
		expectMap := map[string]struct{}{"Check.state_type": {}, "Check.state_configuration": {}}
		deprecated389(m, got)
		if !reflect.DeepEqual(expect, got) {
			t.Fatalf("expected %s, got %s", expect, got)
		}
		if !reflect.DeepEqual(expectMap, m) {
			t.Fatalf("expected deprecation list %s, got %s", expectMap, m)
		}
	}

	{
		got := &api.CreateCheckRequest{
			Check: &api.Check{
				StateType:          "none",
				StateConfiguration: "{}",
			},
		}
		expect := &api.CreateCheckRequest{
			Check: &api.Check{
				Type:          "none",
				Configuration: "{}",
			},
		}
		m := make(map[string]struct{})
		expectMap := map[string]struct{}{"Check.state_type": {}, "Check.state_configuration": {}}
		deprecated389(m, got)
		if !reflect.DeepEqual(expect, got) {
			t.Fatalf("expected %s, got %s", expect, got)
		}
		if !reflect.DeepEqual(expectMap, m) {
			t.Fatalf("expected deprecation list %s, got %s", expectMap, m)
		}
	}
}
