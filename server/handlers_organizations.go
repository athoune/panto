// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"context"
	"path"

	"gitlab.com/pantomath-io/panto/api"
	"gitlab.com/pantomath-io/panto/server/dbconf"
	"gitlab.com/pantomath-io/panto/server/services"
	"gitlab.com/pantomath-io/panto/util"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// ListOrganizations returns all the organizations
func (s *server) ListOrganizations(ctx context.Context, req *api.ListOrganizationsRequest) (res *api.ListOrganizationsResponse, err error) {
	var pageOffset uint64
	if req.PageToken != "" {
		valid, err := util.ValidatePageToken(req.PageToken, req, &pageOffset)
		if err != nil {
			log.Errorf("Failed to validate page token: %s", err)
			return nil, status.Errorf(codes.InvalidArgument, "Invalid page token")
		}
		if !valid {
			log.Errorf("Page token does not match request")
			return nil, status.Errorf(codes.InvalidArgument, "Page token does not match request")
		}
	}

	pageSize := req.PageSize
	if pageSize == 0 {
		pageSize = defaultPageSize
	} else if pageSize > maxPageSize {
		pageSize = maxPageSize
	}

	dbconf, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}
	orgs, err := dbconf.ListOrganizations(pageSize+1, pageOffset)
	if err != nil {
		log.Errorf("failed to list Organizations: %s", err)
		return nil, status.Error(codes.NotFound, "No Organizations found")
	}

	var nextPage bool
	if int32(len(orgs)) > pageSize {
		orgs = orgs[:pageSize]
		nextPage = true
	}

	organizations := make([]*api.Organization, len(orgs))
	for i, org := range orgs {
		organizations[i] = &api.Organization{
			Name:        path.Join(api.ResourceNameOrganization, util.Base64Encode(org.UUID)),
			DisplayName: org.Name,
		}
	}

	res = &api.ListOrganizationsResponse{
		Organizations: organizations,
	}

	if nextPage {
		res.NextPageToken, err = util.NextPageToken(req, pageOffset+uint64(len(organizations)))
		if err != nil {
			log.Warning("Failed to generate next page token")
			// Return values but don't set page token
			res.NextPageToken = ""
		}
	}

	return
}

// GetOrganization returns a specific Organization.
func (s *server) GetOrganization(ctx context.Context, req *api.GetOrganizationRequest) (*api.Organization, error) {
	db, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	if len(req.Name) == 0 {
		log.Errorf("missing organization name")
		return nil, status.Error(codes.InvalidArgument, "Missing Organization name")
	}

	organizationUUID, err := api.ParseNameOrganization(req.Name)
	if err != nil {
		log.Errorf("couldn't parse Organization name: %s", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid Organization name")
	}

	dbOrganization, err := db.GetOrganization(organizationUUID)
	if err != nil {
		log.Errorf("couldn't retrieve organization: %s", err)
		return nil, status.Error(codes.Internal, "Organization not found")
	}
	if dbOrganization == nil {
		log.Errorf("organization not found: %s", req.Name)
		return nil, status.Error(codes.NotFound, "Organization not found")
	}

	res := api.Organization{
		Name: path.Join(
			api.ResourceNameOrganization,
			util.Base64Encode(organizationUUID),
		),
		DisplayName: dbOrganization.Name,
	}

	return &res, nil
}

// UpdateOrganization update an existing Organization
func (s *server) UpdateOrganization(ctx context.Context, req *api.UpdateOrganizationRequest) (res *api.Organization, err error) {
	if len(req.Organization.Name) == 0 {
		log.Errorf("missing Organization name")
		return nil, status.Error(codes.InvalidArgument, "Missing Organization name")
	}

	// get the organization UUID from the name
	organizationUUID, err := api.ParseNameOrganization(req.Organization.Name)
	if err != nil {
		log.Errorf("couldn't parse Organization name: %s", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid Organization name")
	}

	db, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	updateOrganization := new(dbconf.Organization)
	updateOrganization.UUID = organizationUUID

	if req.UpdateMask != nil && util.Contains(req.UpdateMask.Paths, "name") {
		return nil, status.Error(codes.InvalidArgument, "Organization name is read-only")
	}

	if req.UpdateMask == nil || util.Contains(req.UpdateMask.Paths, "display_name") {
		if req.Organization.GetDisplayName() == "" {
			return nil, status.Error(codes.InvalidArgument, "Missing Organization display name")
		}
		updateOrganization.Name = req.Organization.GetDisplayName()
	}

	dbOrganization, err := db.UpdateOrganization(updateOrganization)
	if err != nil {
		log.Errorf("failed to update Organization: %s", err)
		return nil, status.Error(codes.Internal, "Couldn't update Organization")
	}

	res = &api.Organization{
		Name:        path.Join(api.ResourceNameOrganization, util.Base64Encode(dbOrganization.UUID)),
		DisplayName: dbOrganization.Name,
	}

	return
}
