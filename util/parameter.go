package util

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"
	"time"
)

// Parameter is a description of a field in the configuration JSONs
type Parameter struct {
	Name        string      `json:"name"`
	Type        string      `json:"type"`
	Description string      `json:"description"`
	Mandatory   bool        `json:"mandatory"`
	Placeholder interface{} `json:"placeholder"`
	Default     interface{} `json:"default"`
}

// UnmarshalJSON is a custom unmarshal function for Parameter
func (p *Parameter) UnmarshalJSON(b []byte) error {
	type parameter Parameter // func-local parameter avoids calling UnmarshalJSON recursively
	err := UnmarshalJSON(b, (*parameter)(p))
	if err != nil {
		return err
	}

	if p.Mandatory {
		if p.Placeholder != nil {
			if j, ok := p.Placeholder.(json.Number); ok {
				if p.Type == "int" {
					p.Placeholder, err = j.Int64()
					if err != nil {
						return err
					}
				} else if p.Type == "float" {
					p.Placeholder, err = j.Float64()
					if err != nil {
						return err
					}
				}
			}
		}
	} else {
		if p.Default != nil {
			if j, ok := p.Default.(json.Number); ok {
				if p.Type == "int" {
					p.Default, err = j.Int64()
					if err != nil {
						return err
					}
				} else if p.Type == "float" {
					p.Default, err = j.Float64()
					if err != nil {
						return err
					}
				}
			}
		}
	}

	return nil
}

// ParameterValidationError is an error returned by UnmarshalAndValidateParameters if one or more parameters failed to
// validate.
type ParameterValidationError struct {
	Errors []ParameterValidationErrorDetails
}

// ParameterValidationErrorDetails is a validation error for a single Parameter
type ParameterValidationErrorDetails struct {
	Name    string // Parameter name
	Message string // Parameter-related error message
}

func newParameterValidationError() *ParameterValidationError {
	return &ParameterValidationError{
		Errors: make([]ParameterValidationErrorDetails, 0),
	}
}

func (err *ParameterValidationError) Error() string {
	var b strings.Builder

	l := len(err.Errors)
	for i := 0; i < l-1; i++ {
		b.WriteString(err.Errors[i].Name)
		b.WriteString(": ")
		b.WriteString(err.Errors[i].Message)
		b.WriteString(", ")
	}
	b.WriteString(err.Errors[l-1].Name)
	b.WriteString(": ")
	b.WriteString(err.Errors[l-1].Message)

	return b.String()
}

// UnmarshalAndValidateParameters takes a JSON key-value map, makes sure the list of parameters complies with the schema,
// and returns a Go map with resolved typed. `null` values are treated as undefined
func UnmarshalAndValidateParameters(in []byte, ref []Parameter) (map[string]interface{}, error) {
	var l map[string]json.RawMessage
	err := json.Unmarshal(in, &l)
	if err != nil {
		return nil, err
	}

	// filter null values out as if undefined
	for k, v := range l {
		if bytes.Equal([]byte(v), []byte("null")) {
			delete(l, k)
		}
	}

	pvErr := newParameterValidationError()

	// make sure every key in the list is in the schema
Keys:
	for lkey := range l {
		for _, rcp := range ref {
			if rcp.Name == lkey {
				continue Keys
			}
		}
		pvErr.Errors = append(pvErr.Errors, ParameterValidationErrorDetails{lkey, "unknown parameter"})
	}

	m := make(map[string]interface{})
	for _, rcp := range ref {
		// make sure mandatory keys are filled
		if rcp.Mandatory {
			if _, ok := l[rcp.Name]; !ok {
				pvErr.Errors = append(pvErr.Errors, ParameterValidationErrorDetails{rcp.Name, "mandatory parameter missing"})
			}
		}

		if v, ok := l[rcp.Name]; ok {
			// make sure the type of value is the one expected
			m[rcp.Name], err = convertJSON(v, rcp.Type)
			if err != nil {
				pvErr.Errors = append(pvErr.Errors, ParameterValidationErrorDetails{rcp.Name, err.Error()})
			}
		}
	}

	if len(pvErr.Errors) > 0 {
		return nil, pvErr
	}

	return m, nil
}

func convertJSON(v json.RawMessage, t string) (interface{}, error) {
	switch t {
	case "string":
		var s string
		if err := json.Unmarshal(v, &s); err != nil {
			return nil, fmt.Errorf("couldn't convert to %s", t)
		}
		return s, nil
	case "int":
		var i int64
		if err := json.Unmarshal(v, &i); err != nil {
			return nil, fmt.Errorf("couldn't convert to %s", t)
		}
		return i, nil
	case "float":
		var f float64
		if err := json.Unmarshal(v, &f); err != nil {
			return nil, fmt.Errorf("couldn't convert to %s", t)
		}
		return f, nil
	case "time.Duration":
		var d time.Duration
		if err := json.Unmarshal(v, &d); err != nil {
			return nil, fmt.Errorf("couldn't convert to %s", t)
		}
		return d, nil
	case "bool":
		var b bool
		if err := json.Unmarshal(v, &b); err != nil {
			return nil, fmt.Errorf("couldn't convert to %s", t)
		}
		return b, nil
	}
	return nil, fmt.Errorf("unhandled type %s", t)
}

// MarshalJSON takes a Go map and returns a JSON string with `null` values filtered out as undefined
func MarshalJSON(in map[string]interface{}) ([]byte, error) {
	// filter out null values
	for k, v := range in {
		if v == nil {
			delete(in, k)
		}
	}

	// marshal to JSON
	b, err := json.Marshal(in)
	if err != nil {
		return nil, err
	}

	return b, nil
}

// UnmarshalJSON is a thin wrapper around "encoding/json".Decoder.Decode, that configures the decoder to use json.Number
// instead of forcing conversion to float64.
func UnmarshalJSON(data []byte, v interface{}) error {
	d := json.NewDecoder(bytes.NewReader(data))
	d.UseNumber()
	return d.Decode(v)
}
