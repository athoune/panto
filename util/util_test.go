// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package util

import (
	"math"
	"reflect"
	"testing"
	"time"

	"github.com/golang/protobuf/proto"
	"github.com/golang/protobuf/ptypes"
	"github.com/google/uuid"

	"gitlab.com/pantomath-io/panto/util/testdata"
)

func TestBase64(t *testing.T) {
	// Test UUIDs:
	// 00000000-0000-0000-0000-000000000000
	// e8ebd5a4-236c-47e8-b25c-a30a8d868438
	// c82166a5-3b01-4cd6-8ec4-c76a5b37d398
	// f44d6bc6-99a2-47a0-a8b1-0a82f37274e2
	// 5bd8f4ea-3699-4104-8bce-6e3a2847009d
	// bcb8dbec-b743-408a-8ebd-756cf0ca8d91
	// cef35d89-a812-46c0-b856-14742267f389
	// c725e1da-fbff-4d30-a9db-60ff4ed8a007
	// fbd6779f-70f3-46e9-873c-4561f7614ab3
	// b5f41221-4ad3-40c8-932d-59e3de1afd8b
	// b2c95081-82db-4dc9-82b3-bb08e05e0d26

	testUUIDs := []uuid.UUID{
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{232, 235, 213, 164, 35, 108, 71, 232, 178, 92, 163, 10, 141, 134, 132, 56},
		{200, 33, 102, 165, 59, 1, 76, 214, 142, 196, 199, 106, 91, 55, 211, 152},
		{244, 77, 107, 198, 153, 162, 71, 160, 168, 177, 10, 130, 243, 114, 116, 226},
		{91, 216, 244, 234, 54, 153, 65, 4, 139, 206, 110, 58, 40, 71, 0, 157},
		{188, 184, 219, 236, 183, 67, 64, 138, 142, 189, 117, 108, 240, 202, 141, 145},
		{206, 243, 93, 137, 168, 18, 70, 192, 184, 86, 20, 116, 34, 103, 243, 137},
		{199, 37, 225, 218, 251, 255, 77, 48, 169, 219, 96, 255, 78, 216, 160, 7},
		{251, 214, 119, 159, 112, 243, 70, 233, 135, 60, 69, 97, 247, 97, 74, 179},
		{181, 244, 18, 33, 74, 211, 64, 200, 147, 45, 89, 227, 222, 26, 253, 139},
		{178, 201, 80, 129, 130, 219, 77, 201, 130, 179, 187, 8, 224, 94, 13, 38},
	}

	testB64s := []string{
		"AAAAAAAAAAAAAAAAAAAAAA",
		"6OvVpCNsR-iyXKMKjYaEOA",
		"yCFmpTsBTNaOxMdqWzfTmA",
		"9E1rxpmiR6CosQqC83J04g",
		"W9j06jaZQQSLzm46KEcAnQ",
		"vLjb7LdDQIqOvXVs8MqNkQ",
		"zvNdiagSRsC4VhR0ImfziQ",
		"xyXh2vv_TTCp22D_TtigBw",
		"-9Z3n3DzRumHPEVh92FKsw",
		"tfQSIUrTQMiTLVnj3hr9iw",
		"sslQgYLbTcmCs7sI4F4NJg",
	}

	for i, u := range testUUIDs {
		if s := Base64Encode(u); s != testB64s[i] {
			t.Errorf("%s is not correctly encoded: expected %s, got %s", u, testB64s[i], s)
		}
	}
	for i, s := range testB64s {
		u, err := Base64Decode(s)
		if err != nil {
			t.Errorf("error decoding %s: %s", s, err)
		}
		if u != testUUIDs[i] {
			t.Errorf("%s is not correctly decoded: expected %s, got %s", s, testUUIDs[i], u)
		}
	}
}

func TestNextPageToken(t *testing.T) {

	// Simplest test
	req := &testdata.UtilTestRequest{
		I:         13,
		L:         37,
		S:         "Barbès",
		PageToken: "fqsehjflqshfjlqsdfhjkqsldfhjsdklfghqjkl",
	}

	memo := proto.Clone(req)

	tok, err := NextPageToken(req)
	if err != nil {
		t.Errorf("Error generating NextPageToken: %s", err)
	}

	if !proto.Equal(req, memo) {
		t.Errorf("NextPageToken altered request: in %s, out %s", memo, req)
	}

	res, err := ValidatePageToken(tok, req)
	if err != nil {
		t.Errorf("Error validating token: %s", err)
	}
	if !res {
		t.Errorf("Failed to validate token.")
	}

	now := time.Now()
	// Test extra data with all types
	i := int(math.MaxInt32)
	i8 := int8(math.MaxInt8)
	i16 := int16(math.MaxInt16)
	i32 := int32(math.MaxInt32)
	i64 := int64(math.MaxInt64)
	u := uint(math.MaxUint32)
	u8 := uint8(math.MaxUint8)
	u16 := uint16(math.MaxUint16)
	u32 := uint32(math.MaxUint32)
	u64 := uint64(math.MaxUint64)
	f32 := float32(math.MaxFloat32)
	f64 := float64(math.MaxFloat64)
	s := "Barbès"
	b := []byte("Rochechouart")
	ts, _ := ptypes.TimestampProto(now)
	tok, err = NextPageToken(req, i, i8, i16, i32, i64, u, u8, u16, u32, u64, f32, f64, s, b, ts)
	if err != nil {
		t.Errorf("Error generating NextPageToken: %s", err)
	}

	if !proto.Equal(req, memo) {
		t.Errorf("NextPageToken altered request: in %s, out %s", memo, req)
	}

	i = 0
	i8 = 0
	i16 = 0
	i32 = 0
	i64 = 0
	u = 0
	u8 = 0
	u16 = 0
	u32 = 0
	u64 = 0
	f32 = 0
	f64 = 0
	s = ""
	b = []byte{}
	ts.Reset()
	res, err = ValidatePageToken(tok, req, &i, &i8, &i16, &i32, &i64, &u, &u8, &u16, &u32, &u64, &f32, &f64, &s, &b, ts)
	if err != nil {
		t.Errorf("Error validating token: %s", err)
	}
	if !res {
		t.Errorf("Failed to validate token.")
	}
	tspb, _ := ptypes.TimestampProto(now)
	if i != int(math.MaxInt32) ||
		i8 != int8(math.MaxInt8) ||
		i16 != int16(math.MaxInt16) ||
		i32 != int32(math.MaxInt32) ||
		i64 != int64(math.MaxInt64) ||
		u != uint(math.MaxUint32) ||
		u8 != uint8(math.MaxUint8) ||
		u16 != uint16(math.MaxUint16) ||
		u32 != uint32(math.MaxUint32) ||
		u64 != uint64(math.MaxUint64) ||
		f32 != float32(math.MaxFloat32) ||
		f64 != float64(math.MaxFloat64) ||
		s != "Barbès" ||
		!reflect.DeepEqual(b, []byte("Rochechouart")) ||
		!proto.Equal(tspb, ts) {
		t.Errorf("Invalid value decoding extra data from token")
	}

	// Test validation with another PageToken in request
	req2 := &testdata.UtilTestRequest{
		I:         13,
		L:         37,
		S:         "Barbès",
		PageToken: "qdfgjerogzjerokgjzeorgzefgo",
	}

	res, err = ValidatePageToken(tok, req2)
	if err != nil {
		t.Errorf("Error validating token: %s", err)
	}
	if !res {
		t.Errorf("Failed to validate token.")
	}

	// Test failed validation with different request
	req3 := &testdata.UtilTestRequest{
		I:         143,
		L:         37123,
		S:         "Rochechouart",
		PageToken: "qdfgjerogzjerokgjzeorgzefgo",
	}

	res, err = ValidatePageToken(tok, req3)
	if err != nil {
		t.Errorf("Error validating token: %s", err)
	}
	if res {
		t.Errorf("Unexpected validation of token.")
	}

	// Test asking for more data than there is
	tok, err = NextPageToken(req)
	if err != nil {
		t.Errorf("Error generating NextPageToken: %s", err)
	}

	_, err = ValidatePageToken(tok, req, &i8)
	if err == nil {
		t.Errorf("Expected error getting data from token")
	}
}
