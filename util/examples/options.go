// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"bytes"
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"gitlab.com/pantomath-io/panto/util"
)

// A template YAML configuration.
var conf = []byte(`
bool-opt: true
string-opt: "Hello World"
conf-only:
  hello: "Hello"
  world: "World"
nested:
  one: 1
`)

var opts = []util.Option{
	{
		Name:    "bool-opt",
		Default: false,
		Usage:   "a boolean option",
		Env:     "BOOL_OPT",
		Flag:    "bool-opt",
		Short:   "b",
	},
	{
		Name:    "string-opt",
		Default: "",
		Usage:   "a string option",
		Env:     "STRING_OPT",
		Flag:    "string-opt",
		Short:   "s",
	},
	{
		Name:    "nested.one",
		Default: 1000000,
		Usage:   "a nested int option",
		Env:     "NESTED_ONE",
		Flag:    "nested-one",
		Short:   "n",
	},
	{
		Name:    "nested.two",
		Default: 2,
		Usage:   "another nested int option",
		Env:     "NESTED_TWO",
		Flag:    "nested-two",
		Short:   "t",
	},
}

var rootCmd = cobra.Command{
	Use:   "options",
	Short: "options outputs the options as JSON",
	RunE: func(cmd *cobra.Command, args []string) error {
		// Bind all env vars and flags
		for _, o := range opts {
			util.BindOption(o, cmd.Flags())
		}
		// Read configuration "file"
		viper.SetConfigType("yaml")
		if err := viper.ReadConfig(bytes.NewBuffer(conf)); err != nil {
			return err
		}
		// Print configuration
		viper.Debug()

		return nil
	},
}

func init() {
	// Add all options
	for _, o := range opts {
		if err := util.AddOption(o, rootCmd.Flags()); err != nil {
			fmt.Printf("error: failed to add option %s: %s", o.Name, err)
			os.Exit(1)
		}
	}
}

func main() {
	if err := rootCmd.Execute(); err != nil {
		os.Exit(1)
	}
}
