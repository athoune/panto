# Process

## Issues

### Templates

Issues in Panto are usually created using one of the available templates.

* [Feature Proposal](https://gitlab.com/pantomath-io/panto/blob/master/.gitlab/issue_templates/Feature%20Proposal.md) is used to describe an upcoming feature.
* [Bug](https://gitlab.com/pantomath-io/panto/blob/master/.gitlab/issue_templates/Bug.md) is used to report and describe a bug (crash, malfunction) in the software.
* [Engineering Task](https://gitlab.com/pantomath-io/panto/blob/master/.gitlab/issue_templates/Engineering%20Task.md) is used to describe a task that needs to be accomplished, but doesn't answer directly to a user-facing issue.

### Labels

[Labels](https://gitlab.com/pantomath-io/panto/labels) are attach to issues to provide context and help understand the scope and status of the issue.

#### Issue type

* `Feature`: a new feature to be built
* `Bug`: a dysfunction to be fixed
* `Engineering`: an task that affects the inner workings of the software

> TODO: `Product`, `Marketing`, `Finance`, `Legal`...

#### Product

The products that are affected by this issue.

* `panto`: the server
* `panto-agent`: the agent
* `panto-web`: the web client
* `API`: the public API

#### Priority

* `P1`: High priority
* `P2`: Medium priority
* `P3`: Low priority

#### Status

Used to follow the status of the issue over its lifetime.

* `Doing`: the task is currently being accomplished by the assignee
* `Blocked`: the task was started, but cannot continue due to circumstances, usually described in the issue discussions
* `Ready to review`: the task is considered finished by the assignee and waiting for a collaborator to review and discuss

## Milestones

A Milestone is defined as the list of features, improvements and bugfixes to be delivered in the current iteration. It culminates in a new release, (usually minor, occasionally major).

The Milestones are not bound in time, but in features. Rather than setting a release date for the next version, the next version is released as soon as all tasks in the Milestone are done.

### Product pass

A product collaborator will prepare and prioritize an initial set of features, improvements and bugfixes to be delivered in this Milestone. This prioritization will be done according to the current long-term roadmap, user feedback, etc. Issues and user stories should be chosen from the backlog or created at this point.

### Milestone review

A review of the Milestone goals will be performed with the developers, designers, copywriters, etc. that will be implicated in this release. At this point, issues are reviewed, discussed and evaluated by the team. This is a good time to explain why a specific improvement was prioritized, or to raise any technical "red flags" that weren't apparent to the product collaborator.

At the end of this review, the issue list should be considered "frozen", and all issues should be given a priority.

### Technical pass

The technical collaborators (developers, designers, etc.) will then attempt to evaluate the steps necessary to deliver the features. This can mean breaking down an issue into smaller, technical tasks, discussing design and engineering, sharing knowledge and resources, etc.

At this point, an issue is evaluated for "weight". Weight is an abstract evaluation of the difficulty of a task, inspired by the Scrum methodology "Story points". Weight is more important as a value relative to other weights, rather than an actual numeric value. To better reflect these abstract values, and represent the uncertainty of harder tasks relatively to easier ones, a discrete set of values is used: 1, 2, 3, 5, 8. (Sometimes thought of as "t-shirt size" estimation: XS, S, M, L, XL.) The rule of thumb for weighting is the following:

* **1 - XS:** A very easy task that can be performed without hassle. Usually fits in a few hours. Shouldn't entail any risk of breaking the product. E.g. fixing a bad comparison in an `if` statement, correcting a typo on the website.
* **2 - S:** An easy task that will ask for a bit of work, but still doesn't imply any excess complexity. Usually fits in a day. E.g. developing a simple Probe, writing a newsletter.
* **3 - M:** A regular task that will take some time, and will touch different parts of the project. Usually takes a couple of days. E.g. adding a feature across the stack (from DB to web client), creating a new documentation page.
* **5 - L:** A harder task that touches complex parts of the project, resulting in reduced visibility. Usually takes up to 3-4 days. E.g. fixing a pernicious, hard-to-track bug, creating a video tutorial.
* **8 - XL:** A tough task that will require much work, or currently has no known solution. Can take up to a week, or a week-end of binge-coding. E.g. a long refactor, switching the underlying database engine.

At the end of this pass, all issues should be weighted and ready to be executed.

### Workflow

While the milestone is open, collaborators will pick up one of the currently unassigned issues, starting with the highest priority. The "Doing" label will be set on the task, and the task will be assigned to whomever picked it up.

> TODO: Workflow

## Merge requests

> TODO: Merge requests