# Panto Testing

In order to test the application, you need to set it up.

_Note_: all the following commands are run from the root of the corresponding repository.

## Create and initialize the Configuration Database

In order to manage schema migration, we use [goose](https://github.com/loderunner/goose), embedded in `panto-ctl`.

To initialize the Configuration Database, you just need to run migrations:

```bash
$ ./bin/panto-ctl dbconf upgrade --db-path data/panto.sqlite
```

You can manually insert some fixtures data:

```bash
$ sqlite3 data/panto.sqlite
sqlite> .read data/fixtures/001_MinimalFixtures.sql
```

## Create and initialize the TimeSeries Database

To create the TSDB, you just need to initialize it with `influxdb`:

```bash
$ influx
```

You are now in the `influxdb` shell, and you can create the database:

```
> CREATE DATABASE panto
```

## Configure the Server

The default configuration for the server works out-of-the-box, but you can adapt it to your needs:

```yaml
log:
  file: "log/panto.log"
  syslog: false

server:
  address: "localhost:7575"
  certfile: "conf/server.crt"
  certkey: "conf/server.key"

db:
  path: "data/panto.sqlite"
```

## Configure the Agent

The default configuration for the agent works out-of-the-box, but you can adapt it to your needs:

```yaml
server:
  address: "localhost:7575"
  certfile: "conf/server.crt"

agent:
  id: "agent.pantomath.io"

log:
  file: "log/pantomath-agent.log"
  syslog: false
```

## Running the unit tests

The unit tests for the Panto project can be run by calling

```shell
make test
```

from the repository root.

Panto also has a **"live test"** mode that runs tests against the various services and storage required. To run these live tests, first make sure you've configured everything as explained in this guide, then use an environment variable when running the tests.

```shell
LIVE_TEST=y make test
```

## Running functional tests

There is currently no tool or framework to run automated functional tests on `panto`, `panto-agent` or `panto-web`. Should you want to test the `panto` solution, you need to do it manually.

For instance, if you want to test the alerts system, you can configure a Check so that the evaluated State is `WARNING` or `CRITICAL`. If an alert is configured with a `State Recurrence` where `recurrence` is set to `1`, you can trigger an Alert (almost) on demand.

Take the `ping` probe, where you can configure `count` (i.e. the number of ICMP packets to send), and you get a metric called `sent` (i.e. the number of packets sent). It's pretty easy to configure the Probe to send X packets, and then to check if the result has `sent` lower than X (highly improbable). You just need to define `sent < X` as a condition for `WARNING` state.

This is applicable to any of the State evaluation algorithm:

* Threshold: test `sent` against the value of `count`;
* History: test `sent` against its previous value (which shouldn't change, if you don't change `count`);
* Trend: test the trend that should be 0, as soon as you don't change `count`.
