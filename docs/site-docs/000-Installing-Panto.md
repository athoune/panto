% Installing Panto

To install Panto, first make sure you have installed all of the [prerequisites](#prerequisites), then download and install the application using your preferred method.

⚠️ Note: all commands prepended with a `#` prompt might need to be run as `root` or using `sudo`. The commands started with a `$` prompt could be run as any unprivileged user, except stated otherwise.

## Prerequisites

Panto uses [InfluxDB](https://www.influxdata.com/) to store results from Probes. Ensure you have a running instance of InfluxDB reachable by Panto.

Panto uses [Caddy](https://caddyserver.com) to serve the web client. Download and install Caddy from the official website to deploy the web client.

### Panto environment

The recommended setup is to run Panto as a dedicated non privileged user. Create this user and setup the home directory for Panto:

```shell
[root]# groupadd panto
[root]# useradd -g panto -d /opt/panto -m -s /bin/false panto
```

Assuming default locations, it is also recommended to create the main directories Panto will need:

```shell
[root]# mkdir -p /var/lib/panto /var/log/panto /etc/panto
[root]# chown -R panto:panto /var/lib/panto /var/log/panto /etc/panto
```

## Get Panto

### Download binaries

Download the release from the [releases](https://packages.panto.app/bin/stable) page. Official binary distributions are only available for Linux 4.9 and later on x86_64. For other platforms, you will have to [build from source](#build-from-source).

After downloading the archive, extract it to a destination path. For example, using `/opt/panto`:

```shell
[panto]$ tar -C /opt/panto -xzf panto-$(VERSION)-$(OS)_$(ARCH).tar.gz
[panto]$ ln -sf /opt/panto/panto-$(VERSION)-$(OS)_$(ARCH)/* /opt/panto/
```

⚠️ These commands should be run as `panto`, the dedicated Panto user, who will run the server/agent.

### Setup systemd

The convenient way to run Panto is to integrate it in [`systemd`](https://freedesktop.org/wiki/Software/systemd/). Assuming default locations, [this](https://gitlab.com/pantomath-io/panto/blob/master/docs/config/panto.service) `systemd` unit file should do. Copy it to `/etc/systemd/system/panto.service` and adapt the values.

`systemd` needs to be reloaded:

```shell
[root]# chmod 664 /etc/systemd/system/panto.service
[root]# systemctl daemon-reload
```

And you need to `enable` the unit, to auto-start it on reboot:

```shell
[root]# systemctl enable panto
```

The Panto daemon can now be controled as any other `systemd` service:

```shell
[root]# systemctl status panto
[root]# systemctl start panto
[root]# systemctl stop panto
```

This can be adapted to run the `panto-agent` daemon.

### Setup init script

For init users, the convenient way to run `panto-agent` is to integrate it in `initd`. Assuming default locations, [this](https://gitlab.com/pantomath-io/panto/blob/master/docs/config/panto-agent.initd) `initd` script should do. Copy it to `/etc/init.d/panto-agent` and adapt the values.

`initd` needs to be reloaded:

```shell
[root]# chmod 755 /etc/init.d/panto-agent
[root]# update-rc.d panto-agent defaults
```

The `panto-agent` daemon can now be controled as any other `initd` script:

```shell
[root]# service panto-agent start
[root]# service panto-agent status
[root]# service panto-agent stop
```

This can be adapted to run the `panto` daemon.

### Linux packages

**TODO**

### Build from source

For a detailed walkthrough, see the [developer documentation](https://gitlab.com/pantomath-io/panto/blob/develop/docs/BUILD.md).

## Initialization

The `panto-ctl` tool can be used to initialize your server environment and configuration file before running Panto for the first time. Type:

```shell
[panto]$ panto-ctl init
```

⚠️ This command should be run as `panto`, the dedicated Panto user, who will run the server/agent.

A command-line dialog will guide you through the steps of initializing your installation, setting up the database, and configuring the server. See the [`panto-ctl` documentation](004-Running-Panto.md#panto-ctl) for more details.

⚠️ This step has already been performed on the Docker images. Use `panto-ctl` only if you wish to reinitialize your Panto environment.

### Docker

The Panto server can be run using Docker containers. See [Docker](https://www.docker.com/what-docker) for more information about containers.

⚠️ Running Panto in Docker stores all your data to the containers. **Destroying the containers will delete your data.** Make sure to save all your data before destroying the containers, or use [Docker volumes](https://docs.docker.com/storage/volumes/).

#### Docker image

A Docker image is available from the official Panto registry on [GitLab](https://gitlab.com/pantomath-io/panto/). To download and run the image, type:

```shell
$ docker run -p 7575:7575 -p 7576:7576 -p 8080:8080 registry.gitlab.com/pantomath-io/panto/panto
```

This will pull a pre-built docker image from the registry, and will run it, exposing ports for the API and web client.