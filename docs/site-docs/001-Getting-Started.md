% Getting Started

Panto is a platform for monitoring your infrastructure, servers and applications in production. Agents collect metrics about your systems in real time, and report their health and performance to the server. The server stores metrics, evaluates the state according to user-defined rules, and, in case the state is considered dysfunctional, dispatches alerts to your teams.

## Running Panto

Once you've [downloaded and installed](000-Installing-Panto.md) the Panto files, it's time to get Panto running. Panto comes in three parts: the [server](#server), the [agents](#agent) and the [web client](#web-client).

### Server

The server is the "brain" of Panto. It stores the monitoring data for your environment: the metrics from your targets, and the setup of targets, probes and agents in your infrastructure. This is where the agents will report metrics from targets; this is where the web client will fetch and send data to display and update your environment; this is where alerts will be spawned when an agent reports critical behavior.

To run the server, make sure the `panto` executable is on your PATH and type:

```shell
$ panto --conf=/path/to/panto.yaml
```

replacing `/path/to/panto.yaml` with the path to your configuration file (such as `/etc/panto/panto.yaml`).

To learn more about configuration options, use `panto --help` or see the [documentation](004-Running-Panto.md#panto).

### Agent

The agents are the workers of your Panto environment. Agents collect performance and application metrics and report back to the Panto server.

To run the agent, make sure the `panto-agent` executable is on your path and type:

```shell
$ panto-agent --conf=/path/to/panto-agent.yaml
```

replacing `/path/to/panto-agent.yaml` with the path to your configuration file (such as `/etc/panto/panto-agent.yaml`).

To learn more about configuration options, use `panto-agent --help`or check the [documentation](004-Running-Panto.md#panto-agent).

### Web client

The web client is a static web application that runs in the browser. The client is the main frontend to display metrics, create targets, configure alerts, etc. Panto uses [Caddy](https://caddyserver.com/) to serve the files.

After installing Caddy, make sure the `caddy` executable is on your PATH and type:

```shell
$ PANTO_ADDRESS=http://panto.yourdomain.com caddy -conf=/path/to/Caddyfile
```

replacing:

* `/path/to/Caddyfile` to the path of the Panto installation, e.g. `/opt/panto/www/Caddyfile`
* `http://panto.yourdomain.com` by a URL where the client running in the user's browser can reach the Panto API

To learn more about configuration options, see the [documentation](004-Running-Panto.md#web-client).

### Proxy

You can connect directly to your `panto` or `panto-web` applications, or you can have them behind a proxy, such as [nginx](https://nginx.org/). Installing nginx is out of the scope of this documentation, but here are examples of configuration. Obviously, you'll need to adapt these, to include a proper TLS certificate, or to setup logs. The configurations shown focus on the proxy part.

For `panto-web`, it's pretty simple, as the application is serving HTTP content:

```
upstream panto-web {
    server localhost:8080 fail_timeout=0;
}

server {
    listen 443 ssl;
    server_name panto.app;

    location / {
        proxy_pass http://panto-web;
        proxy_http_version 1.1;
        proxy_set_header X-Forwarded-For $remote_addr;
        if ($uri != '/') {
                expires 30d;
            }
    }
}
```

For `panto`, there are 2 sides: the gRPC server, and the REST gateway. The latter is as straightforward as `panto-web`:

```
upstream panto-api {
    server localhost:7576 fail_timeout=0;
}

server {
    listen 443 ssl;
    server_name api.panto.app;

    location / {
        proxy_pass http://panto-api;
        proxy_http_version 1.1;
        proxy_set_header X-Forwarded-For $remote_addr;
    }
}
```

The gRPC server can also be proxied, using the [gRPC module](http://nginx.org/en/docs/http/ngx_http_grpc_module.html) (introduced in nginx-1.13.10: read [announcement](https://www.nginx.com/blog/nginx-1-13-10-grpc/)).

```
server {
    listen 443 ssl http2;
    server_name rpc.panto.app;

    location / {
        grpc_pass grpc://127.0.0.1:7575;
    }
}
```

Note that this configuration assumes that gRPC is started with `--no-tls` activated, and the TLS part is handled by the nginx (so the agent **must not** use `--no-tls`).
Please refer to [the official documentation](http://nginx.org/en/docs/http/ngx_http_grpc_module.html) for other options.

## Your first environment

### Agent

### Target

### Probe

### State

### Alert

## Troubleshooting
