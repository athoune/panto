% Updating Panto

## Download latest Panto

Download the release from the [releases](https://packages.panto.app/bin/stable) page. Official binary distributions are only available for Linux 4.9 and later on x86_64. For other platforms, you will have to [build from source](001-Installing-Panto.md#build-from-source).

## Install

After downloading the archive, extract it to a destination path. For example, using `/opt/panto`:

```shell
[panto]$ tar -C /opt/panto -xzf panto$(VERSION)-$(OS)_$(ARCH).tar.gz
```

⚠️ This command should be run as `panto`, the dedicated Panto user, who will run the server/agent.

## Migrations

```shell
[panto]$ /opt/panto/panto-ctl dbconf upgrade --conf /etc/panto/panto.yaml
```
