% Configuring States

A _state_ is the evaluated from the conditions defined in a __check__ on the __result__ of a probe. It can have 4 different values:

* `Unknown`: when the state can't be determined, either because there's no value, or because the conditions could not be evaluated;
* `OK`: when the result is considered as normal;
* `Warning`: when the result is considered as worth attention;
* `Critical`: when the result shows an issue that needs a reaction;
* `Missing Data`: when the server has not received any results from the agent for too long.

## Algorithms

To determine in which state the result of probe is, you can choose between 3 different algorithms.

### Threshold

The value of one metric is compared to a fixed reference (e.g. free memory is above 1GB).

### Trend

The trend of the last X values of the metric is compared to a fixed reference (e.g. the rate of change of HTTP/500 errors over the last 25 occurences is below 2).

### History

The value of one metric is compared to the value of the same metric taken Y seconds ago (e.g. the count of successful checkouts compared to the same count 24 hours ago).

## Conditions

Each algorithm can use any number of conditions, bound together with a global logical operator `AND` or `OR` (meaning respectively all conditions should be true or any condition should be true).  

Here is the lists of the supported comparison operators.  

### Numeric comparison

Less than (`<`)
Less than or equal (`≤`)
Equal (`=`)
Not equal (`≠`)
Greater than or equal (`≥`)
Greater than (`>`)


## String comparison

Equal (`=`)
Not equal (`≠`)
Match (regular expression `match`)
Find (regular expression `find`)
