% Running Panto

For the `panto` and `panto-agent` executables, the configuration file format is [YAML](http://yaml.org/). Configuration files are hierarchical in nature and paths are separated using a dot (`'.'`). E.g. for `log.file`:

```yaml
log:
  file: /var/log/panto/panto.log
```

# Executables

As a general rule, run any executable with `--help` for more information.

## `panto`

The main server executable. Run this to deploy the Panto service on a machine.

| Configuration file                | Command-line          | Environment variable        | Usage                                                                         | Default                       |
| --------------------------------- | --------------------- | --------------------------- | ----------------------------------------------------------------------------- | ----------------------------- |
|                                   | `--quiet`, `-q`       |                             | suppress all output                                                           | `false`                       |
|                                   | `--version`, `-V`     |                             | display version and exit                                                      | `false`                       |
|                                   | `--conf`              |                             | path to configuration file                                                    |                               |
| `verbose`                         | `--verbose`, `-v`     | `PANTO_VERBOSE`             | set verbosity level (0: silent, 1: warnings and errors, 2: verbose, 3: debug) | `1`                           |
| `log.file`                        | `--log-file`          | `PANTO_LOG_FILE`            | path of a file to log Panto output                                            |                               |
| `log.syslog`                      |                       | `PANTO_LOG_SYSLOG`          | log Panto output to syslog                                                    | `false`                       |
| `server.grpc-address`             | `--grpc-address`      |                             | address to bind gRPC API to                                                   | `:7575`                       |
| `server.rest-address`             | `--rest-address`      |                             | address to bind REST API to                                                   | `:7576`                       |
| `server.allow-origin`             | `--allow-origin`      |                             | comma-separated list of origin addresses to allow in the via CORS             |                               |
| `server.certfile`                 | `--certfile`          | `PANTO_CERTFILE`            | path to a TLS certificate file                                                |                               |
| `server.certkey`                  | `--certkey`           | `PANTO_CERTKEY`             | path to a TLS private key file                                                |                               |
| `server.no-tls`                   | `--no-tls`            |                             | disable SSL/TLS                                                               | `false`                       |
| `server-info.public-grpc-address` |                       | `PANTO_PUBLIC_GRPC_ADDRESS` | the public address where a client can reach the gRPC API                      |                               |
| `server-info.public-rest-address` |                       | `PANTO_PUBLIC_REST_ADDRESS` | the public address where a client can reach the REST API                      |                               |
| `influxdb.address`                | `--influxdb-address`  |                             | address of an InfluxDB server                                                 | `http://localhost:8086`       |
| `influxdb.database`               | `--influxdb-database` |                             | name of the InfluxDB database                                                 | `panto`                       |
| `db.path`                         | `--db-path`           |                             | path to a SQLite configuration database file                                  | `/var/lib/panto/panto.sqlite` |
| `smtp.server`                     |                       | `PANTO_SMTP_SERVER`         | address of a SMTP server                                                      |                               |
| `smtp.port`                       |                       | `PANTO_SMTP_PORT`           | port to use for SMTP server                                                   | `587`                         |
| `smtp.username`                   |                       | `PANTO_SMTP_USERNAME`       | username for SMTP server                                                      |                               |
| `smtp.password`                   |                       | `PANTO_SMTP_PASSWORD`       | password to use for SMTP server                                               |                               |
| `smtp.from`                       |                       | `PANTO_SMTP_FROM`           | email address to send the mails from                                          | `hello@panto.app`             |

## `panto-agent`

The executable for the agents. Typically runs on a target machine and gathers metrics that it reports to the server. It can also be run on a dedicated machine, to monitor a remote target.

| Configuration file          | Command-line            | Environment variable     | Usage                                                                         | Default |
| --------------------------- | ----------------------- | ------------------------ | ----------------------------------------------------------------------------- | ------- |
|                             | `--quiet`, `-q`         |                          | suppress all output                                                           | `false` |
|                             | `--version`, `-V`       |                          | display version and exit                                                      | `false` |
|                             | `--conf`                |                          | path to configuration file                                                    |         |
| `verbose`                   | `--verbose`, `-v`       | `PANTO_AGENT_VERBOSE`    | set verbosity level (0: silent, 1: warnings and errors, 2: verbose, 3: debug) | `1`     |
| `log.file`                  | `--log-file`            | `PANTO_AGENT_LOG_FILE`   | path of a file to log output                                                  |         |
| `log.syslog`                |                         | `PANTO_AGENT_LOG_SYSLOG` | log output to syslog                                                          | `false` |
| `agent.name`                | `--name`, `-n`          | `PANTO_AGENT_NAME`       | name of the agent in the Panto API                                            |         |
| `agent.timeout`             | `--timeout`             | `PANTO_AGENT_TIMEOUT`    | timeout duration                                                              | `45s`   |
| `agent.max-spooled-results` | `--max-spooled-results` | `PANTO_AGENT_TIMEOUT`    | maximum number of results spooled while server can't be reached               | `100`   |
| `server.address`            |                         |                          | address of Panto server (host:port)                                           |         |
| `server.certfile`           | `--certfile`            |                          | path to a TLS certificate file                                                |         |
| `server.no-tls`             | `--no-tls`              |                          | disable SSL/TLS                                                               | `false` |


## `panto-ctl`

The administration tool for the panto server. Manipulates configuration files and the database. `panto-ctl` has 2 subcommands.

### `init` subcommand

Run `panto-ctl init` to initialize configuration and database before running Panto for the first time.

| Configuration file | Command-line       | Environment variable | Usage                                                         | Default |
| ------------------ | ------------------ | -------------------- | ------------------------------------------------------------- | ------- |
| `dry-run`          | `--dry-run`, `-n`  |                      | do not perform any operations, just output what would be done | `false` |
| `install-prefix`   | `--install-prefix` |                      | the default prefix for the Panto installation                 | `/`     |


### `dbconf` subcommand

Run `panto-ctl dbconf` to perform operations on the database. Subcommands:

* `check`: makes sure the current version of the configuration database is the configured one
* `list-migrations`: lists the pending migrations to be applied on the configuration database
* `upgrade`: applies all the migrations to the configuration database
* `downgrade`: reverts the last migration on the configuration database
* `version`: displays the current version of the configuration database

| Configuration file | Command-line | Environment variable | Usage                            | Default |
| ------------------ | ------------ | -------------------- | -------------------------------- | ------- |
| `db.path`          | `--db-path`  |                      | path to the SQLite database file |         |
|                    | `--conf`     |                      | path to the configuration file   |         |

# Web client

The main frontend for Panto, a static website served with [Caddy](https://caddyserver.com). Set the `PANTO_ADDRESS` environment variable inside the container to a URL where _the user's web browser_ can reach a Panto server's REST API. Set the (optional) `PANTO_GA_ID` environment variable inside the container to a valid Google Analytics tracking ID to enable it.

```shell
PANTO_GA_ID=UA-01234567-8 PANTO_ADDRESS=http://panto.yourdomain.com caddy -conf=/opt/panto/www/Caddyfile
```

# Docker

Panto hosts 3 Docker images on the GitLab registry.

* `registry.gitlab.com/pantomath-io/panto/panto-server`: The Panto server
* `registry.gitlab.com/pantomath-io/panto/panto-web`: The Panto web client
* `registry.gitlab.com/pantomath-io/panto/panto`: All-in-one image

## Server

The Panto server image runs a server in a docker container. It exposes port 7575 for the gRPC API and port 7576 for the REST API. Set the `INFLUXDB_ADDRESS` environment variable inside the container to a URL where the container can reach an InfluxDB server.

Run the container by typing:

```shell
docker run -p 7575:7575 -p 7576:7576 --env INFLUXDB_ADDRESS=http://influxdb:8086 registry.gitlab.com/pantomath-io/panto/panto-server
```

## Web client

The Panto web client image runs a static web server in a docker container. It exposes port 8080. Set the `PANTO_ADDRESS` environment variable inside the container to a URL where _the user's web browser_ can reach a Panto server's REST API.

Run the container by typing:

```shell
docker run -p 8080:8080 --env PANTO_ADDRESS=http://panto.yourdomain.com registry.gitlab.com/pantomath-io/panto/panto-web
```

## All-in-one

The "all-in-one" image contains:

* The Panto server
* The Panto web client
* An InfluxDB server

Run the container by typing:

```shell
docker run -p 7575:7575 -p 7576:7576 -p 8080:8080 --env PANTO_ADDRESS=http://panto.yourdomain.com registry.gitlab.com/pantomath-io/panto/panto
```

## Docker Compose

A more modular, multi-container setup is also available, using [Docker Compose](https://docs.docker.com/compose/). After making sure `docker-compose` is installed, download the [`docker-compose.yml`](https://gitlab.com/pantomath-io/panto/raw/master/docker/docker-compose.yml) file to your computer and type:

```shell
$ docker-compose up
```

from the same directory as the `docker-compose.yml` file.

This will download all the necessary images from the official repositories, and run all containers and connect the pieces for you.

When running for the first time, you need to initialize the persistent database. Use the following command, once the docker compose is up:

```shell
$ docker exec -it panto-server /usr/bin/panto-ctl init data --db-path=/var/lib/panto/panto.sqlite --organization-name=Organization
```
