# Running Panto  <!-- omit in TOC -->

This document collects all the prerequisites, dependencies and steps required to run Panto. You can run Panto from your machine, in the official [Vagrant VM](BUILD.md#vagrant) or using [Docker](#docker) (see below).

- [Prerequisites](#prerequisites)
    - [InfluxDB](#influxdb)
    - [SQLite](#sqlite)
    - [Caddy](#caddy)
- [Initializing environment](#initializing-environment)
- [Running Panto](#running-panto)
- [Updating database](#updating-database)
- [Running Panto Agent](#running-panto-agent)
- [Running Panto Web Client](#running-panto-web-client)
- [Docker](#docker)
    - [Pulling the images](#pulling-the-images)
    - [Running with docker](#running-with-docker)

If you are using your own machine or the Vagrant VM, see the [build instructions](BUILD.md) to build Panto from source.

## Prerequisites

Panto relies on a number of outside dependencies to be available at run time, for persistence and other services.

### InfluxDB

[InfluxDB](https://www.influxdata.com/time-series-platform/influxdb) is a time-series database (TSDB) used by Panto to persist the results from the agents. To store and retrieve results, Panto needs an instance of InfluxDB to be running.

Download and install InfluxDB for your platform by following the [official instructions](https://portal.influxdata.com/downloads#influxdb) from the InfluxDB website.

### Caddy

[Caddy](https://caddyserver.com/) is a static HTTP file server, used by Panto to power the web client.

Install Caddy using the `go get` command. Type:

```shell
go get -u github.com/mholt/caddy/caddy
```

You can also [download a binary release](https://caddyserver.com/download) or [install from source](https://github.com/mholt/caddy#install).

## Initializing environment

To run Panto, you first need to initialize the environment. From the project root directory, run:

```shell
bin/panto-ctl init
```

Answer the questions to set up your configuration, and you should see a similar message:

```
Panto successfully configured!

To start Panto server run

    panto --conf=/path/to/panto.yaml
```

You can now run Panto!

## Running Panto

Now that you've configure Panto, you can run it by typing:

```
bin/panto --conf=/path/to/panto.yaml
```

You may encounter issues about the TLS certificate. You will either need to provide Panto with a certificate and key file, or run Panto with the `--no-tls` flag.

## Updating database

Upon upgrading Panto, you may encounter issues with the database version. The `panto-ctl` tool offers facilities to migrate the database to the most recent version. From the project root, type:

```
bin/panto-ctl dbconf upgrade
```

## Running Panto Agent

**TODO**

## Running Panto Web Client

The Panto user interface is a web client, that can be loaded in a browser. The complete web application is a set of static files served with the Caddy web server.

After [building the web client](BUILD.md), run the server by typing:

```shell
cd web
caddy
```

and open http://localhost:8080 in your browser.

## Docker

The Continuous Deployment pipeline on the Panto project pushes Docker images to a GitLab registry. You can use these images to run a Panto server and web client instance.

### Pulling the images

Login to the GitLab docker registry then pull the docker images:

```shell
docker login registry.gitlab.com
docker pull registry.gitlab.com/pantomath-io/panto/panto-server
docker pull registry.gitlab.com/pantomath-io/panto/panto-web
```

The images named `registry.gitlab.com/pantomath-io/panto/panto-server` and `registry.gitlab.com/pantomath-io/panto/panto-web` are the stable images built on the `master` branch. To get the latest development build use `registry.gitlab.com/pantomath-io/panto/panto-server:edge` and `registry.gitlab.com/pantomath-io/panto/panto-web:edge`. Other tags include `:latest` (aliases of the `master` build), `:x.y` and `:x.y.z` where X.Y.Z is a specific version number (e.g. `:1.1.2`).

### Running with docker

**TODO**: Docker compose