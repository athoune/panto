# Panto Concepts

## Introduction

Panto is a monitoring solution based on an agent/server architecture, and uses a timeseries database to store the monitoring data.

## Terms

### Server

The __server__ is the main component of the solution. It is responsible for managing the configuration, communicating with the __agents__, receiving and storing the __results__, testing the __states__ and tigerring the __alerts__.

These different functions can be spread on different hosts or be redundantly deployed, to allow better scalability.


### Target

A __target__ is the thing you want to monitor. It can be anything, like:

* a host (bare metal server, VPS, storage device, ...);
* a service (website, database cluster, ...);
* a network equipment (router, switch, firewall, ...);
* an application (API, website, ...).

A __target__ is identified by a unique hostname (FQDN) or a unique IP address, and needs to be reachable by the __agent__.
It can run an __agent__ or it can receive data scrapping requests from a remote __agent__.


### Agent

The __agent__ is a small application that launches the __probes__ and sends the __results__ to the __server__. The __agent__ is configured to register on a __server__, where it gets its full configuration.

In case of communication issues, the __agent__ spools the __results__ to be sent, until the __server__ is reachable again.

There are two types of __agents__:

* *local*, that means the __agent__ runs on the __target__;
* *remote*, that means the __agent__ runs on the __server__ or on any remote location.


### Probe

A __probe__ is the specific process that requests a metric or tests a behaviour on a __target__. A __probe__ is always spawned by an __agent__, whether it is a *local* or a *remote* __agent__.


### Result

The __result__ is the output of a __probe__. It consists of a timestamp (when the __probe__ was executed), and a dictionary of __metrics__ with their __values__ (int, float, string or boolean).

It is stored in the timeseries database.


### State

A __state__ is the classification of the __result__ of a __probe__. It can have 4 different values:

* __Unknown__: when the __state__ can't be determined, either because there's no value, or because the value can't be compared to configuration;
* __OK__: when the __result__ is considered as normal;
* __Warning__: when the __result__ is considered as worth attention;
* __Critical__: when the __result__ shows an issue that needs a reaction;
* __Missing Data__: when the agent hasn't sent any __result__ for a while.

To determine the __state__ of a __probe__ based on its __result__, the configuration has 3 different algorithms:

* _threshold_: the value of one __metric__ is compared to a fixed reference (e.g. _free memory_ is above 10%);
* _trend_: the trend of the last X values of the __metric__ is compared to a fixed reference (e.g. the derivative of the count of `HTTP/500` over the last 25 occurences is below 2);
* _history_: the value of one __metric__ is compared to the value of the same __metric__ taken Y seconds ago (e.g. the count of successful checkouts compared to the same count 24 hours ago).

Each algorithm can use any number of conditions, bound together with a global logical operator __AND__ or __OR__ (meaning respectively __all__ conditions should be true or __any__ condition should be true).

Here is the lists of the supported comparison operators.

Numeric comparison:

* Lower than (`<`)
* Lower equal (`<=`)
* Equal (`==`)
* Not equal (`!=`)
* Greater equal (`>=`)
* Greater than (`>`)

String comparison:

* Equal (`==`)
* Not equal (`!=`)
* Match (regular expression match)
* Find (regular expression find)


### Alert

An __alert__ may occur when the __state__ of a __check__ has been calculated from a __result__. There are different algorithms to evaluate the alert conditions:

* _state change_: if the current __state__ differs from the previous one (an option can force the alert to happen when the __state__ is _worse_ than the previous one);
* _state recurrence_: if the current __state__ is not __OK__ and lasts for more than X occurences;
* _state combinaison_: if a list of __probes__ has more than X __states__ not __OK__ (e.g. 3 nodes out of 25 have load not __OK__ in a cluster);
* _state flap_: if the __probe__ has __state__ changing more than X times in the last Y seconds;


When an __alert__ occurs, it sends a notification to the __contact__, based on the configuration:

* an email / SMS;
* a webhook (Slack, Flowdock, Mattermost...);
* an API (PagerDuty, OpsGenie...).
