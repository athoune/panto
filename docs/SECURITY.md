# Panto Security

## TLS/SSL integration

In order to secure the communication between the agents and the server, Panto uses a TLS/SSL certficate.

### Unsecure setup

You can run `panto` and the `panto-agent` with the `--no-tls` option to allow unencrypted communication between them. Note that the whole agent-server communication will be unsecure.

### Trusted CA signed certificate

The most recommended option is to use a Trusted CA Signed certificate. To do that, you need to configure `panto` with a fullchain certificate file, along with a certificate key.
By default, the agents will use their trusted CA system pool to validate the certificate.

If you don't have a fullchain certificate, you can still use it, but as your agents won't trust the CA, you'll need to configure the agent to use the certificate file.

### Self-signed certificate

The last option is to use self-signed certificate. This is a setup that should be used only in development/lab environment.
As the agent can't trust the CA (except if you have included your own CA in their CA system pool), you'll need to configure the agent to use the certificate file.

The self-signed certificate in this repository is only there for development purpose, and has been generated with the following commands:

```bash
$ openssl genrsa -out server.key 2048
$ openssl req -new -x509 -sha256 -key server.key -out server.crt -days 3650
Country Name (2 letter code) [AU]:FR
State or Province Name (full name) [Some-State]:idf
Locality Name (eg, city) []:Paris
Organization Name (eg, company) [Internet Widgits Pty Ltd]:Pantomath
Organizational Unit Name (eg, section) []:noc
Common Name (e.g. server FQDN or YOUR name) []:localhost

$ openssl req -new -sha256 -key server.key -out server.csr
Country Name (2 letter code) [AU]:FR
State or Province Name (full name) [Some-State]:idf
Locality Name (eg, city) []:Paris
Organization Name (eg, company) [Internet Widgits Pty Ltd]:Pantomath
Organizational Unit Name (eg, section) []:noc
Common Name (e.g. server FQDN or YOUR name) []:localhost

$ openssl x509 -req -sha256 -in server.csr -signkey server.key -out server.crt -days 3650
Signature ok
subject=/C=FR/ST=idf/L=Paris/O=Pantomath/OU=noc/CN=localhost
Getting Private key
```

Note: details on `openssl` commands and concepts is beyond the scope of this document.

Note: if you are using `127.0.0.1` as server address in your configuration, and as the certificate is generated for `localhost`, it needs to have an Alternate Name to avoid the following error:
```
x509: cannot validate certificate for 127.0.0.1 because it doesn't contain any IP SANs
```

The solution is to add the following to the `/etc/ssl/openssl.cnf` (see http://apetec.com/support/generatesan-csr.htm):
```
[v3_ca]
subjectAltName                  = @alt_names

[alt_names]
IP.1:127.0.0.1
IP.2:10.0.42.1
```
